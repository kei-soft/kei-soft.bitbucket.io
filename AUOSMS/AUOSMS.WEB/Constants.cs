﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.WEB
{
    public class Constants
    {
    }


    /// <summary>
    /// 결과 코드입니다.
    /// </summary>
    public class ResultCode
    {
        /// <summary>
        /// 성공
        /// </summary>
        public static readonly string Sucess = "Sucess";
        /// <summary>
        /// 실패
        /// </summary>
        public static readonly string Fail = "Fail";
    }

    public class RequestType
    {
        /// <summary>
        /// 쿼리하고 결과를 리턴합니다.
        /// </summary>
        public static readonly string SelectQuery = "SelectQuery";

        /// <summary>
        /// CRUD 쿼리를 수행하고 영향받는 행을 리턴합니다.
        /// </summary>
        public static readonly string CrudQuery = "CrudQuery";
    }

    
}