﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

using AUOSMS.WEB.Models;

using MySql.Data.MySqlClient;

using Newtonsoft.Json;

namespace AUOSMS.WEB
{
    public class HomeController : ApiController
    {
        #region Field

        /// <summary>
        /// DB 접속 정보
        /// </summary>
        string connectionString = "server=211.57.201.122;user=auosms_user;database=AUOSMS;password=auosms20201!";

        #endregion

        #region RequestMessage

        [HttpPost]
        [Route("RequestMessage")]
        public dynamic RequestMessage([FromBody]dynamic value)
        {
            string jsonValue = JsonConvert.SerializeObject(value);

            RequestModel requestModel = JsonConvert.DeserializeObject<RequestModel>(jsonValue);

            string resultMessage = string.Empty;

            if (requestModel.RequestType == RequestType.SelectQuery)
            {
                #region SelectQuery - 추후제거 필요, 테스트 용도

                string sql = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(mySqlDataReader);
                        mySqlDataReader.Close();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }

                    //connection.Close();
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.CrudQuery)
            {
                #region CrudQuery - 추후제거 필요, 테스트 용도

                string sql = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                        int i = mySqlCommand.ExecuteNonQuery();
                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }

                    //connection.Close();
                }

                #endregion
            }

            return new ResultModel() { ResultMessage = resultMessage, ResultCode = ResultCode.Sucess };
        }

        #endregion

        #region Example
        // GET: api/Home
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Home/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Home
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Home/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Home/5
        public void Delete(int id)
        {
        }
        #endregion
    }
}
