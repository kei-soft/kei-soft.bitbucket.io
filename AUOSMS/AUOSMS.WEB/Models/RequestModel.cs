﻿namespace AUOSMS.WEB.Models
{
    /// <summary>
    /// 요청 모델
    /// </summary>
    public class RequestModel
    {
        /// <summary>
        /// 요청자 ID
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 요청 Type
        /// </summary>
        public string RequestType { get; set; }

        /// <summary>
        /// 요청 메세지
        /// </summary>
        public string RequestMessage { get; set; }
    }
}