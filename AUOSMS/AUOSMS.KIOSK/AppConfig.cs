﻿using System;
using System.IO;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 앱 설정 클래스
	/// </summary>
	internal static class AppConfig
	{
		/// <summary>
		/// 앱 명칭
		/// </summary>
		public static readonly string AppName = "AUOSMS_KIOSK";

		/// <summary>
		/// 앱 실행 위치
		/// : cf. C:\Program Files (x86)\AUOSMS_KIOSK
		/// </summary>
		public static readonly string StartupPath = AppDomain.CurrentDomain.BaseDirectory;

		/// <summary>
		/// 관련 데이터 기본 경로
		/// : cf. C:\ProgramData\AUOSMS_KIOSK
		/// </summary>
		private static readonly string BaseDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), AppConfig.AppName);

		/// <summary>
		/// 로그 파일 경로
		/// </summary>
		public static readonly string LogDirPath = Path.Combine(AppConfig.BaseDataPath, "LOG");

		/// <summary>
		/// 서버 URL
		/// </summary>
		public static readonly string ServerBaseUrl = "http://211.57.201.122:8200";

		/// <summary>
		/// DB 커넥션 스트링
		/// </summary>
		public static readonly string DBConnectionString = "SERVER=211.57.201.122;DATABASE=AUOSMS;UID=auosms_user;PASSWORD=auosms20201!";

		/// <summary>
		/// 서버 이미지 URL
		/// </summary>
		public static readonly string ServerImageUrl = String.Concat(AppConfig.ServerBaseUrl, "/ADMIN/_Data/ITEM/");

		/// <summary>
		/// 키오스크 ID (from INI > KIOSK_ID : cf. 92124f34-b18e-44a8-b711-7b113681019c)
		/// </summary>
		public static string KioskID = String.Empty;

		/// <summary>
		/// 프린터 COM Port (from INI > PrinterComPort : cf. 5)
		/// </summary>
		public static string PrinterComPort = String.Empty;

		/// <summary>
		/// 카드리더 COM Port (from INI > ReaderComPort : cf. 4)
		/// </summary>
		public static string ReaderComPort = String.Empty;

		/// <summary>
		/// 매장 이름
		/// </summary>
		public static string StoreName = String.Empty;

		/// <summary>
		/// 매장 코드
		/// </summary>
		public static int StoreID = -1;

		/// <summary>
		/// 실 결제 여부 (from INI > REAL_PAY : Y or N)
		/// </summary>
		public static string RealPayYN = String.Empty;

		/// <summary>
		/// 결제 서버 IP (from INI > REAL_PAY)
		/// - 운영(Y) : 210.112.100.63
		/// - 테스트(N) : 210.112.100.97
		/// </summary>
		public static string PayServerIP = "210.112.100.63";

		/// <summary>
		/// 결제 서버 Port
		/// - 운영 : 9011, 60201
		/// - 테스트 : 9019, 60201
		/// </summary>
		public static readonly string PayServerPort = "60201";

		/// <summary>
		/// 가맹점 번호 (from INI > KIS_VAN_ID : cf. 90100546)
		/// </summary>
		public static string KisVanID = String.Empty;
	}
}
