﻿using AUOSMS.KIOSK.Models;
using log4net;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 앱 공통 정적 클래스
	/// </summary>
	internal static class AppCommon
	{
		/// <summary>
		/// 로거
		/// </summary>
		/// <remarks>
		/// - LogConfig.xml 참고
		/// - 작성형식 : [%date{HH:mm:ss,fff}] %message%newline
		/// </remarks>
		public static readonly ILog Logger = LogManager.GetLogger(AppConfig.AppName);

		/// <summary>
		/// 로그 작성
		/// </summary>
		/// <param name="format"></param>
		/// <param name="args"></param>
		public static void Log(string format, params object[] args)
		{
			AppCommon.Logger.Debug(String.Format(format, args));
		}

		/// <summary>
		/// 로그 작성
		/// </summary>
		/// <param name="text"></param>
		public static void Log(string text)
		{
			AppCommon.Logger.Debug(text);
		}

		/// <summary>
		/// 온라인 상태 확인
		/// </summary>
		/// <returns></returns>
		public static bool IsOnline()
		{
			//return NetworkInterface.GetIsNetworkAvailable();

			const string NCSI_TEST_URL = "http://www.msftncsi.com/ncsi.txt";
			const string NCSI_TEST_RESULT = "Microsoft NCSI";
			const string NCSI_DNS = "dns.msftncsi.com";
			const string NCSI_DNS_IP_ADDRESS = "131.107.255.255";

			try
			{
				// Check NCSI test link
				var webClient = new WebClient();
				string result = webClient.DownloadString(NCSI_TEST_URL);
				if (result != NCSI_TEST_RESULT)
				{
					AppCommon.Log("NETWORK :: OFFLINE");
					return false;
				}

				// Check NCSI DNS IP
				var dnsHost = Dns.GetHostEntry(NCSI_DNS);
				if (dnsHost.AddressList.Count() < 0 || dnsHost.AddressList[0].ToString() != NCSI_DNS_IP_ADDRESS)
				{
					AppCommon.Log("NETWORK :: OFFLINE");
					return false;
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
				return false;
			}

			AppCommon.Log("NETWORK :: ONLINE");
			return true;
		}

		/// <summary>
		/// 데이터베이스에 DataSet 결과 요청
		/// </summary>
		/// <param name="storedProcedureName">프로시저 이름</param>
		/// <param name="parameters">프로시저 파라미터</param>
		/// <returns></returns>
		public static DataSet GetDataSet(string storedProcedureName, List<MySqlParameter> parameters)
		{
			using (MySqlConnection connection = new MySqlConnection(AppConfig.DBConnectionString))
			{
				try
				{
					using (MySqlCommand command = new MySqlCommand(storedProcedureName, connection))
					{
						using (DataSet dataSet = new DataSet())
						{
							using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter())
							{
								command.CommandType = CommandType.StoredProcedure;
								dataSet.Locale = CultureInfo.CurrentCulture;

								if (parameters != null)
								{
									foreach (MySqlParameter parameter in parameters)
									{
										command.Parameters.Add(parameter);
									}
								}

								connection.Open();
								dataAdapter.SelectCommand = command;
								dataAdapter.Fill(dataSet);
							}

							return dataSet;
						}
					}
				}
				catch (MySqlException dbx)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", dbx.Message, dbx.StackTrace);
					throw;
				}
				catch (Exception x)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
					throw;
				}
				finally
				{
					connection.Close();
				}
			}
		}

		/// <summary>
		/// 데이터베이스에 DataTable 결과 요청
		/// </summary>
		/// <param name="storedProcedureName">프로시저 이름</param>
		/// <param name="parameters">프로시저 파라미터</param>
		/// <returns></returns>
		public static DataTable GetDataTable(string storedProcedureName, List<MySqlParameter> parameters)
		{
			using (MySqlConnection connection = new MySqlConnection(AppConfig.DBConnectionString))
			{
				try
				{
					using (MySqlCommand command = new MySqlCommand(storedProcedureName, connection))
					{
						using (DataTable dataTable = new DataTable())
						{
							using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter())
							{
								command.CommandType = CommandType.StoredProcedure;
								dataTable.Locale = CultureInfo.CurrentCulture;

								if (parameters != null)
								{
									foreach (MySqlParameter parameter in parameters)
									{
										command.Parameters.Add(parameter);
									}
								}

								connection.Open();
								dataAdapter.SelectCommand = command;
								dataAdapter.Fill(dataTable);
							}

							return dataTable;
						}
					}
				}
				catch (MySqlException dbx)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", dbx.Message, dbx.StackTrace);
					throw;
				}
				catch (Exception x)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
					throw;
				}
				finally
				{
					connection.Close();
				}
			}
		}

		/// <summary>
		/// 데이터베이스에 단일 결과 요청
		/// </summary>
		/// <param name="storedProcedureName">프로시저 이름</param>
		/// <param name="parameters">프로시저 파라미터</param>
		/// <returns></returns>
		public static object GetDataScalar(string storedProcedureName, List<MySqlParameter> parameters)
		{
			using (MySqlConnection connection = new MySqlConnection(AppConfig.DBConnectionString))
			{
				try
				{
					using (MySqlCommand command = new MySqlCommand(storedProcedureName, connection))
					{
						command.CommandType = CommandType.StoredProcedure;

						if (parameters != null)
						{
							foreach (MySqlParameter parameter in parameters)
							{
								command.Parameters.Add(parameter);
							}
						}

						connection.Open();
						return command.ExecuteScalar();
					}
				}
				catch (MySqlException dbx)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", dbx.Message, dbx.StackTrace);
					throw;
				}
				catch (Exception x)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
					throw;
				}
				finally
				{
					connection.Close();
				}
			}
		}

		/// <summary>
		/// 데이터베이스에 쿼리 실행
		/// </summary>
		/// <param name="storedProcedureName">프로시저 이름</param>
		/// <param name="parameters">프로시저 파라미터</param>
		/// <returns></returns>
		public static int ExecuteNonQuery(string storedProcedureName, List<MySqlParameter> parameters)
		{
			using (MySqlConnection connection = new MySqlConnection(AppConfig.DBConnectionString))
			{
				try
				{
					using (MySqlCommand command = new MySqlCommand(storedProcedureName, connection))
					{
						command.CommandType = CommandType.StoredProcedure;

						if (parameters != null)
						{
							foreach (MySqlParameter parameter in parameters)
							{
								command.Parameters.Add(parameter);
							}
						}

						connection.Open();
						return command.ExecuteNonQuery();
					}
				}
				catch (MySqlException dbx)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", dbx.Message, dbx.StackTrace);
					throw;
				}
				catch (Exception x)
				{
					AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
					throw;
				}
				finally
				{
					connection.Close();
				}
			}
		}

		/// <summary>
		/// 메뉴 그룹
		/// </summary>
		public static readonly Dictionary<int, string> MenuGroups = new Dictionary<int, string>();

		/// <summary>
		/// 메뉴 아이템
		/// </summary>
		public static readonly List<ItemMenu> MenuItems = new List<ItemMenu>();

		/// <summary>
		/// 메뉴 옵션
		/// </summary>
		public static readonly List<ItemMenuOptionDetail> MenuOptions = new List<ItemMenuOptionDetail>();

		/// <summary>
		/// 옵션 정보 JSON 형식 문자열을 일반 문자 형식으로 변환 : OPTION_NAME, OPTION_DETAIL_NAME, OPTION_DETAIL_PRICE
		/// </summary>
		/// <param name="jsonString"></param>
		/// <returns></returns>
		public static string ConvertJsonToText(string jsonString)
		{
			string plainText = String.Empty;

			List<Dictionary<string, string>> jsonList = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonString);
			if (jsonList.Count > 0)
			{
				List<string> optionList = new List<string>();
				foreach (var jsonLine in jsonList)
				{
					optionList.Add(String.Join(":", jsonLine["OPTION_NAME"], jsonLine["OPTION_DETAIL_NAME"]));
				}
				plainText = String.Join(", ", optionList);
			}

			return plainText;
		}
	}

	/// <summary>
	/// 수신 이벤트용 전달인자
	/// </summary>
	internal class TxEventArgs : EventArgs
	{
		public TxEventArgs(string data)
		{
			this.Data = data;
		}

		public string Data
		{
			get;
			set;
		}
	}

	/// <summary>
	/// 서비스 상태 열거자
	/// </summary>
	internal enum ServiceStatus
	{
		/// <summary>
		/// 결제완료
		/// </summary>
		PAID,
		/// <summary>
		/// 주문접수
		/// </summary>
		WAITING,
		/// <summary>
		/// 제조중
		/// </summary>
		MAKING,
		/// <summary>
		/// 제조완료
		/// </summary>
		MADE,
		/// <summary>
		/// 픽업완료
		/// </summary>
		SERVED
	}
}
