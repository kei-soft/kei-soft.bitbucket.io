﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// IC카드리더
	/// </summary>
	/// <remarks>
	/// 키스정보통신 VAN 전용 KIS-TDR210S
	/// </remarks>
	internal static class Reader
	{
		/// <summary>
		/// 연결 상태 확인
		/// </summary>
		/// <returns></returns>
		public static bool IsConnected()
		{
			bool isConnected = false;

			string query = "SELECT * FROM Win32_PnPEntity WHERE ClassGuid = '{4d36e978-e325-11ce-bfc1-08002be10318}'";
			try
			{
				using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", query))
				{
					foreach (ManagementObject queryObj in searcher.Get())
					{
						string port = Regex.Replace(queryObj["Name"].ToString(), "[^0-9]", "");

						// 장치를 특정할 수 없음 : INI 설정된 포트 기준으로 확인
						if (port.Equals(AppConfig.ReaderComPort))
						{
							isConnected = true;
							break;
						}
					}
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}

			return isConnected;
		}

		/// <summary>
		/// KIS정보통신 오류 코드 목록
		/// </summary>
		public static Dictionary<string, string> ErrorCode = new Dictionary<string, string>
		{
			// 리더기 거절코드 //
			{"8C","IC 카드 APDU 응답 오류"},
			{"8D","거래 조건이 맞지 않음"},
			{"95","명령/파라미터 오류"},
			{"CC","망 취소 (호스트 승인 후 카드 거절)"},
			{"CD","단말기나 POS가 취소 시"},
			{"CE","카드 거래 거절"},
			{"CF","IC EMV 거래 FALLBACK"},
			{"D0","서명 값이 존재 하지 않음"},
			{"D1","암호화 KEY가 존재하지 않음"},
			{"D2","단말기 ID가 일치하지 않음"},
			{"E1","MSR 동작 오류"},
			{"E2","IFM 동작 오류"},
			{"E4","PMF 검증 오류"},
			{"E6","Safecard Key 일련번호 불일치(최초) PMF Index불일치(갱신)"},
			{"E8","IC 거래 우선 요망"},
			{"E9","FALLBACK 거래 아님"},
			{"EC","Safecard Key 일련번호가 없음 (공장초기화 안됨)"},
			{"F2","카드가 존재하지 않음"},
			{"F5","지원되지 않는 카드"},
			{"F8","요청 Message의 Data 오류"},
			{"FA","Reader 인증 오류"},
			{"FB","Reader 인증이 되지 않음"},
			{"FC","다운로드 프로그램 무결성 훼손"},
			{"FD","암호화 키 무결성 훼손"},
			{"FF","실패"},

			// KIS 거절코드 //
			{"0000","승 인"},
			{"4949","1분후 재조회 요망"},
			{"7001","정상설치완료"},
			{"7002","카드사 연락요망"},
			{"7003","미등록 조회기"},
			{"7004","도난 / 분실"},
			{"7005","거래 정지"},
			{"7006","통신 장애"},
			{"7007","다운로드 재시도"},
			{"7030","할부 기간 오류"},
			{"7083","카드 번호 오류"},
			{"7091","유효기간입력오류"},
			{"7099","한도 초과"},
			{"7570","비할부가맹점카드"},
			{"7571","카드를 홈에 통과"},
			{"7572","KEY_IN 한도 초과"},
			{"7573","기 취소된 건"},
			{"7574","취소 DATA 오류"},
			{"7575","미등록 가맹점"},
			{"7576","CVC/CVV 검증오류"},
			{"7577","MS 훼손 카드"},
			{"7578","MS 훼손 카드"},
			{"7579","전화 등록 불가"},
			{"7580","미등록주유가맹점"},
			{"7581","전화승인 요망"},
			{"7582","가맹점 월한도초과"},
			{"7583","포인트 사용불가"},
			{"7584","CALL 1688-8966"},
			{"7585","가맹점1회한도초과"},
			{"7586","환금성 한도초과"},
			{"7979","미등록 조회기"},
			{"7980","해지,취소 가맹점"},
			{"7981","POINT불가 가맹점"},
			{"8000","해당 카드 없슴"},
			{"8001","예금 잔액 부족"},
			{"8002","거래 정지 계좌"},
			{"8003","전문오류, 재시도"},
			{"8004","비실명 계좌임"},
			{"8007","POINT 미달"},
			{"8009","금액 오류,미달"},
			{"8032","할부 금액 오류"},
			{"8035","구매카드 미등록"},
			{"8036","수금카드 미등록"},
			{"8037","카드 번호 오류"},
			{"8038","할부 불가능 카드"},
			{"8178","연체카드"},
			{"8311","주민등록번호상이"},
			{"8312","사업자번호상이"},
			{"8314","유효 기간 만료"},
			{"8323","거래 정지 카드"},
			{"8324","거래 정지 카드"},
			{"8325","기업카드한도초과"},
			{"8326","사용 한도 초과"},
			{"8328","사용 횟수 초과"},
			{"8329","자가매출거래불가"},
			{"8310","비밀번호 오류"},
			{"8330","주민비밀번호틀림"},
			{"8336","카드 번호 오류"},
			{"8350","도난 / 분실"},
			{"8355","충전불가잔액있음"},
			{"8356","환불대기"},
			{"8357","충전불가승인오류"},
			{"8358","잔액없음"},
			{"8373","전화 요망"},
			{"8393","할부 기간 오류"},
			{"8415","취소데이터 오류"},
			{"8418","조회 불가 카드"},
			{"8419","CALL 080-520-4288"},
			{"8420","CALL 6263-8120"},
			{"8421","CALL 1588-1365"},
			{"8374","일자 투입 오류"},
			{"8376","이중 등록"},
			{"8377","CALL 1588-3200"},
			{"8378","TEL 080-888-8833"},
			{"8379","연체회원"},
			{"8380","타사카드 연체자"},
			{"8381","외환카드전산오류"},
			{"8759","수기특약 1588-8900"},
			{"8760","CALL 1588-8900"},
			{"8338","가족회원한도초과"},
			{"8382","TEL 02-2169-5255"},
			{"8383","CALL 02-3700-2550"},
			{"8384","CALL 1566-6900"},
			{"8385","CALL 1588-1687"},
			{"8386","CALL 1588-1788"},
			{"8387","CALL 1566-8002"},
			{"8388","CALL 3704-7777"},
			{"8389","CALL 1588-1111"},
			{"8390","CALL 229-2607~9"},
			{"8391","할부 한도 초과"},
			{"8392","CALL 1588-8300"},
			{"8396","CALL 1588-1155"},
			{"8397","비밀번호등록요망"},
			{"8398","사용제한 가맹점"},
			{"8399","카드수령등록요망"},
			{"8400","본인확인후재승인"},
			{"8401","할인적용불가카드"},
			{"8402","취소불가 단말기"},
			{"8403","국민은행서버점검"},
			{"8404","승인처리중 기다려주세요"},
			{"8405","Member Only"},
			{"8406","Used Coupon"},
			{"8407","Not Available"},
			{"8408","국민은행가맹요망"},
			{"8409","CALL 1544-8877"},
			{"8410","서비스미적용회원"},
			{"8411","타카드 이용불가"},
			{"8412","약정 미신청"},
			{"8413","상향동의요청"},
			{"8423","인증대상카드아님"},
			{"8424","서비스횟수초과"},
			{"8425","MSorIC칩거래요망"},
			{"8426","회원사고코드등재"},
			{"8427","사용시간제한카드"},
			{"8428","상품권 구매한도 초 과"},
			{"8429","해당체크카드에 계좌 번호 없음"},
			{"8430","CALL 1588-2900"},
			{"8431","약정금액상이"},
			{"8432","할인 미적용 카드"},
			{"8434","할인 미실적 회원"},
			{"8450","전월 실적미달"},
			{"8833","신카드 사용요망"},
			{"8900","정 상"},
			{"8910","무자격자 승인"},
			{"8901","취소불가 기취소거래"},
			{"8914","전송기관 오류"},
			{"8915","가맹점 코드 오류"},
			{"8916","통화코드 오류"},
			{"8917","거래금액 오류"},
			{"8918","카드정보 오류"},
			{"8919","취소구분 오류"},
			{"8920","취소할 승인번호 오류"},
			{"8921","전문 형식 오류"},
			{"8922","보정방법 오류"},
			{"8923","미등록 제휴사"},
			{"8924","미등록 제휴점"},
			{"8925","제휴사코드 다름"},
			{"8926","미등록 M+ 카드"},
			{"8927","미등록 멤버쉽 카드"},
			{"8928","MIC정보 오류"},
			{"8930","M+ 미승인 제휴사"},
			{"8931","사용정지 카드"},
			{"8932","조회불능 카드"},
			{"8933","불량제휴점"},
			{"8934","말소제휴점"},
			{"8935","등록 VAN사 다름"},
			{"8936","말소 단말기"},
			{"8937","카드 유효기간 말료"},
			{"8938","유효기간 표시"},
			{"8939","고객등급오류"},
			{"8940","1일 사용한도초과"},
			{"8941","월 사용한도초과"},
			{"8942","연 사용한도초과"},
			{"8943","1일 사용횟수초과"},
			{"8944","월 사용횟수 초과"},
			{"8945","한도 미부여"},
			{"8946","연간사용금액 한도초과"},
			{"8947","연간사용횟수 한도초과"},
			{"8950","취소할 승인번호 없음"},
			{"8951","취소할 금액이 다름"},
			{"8952","카드입력정보 오류"},
			{"8960","전문전송일시 오류"},
			{"8961","전문추적번호 오류"},
			{"8962","VAN코드 필드 오류"},
			{"8963","제휴사코드 필드 오류"},
			{"8964","제휴점번호 필드 오류"},
			{"8965","WCC필드 오류"},
			{"8966","취소구분 필드 오류"},
			{"8967","승인번호 필드 오류"},
			{"8968","카드번호 필드 오류"},
			{"8969","할인대상금액 필드 오류"},
			{"8970","보정방법 필드 오류"},
			{"8971","금액/카드정보 다름"},
			{"8972","비정상 제휴사"},
			{"8973","주민번호 필드 오류"},
			{"8974","CTN 필드 오류"},
			{"8975","CTN 불일치 오류"},
			{"89SE","카드사 접속 불량"},
			{"89TO","카드사 TIMEOUT"},
			{"4000","정상 수표"},
			{"4001","미등록 조회기"},
			{"4002","사고 수표 아님"},
			{"4003","미발행수표"},
			{"4100","불 량"},
			{"4111","시스템 에러"},
			{"4118","재 입 력"},
			{"4201","중지 가맹점"},
			{"4202","해지 가맹점"},
			{"4203","발행 취소 수표"},
			{"4222","데이타베이스이상"},
			{"4301","수표번호입력오류"},
			{"4302","지점코드입력오류"},
			{"4333","해당 은행 장애"},
			{"4401","기지급 수표"},
			{"4402","발행일 입력 오류"},
			{"4444","전문포맷 에러"},
			{"4501","수표금액입력오류"},
			{"4502","조회 불가 수표"},
			{"4503","권종코드 오류"},
			{"4601","은행 연락"},
			{"4701","한도 초과"},
			{"4801","부도 주의"},
			{"4822","재 시 도"},
			{"4882","거 부"},
			{"4873","서비스시간종료"},
			{"4973","전화 02-2650-1614"},
			{"4998","전화 02-539-9100"},
			{"0400","정상 완료"},
			{"0401","카드발급은행문의"},
			{"0402","카드재발급요망"},
			{"0403","판매점번호 오류"},
			{"0411","정상 (우수고객)"},
			{"0412","업무구분코드오류"},
			{"0414","카드번호 오류"},
			{"0415","카드발급은행문의"},
			{"0423","승인불가 수수료"},
			{"0430","전화문의"},
			{"0433","삭제된 카드"},
			{"0436","기타사고신고카드"},
			{"0441","분실 도난카드"},
			{"0451","예금 잔액부족"},
			{"0454","카드유효기간만료"},
			{"0455","비밀번호 상이"},
			{"0457","해당카드거래불가"},
			{"0460","최저사용금액미달"},
			{"0461","지급한도초과"},
			{"0462","거래제한카드"},
			{"0465","무통장거래 초과"},
			{"0475","비밀번호오류초과"},
			{"0477","암호화 체계 변경"},
			{"0490","카드사응답없음"},
			{"0491","해당은행 장애"},
			{"0492","은행 밴 미등록"},
			{"0493","잔액증명발급계좌"},
			{"0494","중복거래발생"},
			{"0417","정상 취소"},
			{"0422","하드웨어장애"},
			{"0468","시간경과후 응답"},
			{"0496","판매점시스템장애"},
			{"04P1","원거래 없음"},
			{"04P2","밴 처리중"},
			{"04P3","은행 처리중"},
			{"04P4","정상인출된거래"},
			{"04P5","재입금된 거래"},
			{"04DI","승인확인완료"},
			{"04DC","취소확인완료"},
			{"04BI","잔액조회 완료"},
			{"68TO","해당은행응답없음"},
			{"0800","정상 완료"},
			{"0899","정 상"},
			{"0801","업무구분 부적당"},
			{"0802","가맹점번호부적당"},
			{"0803","전문거래 부적당"},
			{"0804","전문작성 부적당"},
			{"0805","발생건수 부적당"},
			{"0806","정상건수 부적당"},
			{"0807","정상금액 부적당"},
			{"0808","취소건수 부적당"},
			{"0809","취소금액 부적당"},
			{"0810","가맹점번호부적당"},
			{"0811","가맹점번호부적당"},
			{"0812","가맹점취번부적당"},
			{"0813","전문구분 부적당"},
			{"0814","금액 부적당"},
			{"0815","비밀번호 부적당"},
			{"0816","수표번호 부적당"},
			{"0817","타행카드사용불가"},
			{"0818","계좌번호 부적당"},
			{"0819","계좌번호 부적당"},
			{"0820","암호비밀 부적당"},
			{"0821","유효기간 부적당"},
			{"0822","부속정보 부적당"},
			{"0823","핀정보 부적당"},
			{"0824","휴일잔액조회불가"},
			{"0825","비밀번호 부적당"},
			{"0830","조회전문취소처리"},
			{"0831","가맹정 등록해지"},
			{"0832","가맹점번호 중복"},
			{"0833","취소전문 비정상"},
			{"0834","가맹점번호 중복"},
			{"0835","취소전문 비정상"},
			{"0836","취소금액 불일치"},
			{"0837","가맹점번호 중복"},
			{"0838","취소계좌 불일치"},
			{"0839","조회전문 미처리"},
			{"0840","은행등록 미처리"},
			{"0841","조회계좌 미처리"},
			{"0901","고객계좌 부적당"},
			{"0942","금액 한도 초과"},
			{"0943","고객번호 무"},
			{"0944","주의사고등록계좌"},
			{"0945","주의사고등록계좌"},
			{"0946","연동코드 부적당"},
			{"0947","주의사고등록계좌"},
			{"0948","고객 해지 계좌"},
			{"0949","범죄수사협조계좌"},
			{"0951","응답메세지 없음"},
			{"0952","고정연체 계좌"},
			{"0953","비밀번호 오류"},
			{"0954","현금카드분실계좌"},
			{"0955","응답메세지 없음"},
			{"0956","결제 잔액 부족"},
			{"0957","잔액증명 거래불가"},
			{"0958","미자금타점권보유"},
			{"0959","현금카드 미등록"},
			{"0960","응답메세지 없음"},
			{"0962","수표 원장 무"},
			{"0963","사용불능 수표"},
			{"0967","수표임력내용부적"},
			{"0968","일중한도 초과"},
			{"0970","마감후 거래보유"},
			{"0973","비밀번호횟수초과"},
			{"0981","조회고객 원장무"},
			{"0982","조회대상해지계좌"},
			{"0988","조회대상계좌 무"},
			{"0996","이자액 부족계좌"},
			{"6001","당행시스템 오류"},
			{"6002","당행시스템 오류"},
			{"6003","당행시스템 오류"},
			{"60SE","하드웨어제어"},
			{"9999","당행시스템 오류"},
			{"5001","승인일자 불일치"},
			{"5002","할부개월 불일치"},
			{"5003","승인금액 불일치"},
			{"5004","유효기간 불일치"},
			{"5005","카드번호 불일치"},
			{"5006","기취소 전문"},
			{"5007","승인번호 불일치"},
			{"5008","원 거래 없음"},
			{"5009","기 매입된 거래"},
			{"5010","구매카드 불일치"},
			{"5011","수금카드 불일치"},
			{"5012","구매거래정지카드"},
			{"5013","수금거래정지카드"},
			{"D002","거래내역 없음"},
			{"D004","단말기정산불가"},
			{"0600","정 상"},
			{"0611","포인트 적립불가"},
			{"0612","포인트 사용불가"},
			{"0613","포인트 취소불가"},
			{"0614","한도오류처리불가"},
			{"0615","카드번호오류"},
			{"0616","가맹점오류"},
			{"0617","제휴계약 오류"},
			{"0618","회원정보 오류"},
			{"0619","기타자료 오류"},
			{"0620","포인트적립 불가"},
			{"0621","포인트사용 불가"},
			{"0622","재시도 요망"},
			{"0623","포인트 미가맹점"},
			{"0624","이미 처리됨"},
			{"0630","재시도 요청"},
			{"0649","비밀번호 오류"},
			{"0655","비밀번호 3회오류"},
			{"0659","비밀번호 변경요망"},
			{"0670","서비스관련"},
			{"0680","데이터베이스관련"},
			{"0690","Control 오류"},
			{"0692","Data 오류"},
			{"0699","Timeout"},
			{"06SE","Timeout"},
			{"06TO","Timeout"},
			{"0601","주유소자료 처리불가"},
			{"0602","가맹점자료 처리불가"},
			{"0603","충전취소 망상만가능"},
			{"0604","전문 단품개수 오류"},
			{"0605","전문 블록개수 오류"},
			{"0606","병설 여부 불일치 오류"},
			{"0607","미등록카드"},
			{"0608","카드발급요망"},
			{"0609","주유기준한도 미등록"},
			{"0610","캐쉬백기준 미등록"},
			{"0625","(-)매출금액 불가"},
			{"0626","매출일자 오류"},
			{"0627","매출허용금액초과"},
			{"0628","회원주유한도미등록"},
			{"0629","부정매출"},
			{"0631","그룹포인트 미처리"},
			{"0632","카드상태 확인요망"},
			{"0633","미등록카드 사용불가"},
			{"0634","포인트사용시 KEY_IN불가"},
			{"0635","포인트사용불가가맹점"},
			{"0636","사은품 신청불가"},
			{"0637","포인트 사용조건미달"},
			{"0638","환급조건미달"},
			{"0639","사용불가비밀번호 1588-0051"},
			{"0640","30일이전자료취소못함"},
			{"0641","원거래 카드번호불일치"},
			{"0642","원거래없음"},
			{"0643","원거래 승인번호불일치"},
			{"0645","원거래 기관불일치"},
			{"0646","원거래가맹점불일치"},
			{"0647","원거래금액불일치"},
			{"0648","원거래포인트불일치"},
			{"0650","원거래전표구분불일치"},
			{"0651","포인트부족,취소불가"},
			{"0652","후정산포인트처리오류"},
			{"0653","할인회원이아닙니다"},
			{"0654","잠시후 재시도요망"},
			{"0656","포인트자동누적겣?"},
			{"0657","현금KEY값길이오류"},
			{"0658","거래자구분오류"},
			{"0660","현금가맹점비정상"},
			{"0661","현금5,000원미만오류"},
			{"0662","적립only 승인"},
			{"0663","사업자번호불일치"},
			{"0664","현금거래금액불일치"},
			{"0665","서비스구분오류"},
			{"0666","현금거래만처리가능"},
			{"0667","현금거래금액확인"},
			{"0668","상담실연락바람 1588-0051"},
			{"0669","현금영수증원거래없음"},
			{"0671","탈회/해지회원"},
			{"0672","주민번호불일치"},
			{"0673","OCBS장애 재시도요망"},
			{"0674","상품코드오류"},
			{"2000","승 인"},
			{"2001","회원카드 아님"},
			{"2002","가맹점번호 틀림"},
			{"2003","원 거래 없음"},
			{"2004","사용중지 카드"},
			{"2005","1일 사용회수 초과"},
			{"2006","올앳카드 전화요망"},
			{"2007","승인일자 오류"},
			{"2008","유효기간 오류"},
			{"2009","전문일자오류"},
			{"2010","전문시간오류"},
			{"2011","카드번호 오류"},
			{"2012","승인시간 초과"},
			{"20TO","올앳카드 전화요망"},
			{"9001","시스템장애(1)"},
			{"9002","시스템장애(2)"},
			{"9003","등록제품번호확인"},
			{"9004","거래번호확인(1)"},
			{"9005","거래번호확인(2)"},
			{"9006","시스템장애(3)"},
			{"2100","정 상"},
			{"2111","포인트 적립 불가"},
			{"2112","포인트 사용 불가"},
			{"2113","적립 취소 불가"},
			{"2114","사용 취소 불가"},
			{"2115","카드번호 오류"},
			{"2116","미등록 가맹점"},
			{"2117","미등록 카드"},
			{"2118","적립 불가 가맹점"},
			{"2119","사용 불가 가맹점"},
			{"2120","도난 카드"},
			{"2121","거래 불가 카드"},
			{"2122","원거래 없음"},
			{"2123","기취소 거래"},
			{"2124","한도 초과 적립"},
			{"2125","한도 초과 사용"},
			{"2126","거래금액미달"},
			{"2127","거래 금액 초과"},
			{"2128","거래 불가"},
			{"2180","포인트적립 요망"},
			{"2190","시스템 오류"},
			{"2191","기타 오류"},
			{"21SE","Timeout"},
			{"21TO","Timeout"},
			{"7600","정 상"},
			{"7609","기관코드 오류"},
			{"7610","응답코드 오류"},
			{"7611","Message Type 오류"},
			{"7612","거래구분코드 오류"},
			{"7613","전송일자 오류"},
			{"7614","전송시간 오류"},
			{"7615","추적번호 오류"},
			{"7616","전문구분 오류"},
			{"7619","가맹점번호 오류"},
			{"7620","거래일자 오류"},
			{"7621","거래시간 오류"},
			{"7626","거래금액 오류"},
			{"7627","사용요청 포인트오류"},
			{"7628","원거래일자 오류"},
			{"7629","원거래금액 오류"},
			{"7630","원거래포인트 오류"},
			{"7631","원거래 승인번호 오류"},
			{"7632","주민등록번호 오류"},
			{"7633","카드번호 오류"},
			{"7634","원거래승인일자오류"},
			{"7651","원거래 미존재"},
			{"7652","이중거래"},
			{"7653","기취소 거래"},
			{"7654","포인트 적립불가"},
			{"7656","포인트 사용불가"},
			{"7657","사용최소포인트미달"},
			{"7659","거래내역 오류"},
			{"7660","적립불가 가맹점"},
			{"7661","사용불가 가맹점"},
			{"7665","기해지된 고객"},
			{"7666","카드분실 미확인"},
			{"7670","서비스 오겥?"},
			{"7680","데이타베이스 오류"},
			{"7690","TIME OUT"},
			{"5000","정 상"},
			{"5031","포인트 적립 불가"},
			{"5032","포인트 사용 불가"},
			{"5033","적립 취소 불가"},
			{"5034","사용 취소 불가"},
			{"5035","카드번호 오류"},
			{"5036","미등록 가맹점"},
			{"5037","미등록 카드"},
			{"5038","적립 불가 가맹점"},
			{"5039","사용 불가 가맹점"},
			{"5020","도난 카드"},
			{"5021","거래 불가 카드"},
			{"5022","원거래 없음"},
			{"5023","기취소 거래"},
			{"5024","한도 초과 적립"},
			{"5025","한도 초과 사용"},
			{"5026","거래금액 미달"},
			{"5027","유효기간입력오류"},
			{"5028","유효기간 경과"},
			{"5029","비밀번호 상이"},
			{"5090","시스템 오류"},
			{"5091","기타 오류"},
			{"50SE","Timeout"},
			{"50TO","Timeout"},
			{"5100","정 상"},
			{"5110","전화요망"},
			{"5111","전화요망"},
			{"5199","전화요망"},
			{"5120","미등록 카드"},
			{"5121","적립 에러"},
			{"5122","사용 에러"},
			{"5123","전화요망"},
			{"5124","사용 한도 초과"},
			{"5125","비밀번호 틀림"},
			{"5126","가용포인트부족"},
			{"5127","미등록 카드"},
			{"5128","미등록 카드"},
			{"5129","미등록 가맹점"},
			{"5130","조회 불가"},
			{"5131","해지 카드"},
			{"5132","거래 정지 카드"},
			{"5133","연체 카드"},
			{"5134","불량 카드"},
			{"5143","원거래 없음"},
			{"5144","승인번호 불일치"},
			{"5145","승인금액 불일치"},
			{"5146","승인일자 불일치"},
			{"51TO","Timeout"},
			{"6900","정 상"},
			{"6901","승인취소 오류"},
			{"6902","원거래 중복"},
			{"6903","원거래 없음"},
			{"6904","내부 오류"},
			{"6905","후불카드 승인오류"},
			{"6906","지불불가 후불카드"},
			{"6907","월한도초과"},
			{"6908","선불승인 오류"},
			{"6909","접속 오류"},
			{"6910","요청 오류"},
			{"6911","응답 오류"},
			{"6912","전문 오류"},
			{"6913","원화아님"},
			{"6914","사용불가카드"},
			{"6915","정지/해지 카드"},
			{"6916","운용자 정지카드"},
			{"6917","잔액부족"},
			{"6918","통화중"},
			{"6919","유효기간경과"},
			{"6920","SDP 교환기 오류"},
			{"6921","후불 인증오류"},
			{"6922","ISN 접속 오류"},
			{"6923","ISN 응답 오류"},
			{"6924","지불불가 후불카드"},
			{"6925","비밀번호오류"},
			{"6926","미개봉 후불카드"},
			{"6927","결번"},
			{"6928","과다사용 중지"},
			{"6929","운영자 서비스 중지"},
			{"6930","비불초 서비스 중지"},
			{"6931","ISN 기타오류"},
			{"6932","ISN 내부 TIMEOUT"},
			{"6933","ISN 내부 전송오류"},
			{"6934","ISN 내부 오류"},
			{"6935","SCP응답없음"},
			{"6936","ISN서비스 거부"},
			{"6937","ISN 미정 오류"},
			{"6938","ISN 요청 오류"},
			{"6939","ISN 응답 오류"},
			{"6940","수납 오류"},
			{"6941","미수납"},
			{"6942","감액"},
			{"6943","원거래없음"},
			{"6944","지불결과 처리오류"},
			{"6945","내부 오류"},
			{"6946","지불 전처리 오류"},
			{"6947","자리수 틀림"},
			{"6948","비밀번호 회수초과"},
			{"6949","지불처리중 카드"},
			{"6950","승인번호 생성실패"},
			{"6951","내부 오류"},
			{"6952","선불카드 인증오류"},
			{"6953","지불불가 선불카드"},
			{"6954","발행번호 오류"},
			{"6955","비밀번호 오류"},
			{"6956","선불카드 코드오류"},
			{"6957","성인용카드아님"},
			{"6958","ONLINE 전용카드"},
			{"6959","지불불가 선불카드"},
			{"6960","유효기간 경과"},
			{"6961","내부 오류"},
			{"6962","가맹점 관련오류"},
			{"6963","미등록가맹점"},
			{"6964","가맹점 아님"},
			{"6965","미개시 가맹점"},
			{"6966","해지 가맹점"},
			{"6967","보류 가맹점"},
			{"6968","계약기간 만료 가맹점"},
			{"6969","가맹점 일한도초과"},
			{"6970","가맹점 월한도초과"},
			{"6971","가맹점 그룹관련오류"},
			{"6972","그룹지불 확인정상"},
			{"6973","미개시 그룹"},
			{"6974","해지 그룹"},
			{"6975","보류 그룹"},
			{"6976","계약기간 만료그룹"},
			{"6977","그룹 일한도액 초과"},
			{"6978","그룹 원한도액 초과"},
			{"6979","내부 오류"},
			{"6980","수납관련 오류"},
			{"6981","내부 오류"},
			{"6982","승인완료"},
			{"6983","인증완료"},
			{"6984","거절"},
			{"6985","취소완료"},
			{"6986","매입요청"},
			{"6987","매입요청중"},
			{"6988","매입완료"},
			{"6989","매출반송"},
			{"6990","정산중"},
			{"6991","정산완료"},
			{"69TO","Timeout"},
			{"69SE","SendError"},
			{"8700","거래불가 가맹점"},
			{"8701","CAVV 검증 오류"},
			{"8702","CAVV 재사용 오류"},
			{"8703","CAVV 전문 오류"},
			{"8500","정 상"},
			{"8511","포인트적립불가"},
			{"8512","포인트사용불가"},
			{"8513","적립취소불가"},
			{"8514","사용취소불가"},
			{"8515","카드번호오류"},
			{"8516","미등록가맹점"},
			{"8517","미등록카드번호"},
			{"8518","적립불가가맹점"},
			{"8519","사용불가가맹점"},
			{"8520","도난카드"},
			{"8521","거래불가카드"},
			{"8522","취소시원거래없음"},
			{"8523","이미취소된거래"},
			{"8524","한도초과point적립"},
			{"8525","한도초과point사용"},
			{"8526","거래금액미달"},
			{"8527","거래금액초과"},
			{"8528","거래불가"},
			{"8590","시스템오류"},
			{"8591","기타오류"},
			{"8800","승인 완료"},
			{"8801","해지카드"},
			{"8802","사용정지카드"},
			{"8803","일시정지카드"},
			{"8804","도난/분실 카드"},
			{"8805","사용불가능카드"},
			{"8806","수령확인후사용요망"},
			{"8807","거래제한카드"},
			{"8808","VAN 미등록가맹점"},
			{"8809","미등록단말기"},
			{"8810","KTF 미등록가맹점"},
			{"8811","KEY-IN 불가가맹점"},
			{"8812","거래 TIMEOUT"},
			{"8813","카드번호오류"},
			{"8814","전문 FORMAT 오류"},
			{"8815","원거래없음"},
			{"8816","기취소거래"},
			{"8817","단말기ID오류"},
			{"8818","거래종류선택오류"},
			{"8819","거래금액오류"},
			{"8820","잔여마일리지부족"},
			{"8821","잔액부족"},
			{"8822","센터문의바람"},
			{"8823","VAN사코드오류"},
			{"8824","사용횟수한도초과"},
			{"8825","가맹점계약오류"},
			{"8826","할인구분오류"},
			{"8827","1회사용금액초과"},
			{"8891","시스템오류"},
			{"8892","시스템오류"},
			{"8893","시스템오류"},
			{"8899","기타오류"},
			{"88TO","카드사 응답없음"},
			{"88SE","하드웨어장애"},
			{"8111","전문구분오류"},
			{"8112","거래금액오류"},
			{"8113","당사회원이아님"},
			{"8114","카드유효기간경과"},
			{"8115","최소사용금액오류"},
			{"8116","카드사용불가"},
			{"8117","유효기간입력오류"},
			{"8118","일일사용횟수초과"},
			{"8121","원거래없음"},
			{"8122","취소금액이다름"},
			{"8123","취소불가기취소됨"},
			{"8131","포인트사용불가카드"},
			{"8132","행사미적용가맹점"},
			{"8133","보너스포인트부족"},
			{"8190","시스템장애"},
			{"81SE","카드사전화요망"},
			{"81TO","카드사전화요망"},
			{"7400","정 상"},
			{"7411","포인트 적립 불가"},
			{"7412","포인트 사용 불가"},
			{"7413","포인트적립취소불가"},
			{"7414","포인트사용취소불가"},
			{"7415","카드번호 오류"},
			{"7416","미등록 가맹점"},
			{"7417","미등록 카드번호"},
			{"7418","적립불가가맹점"},
			{"7419","사용불가가맹점"},
			{"7420","도난카드"},
			{"7421","거래불가카드"},
			{"7422","원거래없음"},
			{"7423","이미취소된거래"},
			{"7424","한도초과 적립"},
			{"7425","한도초과 사용"},
			{"7426","미등록고객"},
			{"7427","고객대기"},
			{"7428","고객탈퇴"},
			{"7429","저장오류"},
			{"7430","유효기간경과"},
			{"7431","거래액 계산불능"},
			{"7433","비밀번호틀림"},
			{"7434","차감금액없음"},
			{"7450","카드사통신장애"},
			{"7451","재조회요망"},
			{"7452","거래정지카드"},
			{"7453","카드사전화요망"},
			{"7470","전문오류"},
			{"7471","수동 불가능카드"},
			{"7472","수동 불가능가맹점"},
			{"7473","거래불가능금액"},
			{"7474","거래불가능금액"},
			{"7475","거래불가능금액"},
			{"7476","유효기간 경과카드"},
			{"7900","정 상"},
			{"7911","포인트 적립 불가"},
			{"7912","포인트 사용 불가"},
			{"7913","포인트적립취소불가"},
			{"7914","포인트사용취소불가"},
			{"7915","카드번호 오류"},
			{"7916","미등록 가맹점"},
			{"7917","미등록 카드번호"},
			{"7918","적립불가가맹점"},
			{"7919","사용불가가맹점"},
			{"7920","도난카드"},
			{"7921","거래불가카드"},
			{"7922","원거래없음"},
			{"7923","이미취소된거래"},
			{"7924","한도초과 적립"},
			{"7925","한도초과 사용"},
			{"7926","미등록고객"},
			{"7927","고객대기"},
			{"7928","고객탈퇴"},
			{"7929","저장오류"},
			{"7930","유효기간경과"},
			{"7931","거래액 계산불능"},
			{"7933","비밀번호틀림"},
			{"7934","차감금액없음"},
			{"7950","카드사통신장애"},
			{"7951","재조회요망"},
			{"7952","거래정지카드"},
			{"7953","카드사전화요망"},
			{"7970","전문오류"},
			{"7971","수동 불가능카드"},
			{"7972","수동 불가능가맹점"},
			{"7973","거래불가능금액"},
			{"7974","거래불가능금액"},
			{"7975","거래불가능금액"},
			{"7976","유효기간 경과카드"},
			{"0103","카드사전화요망 (1588-4500)"},
			{"0104","카드사전화요망 (1588-4500)"},
			{"0105","승인가능 회원 입니다 1588-4500"},
			{"0106","카드사전화요망 (1588-4500)"},
			{"0107","거래정지가맹점 (1588-4500)"},
			{"0108","해지가맹점 (1588-4500)"},
			{"0109","취소보류 (1588-4500)"},
			{"0110","카드사전화요망 (1588-4500)"},
			{"0111","카드사전화요망 (1588-4500)"},
			{"0112","승인일자확인요망 (1588-4500)"},
			{"0113","결제계좌확인 (1588-4500)"},
			{"0114","승인당일만단말취소가능 1588-4500"},
			{"0115","카드사전화요망 (1588-4500)"},
			{"0116","카드사전화요망 (1588-4500)"},
			{"0117","대표자주민번호확인요망 1588-4500"},
			{"0118","이미승인취소완료된거래 1588-4500"},
			{"0119","사용가능한횟수 초과(1588-4500)"},
			{"0120","인터넷승인인증 오류(1588-4500)"},
			{"0121","KEY-IN승인불가 가맹점 1588-4500"},
			{"0122","가맹점KEY-IN한도초과(1588-4500)"},
			{"0123","가맹점매출한도 초과(1588-4500)"},
			{"0124","가맹점자기매출 (1588-4500)"},
			{"0125","1일사용한도초과 (1588-4500)"},
			{"0126","월간사용한도초과 (1588-4500)"},
			{"0127","사고등록계좌 (1588-4500)"},
			{"0128","결제계좌법무절차진행(1588-4500)"},
			{"0129","무통장거래횟수 초과(1588-4500)"},
			{"0130","단말기거래처 확인 요망"},
			{"0131","회원정보오류 (1588-4500)"},
			{"0132","가맹점정보오류 (1588-4500)"},
			{"0133","할부개월수입력 오류(1588-4500)"},
			{"0134","승인금액입력오류 (1588-4500)"},
			{"0135","카드사전화요망 (1588-4500)"},
			{"0136","승인취소대상자료없음(1588-4500)"},
			{"0137","카드사전화요망 (1588-4500)"},
			{"0138","계좌잔액부족 (1588-4500)"},
			{"0139","1회사용한도초과 (1588-4500)"},
			{"0140","카드사전화요망 (1588-4500)"},
			{"0141","유효기한경과카드 (1588-4500)"},
			{"0142","구카드사용불가 (1588-4500)"},
			{"0143","할부거래할수없는카드(1588-4500)"},
			{"0144","취소불가! 매출표기접수 1588-4500"},
			{"0145","승인자료없음 (1588-4500)"},
			{"0146","잠시 후 재시도 (1588-4500)"},
			{"0147","잠시 후 재시도 (1588-4500)"},
			{"0148","개인회원강제승인불가 (1588-4500)"},
			{"0149","일반승인요망 (1588-4500)"},
			{"0150","해지가맹점 (1588-4500)"},
			{"0151","할부거래할수없는가맹점 1588-4500"},
			{"0152","잔액증명발급계좌 (1588-4500)"},
			{"0153","거래정지가맹점 (1588-4500)"},
			{"0154","거치기간입력오류 (1588-4500)"},
			{"0155","유효기한입력오류 (1588-4500)"},
			{"0156","유효기한틀림 (1588-4500)"},
			{"0157","개시등록 후 이용 (1588-4500)"},
			{"0158","승인취소가능기간경과(1588-4500)"},
			{"0159","EDC거래마감 (1588-4500)"},
			{"0160","연합회거래정지 카드(1588-4500)"},
			{"0161","회수된카드 (1588-4500)"},
			{"0162","상품권사용한도 초과(1588-4500)"},
			{"0163","분실/도난 정지 카드(1588-4515)"},
			{"0164","할부거래할수없는회원(1588-4500)"},
			{"0165","결제대금연체 (1588-4500)"},
			{"0166","타사카드연체중 (1588-4500)"},
			{"0167","거래정지카드 (1588-4500)"},
			{"0168","카드를검증할수없음(1588-4500)"},
			{"0169","비밀번호오류 (1588-4500)"},
			{"0170","회원사용한도이미초과(1588-4500)"},
			{"0171","카드사용한도초과 (1588-4500)"},
			{"0172","할부한도가이미초과됨(1588-4500)"},
			{"0173","고객한도초과 (1588-9955)"},
			{"0174","강제승인요망 (1588-4500)"},
			{"0175","회원사용한도초과 (1588-4500)"},
			{"0176","할부한도초과 (1588-4500)"},
			{"0177","강제승인대상 (1588-4500)"},
			{"0178","업체한도초과 (1588-4500)"},
			{"0179","업체한도초과 (1588-4500)"},
			{"0180","특정제휴카드로거래불가 1588-4500"},
			{"0181","회원등록당일거래 (1588-4500)"},
			{"0182","심야승인제한 가맹점 1588-4500"},
			{"0183","비밀번호오류횟수초과(1588-4500)"},
			{"0184","카드사전화요망 (520-4288)"},
			{"0185","키인방식으로입력요망(1588-4500)"},
			{"0186","카드사전화요망 (1588-4500)"},
			{"0187","거래은행전화요망 (2169-5255)"},
			{"0188","카드사시스템장애 (1588-4500)"},
			{"0189","거래은행시스템 장애(1588-4500)"},
			{"0190","카드사 미등록 쇼핑몰 1588-4500"},
			{"0191","카드사전화요망 (1566-8002)"},
			{"0192","클린카드사용제한업종(1588-4500)"},
			{"0193","서비스제공시간 아님(1588-4500)"},
			{"0194","거래은행전화요망 (1588-4500)"},
			{"0195","탑포인트사용대상아님(1588-4500)"},
			{"0196","서비스코드검증 오류(1588-4500)"},
			{"0197","카드사전화요망 (1588-4500)"},
			{"0198","이미취소된거래 (1588-4500)"},
			{"0199","취소되지않은거래 (1588-4500)"},
			{"0212","거래오류 1588-1788"},
			{"0243","도난분실카드 1588-1788"},
			{"0249","인증오류횟수초과 1588-1788"},
			{"0250","비밀번호오류 회수초과"},
			{"0251","개인월간한도초과 1588-1788"},
			{"0252","비밀/주민번호 오류(1588-1788)"},
			{"0253","주민번호오류 1588-1788"},
			{"0254","유효기간경과카드 1588-1788"},
			{"0255","비밀번호오류 1588-1788"},
			{"0256","카드번호오류 1588-1788"},
			{"0257","비밀번호미입력 1588-1788"},
			{"0259","비밀번호등록요망 1588-1788"},
			{"0262","회원거래정지 1588-1788"},
			{"0263","일시불거래제한 회원요청"},
			{"0264","일시불거래제한 회원요청"},
			{"0265","할부거래제한 회원요청"},
			{"0266","할부거래시간제한 회원요청"},
			{"0267","거래제한업종 회원요청"},
			{"0271","신규발급카드 사용요망"},
			{"0272","카드사용등록요망 1588-1788"},
			{"0281","거래시간오류 1588-1788"},
			{"0282","카드M/S값오류 1588-1788"},
			{"0284","거래일자오류 1588-1788"},
			{"0291","상품권한도초과 1588-1788"},
			{"02G1","어음만기일없음"},
			{"02G2","어음만기일 입력오류"},
			{"02G3","어음만기일 지정코드오류"},
			{"02GA","IC카드검증값오류 1588-1788"},
			{"02H6","한도증액요망 1588-1687"},
			{"02H9","포인트사용불가 일반거래승인요망"},
			{"02HA","포인트잔액부족 일반거래승인요망"},
			{"02HB","시스템점검중 일반거래승인요망"},
			{"02HC","시스템점검중 일반거래승인요망"},
			{"02HD","시스템점검중 일반거래승인요망"},
			{"02HE","시스템점검중 일반거래승인요망"},
			{"02I0","기업대표카드 거래불가"},
			{"02I8","기업개별한도초과 1588-1788"},
			{"02I9","기업업체한도초과 1588-1788"},
			{"02IB","기업최고한도초과 1588-1788"},
			{"02IC","공과금한도초과 1588-1788"},
			{"02I5","할부승인불가 1588-1788"},
			{"02JK","현금환입취소요망 1588-1788"},
			{"02L3","놀이공원이용불가 1588-1788"},
			{"02L4","놀이공원이용횟수 초과"},
			{"02T1","카드사전화요망 1588-1788"},
			{"02T2","가족회원한도초과 1588-1788"},
			{"02T3","1일한도초과 1588-1788"},
			{"02T4","1회한도초과 1588-1788"},
			{"02T5","거래불가카드 1588-1788"},
			{"02T7","해지카드 1588-1788"},
			{"02T8","미교부카드 1588-1788"},
			{"02T9","반송(회수)카드 1588-1788"},
			{"02TB","도난분실접수 1588-1788"},
			{"02TC","MemberOnly 1588-1788"},
			{"02TD","UsedCoupon 이미사용한쿠폰"},
			{"02TE","NotAvailable 유용한쿠폰없음"},
			{"02TH","1회승인한도초과행내주유전용카드"},
			{"02TJ","1일승인한도초과행내주유전용카드"},
			{"02TQ","RF1일한도초과 MS로재거래요망"},
			{"02J4","회원청구보류건 승인취소불가"},
			{"02JE","카드사전화요망 1588-1788"},
			{"02JF","카드사전화요망 1588-1788"},
			{"02M1","맞춤서비스미등록 할인서비스불가"},
			{"02M2","VIPS비할인카드 할인서비스불가"},
			{"02P4","사용횟수초과 1588-1788"},
			{"02P9","거래금액오류 1588-1788"},
			{"02Q1","유효기간오류 1588-1788"},
			{"02KZ","카드사전화요망 1566-6900"},
			{"02YZ","카드사전화요망 1588-1788"},
			{"02A1","자동차감금액오류 포인트리카드"},
			{"02A2","할부기간오류 포인트리카드"},
			{"02A3","포인트리카드 미소지자"},
			{"02A4","자동차감사용불가 1588-1788"},
			{"02A5","선할인서비스 이용불가카드"},
			{"02C2","취소불가 가맹점번호불일치"},
			{"02I1","할부거래불가 할부비대상가맹점"},
			{"02I2","할부금액오류 1588-1788"},
			{"02I4","할부기간오류 1588-1788"},
			{"02I6","해지가맹점 1588-1788"},
			{"02ID","지급보류가맹점 1588-1788"},
			{"02IF","거래정지가맹점 1588-1788"},
			{"02IN","구평화가맹점 KB가맹점가입요망"},
			{"0203","미등록가맹점 가맹점원장없음"},
			{"02KG","거래불가가맹점 1588-1788"},
			{"02K9","거래제한업종 1588-1788"},
			{"02KA","제휴가맹점 승인불가"},
			{"02KB","REBATE대상가맹점 1588-1788"},
			{"02KC","공동망승인불가 공동망비대상"},
			{"02KD","심방미완료가맹점 1588-1788"},
			{"02KE","건당승인한도초과 가맹점승인한도"},
			{"02KF","월간승인한도초과 가맹점승인한도"},
			{"02KH","일일승인한도초과 가맹점승인한도"},
			{"02KI","KEY-IN불가가맹점 1588-1788"},
			{"02KJ","할부특약한도초과 가맹점승인한도"},
			{"02KK","PG입점업체 사업자번호미등록"},
			{"02KL","PG특약만료일경과 1588-1788"},
			{"02KM","PG입점업체해지 1588-1788"},
			{"02KN","인증특약체결요망 미체결가맹점"},
			{"02KO","비밀번호입력불가검증비대상가맹점"},
			{"02KS","자기계발카드 이용불가업종"},
			{"02XB","체크카드승인불가 시스템점검시간"},
			{"02R6","승인취소불가 승인내역불일치"},
			{"02J2","승인취소불가 기매입완료"},
			{"02J3","승인취소불가 승인내역불일치"},
			{"02J7","승인취소불가 기취소"},
			{"02JD","승인취소불가 기매입완료"},
			{"02K2","승인취소불가 기매입완료"},
			{"02ZA","인증전문형식오류 1588-1788"},
			{"02ZB","인증유효기간만료 1588-1788"},
			{"02ZC","인증서폐기 1588-1788"},
			{"02ZD","인증서효력정지 1588-1788"},
			{"02ZE","공인인증서오류 1588-1788"},
			{"02ZF","인증서만료 1588-1788"},
			{"02ZG","인증서주민번호 불일치"},
			{"02ZH","시스템오류 1588-1788"},
			{"02ZI","시스템오류 1588-1788"},
			{"02ZJ","공인인증시간초과 1588-1788"},
			{"02ZK","공인인증비밀번호 등록요망"},
			{"02ZL","ISP승인요망 전자상거래가맹점"},
			{"02ZM","ISP거래불가 ISP중계불가VAN사"},
			{"02ZN","공인인증서오류 1588-1788"},
			{"9402","referral"},
			{"9403","invalid Merchant"},
			{"9405","DO NOT HONOR"},
			{"9412","DENY"},
			{"9413","invalid amount"},
			{"9414","invalid card number"},
			{"9489","no such issuer"},
			{"9491","time out"},
			{"9494","Request in progress"},
			{"94N0","no action taken"}
		};
	}

	/// <summary>
	/// KIS정보통신 결제 송수신 전문 클래스
	/// </summary>
	public class KisSpec
	{
		public string TranCode;
		public string PosNo;
		public string TradeSerialNumber;
		public string WCC;
		public string SafeCardICData;
		public string SafeCardMSData;
		public string CardNo;
		public string Installment;
		public string TotAmt;
		public string CatID;
		public string VatAmt;
		public string SvcAmt;
		public string DeviceAuthValue;
		public string VanKey;

		public string OrgAuthNo;
		public string OrgAuthDate;

		public string outTranNo;
		public string outTradeReqDate;
		public string outTradeReqTime;
		public string outReplyCode;
		public string outJanAmt;
		public string outAccepterCode;
		public string outAccepterName;
		public string outAuthNo;
		public string outIssuerCode;
		public string outIssuerName;
		public string outMerchantRegNo;
		public string outReplyMsg1;
		public string outReplyDate;
		public string outEMVData;
		public string outVanKey;

		public KisSpec()
		{
			Init();
		}

		public void Init()
		{
			TranCode = "";
			PosNo = "";
			TradeSerialNumber = "";
			WCC = "";
			SafeCardICData = "";
			SafeCardMSData = "";
			CardNo = "";
			Installment = "";
			TotAmt = "";
			CatID = "";
			VatAmt = "";
			SvcAmt = "";
			DeviceAuthValue = "";
			VanKey = "";

			OrgAuthNo = "";
			OrgAuthDate = "";

			outTranNo = "";
			outTradeReqDate = "";
			outTradeReqTime = "";
			outReplyCode = "";
			outJanAmt = "";
			outAccepterCode = "";
			outAccepterName = "";
			outAuthNo = "";
			outIssuerCode = "";
			outIssuerName = "";
			outMerchantRegNo = "";
			outReplyMsg1 = "";
			outReplyDate = "";
			outEMVData = "";
			outVanKey = "";
		}

		public string MakeReqSpec()
		{
			char STX = (char)0x02;
			char CR = (char)0x0D;
			char ETX = (char)0x03;
			string reqSpec = "";

			if (TranCode.Equals("D1"))
			{
				//STX
				reqSpec += STX.ToString();
				//전문구분
				reqSpec += TranCode.Substring(0, 2);
				//POS번호
				reqSpec += PosNo.PadLeft(3, ' ');
				//일련번호
				reqSpec += TradeSerialNumber.PadLeft(6, '0');
				//거래요청일시
				reqSpec += GetTime_YYYYMMDDhhmmss();
				//WCC
				reqSpec += WCC.PadLeft(1, ' ');

				//카드데이터
				if (CardNo == "")
				{
					// VanID(2)
					reqSpec += "09";
					// 암호화 정보 (96)
					reqSpec += "[##QS][00][0096]";
					// 생성된 MAC (8)
					reqSpec += "[##QS][01][0008]";
					// 단말기 ID (10)
					reqSpec += "[##QS][09][0010]";
					// KSN (20)
					reqSpec += "[##QS][02][0020]";
					// TPL (3)
					reqSpec += "   ";
					// DIKn 일련번호 (16)
					reqSpec += "                ";
					// PMK 유효기간 (6)
					reqSpec += "[##QS][03][0006]";
				}
				else
				{
					// VanID(2)
					reqSpec += "09";
					//카드번호
					reqSpec += CardNo.PadRight(159, ' ');
				}

				//할부개월
				reqSpec += Installment.PadLeft(2, '0');
				//거래금액
				reqSpec += TotAmt.PadLeft(8, '0');
				//단말기번호
				reqSpec += CatID.PadRight(10, ' ');
				//Filler
				reqSpec += "".PadLeft(28, ' ');
				//US
				reqSpec += "US";
				//부가세
				reqSpec += VatAmt.PadLeft(8, '0');
				//봉사료
				reqSpec += SvcAmt.PadLeft(8, '0');
				//Filler
				reqSpec += "".PadLeft(284, ' ');
				//하드코딩값
				reqSpec += "#SK";
				//리더기식별번호
				reqSpec += "[##QS][06][0016]";
				//POS식별번호
				reqSpec += "[##QS][07][0016]";
				//Fallback사유
				reqSpec += "[##QS][08][0002]";
				//하드코딩값
				reqSpec += "#E";
				//카드구분자
				reqSpec += "[##QS][04][0004]";
				//EMVData
				reqSpec += "[##QS][05][0257]";
				//VANKEY
				reqSpec += VanKey.PadLeft(16, ' ');
				//CR
				reqSpec += CR.ToString();
				//ETX
				reqSpec += ETX.ToString();
			}
			else if (TranCode.Equals("D2"))
			{
				//STX
				reqSpec += STX.ToString();
				//전문구분
				reqSpec += TranCode.Substring(0, 2);
				//POS번호
				reqSpec += PosNo.PadLeft(3, ' ');
				//일련번호
				reqSpec += TradeSerialNumber.PadLeft(6, '0');
				//거래요청일시
				reqSpec += GetTime_YYYYMMDDhhmmss();
				//WCC
				reqSpec += WCC.PadLeft(1, ' ');

				//카드데이터
				if (CardNo == "")
				{
					// VanID(2)
					reqSpec += "09";
					// 암호화 정보 (96)
					reqSpec += "[##QS][00][0096]";
					// 생성된 MAC (8)
					reqSpec += "[##QS][01][0008]";
					// 단말기 ID (10)
					reqSpec += "[##QS][09][0010]";
					// KSN (20)
					reqSpec += "[##QS][02][0020]";
					// TPL (3)
					reqSpec += "   ";
					// DIKn 일련번호 (16)
					reqSpec += "                ";
					// PMK 유효기간 (6)
					reqSpec += "[##QS][03][0006]";
				}
				else
				{
					// VanID(2)
					reqSpec += "09";
					//카드번호
					reqSpec += CardNo.PadRight(159, ' ');
				}

				//할부개월
				reqSpec += Installment.PadLeft(2, '0');
				//거래금액
				reqSpec += TotAmt.PadLeft(8, '0');
				//단말기번호
				reqSpec += CatID.PadRight(10, ' ');
				//원승인 번호
				reqSpec += OrgAuthNo.PadRight(8, ' ');
				//원승인 일자
				reqSpec += OrgAuthDate.PadRight(6, ' ');
				//Filler
				reqSpec += "".PadLeft(14, ' ');
				//US
				reqSpec += "US";
				//부가세
				reqSpec += VatAmt.PadLeft(8, '0');
				//봉사료
				reqSpec += SvcAmt.PadLeft(8, '0');
				//Filler
				reqSpec += "".PadLeft(284, ' ');
				//하드코딩값
				reqSpec += "#SK";
				//리더기식별번호
				reqSpec += "[##QS][06][0016]";
				//POS식별번호
				reqSpec += "[##QS][07][0016]";
				//Fallback사유
				reqSpec += "[##QS][08][0002]";
				//하드코딩값
				reqSpec += "#E";
				//카드구분자
				reqSpec += "[##QS][04][0004]";
				//EMVData
				reqSpec += "[##QS][05][0257]";
				//VANKEY
				reqSpec += VanKey.PadLeft(16, ' ');
				//CR
				reqSpec += CR.ToString();
				//ETX
				reqSpec += ETX.ToString();
			}
			if ((TranCode == "H1") || (TranCode == "H2"))
			{
				//STX
				reqSpec += STX.ToString();
				//전문구분
				reqSpec += TranCode.Substring(0, 2);
				//POS번호
				reqSpec += PosNo.PadLeft(3, ' ');
				//일련번호
				reqSpec += TradeSerialNumber.PadLeft(6, '0');
				//거래요청일시
				reqSpec += GetTime_YYYYMMDDhhmmss();
				//WCC
				reqSpec += WCC.PadLeft(1, ' ');

				//카드데이터
				if (CardNo == "")
				{
					// VanID(2)
					reqSpec += "09";
					// 암호화 정보 (96)
					reqSpec += "[##QS][00][0096]";
					// 생성된 MAC (8)
					reqSpec += "[##QS][01][0008]";
					// 단말기 ID (10)
					reqSpec += "[##QS][09][0010]";
					// KSN (20)
					reqSpec += "[##QS][02][0020]";
					// TPL (3)
					reqSpec += "   ";
					// DIKn 일련번호 (16)
					reqSpec += "                ";
					// PMK 유효기간 (6)
					reqSpec += "[##QS][03][0006]";
				}
				else
				{
					// VanID(2)
					reqSpec += "09";
					//카드번호
					reqSpec += CardNo.PadRight(159, ' ');
				}

				//할부개월
				reqSpec += Installment.PadLeft(2, '0');
				//거래금액
				reqSpec += TotAmt.PadLeft(8, '0');
				//단말기번호
				reqSpec += CatID.PadRight(10, ' ');
				// 세금
				reqSpec += VatAmt.PadLeft(8, '0');
				// 셋
				reqSpec += "US";
				// filler
				reqSpec += "".PadLeft(4, ' ');
				// 봉사료
				reqSpec += SvcAmt.PadLeft(8, '0');

				if (TranCode == "H1")
				{
					// filler
					reqSpec += "".PadLeft(72, ' ');
				}
				else
				{
					//원승인 번호
					reqSpec += OrgAuthNo.PadRight(8, ' ');
					//원승인 일자
					reqSpec += OrgAuthDate.PadRight(6, ' ');
					// 취소사유
					reqSpec += "1";
					// filler
					reqSpec += "".PadLeft(56, ' ');
				}

				//CR
				reqSpec += CR.ToString();
				//ETX
				reqSpec += ETX.ToString();
			}

			return reqSpec;
		}

		public int GetResSpec(string resSpec)
		{
			int nRet = -1;
			byte[] tmpResSpec = Encoding.Default.GetBytes(resSpec);
			if ((TranCode == "D1") || (TranCode == "D2"))
			{
				//거래일련번호
				outTranNo = getByteString(resSpec, 6, 6);
				//거래요청날짜
				outTradeReqDate = getByteString(resSpec, 12, 8);
				//거래요청시간
				outTradeReqTime = getByteString(resSpec, 20, 6);
				//응답코드
				outReplyCode = getByteString(resSpec, 26, 4);
				// GIFT카드시 잔액
				outJanAmt = getByteString(resSpec, 30, 6);
				// 매입사코드
				outAccepterCode = getByteString(resSpec, 36, 2);
				// 매입사명
				outAccepterName = getByteString(resSpec, 98, 20);
				// 승인번호
				outAuthNo = getByteString(resSpec, 70, 8);
				// 발급사코드
				outIssuerCode = getByteString(resSpec, 38, 2);
				// 발급사명
				outIssuerName = getByteString(resSpec, 78, 20);
				// 가맹점번호
				outMerchantRegNo = getByteString(resSpec, 40, 20);
				// 메세지
				outReplyMsg1 = getByteString(resSpec, 118, 40);
				// VAN승인일자
				outReplyDate = getByteString(resSpec, 163, 8);
				nRet = 0;
			}
			else if ((TranCode == "H1") || (TranCode == "H2"))
			{
				//거래일련번호
				outTranNo = getByteString(resSpec, 6, 6);
				//거래요청날짜
				outTradeReqDate = getByteString(resSpec, 12, 8);
				//거래요청시간
				outTradeReqTime = getByteString(resSpec, 20, 6);
				//응답코드
				outReplyCode = getByteString(resSpec, 26, 4);
				// 승인번호
				outAuthNo = "09" + getByteString(resSpec, 70, 8);
				// 가맹점번호
				outMerchantRegNo = getByteString(resSpec, 40, 20);
				// 메세지
				outReplyMsg1 = getByteString(resSpec, 98, 60);
				// VAN승인일자
				outReplyDate = getByteString(resSpec, 163, 8);
				nRet = 0;
			}

			return nRet;
		}

		private string GetTime_YYYYMMDDhhmmss()
		{
			DateTime CurDate = DateTime.Now;
			string tmpDate = "";

			tmpDate += CurDate.Year.ToString().PadLeft(4);
			tmpDate += CurDate.Month.ToString().PadLeft(2, '0');
			tmpDate += CurDate.Day.ToString().PadLeft(2, '0');
			tmpDate += CurDate.Hour.ToString().PadLeft(2, '0');
			tmpDate += CurDate.Minute.ToString().PadLeft(2, '0');
			tmpDate += CurDate.Second.ToString().PadLeft(2, '0');

			return tmpDate;
		}

		public static String getByteString(String s, int startIdx, int bytes)
		{
			byte[] tmpResSpec = Encoding.Default.GetBytes(s);
			return Encoding.Default.GetString(tmpResSpec, startIdx, bytes);
		}
	}

	/// <summary>
	/// KIS정보통신 결제 데이터 클래스
	/// </summary>
	public class PayData
	{
		public string inTradeType;
		public string inInstallment;
		public string inOrgAuthDate;
		public string inOrgAuthNo;
		public string inTranAmt;
		public string inVatAmt;
		public string inSvcAmt;
		public string inCatId;
		public string inWCC;
		public string inBarcodeNumber;
		public string inReaderType;

		public string outAgentCode;
		public string outResCode;
		public string outResMsg1;
		public string outResMsg2;
		public string outReaderData;
		public string outCardBin;
		public string outCatId;
		public string outWCC;
		public string outInstallment;
		public string outTranAmt;
		public string outVatAmt;
		public string outSvcAmt;
		public string outJanAmt;
		public string outAuthNo;
		public string outReplyDate;
		public string outAccepterCode;
		public string outAccepterName;
		public string outIssuerCode;
		public string outIssuerName;
		public string outCommissionRate;
		public string outAccountNum;
		public string outTranNo;
		public string outMerchantRegNo;
		public string outTradeNum;
		public string outTradeReqDate;
		public string outTradeReqTime;

		public int Step;
		public int ReaderType;

		public void Init()
		{
			Step = 0;
			ReaderType = 0;

			inTradeType = "";
			inInstallment = "";
			inOrgAuthDate = "";
			inOrgAuthNo = "";
			inTranAmt = "";
			inVatAmt = "";
			inSvcAmt = "";
			inCatId = "";
			inWCC = "";
			inBarcodeNumber = "";
			inReaderType = "";

			outResCode = "";
			outResMsg1 = "";
			outResMsg2 = "";
			outReaderData = "";
			outCardBin = "";
			outCatId = "";
			outWCC = "";
			outInstallment = "";
			outTranAmt = "";
			outVatAmt = "";
			outSvcAmt = "";
			outJanAmt = "";
			outAuthNo = "";
			outReplyDate = "";
			outAccepterCode = "";
			outAccepterName = "";
			outIssuerCode = "";
			outIssuerName = "";
			outCommissionRate = "";
			outAccountNum = "";
			outTranNo = "";
			outMerchantRegNo = "";
			outTradeNum = "";
			outTradeReqDate = "";
			outTradeReqTime = "";
		}
	}
}
