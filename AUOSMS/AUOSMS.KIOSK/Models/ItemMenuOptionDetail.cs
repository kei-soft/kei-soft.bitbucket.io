﻿using System;

namespace AUOSMS.KIOSK.Models
{
	/// <summary>
	/// 메뉴 아이템 옵션 상세 모델
	/// </summary>
	public class ItemMenuOptionDetail
	{
		public int CODE { get; set; }
		public int NO { get; set; }
		public string NAME { get; set; }
		public int PRICE { get; set; }

		/// <summary>
		/// 선택 여부
		/// </summary>
		public bool SELECTED { get; set; }

		/// <summary>
		/// 표시용 가격 문자열
		/// </summary>
		public string PRICE_STRING
		{
			get
			{
				return (this.PRICE > 0) ? String.Format("{0:#,#}{1}", this.PRICE, Properties.Resources.MoneyUnit) : String.Empty;
			}
		}
	}
}
