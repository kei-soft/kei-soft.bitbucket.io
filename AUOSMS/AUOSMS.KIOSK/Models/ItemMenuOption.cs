﻿namespace AUOSMS.KIOSK.Models
{
	/// <summary>
	/// 메뉴 아이템 옵션 모델
	/// </summary>
	public class ItemMenuOption
	{
		public int CODE { get; set; }
		public string NAME { get; set; }
		public bool REQUIRED { get; set; }
	}
}
