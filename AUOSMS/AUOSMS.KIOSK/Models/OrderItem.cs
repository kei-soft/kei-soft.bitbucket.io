﻿namespace AUOSMS.KIOSK.Models
{
	/// <summary>
	/// 주문 메뉴 아이템 모델
	/// </summary>
	public class OrderItem
	{
		public int NO { get; set; }
		public int ITEM_ID { get; set; }
		public string ITEM_NAME { get; set; }
		public int COUNT { get; set; }
		public int PRICE { get; set; }
		public string OPTION_JSON { get; set; }
	}
}
