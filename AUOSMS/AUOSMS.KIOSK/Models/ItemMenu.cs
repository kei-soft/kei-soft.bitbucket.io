﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace AUOSMS.KIOSK.Models
{
	/// <summary>
	/// 메뉴 아이템 모델
	/// </summary>
	public class ItemMenu
	{
		public int GROUP { get; set; }
		public int ID { get; set; }
		public string NAME { get; set; }
		public string ENG_NAME { get; set; }
		public string DESC { get; set; }
		public Image IMAGE { get; set; }
		public int PRICE { get; set; }
		public List<ItemMenuOption> OPTION { get; set; } = new List<ItemMenuOption>();
	}
}
