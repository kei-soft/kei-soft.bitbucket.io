﻿using AUOSMS.KIOSK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;

namespace AUOSMS.KIOSK.Controls
{
	/// <summary>
	/// 옵션 선택 사용자 컨트롤
	/// </summary>
	public partial class OptionSelector : UserControl
	{
		#region Events

		/// <summary>
		/// 옵션 변경 이벤트
		/// </summary>
		public event EventHandler<EventArgs> OptionChanged;

		#endregion

		#region Properties

		/// <summary>
		/// 옵션별 옵션 상세 목록
		/// </summary>
		public List<ItemMenuOptionDetail> OptionDetails { get; set; } = new List<ItemMenuOptionDetail>();

		#endregion

		#region Fields

		/// <summary>
		/// 선택된 옵션 상세 목록
		/// </summary>
		public List<ItemMenuOptionDetail> SelectedOptionDetails = new List<ItemMenuOptionDetail>();

		/// <summary>
		/// 필수 옵션 여부
		/// </summary>
		private bool IsRequired = false;

		#endregion
		
		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="name">옵션 이름</param>
		/// <param name="details">옵션 상세 목록</param>
		/// <param name="required">필수 여부</param>
		public OptionSelector(string name, List<ItemMenuOptionDetail> details, bool required)
		{
			InitializeComponent();

			this.IsRequired = required;
			if (this.IsRequired)
			{
				details.First().SELECTED = true;
			}

			this.OptionNameTextBlock.Text = name;
			this.OptionDetails = details;

			this.DataContext = this;
		}

		/// <summary>
		/// 옵션 선택 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Option_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (this.IsRequired)
			{
				this.OptionDetails.ForEach(x => { x.SELECTED = false; });
			}

			Grid currentOption = sender as Grid;
			int no = (int)currentOption.Tag;
			var detail = this.OptionDetails.First(x => x.NO == no);
			detail.SELECTED = !detail.SELECTED;

			this.OptionDetailsItemsControl.Items.Refresh();
			this.SelectedOptionDetails.First(x => x.CODE == detail.CODE && x.NO == detail.NO).SELECTED = detail.SELECTED;

			this.OptionChanged?.Invoke(this, new EventArgs());
		}
	}
}
