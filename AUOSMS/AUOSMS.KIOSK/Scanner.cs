﻿using System;
using System.Management;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// QR코드 스캐너
	/// </summary>
	/// <remarks>
	/// DYSCAN DE2110 : SM-2D PRODUCT HID KBW : VID=AC90, PID=3002
	/// </remarks>
	internal static class Scanner
	{
		/// <summary>
		/// 연결 상태 확인
		/// </summary>
		/// <returns></returns>
		public static bool IsConnected()
		{
			bool isConnected = false;

			string query = "SELECT * FROM Win32_PnPEntity WHERE DeviceID LIKE '%VID_AC90&PID_3002%'";
			try
			{
				int cnt = 0;
				using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", query))
				{
					foreach (ManagementObject queryObj in searcher.Get())
					{
						cnt++;
					}
				}

				if (cnt == 2) // HID + USB
				{
					isConnected = true;
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}

			return isConnected;
		}
	}
}
