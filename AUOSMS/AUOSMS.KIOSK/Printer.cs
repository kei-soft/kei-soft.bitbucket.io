﻿using System;
using System.IO.Ports;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 프린터
	/// </summary>
	/// <remarks>
	/// 유진시스텍 YJ-730T(3) : VID=1CBE, PID=0005
	/// </remarks>
	internal static class Printer
	{
		/// <summary>
		/// 시리얼 포트
		/// </summary>
		private static SerialPort SP = new SerialPort();

		/// <summary>
		/// 연결 상태 확인
		/// </summary>
		/// <returns></returns>
		public static bool IsConnected()
		{
			bool isConnected = false;

			string query = "SELECT * FROM Win32_PnPEntity WHERE ClassGuid = '{4d36e978-e325-11ce-bfc1-08002be10318}'";
			try
			{
				using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", query))
				{
					foreach (ManagementObject queryObj in searcher.Get())
					{
						string port = Regex.Replace(queryObj["Name"].ToString(), "[^0-9]", "");

						// 장치를 특정할 수 없음 : INI 설정된 포트 기준으로 확인
						if (port.Equals(AppConfig.PrinterComPort))
						{
							isConnected = true;
							break;
						}
					}
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}

			return isConnected;
		}

		/// <summary>
		/// 하드웨어 연결
		/// </summary>
		/// <returns></returns>
		public static bool Open()
		{
			try
			{
				if (SP != null)
				{
					SP.Close();
				}

				SP = new SerialPort();
				SP.DataReceived += SP_DataReceived;
				SP.PortName = $"COM{AppConfig.PrinterComPort}";
				SP.BaudRate = 9600;
				SP.Parity = Parity.None;
				SP.DataBits = 8;
				SP.StopBits = StopBits.One;
				SP.Handshake = Handshake.XOnXOff;
				SP.Open();

				AppCommon.Log("PRINTER - OPEN :: {0} = {1}", SP.PortName, SP.IsOpen);
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}

			return SP.IsOpen;
		}

		/// <summary>
		/// 하드웨어 연결 종료
		/// </summary>
		public static void Close()
		{
			SP.Close();
			AppCommon.Log("PRINTER - CLOSE :: {0} = {1}", SP.PortName, SP.IsOpen);
		}

		#region RX : Send

		/// <summary>
		/// 상태정보 요청
		/// </summary>
		public static void GetStatus()
		{
			try
			{
				AppCommon.Log("PRINTER - GetStatus");

				byte[] command = new byte[2] { 0x1B, 0x07 };
				SendCommand(command);
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}
		}

		/// <summary>
		/// 주문서 출력
		/// </summary>
		/// <param name="orderNo">주문번호(접수번호)</param>
		public static void PrintOrderNo(string orderNo)
		{
			try
			{
				AppCommon.Log("PRINTER - PRINT :: OrderNo = {0}", orderNo);

				// Center Alignment
				SendCommand(new byte[] { 0x1B, 0x61, 0x01 });
				// Font Size
				byte size1 = ((byte)0xF0 & ((byte)0x00 << 4) | ((byte)0x0F & (byte)(0x00)));
				byte size2 = ((byte)0xF0 & ((byte)0x01 << 4) | ((byte)0x0F & (byte)(0x01)));

				SendCommand(new byte[] { 0x1D, 0x21, size1 });
				byte[] command = Encoding.Default.GetBytes($"{AppConfig.StoreName}\n{DateTime.Now:yyyy-MM-dd HH:mm:ss}");
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x02 }); // Print & n Line Feed

				SendCommand(new byte[] { 0x1D, 0x21, size2 });
				command = Encoding.Default.GetBytes($"[{Properties.Resources.OrderNo} : {orderNo}]");
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x02 }); // Print & n Line Feed

				SendCommand(new byte[] { 0x1D, 0x21, size1 });
				command = Encoding.Default.GetBytes($"{Properties.Resources.ThankYou}\n");
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x06 }); // Print & n Line Feed

				//SendCommand(new byte[] { 0x1B, 0x69 }); // Partial Cut
				SendCommand(new byte[] { 0x1B, 0x6D }); // Full Cut
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}
		}

		/// <summary>
		/// 카드영수증 출력 요청
		/// </summary>
		/// <param name="storeInfo">매장정보</param>
		/// <param name="orderNoInfo">주문번호</param>
		/// <param name="orderItemInfo">품목 및 가격 정보</param>
		/// <param name="orderReceipt">신용카드 결제정보</param>
		public static void PrintReceipt(string storeInfo, string orderNoInfo, string orderItemInfo, string orderReceipt)
		{
			try
			{
				AppCommon.Log("PRINTER - PRINT :: Receipt = {0}", orderNoInfo);

				// Font Size
				byte size1 = ((byte)0xF0 & ((byte)0x00 << 4) | ((byte)0x0F & (byte)(0x00)));
				byte size2 = ((byte)0xF0 & ((byte)0x01 << 4) | ((byte)0x0F & (byte)(0x01)));

				SendCommand(new byte[] { 0x1B, 0x61, 0x00 }); // Left Alignment
				SendCommand(new byte[] { 0x1D, 0x21, size1 });
				byte[] command = Encoding.Default.GetBytes(storeInfo);
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x01 }); // Print & n Line Feed

				SendCommand(new byte[] { 0x1B, 0x61, 0x01 }); // Center Alignment
				SendCommand(new byte[] { 0x1D, 0x21, size2 });
				command = Encoding.Default.GetBytes(orderNoInfo);
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x01 }); // Print & n Line Feed

				SendCommand(new byte[] { 0x1B, 0x61, 0x00 }); // Left Alignment
				SendCommand(new byte[] { 0x1D, 0x21, size1 });
				command = Encoding.Default.GetBytes(orderItemInfo);
				SendCommand(command); // Write Buffer
				//SendCommand(new byte[] { 0x1B, 0x64, 0x00 }); // Print & n Line Feed

				//SendCommand(new byte[] { 0x1B, 0x61, 0x01 }); // Center Alignment
				command = Encoding.Default.GetBytes(Properties.Resources.ReceiptTItle.PadLeft(26, ' '));
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x00 }); // Print & n Line Feed

				SendCommand(new byte[] { 0x1B, 0x61, 0x00 }); // Left Alignment
				command = Encoding.Default.GetBytes(orderReceipt);
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x01 }); // Print & n Line Feed

				SendCommand(new byte[] { 0x1B, 0x61, 0x01 }); // Center Alignment
				command = Encoding.Default.GetBytes(Properties.Resources.ThankYou);
				SendCommand(command); // Write Buffer
				SendCommand(new byte[] { 0x1B, 0x64, 0x06 }); // Print & n Line Feed

				//SendCommand(new byte[] { 0x1B, 0x69 }); // Partial Cut
				SendCommand(new byte[] { 0x1B, 0x6D }); // Full Cut
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}
		}

		/// <summary>
		/// 명령 전송
		/// </summary>
		/// <param name="commandData"></param>
		private static void SendCommand(byte[] commandData)
		{
			try
			{
				if (!SP.IsOpen)
				{
					Printer.Open();
				}

				if (SP.IsOpen)
				{
					SP.Write(commandData, 0, commandData.Length);
					AppCommon.Log("PRINTER - SendCommand :: [RX][{0}]", BitConverter.ToString(commandData));
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}
		}

		#endregion

		#region TX : Receive

		/// <summary>
		/// 수신 이벤트
		/// </summary>
		public static event EventHandler<TxEventArgs> DataReceived;

		/// <summary>
		/// 응답 수신
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private static void SP_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			try
			{
				int bytesCount = SP.BytesToRead;
				if (bytesCount > 0)
				{
					byte[] buffer = new byte[bytesCount];
					SP.Read(buffer, 0, bytesCount);

					if (DataReceived != null)
					{
						AppCommon.Log("PRINTER - ReceiveData :: [TX][{0}]", BitConverter.ToString(buffer));

						string arg = String.Empty;
						byte flagByte = bytesCount == 2 ? buffer[1] : buffer[0];

						switch (flagByte)
						{
							case 0x41: // Paper Empty
								arg = "Paper Empty";
								break;
							case 0x42: // Caver Open
								arg = "Caver Open";
								break;
							case 0x50: // Roll Paper Empty
							case 0x21: // Roll Paper Empty
								arg = "Roll Paper Empty";
								break;
							case 0x44: // Head TM Over
								arg = "Head TM Over";
								break;
							case 0x40: // OK
								break;
						}

						if (!String.IsNullOrEmpty(arg))
						{
							DataReceived(sender, new TxEventArgs(arg));
						}
					}
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}
		}

		#endregion
	}
}
