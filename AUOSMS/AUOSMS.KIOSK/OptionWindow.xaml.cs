﻿using AUOSMS.KIOSK.Controls;
using AUOSMS.KIOSK.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 키오스크 주문시 옵션선택 윈도우
	/// </summary>
	public partial class OptionWindow : Window
	{
		#region Fields

		/// <summary>
		/// 선택된 아이템
		/// </summary>
		private ItemMenu SelectedItem = null;

		/// <summary>
		/// 선택된 옵션 상세 목록
		/// </summary>
		public List<ItemMenuOptionDetail> SelectedOptionDetails = AppCommon.MenuOptions;

		/// <summary>
		/// 주문 수량
		/// </summary>
		private int OrderCount = 1;

		/// <summary>
		/// 주문 금액
		/// </summary>
		private int OrderPrice = 0;

		/// <summary>
		/// 선택된 아이템 목록 : 장바구니
		/// </summary>
		public ObservableCollection<OrderItem> OrderBasket = new ObservableCollection<OrderItem>();

		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		public OptionWindow(int itemID)
		{
			InitializeComponent();

			this.Loaded += OptionWindow_Loaded;
			this.Closed += OptionWindow_Closed;

			this.OKButton.Click += OKButton_Click;
			this.CancelButton.Click += CancelButton_Click;
			this.CloseButton.PreviewMouseUp += CloseButton_PreviewMouseUp;

			this.DecreaseButton.PreviewMouseUp += DecreaseButton_PreviewMouseUp;
			this.IncreaseButton.PreviewMouseUp += IncreaseButton_PreviewMouseUp;

			this.OptionHotButton.PreviewMouseUp += OptionHotButton_PreviewMouseUp;
			this.OptionIceButton.PreviewMouseUp += OptionIceButton_PreviewMouseUp;
			this.OptionHotButton.IsEnabled = false;
			this.OptionIceButton.IsEnabled = true;

			this.SelectedItem = AppCommon.MenuItems.First(item => item.ID == itemID);
			this.SelectedOptionDetails.ForEach(x => x.SELECTED = false);
		}

		/// <summary>
		/// 윈도우 로드
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OptionWindow_Loaded(object sender, RoutedEventArgs e)
		{
			AppCommon.Log("OPTION - OPEN");
			this.DisplayOption();
		}

		/// <summary>
		/// 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OptionWindow_Closed(object sender, EventArgs e)
		{
			AppCommon.Log("OPTION - CLOSE");
		}

		/// <summary>
		/// 수량 감소 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DecreaseButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (this.OrderCount == 1) return;
			this.OrderCount--;
			this.CountTextBlock.Text = this.OrderCount.ToString();
			this.DisplayPrice();
		}

		/// <summary>
		/// 수량 증가 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void IncreaseButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			this.OrderCount++;
			this.CountTextBlock.Text = this.OrderCount.ToString();
			this.DisplayPrice();
		}

		/// <summary>
		/// 아이템 및 옵션 항목 표시
		/// </summary>
		private void DisplayOption()
		{
			this.ItemImage.Source = this.SelectedItem.IMAGE.Source;
			this.NameTextBlock.Text = this.SelectedItem.NAME;
			this.CountTextBlock.Text = this.OrderCount.ToString();

			this.OptionsListPanel.Children.Clear();
			bool optHotIceVisible = false;

			foreach (var option in this.SelectedItem.OPTION)
			{
				List<ItemMenuOptionDetail> details = this.SelectedOptionDetails.Where(x => x.CODE == option.CODE).ToList();

				string chkName = Regex.Replace(option.NAME, @"[^a-zA-Z]", "").ToUpper();
				if (chkName == "HOTICE")
				{
					var hot = details.First(x => x.NAME.ToUpper() == "HOT");
					var ice = details.First(x => x.NAME.ToUpper() == "ICE");

					this.OptionHotButton.Tag = hot;
					this.OptionIceButton.Tag = ice;

					if (hot.PRICE > 0) this.OptionHotButton.Text += $" ({hot.PRICE_STRING})";
					if (ice.PRICE > 0) this.OptionIceButton.Text += $" ({ice.PRICE_STRING})";

					if (!optHotIceVisible)
					{
						optHotIceVisible = true;
						this.SelectedOptionDetails.First(x => x.CODE == hot.CODE && x.NO == hot.NO).SELECTED = true;
					}
				}
				else
				{
					OptionSelector optionSelector = new OptionSelector(option.NAME, details, option.REQUIRED);
					optionSelector.SelectedOptionDetails = this.SelectedOptionDetails;
					optionSelector.OptionChanged += OptionSelector_OptionChanged;

					this.OptionsListPanel.Children.Add(optionSelector);
				}
			}

			this.HotIceOption.Visibility = (optHotIceVisible) ? Visibility.Visible : Visibility.Collapsed;
			this.DisplayPrice();
		}

		/// <summary>
		/// 아이템 및 옵션 합산 금액 표시
		/// </summary>
		private void DisplayPrice()
		{
			this.OrderPrice = this.SelectedItem.PRICE;
			foreach (var option in this.SelectedOptionDetails.Where(x => x.SELECTED))
			{
				this.OrderPrice += option.PRICE;
			}
			this.OrderPrice *= this.OrderCount;

			this.PriceTextBlock.Text = $"{this.OrderPrice:#,#}{Properties.Resources.MoneyUnit}";
		}

		/// <summary>
		/// 옵션 HOT 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OptionHotButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (!this.OptionHotButton.IsEnabled) return;
			this.OptionHotButton.IsEnabled = false;
			this.OptionIceButton.IsEnabled = true;

			ItemMenuOptionDetail hotDetail = this.OptionHotButton.Tag as ItemMenuOptionDetail;
			this.SelectedOptionDetails.First(x => x.CODE == hotDetail.CODE && x.NO == hotDetail.NO).SELECTED = true;
			ItemMenuOptionDetail iceDetail = this.OptionIceButton.Tag as ItemMenuOptionDetail;
			this.SelectedOptionDetails.First(x => x.CODE == iceDetail.CODE && x.NO == iceDetail.NO).SELECTED = false;
		}

		/// <summary>
		/// 옵션 ICE 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OptionIceButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (!this.OptionIceButton.IsEnabled) return;
			this.OptionHotButton.IsEnabled = true;
			this.OptionIceButton.IsEnabled = false;

			ItemMenuOptionDetail hotDetail = this.OptionHotButton.Tag as ItemMenuOptionDetail;
			this.SelectedOptionDetails.First(x => x.CODE == hotDetail.CODE && x.NO == hotDetail.NO).SELECTED = false;
			ItemMenuOptionDetail iceDetail = this.OptionIceButton.Tag as ItemMenuOptionDetail;
			this.SelectedOptionDetails.First(x => x.CODE == iceDetail.CODE && x.NO == iceDetail.NO).SELECTED = true;
		}

		/// <summary>
		/// 옵션 선택 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OptionSelector_OptionChanged(object sender, EventArgs e)
		{
			this.DisplayPrice();
		}

		/// <summary>
		/// 옵션 정보 JSON 처리 : OPTION_NAME, OPTION_DETAIL_NAME, OPTION_DETAIL_PRICE
		/// </summary>
		private string ConvertToJson()
		{
			var options = this.SelectedOptionDetails.Where(x => x.SELECTED)
													.Join(this.SelectedItem.OPTION, d => d.CODE, o => o.CODE,
														 (d, o) => new { OPTION_NAME = o.NAME, OPTION_DETAIL_NAME = d.NAME, OPTION_DETAIL_PRICE = d.PRICE })
													.ToList();
			string json = JsonConvert.SerializeObject(options);
			AppCommon.Log("OPTION - JSON :: {0}", json);

			return json;
		}

		/// <summary>
		/// 확인 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OKButton_Click(object sender, RoutedEventArgs e)
		{
			OrderItem orderItem = new OrderItem
			{
				ITEM_ID = this.SelectedItem.ID,
				ITEM_NAME = this.SelectedItem.NAME,
				COUNT = this.OrderCount,
				PRICE = this.OrderPrice,
				OPTION_JSON = this.ConvertToJson()
			};

			AppCommon.Log("OPTION - SELECTED :: {0}", orderItem.ITEM_NAME);

			var sameItem = this.OrderBasket.FirstOrDefault(x => x.ITEM_ID == orderItem.ITEM_ID && x.OPTION_JSON == orderItem.OPTION_JSON);
			if (sameItem == null)
			{
				this.OrderBasket.Add(orderItem);
			}
			else
			{
				sameItem.COUNT += orderItem.COUNT;
				sameItem.PRICE += orderItem.PRICE;
			}

			this.Close();
		}

		/// <summary>
		/// 취소 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// 닫기 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CloseButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			this.Close();
		}
	}
}
