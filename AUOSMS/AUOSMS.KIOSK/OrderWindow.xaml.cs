﻿using AUOSMS.KIOSK.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 키오스크 주문 윈도우
	/// </summary>
	public partial class OrderWindow : Window
	{
		#region Properties
		
		/// <summary>
		/// 메뉴 그룹
		/// </summary>
		public Dictionary<int, string> MenuGroups { get; set; } = AppCommon.MenuGroups;

		/// <summary>
		/// 그룹별 메뉴
		/// </summary>
		public List<ItemMenu>[] MenuItems { get; set; }

		/// <summary>
		/// 선택된 아이템 목록 : 장바구니
		/// </summary>
		public ObservableCollection<OrderItem> OrderBasket { get; set; } = new ObservableCollection<OrderItem>();

		#endregion

		#region Fields
		
		/// <summary>
		/// 메뉴 그룹 탭 헤더 기본 스타일
		/// </summary>
		private readonly Style[] MenuGroupStyle = new Style[2];

		/// <summary>
		/// 메뉴 그룹 탭 헤더 선택 스타일
		/// </summary>
		private readonly Style[] MenuGroupSeletedStyle = new Style[2];

		/// <summary>
		/// 페이지 당 표시할 메뉴 수 : Rows * Cols
		/// </summary>
		private readonly int PageItemCount = 9;

		/// <summary>
		/// 메뉴 그룹 DB 코드 목록
		/// </summary>
		private readonly List<int> GroupCodeList = AppCommon.MenuGroups.Keys.ToList();

		/// <summary>
		/// 전체 주문 수량
		/// </summary>
		private int TotalCount = 0;

		/// <summary>
		/// 전체 주문 금액
		/// </summary>
		private int TotalPrice = 0;

		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		public OrderWindow()
		{
			InitializeComponent();

			#region MenuGroup

			this.MenuGroupStyle[0] = this.FindResource("MenuGroupBorderStyle") as Style;
			this.MenuGroupStyle[1] = this.FindResource("MenuGroupTextStyle") as Style;
			this.MenuGroupSeletedStyle[0] = this.FindResource("MenuGroupBorderSelectedStyle") as Style;
			this.MenuGroupSeletedStyle[1] = this.FindResource("MenuGroupTextSelecedStyle") as Style;

			#endregion

			#region MenuItem

			int maxItemCount = AppCommon.MenuItems.GroupBy(x => x.GROUP).Max(x => x.Count());
			int maxPageCount = maxItemCount / this.PageItemCount + ((maxItemCount % this.PageItemCount == 0) ? 0 : 1);
			this.MenuItems = new List<ItemMenu>[maxPageCount * this.GroupCodeList.Count];

			for (int i = 0; i < this.GroupCodeList.Count; i++)
			{
				int j = 0, n = 0;
				while (true)
				{
					var items = AppCommon.MenuItems.Where(x => x.GROUP == this.GroupCodeList[i]).Skip(n).Take(this.PageItemCount).ToList();
					n += this.PageItemCount;

					if (items.Count == 0) break;

					this.MenuItems[i + j++ * this.GroupCodeList.Count] = items;
				}
			}

			#endregion

			this.Loaded += OrderWindow_Loaded;
			this.Closed += OrderWindow_Closed;

			this.MenuItemTabControl.SelectionChanged += MenuItemTabControl_SelectionChanged;
			this.PreviousButton.PreviewMouseUp += PreviousButton_PreviewMouseUp;
			this.NextButton.PreviewMouseUp += NextButton_PreviewMouseUp;
			this.ScrollUpButton.PreviewMouseUp += ScrollUpButton_PreviewMouseUp;
			this.ScrollDownButton.PreviewMouseUp += ScrollDownButton_PreviewMouseUp;
			this.OrderButton.PreviewMouseUp += OrderButton_PreviewMouseUp;
			this.OrderCancelButton.PreviewMouseUp += OrderCancelButton_PreviewMouseUp;

			this.DataContext = this;

			this.OrderBasket.CollectionChanged += OrderBasket_CollectionChanged;
		}

		/// <summary>
		/// 윈도우 로드
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderWindow_Loaded(object sender, RoutedEventArgs e)
		{
			AppCommon.Log("ORDER - OPEN");
			this.MenuGroup_PreviewMouseUp(new TextBlock { Text = this.MenuGroups.First().Value }, null);
		}

		/// <summary>
		/// 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderWindow_Closed(object sender, EventArgs e)
		{
			AppCommon.Log("ORDER - CLOSE");
		}

		/// <summary>
		/// 메뉴 그룹 선택 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MenuGroup_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			// NOTE : Changeable to IsEnabled Trigger

			TextBlock selectedTextBlock = sender as TextBlock;
			this.MenuItemTabControl.SelectedIndex = this.MenuGroups.Values.ToList().IndexOf(selectedTextBlock.Text); ;

			for (int i = 0; i < this.MenuGroups.Count; i++)
			{
				UIElement groupItem = this.MenuGroupItemsControl.ItemContainerGenerator.ContainerFromIndex(i) as UIElement;
				Border border = VisualTreeHelper.GetChild(groupItem, 0) as Border;
				TextBlock textBlock = VisualTreeHelper.GetChild(border, 0) as TextBlock;

				if (textBlock.Text == selectedTextBlock.Text)
				{
					border.Style = this.MenuGroupSeletedStyle[0];
					textBlock.Style = this.MenuGroupSeletedStyle[1];
				}
				else
				{
					border.Style = this.MenuGroupStyle[0];
					textBlock.Style = this.MenuGroupStyle[1];
				}
			}
		}

		/// <summary>
		/// 탭 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MenuItemTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			AppCommon.Log("ORDER - CHANGE TAB/PAGE :: {0}", this.MenuItemTabControl.SelectedIndex);
			this.SetPager(this.MenuItemTabControl.SelectedIndex);
		}

		/// <summary>
		/// 페이저 세팅
		/// </summary>
		/// <param name="tabIndex"></param>
		private void SetPager(int tabIndex)
		{
			bool hasPreviousPage = (tabIndex >= this.MenuGroups.Count);
			this.PreviousButton.IsEnabled = hasPreviousPage;

			int nextTabIndex = tabIndex + this.MenuGroups.Count;
			bool hasNextPage = (nextTabIndex < this.MenuItems.Count());
			if (hasNextPage) hasNextPage = (this.MenuItems[nextTabIndex] != null);
			this.NextButton.IsEnabled = hasNextPage;

			int groupIndex = (tabIndex >= this.MenuGroups.Count) ? tabIndex - 4 : tabIndex;
			int itemCount = AppCommon.MenuItems.Count(x => x.GROUP == this.GroupCodeList[groupIndex]);
			int pageCount = itemCount / this.PageItemCount + (itemCount % this.PageItemCount == 0 ? 0 : 1);

			List<bool> pageIcons = new List<bool>();
			for (int i = 0; i < pageCount; i++)
			{
				bool isCurrentPage = (tabIndex == i * this.MenuGroups.Count + groupIndex);
				pageIcons.Add(isCurrentPage);
			}
			this.PagerIconPanel.ItemsSource = pageIcons;
		}

		/// <summary>
		/// 페이저 이전 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PreviousButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			this.MenuItemTabControl.SelectedIndex -= AppCommon.MenuGroups.Count;
		}

		/// <summary>
		/// 페이저 다음 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void NextButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			this.MenuItemTabControl.SelectedIndex += AppCommon.MenuGroups.Count;
		}

		/// <summary>
		/// 아이템 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MenuItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			Grid item = sender as Grid;
			AppCommon.Log("ORDER - SELECT ITEM :: {0}", item.Tag);

			Window popup = new OptionWindow((int)item.Tag)
			{
				Owner = this,
				Width = this.Width,
				Height = this.Height,
				WindowStartupLocation = WindowStartupLocation.CenterOwner,
				WindowState = WindowState.Maximized,
				OrderBasket = this.OrderBasket,
			};
			popup.Closing += OptionWindow_Closing;
			popup.ShowDialog();
		}

		/// <summary>
		/// 아이템 및 옵션 선택 완료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OptionWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			this.UpdateTotal();
			this.OrderBasketItemsControl.Items.Refresh();
		}

		/// <summary>
		/// 아이템 및 옵션 선택 완료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderBasket_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			AppCommon.Log("ORDER - BASKET");

			this.UpdateTotal();

			for (int i = 0; i < this.OrderBasket.Count; i++)
			{
				this.OrderBasket[i].NO = i + 1;
			}

			this.OrderBasketScrollViewer.ScrollToBottom();
		}

		/// <summary>
		/// 전체 금액 및 수량 계산
		/// </summary>
		private void UpdateTotal()
		{
			this.TotalCount = this.TotalPrice = 0;
			foreach (var item in this.OrderBasket)
			{
				this.TotalCount += item.COUNT;
				this.TotalPrice += item.PRICE;
			}

			this.TotalCountTextBlock.Text = this.TotalCount.ToString();
			this.TotalPriceTextBlock.Text = $"{this.TotalPrice:#,0}{Properties.Resources.MoneyUnit}";
		}

		/// <summary>
		/// 아이템별 수량 감소 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DecreaseButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			Border border = sender as Border;
			int itemID = (int)border.Tag;

			var item = this.OrderBasket.Where(x => x.ITEM_ID == itemID).First();

			if (item.COUNT == 1) return;

			int unitPrice = item.PRICE / item.COUNT;
			item.COUNT--;
			item.PRICE -= unitPrice;

			this.UpdateTotal();
			this.OrderBasketItemsControl.Items.Refresh();
		}

		/// <summary>
		/// 아이템별 수량 증가 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void IncreaseButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			Border border = sender as Border;
			int itemID = (int)border.Tag;

			var item = this.OrderBasket.Where(x => x.ITEM_ID == itemID).First();
			int unitPrice = item.PRICE / item.COUNT;
			item.COUNT++;
			item.PRICE += unitPrice;

			this.UpdateTotal();
			this.OrderBasketItemsControl.Items.Refresh();
		}

		/// <summary>
		/// 아이템별 삭제 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderDeleteButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			Border border = sender as Border;
			int itemID = (int)border.Tag;

			var item = this.OrderBasket.Where(x => x.ITEM_ID == itemID).First();
			this.OrderBasket.Remove(item);
		}

		/// <summary>
		/// 주문 목록 위 스크롤 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ScrollUpButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			double offset = this.OrderBasketScrollViewer.VerticalOffset - 71;
			this.OrderBasketScrollViewer.ScrollToVerticalOffset(offset);
		}

		/// <summary>
		/// 주문 목록 아래 스크롤 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ScrollDownButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			double offset = this.OrderBasketScrollViewer.VerticalOffset + 71;
			this.OrderBasketScrollViewer.ScrollToVerticalOffset(offset);
		}

		/// <summary>
		/// 주문취소 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderCancelButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			this.OrderBasket.Clear();
			this.Close();
			AppCommon.Log("ORDER - CANCEL");
		}

		/// <summary>
		/// 주문하기 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (this.OrderBasket.Count == 0) return;
			
			AppCommon.Log("ORDER - OK");

			Window popup = new PayWindow()
			{
				Owner = this,
				Width = this.Width,
				Height = this.Height,
				WindowStartupLocation = WindowStartupLocation.CenterOwner,
				WindowState = WindowState.Maximized,
				OrderBasket = this.OrderBasket,
			};
			popup.Closed += PayWindow_Closed;
			popup.ShowDialog();
		}

		/// <summary>
		/// 결제 윈도우 닫음 처리
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PayWindow_Closed(object sender, EventArgs e)
		{
			Window popup = sender as Window;
			if (popup.Tag != null)
			{
				this.Dispatcher.Invoke(() =>
				{
					this.OrderBasket.Clear();
					this.Close();
				});
			}
		}
	}
}
