﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// AUOSMS.KIOSK
	/// </summary>
	public partial class App : Application
	{
		/// <summary>
		/// 진입점 오버라이드
		/// </summary>
		/// <param name="e"></param>
		protected override void OnStartup(StartupEventArgs e)
		{
			this.Exit += App_Exit;
			this.DispatcherUnhandledException += App_DispatcherUnhandledException;

			Process[] nowProcesses = Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName);
			if (nowProcesses.Length == 1)
			{
				// DIR
				if (!Directory.Exists(AppConfig.LogDirPath)) Directory.CreateDirectory(AppConfig.LogDirPath);

				// LOG
				FileInfo logConfig = new FileInfo(Path.Combine(AppConfig.StartupPath, "LogConfig.xml"));
				log4net.Config.XmlConfigurator.Configure(logConfig);
				AppCommon.Log("APP START ==================================================");

				// INI
				bool iniHasError = true;
				FileInfo ini = new FileInfo(Path.Combine(AppConfig.StartupPath, "Preferences.ini"));
				if (ini.Exists)
				{
					using (StreamReader sr = new StreamReader(ini.FullName))
					{
						AppConfig.KioskID = sr.ReadLine().Replace("KIOSK_ID=", "");
						AppConfig.PrinterComPort = sr.ReadLine().Replace("PrinterComPort=", "");
						AppConfig.ReaderComPort = sr.ReadLine().Replace("ReaderComPort=", "");
						AppConfig.KisVanID = sr.ReadLine().Replace("KIS_VAN_ID=", "");
						AppConfig.RealPayYN = sr.ReadLine().Replace("REAL_PAY=", "");
					}
					iniHasError = (String.IsNullOrEmpty(AppConfig.KioskID)
						|| String.IsNullOrEmpty(AppConfig.PrinterComPort) || String.IsNullOrEmpty(AppConfig.ReaderComPort)
						|| String.IsNullOrEmpty(AppConfig.KisVanID) || String.IsNullOrEmpty(AppConfig.RealPayYN));
				}
				if (iniHasError)
				{
					AppCommon.Log("APP :: INI ERROR : {0},{1},{2}", AppConfig.KioskID, AppConfig.PrinterComPort, AppConfig.ReaderComPort);
					MessageBox.Show(KIOSK.Properties.Resources.App_NoINI, KIOSK.Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
					Application.Current.Shutdown(-1);
				}

				Application.Current.StartupUri = new Uri("/AUOSMS.KIOSK;component/MainWindow.xaml", UriKind.Relative);
				base.OnStartup(e);
			}
			else
			{
				AppCommon.Log("APP :: EXISTS PROCESS");
				MessageBox.Show(KIOSK.Properties.Resources.App_MultipleRun, KIOSK.Properties.Resources.Information, MessageBoxButton.OK, MessageBoxImage.Information);
				Application.Current.Shutdown(-1);
			}
		}

		/// <summary>
		/// 앱 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void App_Exit(object sender, ExitEventArgs e)
		{
			AppCommon.Log("APP EXIT");
		}

		/// <summary>
		/// 처리되지 않은 예외 처리
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			AppCommon.Log("Exception :: {0} ==>\r\n{1}", e.Exception.Message, e.Exception.StackTrace);
			MessageBox.Show(e.Exception.Message, KIOSK.Properties.Resources.Exception, MessageBoxButton.OK, MessageBoxImage.Error);
			e.Handled = true;
		}
	}
}
