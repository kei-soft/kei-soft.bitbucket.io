﻿using AUOSMS.KIOSK.Models;
using AxKisPosAgentLib;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Win = System.Windows.Forms;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 키오스크 메인 윈도우
	/// </summary>
	public partial class MainWindow : Window
	{
		/// <summary>
		/// 결제 에이전트
		/// </summary>
		private AxKisPosAgent PosAgent = new AxKisPosAgent();

		/// <summary>
		/// 생성자
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();

			this.Loaded += MainWindow_Loaded;
			this.Closed += MainWindow_Closed;
			this.StateChanged += MainWindow_StateChanged;
			this.PreviewKeyDown += MainWindow_PreviewKeyDown;

			Printer.DataReceived += Printer_DataReceived;

			this.PosAgent.OnApprovalEnd += PosAgent_OnApprovalEnd;
			this.KIS.Child = this.PosAgent;
			WindowsFormsHost.EnableWindowsFormsInterop();

			this.OrderButton.PreviewMouseUp += OrderButton_PreviewMouseUp;
			this.PickupButton.PreviewMouseUp += PickupButton_PreviewMouseUp;
		}

		/// <summary>
		/// 윈도우 로드
		/// : 장비 연결 상태 확인, 메뉴 정보 갱신
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			AppCommon.Log("MAIN - OPEN");

			#region DEV
			if (Win.Screen.AllScreens.Count() > 1)
			{
				this.WindowState = WindowState.Normal;
				Win.Screen screen = Win.Screen.AllScreens[1];
				this.Left = screen.WorkingArea.Left;
				this.Top = screen.WorkingArea.Top;
				this.WindowState = WindowState.Maximized;
				//Printer.PrintOrderNo("777");
			}
			#endregion

			this.OrderButton.IsEnabled = this.PickupButton.IsEnabled = false;
			this.CheckHW();

			if (AppCommon.IsOnline())
			{
				this.GetKioskInfo();
				this.UpdateMenu();
				this.SetMessage(String.Empty);
			}
			else
			{
				this.SetMessage(Properties.Resources.HW_NetworkNO);
			}
		}

		/// <summary>
		/// 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MainWindow_Closed(object sender, EventArgs e)
		{
			AppCommon.Log("MAIN - CLOSE");
		}

		/// <summary>
		/// 윈도우 상태 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MainWindow_StateChanged(object sender, EventArgs e)
		{
			this.WindowStyle = (this.WindowState == WindowState.Maximized) ? WindowStyle.None : WindowStyle.SingleBorderWindow;
		}

		/// <summary>
		/// 키보딩 처리
		/// : ESC키 입력시 윈도우 상태를 기본값으로 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MainWindow_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				this.WindowState = WindowState.Normal;
			}
		}

		/// <summary>
		/// 프린터 데이터 수신
		/// : 장비에 이상이 있는 경우에만 수신됨
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Printer_DataReceived(object sender, TxEventArgs e)
		{
			string message = e.Data + "\n" + Properties.Resources.App_Close;
			AppCommon.Log("PRINTER - ERROR :: {0}", message);
			MessageBox.Show(message, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
			this.Dispatcher.Invoke(() => { this.Close(); });
		}

		/// <summary>
		/// IC리더 응답 수신
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PosAgent_OnApprovalEnd(object sender, EventArgs e)
		{
			if (this.PosAgent.outRtn != 0)
			{
				AppCommon.Log("READER - ERROR :: CONNECTION FAIL");
				MessageBox.Show("IC Reader Connection Fail", Properties.Resources.Exception, MessageBoxButton.OK, MessageBoxImage.Error);
				this.Dispatcher.Invoke(() => { this.Close(); });
			}
			else if (this.PosAgent.outAgentCode != "0000")
			{
				AppCommon.Log("READER - ERROR :: #{0}", this.PosAgent.outAgentCode);
				MessageBox.Show($"IC Reader Error [{this.PosAgent.outAgentCode}]", Properties.Resources.Exception, MessageBoxButton.OK, MessageBoxImage.Error);
				this.Dispatcher.Invoke(() => { this.Close(); });
			}
			else
			{
				AppCommon.Log("READER - RECEIVED :: {0}", this.PosAgent.outAgentData);
			}
		}

		/// <summary>
		/// 메시지 표시
		/// </summary>
		/// <param name="message"></param>
		private void SetMessage(string message)
		{
			this.MessageTextBlock.Dispatcher.Invoke(() => { }, DispatcherPriority.ApplicationIdle);
			Thread.Sleep(500);
			this.MessageTextBlock.Text = message;
		}

		/// <summary>
		/// 장비 연결 상태 확인
		/// </summary>
		private void CheckHW()
		{
			AppCommon.Log("H/W CHECK - BEGIN");
			this.SetMessage(Properties.Resources.HW_Check);

			// Scanner
			while (!Scanner.IsConnected())
			{
				AppCommon.Log("SCANNER :: NO");
				if (MessageBox.Show(Properties.Resources.HW_ScannerNO, Properties.Resources.Error, MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.No)
				{
					this.Close();
					return;
				}
			}
			AppCommon.Log("SCANNER :: OK");
			this.SetMessage(Properties.Resources.HW_ScannerOK);

			// Printer
			while (!Printer.IsConnected())
			{
				AppCommon.Log("PRINTER :: NO");
				if (MessageBox.Show(Properties.Resources.HW_PrinterNO, Properties.Resources.Error, MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.No)
				{
					this.Close();
					return;
				}
			}
			AppCommon.Log("PRINTER :: OK");
			this.SetMessage(Properties.Resources.HW_PrinterOK);

			// Reader
			while (!Reader.IsConnected())
			{
				AppCommon.Log("READER :: NO");
				if (MessageBox.Show(Properties.Resources.HW_ReaderNO, Properties.Resources.Error, MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.No)
				{
					this.Close();
					return;
				}
			}
			AppCommon.Log("READER :: OK");
			this.SetMessage(Properties.Resources.HW_ReaderOK);

			AppCommon.Log("H/W CHECK - END");
		}

		/// <summary>
		/// 키오스크 관련 정보 확인
		/// </summary>
		private void GetKioskInfo()
		{
			try
			{
				List<MySqlParameter> parameters = new List<MySqlParameter>();
				parameters.Add(new MySqlParameter("P_KIOSK_ID", AppConfig.KioskID));

				using (DataTable dt = AppCommon.GetDataTable("USP_KIOSK_INFO_GET", parameters))
				{
					AppConfig.StoreID = (int)dt.Rows[0]["STORE_ID"];
					AppConfig.StoreName = dt.Rows[0]["STORE_NAME"].ToString();
				}
			}
			catch (Exception)
			{
				MessageBox.Show(KIOSK.Properties.Resources.App_NoKioskID, KIOSK.Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
				AppCommon.Log("KIOSK INFO :: STORE_ID={0}, STORE_NAME={1}", AppConfig.StoreID, AppConfig.StoreName);
			}
		}

		/// <summary>
		/// 메뉴 업데이트
		/// </summary>
		private void UpdateMenu()
		{
			AppCommon.Log("MENU UPDATE - BEGIN");
			this.SetMessage(Properties.Resources.HW_MenuUpdate);

			// ITEM_GROUP
			using (DataTable dt = AppCommon.GetDataTable("USP_ITEM_GROUP_LIST", null))
			{
				foreach (DataRow row in dt.Rows)
				{
					AppCommon.MenuGroups.Add((int)row["ITEM_GROUP_CODE"], row["ITEM_GROUP_NAME"].ToString());
				}
			}

			// STORE_ITEM
			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));
			using (DataTable dt = AppCommon.GetDataTable("USP_KIOSK_ITEM_LST", parameters))
			{
				foreach (DataRow row in dt.Rows)
				{
					string imageUri = String.Concat(AppConfig.ServerImageUrl, row["ITEM_IMAGE"].ToString());

					BitmapImage bitmap = new BitmapImage();
					bitmap.BeginInit();
					bitmap.UriSource = new Uri(imageUri);
					bitmap.EndInit();

					Image image = new Image();
					image.Source = bitmap;

					ItemMenu item = new ItemMenu
					{
						GROUP = (int)row["ITEM_GROUP_CODE"],
						ID = (int)row["ITEM_ID"],
						NAME = row["ITEM_NAME"].ToString(),
						ENG_NAME = row["ITEM_ENG_NAME"].ToString(),
						DESC = row["ITEM_DESC"].ToString(),
						IMAGE = image,
						PRICE = (int)row["ITEM_PRICE"]
					};

					AppCommon.MenuItems.Add(item);
				}
			}

			using (DataSet ds = AppCommon.GetDataSet("USP_KIOSK_OPT_LST", null))
			{
				// ITEM_OPTION
				foreach (DataRow row in ds.Tables[0].Rows)
				{
					ItemMenuOption option = new ItemMenuOption
					{
						CODE = (int)row["OPTION_CODE"],
						NAME = row["OPTION_NAME"].ToString(),
						REQUIRED = row["ESSENTIAL"].ToString().Equals("Y")
					};

					int id = (int)row["ITEM_ID"];
					ItemMenu item = AppCommon.MenuItems.FirstOrDefault(x => x.ID == id);
					if (item != null) item.OPTION.Add(option);
				}

				// OPTION_DETAIL
				foreach (DataRow row in ds.Tables[1].Rows)
				{
					ItemMenuOptionDetail option = new ItemMenuOptionDetail
					{
						CODE = (int)row["OPTION_CODE"],
						NO = (int)row["OPTION_DETAIL_NO"],
						NAME = row["OPTION_DETAIL_NAME"].ToString(),
						PRICE = (int)row["EXTRA_COST"]
					};

					AppCommon.MenuOptions.Add(option);
				}
			}

			AppCommon.Log("MENU UPDATE - END");
			this.OrderButton.IsEnabled = this.PickupButton.IsEnabled = true;
		}

		/// <summary>
		/// 주문 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OrderButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			Window popup = new OrderWindow
			{
				Owner = this,
				Width = this.Width,
				Height = this.Height,
				WindowStartupLocation = WindowStartupLocation.CenterOwner,
				WindowState = WindowState.Maximized
			};
			popup.ShowDialog();
		}

		/// <summary>
		/// 픽업 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PickupButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			Window popup = new PickupWindow
			{
				Owner = this,
				Width = this.Width,
				Height = this.Height,
				WindowStartupLocation = WindowStartupLocation.CenterOwner,
				WindowState = WindowState.Maximized
			};
			popup.ShowDialog();
		}
	}
}
