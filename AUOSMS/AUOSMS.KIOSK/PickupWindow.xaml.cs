﻿using AUOSMS.KIOSK.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 키오스크 주문 찾기 윈도우
	/// </summary>
	public partial class PickupWindow : Window
	{
		#region Fields

		/// <summary>
		/// 주문 ID
		/// </summary>
		private string OrderID = String.Empty;

		/// <summary>
		/// 스캐너 인식 값
		/// </summary>
		private string QRValue = String.Empty;

		/// <summary>
		/// 입력키 변환기
		/// </summary>
		private KeyConverter Converter = new KeyConverter();

		/// <summary>
		/// 주문번호 인식 완료 여부
		/// </summary>
		private bool ReadCompleted = false;

		#endregion
		
		/// <summary>
		/// 생성자
		/// </summary>
		public PickupWindow()
		{
			InitializeComponent();

			this.Loaded += PickupWindow_Loaded;
			this.Closed += PickupWindow_Closed;
			this.PreviewKeyUp += PickupWindow_PreviewKeyUp;

			this.OKButton.Click += OKButton_Click;
			this.CancelButton.Click += CancelButton_Click;
			this.CloseButton.PreviewMouseUp += CloseButton_PreviewMouseUp;
		}

		/// <summary>
		/// 윈도우 로드
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PickupWindow_Loaded(object sender, RoutedEventArgs e)
		{
			#region DEV
			//this.QRValue = "d6632d85-ecde-4475-b388-c5ed48e55d42";
			#endregion

			AppCommon.Log("PICKUP - OPEN");
		}

		/// <summary>
		/// 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PickupWindow_Closed(object sender, EventArgs e)
		{
			AppCommon.Log("PICKUP - CLOSE");
		}

		/// <summary>
		/// 리더기에서 입력된 정보 수집
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PickupWindow_PreviewKeyUp(object sender, KeyEventArgs e)
		{
			if (this.ReadCompleted) return;
			this.MessageTextBlock.Text = String.Empty;
			this.PriceTextBlock.Text = String.Empty;
			this.InfoItemsControl.ItemsSource = null;

			if (e.Key == Key.Return)
			{
				AppCommon.Log("PICKUP - QRValue :: {0}", this.QRValue);

				this.OrderID = this.QRValue;
				this.CheckOrder();
				this.QRValue = String.Empty;

				AppCommon.Log("PICKUP - OrderID :: {0}", this.OrderID);
			}
			else if (e.Key == Key.Subtract)
			{
				this.QRValue += "-";
			}
			else
			{
				this.QRValue += Converter.ConvertToString(e.Key);
			}
		}

		/// <summary>
		/// 주문(결제) 정보 확인 및 표시
		/// </summary>
		private void CheckOrder()
		{
			string message = String.Empty;
			Dictionary<string, string> messageInfo = new Dictionary<string, string>();

			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_ORDER_ID", this.OrderID ));
			parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));

			using (DataSet ds = AppCommon.GetDataSet("USP_KIOSK_ORDER_INFO_GET", parameters))
			{
				// ORDER	   : ORDER_DATE, ORDER_STATUS, PRICE
				// ORDER_ITEM  : ITEM_ID, QUANTITY, PRICE, ITEM_OPTION
				// STORE_ORDER : ORDER_DATE, ORDER_TIME, ORDER_NO, MADE, SERVED

				if (ds.Tables[0].Rows.Count == 0)
				{
					AppCommon.Log("PICKUP - ORDER :: NO COUNT");
					message = Properties.Resources.Pickup_NoOrder;
				}
				else
				{
					string orderStatus = ds.Tables[0].Rows[0]["ORDER_STATUS"].ToString();

					// 결제 완료 후 접수 완료 OR 제조 중 OR 제조 완료 : 주문번호 프린팅 가능 (중복 프린팅 가능)
					if (orderStatus == ServiceStatus.WAITING.ToString() || orderStatus == ServiceStatus.MAKING.ToString() || orderStatus == ServiceStatus.MADE.ToString())
					{
						AppCommon.Log("PICKUP - ORDER :: PAID & WAITING or MAKING or MADE");
						message = Properties.Resources.Pickup_Order;

						messageInfo.Clear();
						int no = 0;
						foreach (DataRow row in ds.Tables[1].Rows)
						{
							string info = String.Empty;
							string desc = String.Empty;
							no++;

							int id = (int)row["ITEM_ID"];
							ItemMenu item = AppCommon.MenuItems.First(x => x.ID == id);
							info = $"{no}. {item.NAME} x {ds.Tables[1].Rows[0]["QUANTITY"]}";

							string jsonString = row["ITEM_OPTION"].ToString();
							desc = AppCommon.ConvertJsonToText(jsonString);

							messageInfo.Add(info, desc);
						}

						string price = $"{Properties.Resources.PriceTotal} : {ds.Tables[0].Rows[0]["PRICE"]:#,#}{Properties.Resources.MoneyUnit}\n";
						this.PriceTextBlock.Text = price;

						this.ReadCompleted = true;
						this.OKButton.Visibility = Visibility.Visible;
						this.CancelButton.Visibility = Visibility.Collapsed;

						// NOTE : PAID 상태로 남아 있는 것은 로직 진행 중단과 동일한 상태로 봐야함 : 코드 사용하지 않음
						//if (orderStatus == ServiceStatus.PAID.ToString())
						//{
						//	this.SetWaiting(ServiceStatus.PAID);
						//}

						Printer.PrintOrderNo(this.GetOrderNo());
					}
					// 픽업 완료 : 주문번호 화면 확인
					else if (orderStatus == ServiceStatus.SERVED.ToString())
					{
						AppCommon.Log("PICKUP - ORDER :: SERVED");
						message = Properties.Resources.Pickup_Used;

						string info = String.Empty;
						info += $"{Properties.Resources.OrderNo} : {ds.Tables[2].Rows[0]["ORDER_NO"]}\n";
						info += $"{Properties.Resources.OrderDate} : {ds.Tables[2].Rows[0]["ORDER_DATE"]}\n";
						info += $"{Properties.Resources.OrderTime} : {ds.Tables[2].Rows[0]["ORDER_TIME"]}\n";
						messageInfo.Clear();
						messageInfo.Add(info, "");

						this.OKButton.Visibility = Visibility.Visible;
						this.CancelButton.Visibility = Visibility.Collapsed;
					}
					else
					{
						AppCommon.Log("PICKUP - ORDER :: OrderID={0}, OrderStatus={1}", this.OrderID, orderStatus);
						message = Properties.Resources.Pickup_NoOrder;
					}
				}
			}

			this.MessageTextBlock.Text = message;
			this.InfoItemsControl.ItemsSource = messageInfo;
		}

		/// <summary>
		/// 결제완료 상태에 있는 경우 주문접수 상태로 변경
		/// </summary>
		/// <param name="status"></param>
		private void SetWaiting(ServiceStatus status)
		{
			if (status != ServiceStatus.PAID) return;
			if (String.IsNullOrEmpty(this.OrderID)) return;

			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));
			parameters.Add(new MySqlParameter("P_ORDER_ID", this.OrderID ));

			int orderNo = Convert.ToInt32(AppCommon.GetDataScalar("USP_KIOSK_WAITING_INS", parameters));

			if (orderNo > 100)
			{
				AppCommon.Log("PICKUP - OrderNo :: {0}", orderNo);
			}
			else
			{
				// IFNEED : Error Handle
				AppCommon.Log("PICKUP - OrderNo :: {0} :: ERROR", orderNo);
			}
		}

		/// <summary>
		/// 주문번호 확인
		/// </summary>
		/// <returns></returns>
		private string GetOrderNo()
		{
			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));
			parameters.Add(new MySqlParameter("P_ORDER_ID", this.OrderID));

			return AppCommon.GetDataScalar("USP_KIOSK_STORE_ORDER_GET", parameters).ToString();
		}

		/// <summary>
		/// 확인 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OKButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// 취소 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// 닫기 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CloseButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			this.Close();
		}
	}
}
