﻿using AUOSMS.KIOSK.Models;
using AxKisPosAgentLib;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms.Integration;
using System.Windows.Input;

namespace AUOSMS.KIOSK
{
	/// <summary>
	/// 키오스크 주문 결제 윈도우
	/// </summary>
	public partial class PayWindow : Window
	{
		#region Fields

		/// <summary>
		/// 선택된 아이템 목록 : 장바구니
		/// </summary>
		public ObservableCollection<OrderItem> OrderBasket = new ObservableCollection<OrderItem>();

		/// <summary>
		/// 결제 에이전트
		/// </summary>
		private AxKisPosAgent PosAgent = new AxKisPosAgent();

		/// <summary>
		/// 결제 데이터
		/// </summary>
		private PayData PayData = new PayData();

		/// <summary>
		/// 결제 송수신 전문
		/// </summary>
		private KisSpec KisSpec = new KisSpec();

		/// <summary>
		/// 결제 총액
		/// </summary>
		private int TotalPrice = 0;

		/// <summary>
		/// 부가세액
		/// </summary>
		private int VatPrice = 0;

		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		public PayWindow()
		{
			InitializeComponent();

			this.Loaded += PayWindow_Loaded;
			this.Closed += PayWindow_Closed;
			this.CloseButton.PreviewMouseUp += CloseButton_PreviewMouseUp;

			this.PosAgent.OnApprovalEnd += PosAgent_OnApprovalEnd;
			this.KIS.Child = this.PosAgent;
			WindowsFormsHost.EnableWindowsFormsInterop();

			this.PayButton.Click += PayButton_Click;
			this.OKButton1.Click += OKButton_Click;
			this.OKButton2.Click += OKButton_Click;
		}

		/// <summary>
		/// 윈도우 로드
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PayWindow_Loaded(object sender, RoutedEventArgs e)
		{
			AppCommon.Log("PAY - OPEN");
			this.StartInfoPanel.Visibility = Visibility.Visible;

			this.TotalPrice = this.OrderBasket.Sum(x => x.PRICE);
			this.VatPrice = this.TotalPrice / 10;

			#region DEV
			if (AppConfig.RealPayYN == "N")
			{
				AppConfig.PayServerIP = "210.112.100.97";
			}
			else
			{
				//// TEST PRICE
				//this.TotalPrice = 1004;
				//this.VatPrice = 91;
			}
			#endregion
		}

		/// <summary>
		/// 결제 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PayButton_Click(object sender, RoutedEventArgs e)
		{
			this.StartInfoPanel.Visibility = Visibility.Collapsed;
			this.WorkingInfoPanel.Visibility = Visibility.Visible;
			this.FinishInfoPanel.Visibility = Visibility.Collapsed;

			this.PayToKIS_Init();
		}

		/// <summary>
		/// IC리더 응답 수신
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PosAgent_OnApprovalEnd(object sender, EventArgs e)
		{
			if (this.PosAgent.outRtn != 0)
			{
				AppCommon.Log("PAY - READER ERROR :: CONNECTION FAIL");
				MessageBox.Show("IC Reader Connection Fail", Properties.Resources.Exception, MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else if (this.PosAgent.outAgentCode != "0000")
			{
				AppCommon.Log("PAY - READER ERROR :: #{0}", this.PosAgent.outAgentCode);
				MessageBox.Show($"IC Reader Error [{this.PosAgent.outAgentCode}]", Properties.Resources.Exception, MessageBoxButton.OK, MessageBoxImage.Error);
			}
			else
			{
				AppCommon.Log("PAY - READER RECEIVED :: {0}", this.PosAgent.outAgentData.Trim());

				switch (this.PayData.Step)
				{
					case 0:
						this.PayToKIS_Work();
						break;
					case 3:
						this.PayToKIS_Done();
						break;
				}
			}
		}

		/// <summary>
		/// 결제 요청 초기화
		/// </summary>
		private void PayToKIS_Init()
		{
			AppCommon.Log("PAY - KIS :: INIT");

			this.PayData.Init();

			this.PayData.Step = 0;
			this.PayData.inWCC = "C";
			this.PayData.inTradeType = "D1";	// 신용승인
			this.PayData.inInstallment = "00";  // 할부개월
			this.PayData.inTranAmt = this.TotalPrice.ToString();	// 결제금액 : 테스트=1004,50001
			this.PayData.inVatAmt = this.VatPrice.ToString();		// 부가세액 : 테스트=91
			this.PayData.inSvcAmt = "0";		// 봉사료
			this.PayData.inOrgAuthDate = "";    // 취소시 원거래일자 : YYMMDD
			this.PayData.inOrgAuthNo = "";      // 취소시 원승인번호

			this.PosAgent.Init();
			this.PosAgent.inAgentIP = "127.0.0.1";
			this.PosAgent.inAgentPort = 1515;
			this.PosAgent.inUnitTimeOut = "10"; // 응답대기시간
			this.PosAgent.inTranAmt = this.PayData.inTranAmt; // 결제금액
			this.PosAgent.inTranCode = "IC"; // 거래코드
			this.PosAgent.KIS_Approval_Unit();
		}

		/// <summary>
		/// 결제 요청
		/// </summary>
		private void PayToKIS_Work()
		{
			AppCommon.Log("PAY - KIS :: WORK");

			if (this.PosAgent.outAgentData.Length > 5)
			{
				this.PayData.outReaderData = this.PosAgent.outAgentData.Substring(0, 6);
				this.PayData.Step = 3;

				this.PosAgent.Init();
				this.PosAgent.inAgentIP = "127.0.0.1";
				this.PosAgent.inAgentPort = 1515;
				this.PosAgent.inAddressNo1 = AppConfig.PayServerIP;
				this.PosAgent.inAddressNo2 = AppConfig.PayServerPort;
				this.PosAgent.inTranCode = "NV"; // 거래코드

				this.KisSpec.Init();
				this.KisSpec.Installment = this.PayData.inInstallment;
				this.KisSpec.TotAmt = this.PayData.inTranAmt;
				this.KisSpec.VatAmt = this.PayData.inVatAmt;
				this.KisSpec.SvcAmt = this.PayData.inSvcAmt;
				this.KisSpec.TranCode = this.PayData.inTradeType;

				this.KisSpec.CatID = AppConfig.KisVanID;
				this.KisSpec.CardNo = this.PayData.inBarcodeNumber;
				this.KisSpec.OrgAuthDate = this.PayData.inOrgAuthDate;
				this.KisSpec.OrgAuthNo = this.PayData.inOrgAuthNo;
				this.KisSpec.WCC = this.PayData.inWCC;

				if (Convert.ToInt32(this.PayData.inTranAmt) > 50000)
				{
					this.PosAgent.inSignYN = "Y";
				}

				this.PosAgent.inAgentData = this.KisSpec.MakeReqSpec();
				this.PosAgent.KIS_Approval_Unit();

				AppCommon.Log("PAY - KIS :: WORK-OK");
			}
			else
			{
				if (this.PosAgent.outAgentCode == "0000")
				{
					this.ShowError(this.PosAgent.outAgentCode, Properties.Resources.Pay_CardEmpty);
				}
				else
				{
					this.ShowError(this.PosAgent.outAgentCode, Reader.ErrorCode[this.PosAgent.outAgentCode]);
					AppCommon.Log("PAY - KIS :: WORK-FAIL");
				}
			}
		}

		/// <summary>
		/// 결제 요청 결과 처리
		/// </summary>
		private void PayToKIS_Done()
		{
			AppCommon.Log("PAY - KIS :: DONE");

			this.PayData.outAgentCode = this.PosAgent.outAgentCode;
			if (this.PayData.outAgentCode == "0000")
			{
				if (this.KisSpec.GetResSpec(this.PosAgent.outAgentData) != 0)
				{
					this.ShowError(this.PosAgent.outAgentCode, this.PosAgent.outAgentData);
					AppCommon.Log("PAY - KIS :: DONE-FAIL (KisSpec)");
					return;
				}

				this.PayData.outResCode = this.KisSpec.outReplyCode;
				this.PayData.outResMsg1 = this.KisSpec.outReplyMsg1;

				this.PayData.outWCC = this.PayData.inWCC;
				this.PayData.outInstallment = this.PayData.inInstallment;
				this.PayData.outTranAmt = this.PayData.inTranAmt;
				this.PayData.outVatAmt = this.PayData.inVatAmt;
				this.PayData.outSvcAmt = this.PayData.inSvcAmt;
				this.PayData.outJanAmt = this.KisSpec.outJanAmt;
				this.PayData.outAuthNo = this.KisSpec.outAuthNo;
				this.PayData.outReplyDate = this.KisSpec.outReplyDate;
				this.PayData.outAccepterCode = this.KisSpec.outAccepterCode;
				this.PayData.outAccepterName = this.KisSpec.outAccepterName;
				this.PayData.outIssuerCode = this.KisSpec.outIssuerCode;
				this.PayData.outIssuerName = this.KisSpec.outIssuerName;
				this.PayData.outTranNo = this.KisSpec.outTranNo;
				this.PayData.outMerchantRegNo = this.KisSpec.outMerchantRegNo;
				this.PayData.outTradeReqDate = this.KisSpec.outTradeReqDate;
				this.PayData.outTradeReqTime = this.KisSpec.outTradeReqTime;

				if (this.PayData.Step < 2)
				{
					this.ShowError(this.PayData.outAgentCode, this.PayData.outReaderData);
					AppCommon.Log("PAY - KIS :: DONE-FAIL (outAgentCode)");
				}

				if (this.PayData.outResCode.Equals("0000"))
				{
					AppCommon.Log("PAY - KIS :: DONE-OK");
					this.PayToDB();
				}
				else
				{
					//this.ShowError(this.PayData.outResCode, Reader.ErrorCode[this.PayData.outResCode]);
					this.ShowError(this.PayData.outResCode, $"{this.PayData.outResMsg1.Trim()}\n{this.PayData.outResMsg2.Trim()}");
					AppCommon.Log("PAY - KIS :: DONE-FAIL (outResCode)");
				}
			}
			else
			{
				this.ShowError(this.PosAgent.outAgentCode, Reader.ErrorCode[this.PosAgent.outAgentCode]);
				AppCommon.Log("PAY - KIS :: DONE-FAIL");
			}
		}

		/// <summary>
		/// 결제 정보 DB 입력
		/// </summary>
		private void PayToDB()
		{
			AppCommon.Log("PAY - DB :: START");

			int result1 = 0;
			int result2 = 0;
			int orderNo = 0;
			Guid orderID = Guid.NewGuid();

			// 1. 주문 및 결제 정보
			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_ORDER_ID", orderID));
			parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));
			parameters.Add(new MySqlParameter("P_PRICE", this.TotalPrice));
			parameters.Add(new MySqlParameter("P_PAY_CODE", Guid.NewGuid()));

			result1 = Convert.ToInt32(AppCommon.GetDataScalar("USP_KIOSK_ORDER_PAY_INS", parameters));
			if (result1 == 1)
			{
				// 2. 주문 상세 정보
				int itemsCount = 0;
				foreach (var item in this.OrderBasket)
				{
					parameters.Clear();
					parameters.Add(new MySqlParameter("P_ORDER_ID", orderID));
					parameters.Add(new MySqlParameter("P_ITEM_ID", item.ITEM_ID));
					parameters.Add(new MySqlParameter("P_QUANTITY", item.COUNT));
					parameters.Add(new MySqlParameter("P_PRICE", item.PRICE));
					parameters.Add(new MySqlParameter("P_ITEM_OPTION", item.OPTION_JSON));

					result2 = Convert.ToInt32(AppCommon.GetDataScalar("USP_KIOSK_ORDER_ITEM_INS", parameters));
					if (result2 == 1) itemsCount++;
				}

				if (itemsCount == this.OrderBasket.Count)
				{
					// 3. 매장에 주문 할당
					parameters.Clear();
					parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));
					parameters.Add(new MySqlParameter("P_ORDER_ID", orderID));

					orderNo = Convert.ToInt32(AppCommon.GetDataScalar("USP_KIOSK_WAITING_INS", parameters));
					if (orderNo > 100)
					{
						AppCommon.Log("PAY - DB :: OK :: {0}({1}) = {2}", orderNo, orderID, this.TotalPrice);
					}
				}
				else
				{
					result2 = 0;
				}
			}

			if (result1 != 1 || result2 != 1 || orderNo < 100)
			{
				// IFNEED : Rollback Scenario
				AppCommon.Log("PAY - DB :: FAIL :: {0}{1}-{2}", result1, result2, orderNo);
				this.ShowError($"DB({result1}{result2}-{orderNo})", $"PAID={this.TotalPrice:#,#}");
			}
			else
			{
				this.StartInfoPanel.Visibility = Visibility.Collapsed;
				this.WorkingInfoPanel.Visibility = Visibility.Collapsed;
				this.FinishInfoPanel.Visibility = Visibility.Visible;
				this.FinishFailPanel.Visibility = Visibility.Collapsed;
				this.FinishOKPanel.Visibility = Visibility.Visible;

				this.PrintReceipt(orderNo);

				Task.Delay(3000).ContinueWith(x =>
				{
					this.Dispatcher.Invoke(() =>
					{
						this.OrderBasket.Clear();
						this.Tag = orderID;
						this.Close();
					});
				});
			}
		}

		/// <summary>
		/// 주문번호 및 영수증 출력
		/// </summary>
		/// <param name="orderNo"></param>
		private void PrintReceipt(int orderNo)
		{
			string line = "-----------------------------------------------\n";

			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));

			string storeInfo = String.Empty;
			using (DataTable dt = AppCommon.GetDataTable("USP_STORE_GET", parameters))
			{
				storeInfo += $"{Properties.Resources.StoreName} : {dt.Rows[0]["STORE_NAME"]}\n";
				storeInfo += $"{Properties.Resources.StoreAddress} : {dt.Rows[0]["STATE"]} {dt.Rows[0]["CITY"]} {dt.Rows[0]["ADDRESS"]} {dt.Rows[0]["ADDRESS_DETAIL"]}";
			}

			string orderNoInfo = $"\n[{Properties.Resources.OrderNo} : {orderNo}]";

			string orderItemInfo = line;
			foreach (var item in this.OrderBasket)
			{
				orderItemInfo += $"{Properties.Resources.ItemName} : {item.ITEM_NAME}\n";
				orderItemInfo += $"{Properties.Resources.ItemCount} : {item.COUNT}\n";
				orderItemInfo += $"{Properties.Resources.ItemOption} : {AppCommon.ConvertJsonToText(item.OPTION_JSON)}\n";
				orderItemInfo += $"{Properties.Resources.PriceEach} : {item.PRICE:#,#}{Properties.Resources.MoneyUnit}\n";
				orderItemInfo += $"{line}";
			}

			string supply = $"{this.TotalPrice - this.VatPrice:#,#}{Properties.Resources.MoneyUnit}";
			string vat = $"{this.VatPrice:#,#}{Properties.Resources.MoneyUnit}";
			string total = $"{this.TotalPrice:#,#}{Properties.Resources.MoneyUnit}";
			orderItemInfo += $"{Properties.Resources.PriceSupply} : {supply.PadLeft(10, ' ')}\n";
			orderItemInfo += $"{Properties.Resources.PriceVAT} : {vat.PadLeft(12, ' ')}\n";
			orderItemInfo += $"{Properties.Resources.PriceTotal} : {total.PadLeft(10, ' ')}\n";
			orderItemInfo += $"{line}{line}";

			string orderReceipt = String.Empty;
			orderReceipt += $"\n{line}";
			orderReceipt += $"{Properties.Resources.CardCompany} : {this.PayData.outIssuerName}\n";
			orderReceipt += $"{Properties.Resources.CardNo} : {this.PosAgent.outCardNo.PadRight(16, '*')}\n";
			orderReceipt += $"{Properties.Resources.CardOKDate} : {this.PayData.outReplyDate}\n";
			orderReceipt += $"{Properties.Resources.CardOKNo} : {this.PayData.outAuthNo}\n";
			orderReceipt += $"{Properties.Resources.CardOKPrice} : {this.PayData.outTranAmt:#,#}{Properties.Resources.MoneyUnit}\n";
			orderReceipt += $"{Properties.Resources.CardInstallment} : {this.PayData.outInstallment}\n";
			orderReceipt += $"{Properties.Resources.CardStoreNo} : {this.PayData.outMerchantRegNo}\n";
			orderReceipt += $"{Properties.Resources.CardVanID} : {this.PayData.outCatId}\n";
			orderReceipt += $"{line}";
			orderReceipt += $"{Properties.Resources.PrintDate} : {DateTime.Now:yyyy-MM-dd HH:mm:ss}\n\n";

			Printer.PrintReceipt(storeInfo, orderNoInfo, orderItemInfo, orderReceipt);
		}

		/// <summary>
		/// 에러 화면 표시
		/// </summary>
		/// <param name="errorCode"></param>
		/// <param name="errorMessage"></param>
		private void ShowError(string errorCode, string errorMessage)
		{
			AppCommon.Log("PAY - ERROR({0}) :: {1}", errorCode, errorMessage);

			this.Dispatcher.Invoke(() => {
				this.StartInfoPanel.Visibility = Visibility.Collapsed;
				this.WorkingInfoPanel.Visibility = Visibility.Collapsed;
				this.FinishInfoPanel.Visibility = Visibility.Visible;
				this.FinishOKPanel.Visibility = Visibility.Collapsed;
				this.FinishFailPanel.Visibility = Visibility.Visible;

				this.ErrorCodeTextBlock.Text = $"[ {errorCode} ]";
				this.ErrorMessageTextBlock.Text = errorMessage;
			});
		}

		/// <summary>
		/// 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void PayWindow_Closed(object sender, EventArgs e)
		{
			AppCommon.Log("PAY - CLOSE");
		}

		/// <summary>
		/// 확인 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OKButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// 닫기 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CloseButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			this.Close();
		}
	}
}
