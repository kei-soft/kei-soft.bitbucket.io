﻿using System;

namespace AUOSMS.MOBILE.NETCOREIMPORT
{
    /// <summary>
    /// 중복된 키 예외
    /// </summary>
    public class DuplicatedKeyException : Exception
    {
        public DuplicatedKeyException(string message) : base(message)
        {
        }
    }
}
