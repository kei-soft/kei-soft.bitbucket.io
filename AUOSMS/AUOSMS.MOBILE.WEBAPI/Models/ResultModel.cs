﻿namespace AUOSMS.MOBILE.WEBAPI.Models
{
    /// <summary>
    /// 결과 모델
    /// </summary>
    public class ResultModel
    {
        /// <summary>
        /// 결과 코드
        /// </summary>
        public string ResultCode { get; set; }

        /// <summary>
        /// 결과 메시지
        /// </summary>
        public string ResultMessage { get; set; }

        /// <summary>
        /// 결과 상세 메시지
        /// </summary>
        public string ResultDetailMessage { get; set; }
    }
}