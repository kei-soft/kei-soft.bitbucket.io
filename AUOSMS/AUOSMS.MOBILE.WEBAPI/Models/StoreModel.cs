﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class StoreModel
    {
        public int STORE_ID { get; set; }
        public string STORE_NAME { get; set; }
        public string STATE { get; set; }
        public string CITY { get; set; }
        public string ADDRESS { get; set; }
        public string ADDRESS_DETAIL { get; set; }
        public string ADDRESS_FULL { get; set; }
        public string PHONE { get; set; }
        public string STORE_IMAGE { get; set; }
        public string LOC_LATITUDE { get; set; }
        public string LOC_LONGITUDE { get; set; }
        public string STORE_DISTANCE { get; set; }
        public double STORE_DISTANCEKMETER { get; set; }
        public string ORDER_START_TIME { get; set; }
        public string ORDER_END_TIME { get; set; }
        /// <summary>
        /// 아임포트 가맹점 식별 번호
        /// </summary>
        public string IMPORT_ID { get; set; }
        /// <summary>
        /// 아임포트 가맹점식별코드
        /// </summary>
        public string IMPORT_STORE_ID { get; set; }
        /// <summary>
        /// 아임포트 REST APl 키
        /// </summary>
        public string IMPORT_RESTAPI_KEY { get; set; }
        /// <summary>
        /// 아임포트 REST API Secret
        /// </summary>
        public string IMPORT_RESTAPI_SECRET { get; set; }

        public string COUPON_USE_YN { get; set; }
        

    }
}