﻿using System;
using System.Collections.Generic;

namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class Order
    {
        public string ORDER_ID { get; set; }
        public string STORE_ID { get; set; }
        public string MEMBER_ID { get; set; }
        public DateTime? ARRIVE_DATE { get; set; }
        public int TOTAL_PRICE { get; set; }
        public List<OrderItem> ORDER_ITEM_LIST { get; set; }
        public string COUPON_ITEM_ID { get; set; }
        public string COUPON_NO { get; set; }
    }
}