﻿namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class OptionModel
    {
        public long OPTION_CODE { get; set; }

        public string OPTION_NAME { get; set; }

        public string ESSENTIAL { get; set; }

        public string OPTION_DETAIL { get; set; }
    }
}