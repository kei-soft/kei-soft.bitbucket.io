﻿namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class OrderItemOption
    {
        public string OPTION_NAME { get; set; }
        public string OPTION_DETAIL_NAME { get; set; }
        public string OPTION_DETAIL_PRICE { get; set; }
    }
}