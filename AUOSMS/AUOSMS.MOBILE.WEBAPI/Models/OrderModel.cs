﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class OrderModel
    {
        public string ORDER_ID { get; set; }
        public DateTime ORDER_DATE { get; set; }
        public long MEMBER_ID { get; set; }
        public long STORE_ID { get; set; }
        public long PRICE { get; set; }
        public double ORDER_COUNT { get; set; }
        public long COUPON_ITEM_ID { get; set; } = 0;

        public string COUPON_NO { get; set; }
        public long STAMP_CNT { get; set; } = 0; 
    }
}