﻿namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class SignUpInfo
    {
        public string MemberId { get; set; }
        public bool IsNew { get; set; }
        public string PhoneNumber { get; set; }
        public string NickName { get; set; }
    }
}