﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class QnaModel
    {
        public string TITLE { get; set; }
        public string CONTENTS { get; set; }
        public string MEMBER_ID { get; set; }
        public string STORE_ID { get; set; }
    }
}