﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class OrderItem
    {
        public string ORDER_ITEM_ID { get; set; }
        public string ITEM_ID { get; set; }
        public string ITEM_NAME { get; set; }
        public int ITEM_PRICE { get; set; }
        public int ITEM_COUNT { get; set; }
        public string ITEM_OPTION_FULL { get; set; }
        public int ITEM_OPTION_TOTAL_PRICE { get; set; } = 0;
        public int TOTAL_PRICE { get; set; }
    }
}