﻿namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class ItemOption
    {
        public long ITEM_ID { get; set; }

        public string ITEM_NAME { get; set; }

        public long OPTION_CODE { get; set; }

        public long OPTION_PRICE { get; set; }

        public string OPTION_NAME { get; set; }

        public string ESSENTIAL { get; set; }

        public string OPTION_DETAIL { get; set; }
    }
}