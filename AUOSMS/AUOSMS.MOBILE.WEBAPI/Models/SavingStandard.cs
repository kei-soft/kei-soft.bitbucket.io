﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class SavingStandard
    {
        public string STANDARD_TYPE { get; set; }
        public string STANDARD_CODE { get; set; }
        public string STANDARD_NAME { get; set; }
        public string STANDARD_VALUE { get; set; }
        public int STANDARD_SORT { get; set; }

    }
}