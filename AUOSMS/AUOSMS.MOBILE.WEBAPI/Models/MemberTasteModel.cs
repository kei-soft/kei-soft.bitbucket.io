﻿namespace AUOSMS.MOBILE.WEBAPI.Models
{
    public class MemberTasteModel
    {
        public string MEMBER_ID { get; set; }

        public string ITEM_GROUP { get; set; }

        public string COFFEE_SHOT { get; set; }

        public string SYRUP_TYPE { get; set; }

        public string SYRUP_DENSITY { get; set; }

        public string ICE { get; set; }

        public string CREAM { get; set; }

        public string MILK_HOT { get; set; }
    }
}