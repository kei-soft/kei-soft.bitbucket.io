﻿namespace AUOSMS.MOBILE.WEBAPI
{
    public class Constant
    {

    }

    public class OrderStatus
    {
        /// <summary>
        /// 결제완료
        /// </summary>
        public static string PAID = "PAID";

        /// <summary>
        /// 접수완료(제조중)
        /// </summary>
        public static string MAKING = "MAKING";

        /// <summary>
        /// 제조완료
        /// </summary>
        public static string MADE = "MADE";

        /// <summary>
        /// 픽업완료
        /// </summary>
        public static string SERVED = "SERVED";
    }


    public class StandardType
    {
        /// <summary>
        /// COUPON
        /// </summary>
        public static string COUPON = "COUPON";

        /// <summary>
        /// STAMP
        /// </summary>
        public static string STAMP = "STAMP";
    }

    public class StandardValue
    {
        /// <summary>
        /// QUNATITY (수량)
        /// </summary>
        public static string QUNATITY = "QUNATITY";

        /// <summary>
        /// PRICE (금액)
        /// </summary>
        public static string PRICE = "PRICE";
    }



    public class StandardCode
    {
        /// <summary>
        /// 스탬프 수량
        /// </summary>
        public static string STAMP_QUANTITY = "STAMP_QUANTITY";

        /// <summary>
        /// 사용기한(일)
        /// </summary>
        public static string USEABLE_DAYS = "USEABLE_DAYS";

        /// <summary>
        /// 스탬프 발생기준
        /// </summary>
        public static string SAVING_STANDARD = "SAVING_STANDARD";

        /// <summary>
        /// 발행기준값
        /// </summary>
        public static string STANDARD_VALUE = "STANDARD_VALUE";
    }
}