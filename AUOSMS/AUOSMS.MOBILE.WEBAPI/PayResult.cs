﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI
{
    public class PayResult
    {
        /// <summary>
        /// 상태 코드
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// 결과 메시지
        /// </summary>
        public string message { get; set; }
    }
}