﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

using AUOSMS.MOBILE.WEBAPI.Constants;
using AUOSMS.MOBILE.WEBAPI.Models;

using MySql.Data.MySqlClient;

using Newtonsoft.Json;

namespace AUOSMS.MOBILE.WEBAPI.Controllers
{
    public class HomeController : ApiController
    {
        #region Field

        /// <summary>
        /// DB 접속 정보
        /// </summary>
        //string connectionString = "server=211.57.201.122;user=auosms_user;database=AUOSMS;password=auosms20201!";
        // real : 211.115.107.41 , dev : 211.57.201.122
        string connectionString = "server=211.57.201.122;user=auosms2_user;database=AUOSMS2;password=auosms20201!";
        //string connectionString = "server=211.115.107.41;user=auosms2_user;database=AUOSMS2;password=auosms20201!";

        #endregion

        // RequestMessage ============================================================================================================================================================
        #region RequestMessage ★★★★★★

        /// <summary>
        /// 요청에 대한 응답을 처리합니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RequestMessage")]
        public dynamic RequestMessage([FromBody]dynamic value)
        {
            string jsonValue = JsonConvert.SerializeObject(value);

            RequestModel requestModel = JsonConvert.DeserializeObject<RequestModel>(jsonValue);

            string resultMessage = string.Empty;
            string resultDetailMessage = string.Empty;

            if (requestModel.RequestType == RequestType.SelectQuery)
            {
                #region SelectQuery - 추후제거 필요, 테스트 용도

                string sql = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);
                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(mySqlDataReader);
                        mySqlDataReader.Close();
                        mySqlDataReader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.CrudQuery)
            {
                #region CrudQuery - 추후제거 필요, 테스트 용도

                string sql = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                        int i = mySqlCommand.ExecuteNonQuery();
                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.Login)
            {
                #region Login
                // 휴대폰번호/비밀번호
                string[] loginInfos = requestModel.RequestMessage.Split('∀');

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $"SELECT * FROM member WHERE MEMBER_NAME = '" + loginInfos[0] + "' AND CERTIFICATION_NO = '" + loginInfos[1] + "'";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectMainImage)
            {
                #region SelectMainImage

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $"SELECT IMAGE_NAME,IMAGE_SORT FROM intro_image WHERE USE_YN = 'Y' AND STORE_ID = 1  AND IMAGE_NAME <> '' AND DELETE_DATE IS NULL ORDER BY IMAGE_SORT";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectItemOption)
            {
                #region SelectItemOption

                string itemId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();

                    // 종합쿼리 결과가 이상해 각개 쿼리로 변경함
                    string sql = $@"
SELECT 
       A.ITEM_ID 
      ,A.ITEM_NAME 
      ,B.OPTION_CODE
 FROM ITEM A, ITEM_OPTION B
WHERE A.ITEM_ID  = B.ITEM_ID
  AND A.ITEM_ID = {itemId}
  AND A.DELETE_DATE IS NULL 
ORDER BY OPTION_SORT";

                    MySqlCommand command = new MySqlCommand(sql, connection);
                    MySqlDataReader dataReader = command.ExecuteReader();

                    DataTable dt = new DataTable();
                    dt.Load(dataReader);
                    dataReader.Close();

                    List<ItemOption> itemOptionModels = Function.CreateListFromTable<ItemOption>(dt);

                    List<ItemOption> resultitemOptionModels = new List<ItemOption>();

                    foreach (ItemOption item in itemOptionModels)
                    {
                        sql = $@"
SELECT a.OPTION_CODE, a.OPTION_NAME, a.ESSENTIAL
  FROM option a
 WHERE a.OPTION_CODE = {item.OPTION_CODE}
   AND a.USE_YN ='Y'
   AND a.DELETE_DATE is null ";

                        command = new MySqlCommand(sql, connection);
                        dataReader = command.ExecuteReader();

                        dt = new DataTable();
                        dt.Load(dataReader);
                        dataReader.Close();

                        List<OptionModel> optionModels = Function.CreateListFromTable<OptionModel>(dt);

                        if (optionModels == null || optionModels.Count == 0) continue;

                        item.ESSENTIAL = optionModels[0].ESSENTIAL;
                        item.OPTION_NAME = optionModels[0].OPTION_NAME;

                        sql = $@"
SELECT GROUP_CONCAT(CONCAT(OPTION_DETAIL_NAME, '[', FORMAT(EXTRA_COST, 0), ']') ORDER BY OPTION_SORT SEPARATOR ',') OPTION_DETAIL
  FROM OPTION_DETAIL A
 WHERE A.OPTION_CODE = {item.OPTION_CODE} ";

                        command = new MySqlCommand(sql, connection);

                        item.OPTION_DETAIL = command.ExecuteScalar()?.ToString();

                        resultitemOptionModels.Add(item);
                    }

                    resultMessage = JsonConvert.SerializeObject(resultitemOptionModels);
                }

                #region Call Procedure - 이상함
                /* - 
                string procedure = $@"USP_M_ITEM_OPTION_LIST";

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        MySqlCommand mySqlCommand = new MySqlCommand(procedure, connection);
                        mySqlCommand.CommandType = CommandType.StoredProcedure;
                        mySqlCommand.Parameters.Add(new MySqlParameter("P_ITEM_ID", int.Parse(itemid)));

                        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(mySqlDataReader);
                        mySqlDataReader.Close();
                        mySqlDataReader.Dispose();
                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
                */
                #endregion

                #endregion
            }
            else if (requestModel.RequestType == RequestType.OrderItem)
            {
                #region OrderItem

                int i = -1;

                string jsonData = requestModel.RequestMessage;

                Order order = JsonConvert.DeserializeObject<Order>(jsonData);

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string memberID = order.MEMBER_ID;
                        string payCode = Guid.NewGuid().ToString();
                        string payStatus = "ORDER";

                        #region Order 테이블 Insert

                        DateTime nowDateTime = DateTime.Now;

                        string orderSql = $@"
INSERT INTO `order` (`ORDER_ID`, `ORDER_DATE`, `STORE_ID`, `MEMBER_ID`, `PRICE`, `COUPON_ITEM_ID`,  `PAY_CODE`, `ARRIVE_DATE`, `ORDER_STATUS`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`, `TERMINAL`, `COUPON_NO`, `COUPON_PRICE`, `REAL_PRICE`) VALUES
                    (@ORDER_ID,  @ORDER_DATE,  @STORE_ID,  @MEMBER_ID,  @PRICE,  @COUPON_ITEM_ID,   @PAY_CODE,  @ARRIVE_DATE,  @ORDER_STATUS,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER, @TERMINAL , @COUPON_NO, @COUPON_PRICE, @REAL_PRICE) ";

                        MySqlCommand orderCommand = new MySqlCommand(orderSql, connection);

                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", order.ORDER_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_DATE", nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@STORE_ID", order.STORE_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@PRICE", order.TOTAL_PRICE));
                        orderCommand.Parameters.Add(new MySqlParameter("@COUPON_ITEM_ID", order.COUPON_ITEM_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                        orderCommand.Parameters.Add(new MySqlParameter("@ARRIVE_DATE", order.ARRIVE_DATE));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", payStatus));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@TERMINAL", "APP"));

                        // COUPON_NO 는 있는데 order.COUPON_ITEM_ID 가 없는 경우는 할인률 쿠폰이 적용된 것임 추가 처리 필요

                        orderCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", order.COUPON_NO));

                        int realPrice = order.ORDER_ITEM_LIST.Sum(c => c.TOTAL_PRICE);
                        int couponPrice = realPrice - order.TOTAL_PRICE;

                        orderCommand.Parameters.Add(new MySqlParameter("@COUPON_PRICE", couponPrice));
                        orderCommand.Parameters.Add(new MySqlParameter("@REAL_PRICE", realPrice));

                        i += orderCommand.ExecuteNonQuery();

                        #endregion

                        int orderItemCount = 0;

                        // 쿠폰 항목 적용되었는지 여부
                        bool completeCoupon = false;

                        if (order.ORDER_ITEM_LIST != null)
                        {
                            foreach (OrderItem item in order.ORDER_ITEM_LIST)
                            {
                                #region Order_Item 테이블 Insert

                                string orderitemSql = @"
INSERT INTO order_item (ORDER_ID,   ORDER_ITEM_ID,  ITEM_ID,  QUANTITY,  PRICE,  ITEM_OPTION,  COUPON_NO,   COUPON_PRICE,   REAL_PRICE) 
       VALUES	       (@ORDER_ID, @ORDER_ITEM_ID, @ITEM_ID, @QUANTITY, @PRICE, @ITEM_OPTION, @COUPON_NO , @COUPON_PRICE,  @REAL_PRICE) ";

                                MySqlCommand orderItemCommand = new MySqlCommand(orderitemSql, connection);

                                orderItemCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", order.ORDER_ID));
                                orderItemCommand.Parameters.Add(new MySqlParameter("@ORDER_ITEM_ID", item.ORDER_ITEM_ID));
                                orderItemCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", item.ITEM_ID));
                                orderItemCommand.Parameters.Add(new MySqlParameter("@QUANTITY", item.ITEM_COUNT));
                                
                                if (!string.IsNullOrEmpty(order.COUPON_ITEM_ID) && !completeCoupon)
                                {
                                    if (item.ITEM_ID == order.COUPON_ITEM_ID)
                                    {
                                        orderItemCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", order.COUPON_NO));
                                        orderItemCommand.Parameters.Add(new MySqlParameter("@COUPON_PRICE", couponPrice));

                                        int orderItemPrice = item.TOTAL_PRICE - couponPrice;
                                        //item.TOTAL_PRICE;
                                        //item.ITEM_PRICE

                                        orderItemCommand.Parameters.Add(new MySqlParameter("@PRICE", orderItemPrice));

                                        // 같은 메뉴 2잔인 경우 하나만 적용하기 위함
                                        completeCoupon = true;
                                    }
                                    else
                                    {
                                        orderItemCommand.Parameters.Add(new MySqlParameter("@PRICE", item.TOTAL_PRICE));
                                    }
                                }
                                else
                                {
                                    orderItemCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", DBNull.Value));
                                    orderItemCommand.Parameters.Add(new MySqlParameter("@COUPON_PRICE", 0));
                                    orderItemCommand.Parameters.Add(new MySqlParameter("@PRICE", item.TOTAL_PRICE));
                                }

                                orderItemCommand.Parameters.Add(new MySqlParameter("@REAL_PRICE", item.TOTAL_PRICE));

                                orderItemCount += item.ITEM_COUNT;

                                // 옵션그룹:옵션디테일넘버(옵션가격)|옵션그룹:옵션디테일넘버(옵션가격)
                                string itemOption = "";

                                if (!string.IsNullOrEmpty(item.ITEM_OPTION_FULL))
                                {
                                    List<OrderItemOption> orderItemOptions = new List<OrderItemOption>();
                                    foreach (string optionData in item.ITEM_OPTION_FULL.Split('|'))
                                    {
                                        OrderItemOption orderItemOption = new OrderItemOption();

                                        string[] options = optionData.Split(':');

                                        orderItemOption.OPTION_NAME = options[0];

                                        if (options.Length > 1)
                                        {
                                            // 가격분류
                                            string[] optionDetailPrice = options[1].Split('[');

                                            orderItemOption.OPTION_DETAIL_NAME = optionDetailPrice[0];
                                            orderItemOption.OPTION_DETAIL_PRICE = optionDetailPrice.Length == 1 ? "" : optionDetailPrice[1].TrimEnd(']');
                                        }

                                        orderItemOptions.Add(orderItemOption);
                                    }

                                    itemOption = JsonConvert.SerializeObject(orderItemOptions);
                                }

                                orderItemCommand.Parameters.Add(new MySqlParameter("@ITEM_OPTION", itemOption));

                                i += orderItemCommand.ExecuteNonQuery();

                                #endregion
                            }
                        }

                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.OrderComplete)
            {
                #region OrderComplete

                string errorMessage = "";
                string returnstatus = "PAID";

                int i = -1;

                string jsonData = requestModel.RequestMessage;

                Order order = JsonConvert.DeserializeObject<Order>(jsonData);

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string memberID = order.MEMBER_ID;
                        string payCode = Guid.NewGuid().ToString();
                        string payStatus = "ORDER";

                        #region Order 테이블 Insert

                        DateTime nowDateTime = DateTime.Now;

                        string orderSql = $@"
INSERT INTO `order` (`ORDER_ID`, `ORDER_DATE`, `STORE_ID`, `MEMBER_ID`, `PRICE`, `COUPON_ITEM_ID`,  `PAY_CODE`, `ARRIVE_DATE`, `ORDER_STATUS`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`, `TERMINAL`, `COUPON_NO`, `COUPON_PRICE`, `REAL_PRICE`) VALUES
                    (@ORDER_ID,  @ORDER_DATE,  @STORE_ID,  @MEMBER_ID,  @PRICE,  @COUPON_ITEM_ID,   @PAY_CODE,  @ARRIVE_DATE,  @ORDER_STATUS,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER, @TERMINAL , @COUPON_NO, @COUPON_PRICE, @REAL_PRICE) ";

                        MySqlCommand orderCommand = new MySqlCommand(orderSql, connection);

                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", order.ORDER_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_DATE", nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@STORE_ID", order.STORE_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@PRICE", order.TOTAL_PRICE));
                        orderCommand.Parameters.Add(new MySqlParameter("@COUPON_ITEM_ID", order.COUPON_ITEM_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                        orderCommand.Parameters.Add(new MySqlParameter("@ARRIVE_DATE", order.ARRIVE_DATE));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", payStatus));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@TERMINAL", "APP"));
                        orderCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", order.COUPON_NO));

                        int realPrice = order.ORDER_ITEM_LIST.Sum(c => c.TOTAL_PRICE);
                        int couponPrice = realPrice - order.TOTAL_PRICE;

                        orderCommand.Parameters.Add(new MySqlParameter("@COUPON_PRICE", couponPrice));
                        orderCommand.Parameters.Add(new MySqlParameter("@REAL_PRICE", realPrice));

                        i += orderCommand.ExecuteNonQuery();

                        #endregion

                        int orderItemCount = 0;

                        if (order.ORDER_ITEM_LIST != null)
                        {
                            foreach (OrderItem item in order.ORDER_ITEM_LIST)
                            {
                                #region Order_Item 테이블 Insert

                                string orderitemSql = @"
INSERT INTO order_item (ORDER_ID,   ORDER_ITEM_ID,  ITEM_ID,  QUANTITY,  PRICE,  ITEM_OPTION,  COUPON_NO,   COUPON_PRICE,   REAL_PRICE) 
       VALUES	       (@ORDER_ID, @ORDER_ITEM_ID, @ITEM_ID, @QUANTITY, @PRICE, @ITEM_OPTION, @COUPON_NO , @COUPON_PRICE,  @REAL_PRICE) ";

                                MySqlCommand orderItemCommand = new MySqlCommand(orderitemSql, connection);

                                orderItemCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", order.ORDER_ID));
                                orderItemCommand.Parameters.Add(new MySqlParameter("@ORDER_ITEM_ID", item.ORDER_ITEM_ID));
                                orderItemCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", item.ITEM_ID));
                                orderItemCommand.Parameters.Add(new MySqlParameter("@QUANTITY", item.ITEM_COUNT));

                                if (!string.IsNullOrEmpty(order.COUPON_ITEM_ID))
                                {
                                    if (item.ITEM_ID == order.COUPON_ITEM_ID)
                                    {
                                        orderItemCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", order.COUPON_NO));
                                        orderItemCommand.Parameters.Add(new MySqlParameter("@COUPON_PRICE", couponPrice));

                                        int orderItemPrice = item.TOTAL_PRICE - couponPrice;
                                        orderItemCommand.Parameters.Add(new MySqlParameter("@PRICE", orderItemPrice));
                                    }
                                    else
                                    {
                                        orderItemCommand.Parameters.Add(new MySqlParameter("@PRICE", item.TOTAL_PRICE));
                                    }
                                }
                                else
                                {
                                    orderItemCommand.Parameters.Add(new MySqlParameter("@PRICE", item.TOTAL_PRICE));
                                }

                                orderItemCommand.Parameters.Add(new MySqlParameter("@REAL_PRICE", item.TOTAL_PRICE));

                                orderItemCount += item.ITEM_COUNT;

                                // 옵션그룹:옵션디테일넘버(옵션가격)|옵션그룹:옵션디테일넘버(옵션가격)
                                string itemOption = "";

                                if (!string.IsNullOrEmpty(item.ITEM_OPTION_FULL))
                                {
                                    List<OrderItemOption> orderItemOptions = new List<OrderItemOption>();
                                    foreach (string optionData in item.ITEM_OPTION_FULL.Split('|'))
                                    {
                                        OrderItemOption orderItemOption = new OrderItemOption();

                                        string[] options = optionData.Split(':');

                                        orderItemOption.OPTION_NAME = options[0];

                                        if (options.Length > 1)
                                        {
                                            // 가격분류
                                            string[] optionDetailPrice = options[1].Split('[');

                                            orderItemOption.OPTION_DETAIL_NAME = optionDetailPrice[0];
                                            orderItemOption.OPTION_DETAIL_PRICE = optionDetailPrice.Length == 1 ? "" : optionDetailPrice[1].TrimEnd(']');
                                        }

                                        orderItemOptions.Add(orderItemOption);
                                    }

                                    itemOption = JsonConvert.SerializeObject(orderItemOptions);
                                }

                                orderItemCommand.Parameters.Add(new MySqlParameter("@ITEM_OPTION", itemOption));

                                i += orderItemCommand.ExecuteNonQuery();

                                #endregion
                            }
                        }

                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string orderId = order.ORDER_ID;

                        #region order 테이블 status update

                        string sql = @"
UPDATE `order` SET ORDER_STATUS = @ORDER_STATUS,  PAY_CODE = @PAY_CODE, UPDATE_DATE = @UPDATE_DATE WHERE ORDER_ID = @ORDER_ID ";

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                        mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", "COUPON"));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", returnstatus));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));

                        errorMessage += "\r\n START order update ";
                        i = mySqlCommand.ExecuteNonQuery();
                        errorMessage += "\r\n END order update count : " + i.ToString();

                        #endregion

                        if (returnstatus == "PAID")
                        {
                            DateTime nowDateTime = DateTime.Now;

                            #region 기준 주문 정보 가져오기
                            string ordersql = @"
SELECT A.*, SUM(B.QUANTITY) ORDER_COUNT FROM `order` AS A, order_item B WHERE A.ORDER_ID = @ORDER_ID AND A.ORDER_ID = B.ORDER_ID ";

                            MySqlCommand storeidCommand = new MySqlCommand(ordersql, connection);
                            storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                            errorMessage += "\r\n START order select ";

                            MySqlDataReader dataReader = storeidCommand.ExecuteReader();

                            errorMessage += "\r\n END order select ";

                            DataTable dt = new DataTable();
                            dt.Load(dataReader);
                            dataReader.Close();

                            DataRow dr = dt.Rows[0];

                            errorMessage += "\r\n dt ORDER_ID :  " + dr["ORDER_ID"].ToString();
                            errorMessage += "\r\n dt MEMBER_ID :  " + dr["MEMBER_ID"].ToString();
                            errorMessage += "\r\n dt STORE_ID :  " + dr["STORE_ID"].ToString();
                            errorMessage += "\r\n dt PRICE :  " + dr["PRICE"].ToString();
                            errorMessage += "\r\n dt ORDER_COUNT :  " + dr["ORDER_COUNT"].ToString();
                            errorMessage += "\r\n dt COUPON_ITEM_ID :  " + dr["COUPON_ITEM_ID"].ToString();

                            List<OrderModel> orderModelList = Function.CreateListFromTable<OrderModel>(dt);

                            errorMessage += "\r\n orderModelList Count : " + orderModelList.Count;

                            OrderModel orderModel = orderModelList[0];

                            errorMessage += "\r\n ORDER_ID :  " + orderModel.ORDER_ID;
                            errorMessage += "\r\n MEMBER_ID :  " + orderModel.MEMBER_ID;
                            errorMessage += "\r\n STORE_ID :  " + orderModel.STORE_ID;
                            errorMessage += "\r\n PRICE :  " + orderModel.PRICE;
                            errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;

                            orderModel.ORDER_COUNT = double.Parse(dr["ORDER_COUNT"].ToString());

                            errorMessage += "\r\n COUPON_ITEM_ID :  " + orderModel.COUPON_ITEM_ID;

                            #endregion

                            #region order_stamp 테이블 Insert

                            double orderStampCount = 0;

                            string stampSql = $@"
INSERT INTO `order_stamp` (`ORDER_ID`, `MEMBER_ID`, `COUNT`, `USE_YN`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@ORDER_ID,  @MEMBER_ID,  @STAMP_COUNT,   @USE_YN,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                            MySqlCommand stampCommand = new MySqlCommand(stampSql, connection);

                            stampCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                            stampCommand.Parameters.Add(new MySqlParameter("@USE_YN", "N"));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START order_stamp insert";
                            i += stampCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END order_stamp insert";
                            #endregion

                            #region 쿠폰 사용처리 - (USE_COUNT+1)
                            sql = @"
                            UPDATE coupon SET USE_COUNT = USE_COUNT+1 WHERE COUPON_NO = @COUPON_NO ";
                            // UPDATE coupon SET USEABLE_COUNT = USEABLE_COUNT-1, USE_COUNT = USE_COUNT+1 WHERE COUPON_NO = @COUPON_NO ";

                            mySqlCommand = new MySqlCommand(sql, connection);
                            mySqlCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", orderModel.COUPON_NO));

                            errorMessage += "\r\n START coupon update ";
                            i = mySqlCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END coupon update ";
                            #endregion

                            #region Pay 테이블 Insert

                            string paySql = $@"
INSERT INTO `pay` (`PAY_CODE`, `PAY_TYPE`, `PAY_PRICE`, `PAY_DATE`, `ORDER_ID`) VALUES
                  (@PAY_CODE,  @PAY_TYPE,  @PAY_PRICE,  @PAY_DATE,  @ORDER_ID) ";

                            MySqlCommand payCommand = new MySqlCommand(paySql, connection);

                            payCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", "COUPON_" + orderId));
                            payCommand.Parameters.Add(new MySqlParameter("@PAY_TYPE", "COUPON"));
                            payCommand.Parameters.Add(new MySqlParameter("@PAY_PRICE", orderModel.PRICE));
                            payCommand.Parameters.Add(new MySqlParameter("@PAY_DATE", nowDateTime));
                            payCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                            i += payCommand.ExecuteNonQuery();

                            #endregion

                            #region USP_KIOSK_WAITING_INS 호출하여 Waiting 상태로 만듭니다
                            // ORDER 테이블 ORDER_STATUS ='WAITING' 변경, STORE_ORDER 테이블 Insert
                            string orderWaitSql = "USP_KIOSK_WAITING_INS ";

                            MySqlCommand orderwaitCommand = new MySqlCommand(orderWaitSql, connection);
                            orderwaitCommand.CommandType = CommandType.StoredProcedure;

                            orderwaitCommand.Parameters.Add(new MySqlParameter("P_STORE_ID", orderModel.STORE_ID));
                            orderwaitCommand.Parameters.Add(new MySqlParameter("P_ORDER_ID", orderId));

                            i += orderwaitCommand.ExecuteNonQuery();
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        string errorfilename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                        File.WriteAllText($@"D:\IIS\Auosms\M\Order\{errorfilename}_Error.txt", $"imp_uid : COUPON, merchant_uid : COUPON = {returnstatus} \r\nex : {errorMessage} \r\n{ex.ToString()}");

                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

                File.WriteAllText($@"D:\IIS\Auosms\M\Order\{filename}.txt", $"imp_uid : COUPON, merchant_uid : COUPON = {returnstatus} Message : {errorMessage}");

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SignUp)
            {
                #region SignUp

                string jsonData = requestModel.RequestMessage;

                User user = JsonConvert.DeserializeObject<User>(jsonData);

                string memberName = user.Id + user.SNSType;
                string nickName = user.NickName;
                string phoneNumber = user.PhoneNumber;
                string certificationNo = user.Token;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        bool isExist = false;

                        // 존재하는지 확인합니다.
                        using (var existCommand = new MySqlCommand("SELECT * FROM member WHERE MEMBER_NAME ='" + memberName + "'", connection))
                        {
                            MySqlDataReader reader = existCommand.ExecuteReader();

                            if (reader.HasRows)
                            {
                                isExist = true;

                                DataTable dt = new DataTable();
                                dt.Load(reader);

                                string memberId = dt.Rows[0]["MEMBER_ID"].ToString();
                                string phone = dt.Rows[0]["PHONE"] == DBNull.Value ? null : dt.Rows[0]["PHONE"]?.ToString();
                                nickName = dt.Rows[0]["MEMBER_NICKNAME"] == DBNull.Value ? null : dt.Rows[0]["MEMBER_NICKNAME"]?.ToString();

                                SignUpInfo signUpInfo = new SignUpInfo();
                                signUpInfo.MemberId = memberId;
                                signUpInfo.PhoneNumber = phone;
                                signUpInfo.NickName = nickName;
                                signUpInfo.IsNew = false;

                                resultMessage = JsonConvert.SerializeObject(signUpInfo);
                            }
                            else
                            {
                            }

                            reader.Close();
                            reader.Dispose();
                        }

                        if (!isExist)
                        {
                            string sql = @"
INSERT INTO member(PHONE, CERTIFICATION_NO,MEMBER_NAME, MEMBER_NICKNAME, CREATE_DATE, UPDATE_DATE) VALUES(@PHONE, @CERTIFICATION_NO, @MEMBER_NAME, @MEMBER_NICKNAME, @CREATE_DATE, @UPDATE_DATE) ";

                            MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                            mySqlCommand.Parameters.Add(new MySqlParameter("@PHONE", phoneNumber));
                            mySqlCommand.Parameters.Add(new MySqlParameter("@CERTIFICATION_NO", certificationNo));

                            mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_NAME", memberName));
                            mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_NICKNAME", nickName));
                            mySqlCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", DateTime.Now));
                            mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));

                            mySqlCommand.ExecuteNonQuery();

                            // 추가된 Member 의 ID 를 가져옵니다.
                            sql = "SELECT * FROM member WHERE MEMBER_NAME ='" + memberName + "'";

                            mySqlCommand = new MySqlCommand(sql, connection);
                            MySqlDataReader reader = mySqlCommand.ExecuteReader();
                            DataTable dt = new DataTable();
                            dt.Load(reader);

                            reader.Close();
                            reader.Dispose();

                            SignUpInfo signUpInfo = new SignUpInfo();
                            signUpInfo.MemberId = dt.Rows[0]["MEMBER_ID"].ToString();
                            signUpInfo.IsNew = true;

                            resultMessage = JsonConvert.SerializeObject(signUpInfo);
                        }
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SignUpAddInfo)
            {
                #region SignUpAddInfo

                // memberId,별명,폰번호
                string jsonData = JsonConvert.DeserializeObject<string>(requestModel.RequestMessage);

                string memberId = string.Empty;
                string nickName = string.Empty;

                try
                {

                    string[] infoDtas = jsonData.Split(',');

                    memberId = infoDtas[0];
                    nickName = infoDtas[1];
                }
                catch (Exception ex)
                {
                    return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                }
                finally
                {

                }

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        int i = -1;

                        string sql = @"
UPDATE member SET MEMBER_NICKNAME = @NICKNAME WHERE MEMBER_ID = @MEMBER_ID ";

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                        mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberId));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@NICKNAME", nickName));

                        i = mySqlCommand.ExecuteNonQuery();

                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.Taste)
            {
                #region Taste

                string jsonData = requestModel.RequestMessage;

                MemberTasteModel model = JsonConvert.DeserializeObject<MemberTasteModel>(jsonData);

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string memberId = model.MEMBER_ID;

                        // 기존 데이터는 제거 합니다.
                        using (var deleteCommand = new MySqlCommand($"DELETE FROM member_taste WHERE MEMBER_ID = {memberId}", connection))
                        {
                            deleteCommand.ExecuteNonQuery();
                        }

                        int i = 0;

                        string sql = @"
INSERT INTO member_taste( MEMBER_ID,  ITEM_GROUP,  COFFEE_SHOT,  SYRUP_TYPE,  SYRUP_DENSITY,  ICE,  CREAM,  MILK_HOT) 
                  VALUES(@MEMBER_ID, @ITEM_GROUP, @COFFEE_SHOT, @SYRUP_TYPE, @SYRUP_DENSITY, @ICE, @CREAM, @MILK_HOT) ";

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                        mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberId));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@ITEM_GROUP", model.ITEM_GROUP));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@COFFEE_SHOT", model.COFFEE_SHOT));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@SYRUP_TYPE", model.SYRUP_TYPE));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@SYRUP_DENSITY", model.SYRUP_DENSITY));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@ICE", model.ICE));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@CREAM", model.CREAM));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@MILK_HOT", model.MILK_HOT));

                        i = mySqlCommand.ExecuteNonQuery();

                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectLastOrder)
            {
                #region SelectLastOrder

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $"SELECT a.*, b.STORE_NAME FROM `order` a , store b WHERE MEMBER_ID = {memberId} AND a.STORE_ID = b.STORE_ID AND ORDER_STATUS <> 'CALCEL' AND ORDER_STATUS <> 'ORDER' ORDER BY ORDER_DATE DESC LIMIT 1";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectAllOrder)
            {
                #region SelectAllOrder

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        //string sql = $"SELECT a.*, b.STORE_NAME FROM `order` a , store b WHERE MEMBER_ID = {memberId} AND a.STORE_ID = b.STORE_ID ORDER BY ORDER_DATE DESC";

                        string sql = $@"
        SELECT
			A.*
			,D.CODE_NAME AS ORDER_STATUS_NAME
			,E.STORE_NAME
			,GROUP_CONCAT(C.ITEM_NAME SEPARATOR '|') ITEM_NAME
		FROM `ORDER` A, ORDER_ITEM B, ITEM C, COMMON_CODE D, STORE E
		WHERE A.ORDER_ID = B.ORDER_ID
		AND B.ITEM_ID = C.ITEM_ID
		AND A.ORDER_STATUS = D.CODE_ID
		AND D.CODE_GROUP = 'ORDER_STATUS'
		AND A.MEMBER_ID = {memberId}
		AND A.STORE_ID = E.STORE_ID
        AND A.ORDER_STATUS <> 'CANCEL'
        AND A.ORDER_STATUS <> 'ORDER'
		GROUP BY
			A.ORDER_ID
			,A.ORDER_DATE
			,A.PRICE
			,A.STORE_ID
			,A.ARRIVE_DATE
			,A.ORDER_STATUS
		ORDER BY A.ORDER_DATE DESC ";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectOrderItem)
            {
                #region SelectOrderItem

                string orderId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"
SELECT c.ORDER_NO, a.*, a.QUANTITY ITEM_COUNT, a.ITEM_OPTION ITEM_OPTION_FULL, a.PRICE  TOTAL_PRICE, b.ITEM_NAME, b.ITEM_PRICE
FROM order_item a , item b , store_order c
WHERE a.ORDER_ID = '{orderId}' 
AND a.ITEM_ID = b.ITEM_ID
AND a.ORDER_ID = c.ORDER_ID ";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectTaste)
            {
                #region SelectTaste

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $"SELECT * FROM member_taste WHERE MEMBER_ID = {memberId}";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectMenu)
            {
                #region SelectMenu

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"
SELECT a.*, b.ITEM_GROUP_CODE , b.ITEM_GROUP_NAME
  FROM item a, item_group b, item_category c
 WHERE a.ITEM_ID = c.ITEM_ID
   AND b.ITEM_GROUP_CODE = c.ITEM_GROUP_CODE 
   AND b.DELETE_USER IS NULL
   order BY b.CODE_SEQ ";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectStore)
            {
                #region SelectStore

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"SELECT * FROM STORE WHERE USE_YN='Y' AND DELETE_DATE IS NULL ";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectStoreItem)
            {
                #region SelectStoreItem

                string storeId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"SELECT* FROM STORE_ITEM WHERE STORE_ID = {storeId}";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectStampCount)
            {
                #region SelectStampCount - 사용안함

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"SELECT SUM(COUNT) STAMP_COUNT FROM order_stamp WHERE MEMBER_ID = {memberId}";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectMemberInfo)
            {
                #region SelectMemberInfo

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"SELECT * FROM member WHERE MEMBER_ID = {memberId}";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectOption)
            {
                #region SelectOption

                string optionIds = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    connection.Open();

                    List<OptionModel> resultOptionModels = new List<OptionModel>();

                    foreach (string optionId in optionIds.Split(','))
                    {
                        string sql = $@"
SELECT a.OPTION_CODE, a.OPTION_NAME, a.ESSENTIAL
  FROM option a
 WHERE a.OPTION_CODE = {optionId}
   AND a.USE_YN ='Y'
   AND a.DELETE_DATE is null ";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader dataReader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(dataReader);
                        dataReader.Close();

                        List<OptionModel> optionModels = Function.CreateListFromTable<OptionModel>(dt);

                        if (optionModels == null || optionModels.Count == 0) continue;

                        OptionModel optionModel = optionModels[0];

                        sql = $@"
SELECT GROUP_CONCAT(CONCAT(OPTION_DETAIL_NAME, '[', FORMAT(EXTRA_COST, 0), ']') ORDER BY OPTION_SORT SEPARATOR ',') OPTION_DETAIL
  FROM OPTION_DETAIL A
 WHERE A.OPTION_CODE = {optionId} ";

                        command = new MySqlCommand(sql, connection);

                        optionModel.OPTION_DETAIL = command.ExecuteScalar()?.ToString();

                        resultOptionModels.Add(optionModel);
                    }

                    resultMessage = JsonConvert.SerializeObject(resultOptionModels);
                }

                #region Call Procedure - 이상함

                #endregion

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectOptionDetail)
            {
                #region SelectOptionDetail

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"
SELECT *
  FROM option_detail
 WHERE USE_YN ='Y'";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.CheckNickName)
            {
                #region CheckNickName
                string[] data = JsonConvert.DeserializeObject<string>(requestModel.RequestMessage).Split(',');

                string memberId = data[0];
                string nickName = data[1];

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $"SELECT * FROM member WHERE MEMBER_ID <> '{memberId}' AND MEMBER_NICKNAME ='{nickName}'";

                        // 존재하는지 확인합니다.
                        using (var existCommand = new MySqlCommand(sql, connection))
                        {
                            MySqlDataReader reader = existCommand.ExecuteReader();

                            if (reader.HasRows)
                            {
                                resultDetailMessage = sql;
                                resultMessage = "Y";
                            }
                            else
                            {
                                resultMessage = "N";
                            }

                            resultDetailMessage = sql;

                            reader.Close();
                            reader.Dispose();
                        }

                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectOrderStatus)
            {
                #region SelectOrderStatus

                string orderId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"SELECT ORDER_STATUS FROM `order` WHERE ORDER_ID = '{orderId}'";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        string result = command.ExecuteScalar()?.ToString();

                        resultMessage = result;
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectStampCuponCount)
            {
                #region SelectStampCount

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        //SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;
                        string sql = $@"
SELECT STAMP_COUNT FROM member_stamp WHERE MEMBER_ID = {memberId}";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SendSMS)
            {
                #region SendSMS

                string phonenumber = requestModel.RequestMessage;

                phonenumber = phonenumber.Replace("-", "");

                WebClient webClient = new WebClient();
                Random r = new Random();
                var x = r.Next(0, 1000000);
                string certificationNo = x.ToString("000000");

                try
                {
                    // http://211.115.107.41:10000/sms.asp?phone=01043780425&code=12345 
                    // http://211.57.200.72:10000/sms.asp?phone={phonenumber}&code={certificationNo
                    string result = webClient.DownloadString($"http://211.115.107.41:10000/sms.asp?phone={phonenumber}&code={certificationNo}");

                    if (result == "Fail")
                    {
                        return new ResultModel() { ResultMessage = "Send Fail", ResultCode = ResultCode.Fail };
                    }

                    // result= "Sucess/Fail"
                }
                catch (Exception ex)
                {
                    return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                }

                int i = 0;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        DateTime nowDateTime = DateTime.Now;

                        string sql = $@"
DELETE FROM `member_sms` WHERE `PHONE_NO`= @PHONE_NO ";

                        MySqlCommand command = new MySqlCommand(sql, connection);

                        command.Parameters.Add(new MySqlParameter("@PHONE_NO", phonenumber));

                        i += command.ExecuteNonQuery();

                        sql = $@"
INSERT INTO `member_sms` (`PHONE_NO`, `SMS`,`CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                         (@PHONE_NO,  @SMS, @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                        command = new MySqlCommand(sql, connection);

                        command.Parameters.Add(new MySqlParameter("@PHONE_NO", phonenumber));
                        command.Parameters.Add(new MySqlParameter("@SMS", certificationNo));
                        command.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                        command.Parameters.Add(new MySqlParameter("@CREATE_USER", "system"));
                        command.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                        command.Parameters.Add(new MySqlParameter("@UPDATE_USER", "system"));

                        i += command.ExecuteNonQuery();

                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.ConfirmSMS)
            {
                #region ConfirmSMS

                string phoneCertificationNo = requestModel.RequestMessage;

                string[] datas = phoneCertificationNo.Split(',');

                string phonenumber = datas[0];
                string certificationNo = datas[1];

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"SELECT SMS FROM member_sms WHERE PHONE_NO = {phonenumber}";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        string sms = command.ExecuteScalar()?.ToString();

                        if (!string.IsNullOrEmpty(sms) && sms == certificationNo)
                        {
                            resultMessage = "Y";
                        }
                        else
                        {
                            resultMessage = "N";
                        }
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.DeleteMember)
            {
                #region DeleteMember

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        using (var deleteCommand = new MySqlCommand($"DELETE FROM member WHERE MEMBER_ID = {memberId}", connection))
                        {
                            deleteCommand.ExecuteNonQuery();
                        }

                        resultMessage = "Y";
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectManyOrder)
            {
                #region SelectManyOrder

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        //string sql = $"SELECT a.*, b.STORE_NAME FROM `order` a , store b WHERE MEMBER_ID = {memberId} AND a.STORE_ID = b.STORE_ID ORDER BY ORDER_DATE DESC";

                        string sql = $@"
		SELECT
			C.*
			,E.STORE_ID
			,E.STORE_NAME
		FROM `ORDER` A, ORDER_ITEM B, ITEM C, COMMON_CODE D, STORE E
		WHERE A.ORDER_ID = B.ORDER_ID
		AND B.ITEM_ID = C.ITEM_ID
		AND A.ORDER_STATUS = D.CODE_ID
		AND D.CODE_GROUP = 'ORDER_STATUS'
		AND A.MEMBER_ID = {memberId}
		AND A.STORE_ID = E.STORE_ID
        AND A.ORDER_STATUS <> 'CANCEL'
        AND A.ORDER_STATUS <> 'ORDER'
		GROUP BY
			B.ITEM_ID
		  ,A.STORE_ID
		ORDER BY COUNT(*) DESC
		LIMIT 5 ";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.UpdateScore)
            {
                #region UpdateScore

                // orderid, orderitemid
                string[] orderData = requestModel.RequestMessage.Split('∀');
                string orderid = orderData[0];
                string orderitemid = orderData[1];
                string itemid = orderData[2];
                string star = orderData[3];

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        // 별점 반영하기
                        string sql = $@"
		UPDATE
			order_item
		SET
            ITEM_STAR = @ITEM_STAR
        WHERE ORDER_ID = @ORDER_ID
        AND   ORDER_ITEM_ID = @ORDER_ITEM_ID
        AND   ITEM_ID = @ITEM_ID
		 ";

                        MySqlCommand command = new MySqlCommand(sql, connection);

                        command.Parameters.Add(new MySqlParameter("@ORDER_ID", orderid));
                        command.Parameters.Add(new MySqlParameter("@ORDER_ITEM_ID", orderitemid));
                        command.Parameters.Add(new MySqlParameter("@ITEM_ID", itemid));
                        command.Parameters.Add(new MySqlParameter("@ITEM_STAR", star));

                        int i = command.ExecuteNonQuery();

                        resultMessage += " 별점 반영하기 ";

                        resultMessage += i.ToString();

                        // 평균구하기
                        string avgsql = $@"
        SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;
		SELECT AVG(ITEM_STAR) AVERAGE FROM order_item WHERE item_id = @ITEM_ID
		 ";

                        MySqlCommand avgCommand = new MySqlCommand(avgsql, connection);
                        avgCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", itemid));

                        string avgString = avgCommand.ExecuteScalar()?.ToString();

                        //if (string.IsNullOrEmpty(avgString))
                        //{
                        //    avgString = star;
                        //}

                        resultMessage += " 평균구하기[" + avgString + "] ";

                        double avg = double.Parse(avgString);

                        // 평균 반영하기
                        string updatestarsql = $@"
		UPDATE
			item
		SET
            ITEM_AVG_STAR = @AVG
        WHERE ITEM_ID = @ITEM_ID
		 ";

                        MySqlCommand updatestarcommand = new MySqlCommand(updatestarsql, connection);
                        updatestarcommand.Parameters.Add(new MySqlParameter("@AVG", avg));
                        updatestarcommand.Parameters.Add(new MySqlParameter("@ITEM_ID", itemid));

                        i += updatestarcommand.ExecuteNonQuery();

                        resultMessage += " 평균 반영하기 ";

                        resultMessage += i.ToString();

                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = resultMessage + ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.CancelOrder)
            {
                #region CancelOrder

                string errorMessage = "";

                try
                {
                    // orderId/imp_uid(paycode)/취소사유
                    string[] receiveinInfos = requestModel.RequestMessage.Split('∀');

                    string orderId = receiveinInfos[0];
                    string payCode = receiveinInfos[1];
                    string reason = receiveinInfos[2];

                    string imp_key = "";
                    string imp_secret = "";
                    bool onlyCupon = false;

                    #region 기존의 주문 정보 조회가 필요할수도 있음 (참고코드)
                    /*
                    string tockenKey = tockenMessage.response.access_token;
                    string orderId = iamPortResult.merchant_uid;
                    string payCode = iamPortResult.imp_uid;
                    string url = "https://api.iamport.kr/payments/" + iamPortResult.imp_uid;  //테스트 사이트
                    string responseText = string.Empty;

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = "GET";
                    request.Timeout = 30 * 1000; // 30초
                    request.Headers.Add("Authorization", "Bearer " + tockenKey); // 헤더 추가 방법

                    using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
                    {
                        HttpStatusCode status = resp.StatusCode;
                        Console.WriteLine(status);  // 정상이면 "OK"

                        Stream respStream = resp.GetResponseStream();
                        using (StreamReader sr = new StreamReader(respStream))
                        {
                            responseText = sr.ReadToEnd();
                        }
                    }
                    PayStatusMessage payStatusMessage = JsonConvert.DeserializeObject<PayStatusMessage>(responseText);
                    string paystatus = payStatusMessage.response.status;
                     */
                    #endregion

                    #region SQL 작업
                    using (MySqlConnection connection = new MySqlConnection(connectionString))
                    {
                        try
                        {
                            connection.Open();

                            //connection.BeginTransaction()
                            
                            int i = -1;

                            #region 주문 정보 조회

                            string sql = @"
                            SELECT A.*, SUM(B.QUANTITY) ORDER_COUNT FROM `order` AS A, order_item B WHERE A.ORDER_ID = @ORDER_ID AND A.ORDER_ID = B.ORDER_ID ";

                            MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);
                            mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                            errorMessage += "\r\n START order";

                            MySqlDataReader dataReader = mySqlCommand.ExecuteReader();

                            errorMessage += "\r\n END order";

                            DataTable dt = new DataTable();
                            dt.Load(dataReader);
                            dataReader.Close();

                            List<OrderModel> orderModelList = Function.CreateListFromTable<OrderModel>(dt);

                            OrderModel orderModel = orderModelList[0];

                            errorMessage += "\r\n ORDER_ID :  " + orderModel.ORDER_ID;
                            errorMessage += "\r\n MEMBER_ID :  " + orderModel.MEMBER_ID;
                            errorMessage += "\r\n STORE_ID :  " + orderModel.STORE_ID;
                            errorMessage += "\r\n PRICE :  " + orderModel.PRICE;
                            errorMessage += "\r\n STAMP_CNT :  " + orderModel.STAMP_CNT;

                            DataRow dr = dt.Rows[0];
                            orderModel.ORDER_COUNT = double.Parse(dr["ORDER_COUNT"].ToString());

                            errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;
                            errorMessage += "\r\n COUPON_ITEM_ID :  " + orderModel.COUPON_ITEM_ID;

                            // 주문이 1건인데 결제금액이 0원인 경우 - 카드결재 취소를 할필요가 없다.
                            if (orderModel.ORDER_COUNT == 1 && orderModel.PRICE == 0)
                            {
                                onlyCupon = true;
                            }

                            #endregion

                            bool isStampStore = true;

                            //if (!onlyCupon)
                            //{
                                // Store 정보는 카드 결재 취소에 필요한 IMPORT 정보 조회를 위함
                                #region Store 정보 Select

                                sql = @"
                            SELECT * FROM `store` WHERE STORE_ID = @STORE_ID ";

                                MySqlCommand storeCommand = new MySqlCommand(sql, connection);
                                storeCommand.Parameters.Add(new MySqlParameter("@STORE_ID", orderModel.STORE_ID));

                                errorMessage += "\r\n START store";

                                MySqlDataReader storeDataReader = storeCommand.ExecuteReader();

                                errorMessage += "\r\n END store";

                                DataTable dtStore = new DataTable();
                                dtStore.Load(storeDataReader);
                                storeDataReader.Close();

                                List<StoreModel> storeModelList = Function.CreateListFromTable<StoreModel>(dt);

                                errorMessage += $"\r\n storeModelList : {storeModelList.Count}";

                                StoreModel storeModel = storeModelList[0];

                                storeModel.IMPORT_RESTAPI_KEY = dtStore.Rows[0]["IMPORT_RESTAPI_KEY"].ToString();
                                storeModel.IMPORT_RESTAPI_SECRET = dtStore.Rows[0]["IMPORT_RESTAPI_SECRET"].ToString();

                                // 결재 취소에 필요한 정보를 가져옵니다.
                                imp_key = storeModel.IMPORT_RESTAPI_KEY;
                                imp_secret = storeModel.IMPORT_RESTAPI_SECRET;

                                errorMessage += $"\r\n imp_key : {imp_key} , imp_secret : {imp_secret}";

                                if (storeModel.COUPON_USE_YN == "N")
                                {
                                    isStampStore = false;
                                }

                                #endregion
                            //}

                            if (isStampStore)
                            {
                                // 스탬프 적용내용 취소
                                #region 주문 아이템 정보 조회 - 제거

                            //    sql = @"
                            //SELECT SUM(A.QUANTITY) STAMP_COUNT 
                            //FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
                            //WHERE A.ORDER_ID = @ORDER_ID";

                            //    int minusStamp = 0;
                            //    if (orderModel.COUPON_ITEM_ID != 0)
                            //    {
                            //        minusStamp = 1;
                            //    }

                            //    MySqlCommand orderitemCommand = new MySqlCommand(sql, connection);
                            //    orderitemCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                            //    if (orderModel.COUPON_ITEM_ID != 0)
                            //    {
                            //        orderitemCommand.Parameters.Add(new MySqlParameter("@CUPON_ITEM_ID", orderModel.COUPON_ITEM_ID));
                            //    }

                            //    errorMessage += "\r\n START OrderITem ";

                            //    string stampCountData = orderitemCommand.ExecuteScalar()?.ToString();

                            //    errorMessage += "\r\n END OrderITem ";

                            //    int stampCount = 0;

                            //    if (!string.IsNullOrEmpty(stampCountData))
                            //    {
                            //        stampCount = int.Parse(stampCountData);
                            //    }

                            //    errorMessage += "\r\n stampCount : " + stampCount;

                                #endregion
                                #region order_stamp 테이블 Delete - 제거

                            //    sql = $@"
                            //DELETE FROM `order_stamp` WHERE ORDER_ID = @ORDER_ID";

                            //    MySqlCommand stampCommand = new MySqlCommand(sql, connection);

                            //    stampCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                            //    errorMessage += "\r\n START order_stamp";
                            //    i += stampCommand.ExecuteNonQuery();
                            //    errorMessage += "\r\n END order_stamp";

                                #endregion

                                #region member_stamp 테이블 Update

                                sql = "SELECT STAMP_COUNT FROM member_stamp WHERE MEMBER_ID = @MEMBER_ID";

                                MySqlCommand memberStampCommand = new MySqlCommand(sql, connection);
                                memberStampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));

                                errorMessage += "\r\n START member_stamp ";

                                errorMessage += "\r\n MEMBER_ID : " + orderModel.MEMBER_ID;

                                string memberStampCountData = memberStampCommand.ExecuteScalar()?.ToString();

                                errorMessage += "\r\n END member_stamp ";

                                errorMessage += "\r\n STAMP_COUNT : " + memberStampCountData ?? "";

                                int memberStampCount = int.Parse(memberStampCountData);

                                // 사용한 쿠폰 복구
                                if (!string.IsNullOrEmpty(orderModel.COUPON_NO))
                                {
                                    // 쿠폰 사용 다시 원복
                                    #region 쿠폰 사용 원복 처리 - (USE_COUNT-1)
                                    sql = @"
                            UPDATE coupon SET USE_COUNT = USE_COUNT-1 WHERE COUPON_NO = @COUPON_NO ";
                                    //UPDATE coupon SET USEABLE_COUNT = USEABLE_COUNT + 1, USE_COUNT = USE_COUNT - 1 WHERE COUPON_NO = @COUPON_NO ";

                                    mySqlCommand = new MySqlCommand(sql, connection);
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", orderModel.COUPON_NO));

                                    errorMessage += "\r\n START coupon update ";
                                    i = mySqlCommand.ExecuteNonQuery();
                                    errorMessage += "\r\n END coupon update ";
                                    #endregion
                                }

                                // 적립된 스템프 제거
                                memberStampCount = memberStampCount - Convert.ToInt32(orderModel.STAMP_CNT);

                                // 뺐을때 0보다 작으면 0으로 처리
                                if (memberStampCount < 0) memberStampCount = 0;

                                sql = $@"
                            UPDATE `member_stamp` 
                               SET `STAMP_COUNT` = @STAMP_COUNT, `UPDATE_DATE` = @UPDATE_DATE, `UPDATE_USER` = @UPDATE_USER
                             WHERE `MEMBER_ID` = @MEMBER_ID";

                                MySqlCommand stampCommand = new MySqlCommand(sql, connection);

                                stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", memberStampCount));
                                stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                                stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));
                                stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                                errorMessage += "\r\n START member_stamp update";
                                i += stampCommand.ExecuteNonQuery();
                                errorMessage += "\r\n END member_stamp update";
                                #endregion
                            }

                            #region Pay 테이블 Insert -값 insert

                            string paySql = $@"
INSERT INTO `pay` (`PAY_CODE`, `PAY_TYPE`, `PAY_PRICE`, `PAY_DATE`, `ORDER_ID`) VALUES
                  (@PAY_CODE,  @PAY_TYPE,  @PAY_PRICE,  @PAY_DATE,  @ORDER_ID) ";

                            MySqlCommand payCommand = new MySqlCommand(paySql, connection);

                            payCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", Guid.NewGuid().ToString()));
                            payCommand.Parameters.Add(new MySqlParameter("@PAY_TYPE", "CARD"));
                            payCommand.Parameters.Add(new MySqlParameter("@PAY_PRICE", -orderModel.PRICE));
                            payCommand.Parameters.Add(new MySqlParameter("@PAY_DATE", DateTime.Now));
                            payCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                            i += payCommand.ExecuteNonQuery();

                            #endregion

                            #region PAY_CANCEL 상태로 변경
                            sql = @"
                            UPDATE `order` SET ORDER_STATUS = @ORDER_STATUS, UPDATE_DATE = @UPDATE_DATE WHERE ORDER_ID = @ORDER_ID ";

                            mySqlCommand = new MySqlCommand(sql, connection);

                            mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                            //mySqlCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                            mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", "PAY_CANCEL"));
                            mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));

                            errorMessage += "\r\n Cancel START sql ";
                            i = mySqlCommand.ExecuteNonQuery();
                            errorMessage += "\r\n Cancel END sql ";
                            #endregion
                            #region Cancel Order Import
                            if (!onlyCupon) // 쿠폰만 사용한 경우는 제외
                            {
                                TockenMessage tockenMessage = GetTockenPostData(new IamPortTocken() { imp_key = imp_key, imp_secret = imp_secret }, "https://api.iamport.kr/users/getToken");

                                string tockenKey = tockenMessage.response.access_token;

                                resultMessage += $"impuid : {payCode}, reason : {reason}";

                                string cancelResultData = GetOrderCancelData(new OrderCancelData() { imp_uid = payCode, reason = reason }, tockenKey, "https://api.iamport.kr/payments/cancel");

                                OrderCancelResultData orderCancelData = JsonConvert.DeserializeObject<OrderCancelResultData>(cancelResultData);

                                if (orderCancelData.code != 0) // 에러
                                {
                                    return new ResultModel() { ResultMessage = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(orderCancelData.message ?? "")), ResultCode = ResultCode.Fail };
                                }

                                //string messagestring = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(orderCancelData.message));

                                string messagestring = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(cancelResultData));

                                resultMessage += messagestring;
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            string errorfilename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                            File.WriteAllText($@"D:\IIS\Auosms\M\Order\{errorfilename}_Error.txt", $"message : {requestModel.RequestMessage} \r\nex : {errorMessage} \r\n{ex.ToString()}");
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                    #endregion
                    
                }
                catch (Exception ex)
                {
                    return new ResultModel() { ResultMessage = resultMessage + ex.ToString(), ResultCode = ResultCode.Fail };
                }
                finally
                {
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectCoupon)
            {
                #region SelectCoupon

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

    //                    string sql = $@"
    //   SELECT A.*, B.CODE_NAME AS COUPON_NAME 
    //     FROM coupon A, common_code B 
    //    WHERE A.COUPON_TYPE = B.CODE_ID 
    //      AND A.MEMBER_ID = @MEMBER_ID  
    //      AND USEABLE_COUNT > 0
    //      AND A.USE_START_DATE <= @DATE
		  //AND A.USE_END_DATE   >= @DATE";

                        string sql = $@"
	  SELECT 
          A.COUPON_NO
         ,A.COUPON_TYPE
         ,(SELECT CODE_NAME FROM COMMON_CODE CC WHERE CC.CODE_GROUP = 'COUPON_TYPE' AND CC.CODE_ID = A.COUPON_TYPE AND CC.DELETE_DATE IS NULL) AS COUPON_NAME
		,C.EVENT_COUPON_TYPE
		,(SELECT CODE_NAME FROM COMMON_CODE CC WHERE CC.CODE_GROUP = 'EVENT_COUPON_TYPE' AND CC.CODE_ID = C.EVENT_COUPON_TYPE AND CC.DELETE_DATE IS NULL) AS EVENT_CONDITION_TYPE_NM
		,C.ITEM_ID
		,B.ITEM_NAME
		,B.ITEM_PRICE
		,B.ITEM_IMAGE
		,C.DISCOUNT_TYPE
		,C.DISCOUNT_VALUE
		,C.DISCOUNT_LIMIT_PRICE
		,A.USEABLE_COUNT - A.USE_COUNT USEABLE_COUNT
		,DATE_FORMAT(A.USE_START_DATE, '%Y-%m-%d') AS USE_START_DATE
		,CASE WHEN A.USE_END_DATE IS NULL THEN '' ELSE DATE_FORMAT(A.USE_END_DATE, '%Y-%m-%d') END AS USE_END_DATE
		FROM COUPON A
		LEFT OUTER JOIN EVENT_COUPON C ON A.EVENT_COUPON_NO = C.EVENT_COUPON_NO
		LEFT OUTER JOIN ITEM B ON C.ITEM_ID = B.ITEM_ID 
		WHERE C.DELETE_DATE IS NULL
        AND (A.USEABLE_COUNT - A.USE_COUNT) > 0
		AND A.MEMBER_ID = @MEMBER_ID
        AND A.USE_START_DATE <= @DATE
		AND A.USE_END_DATE   >= @DATE";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        command.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberId));
                        command.Parameters.Add(new MySqlParameter("@DATE", DateTime.Now));

                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectQnA)
            {
                #region SelectQnA

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"
        SELECT 
			A.BOARD_ID
			,A.BOARD_NUM
			,A.TITLE
			,A.CONTENTS
			,A.NOTICE_YN
            ,A.CREATE_DATE
			,DATE_FORMAT(A.CREATE_DATE, '%Y-%m-%d %H:%i:%S') AS CREATE_DATE_STRING
			,CONCAT(B.MEMBER_NICKNAME, ' (', B.MEMBER_NAME, ')') AS MEMBER_NAME
            ,A.MEMBER_ID
			,IFNULL(C.STORE_NAME, '본사') AS STORE_NAME
            ,A.STORE_ID
			,(SELECT COUNT(*) FROM BOARD_REPLY D WHERE D.BOARD_ID = A.BOARD_ID AND D.DELETE_DATE IS NULL) REPLY_CNT
            ,(SELECT CONTENTS FROM BOARD_REPLY D WHERE D.BOARD_ID = A.BOARD_ID AND D.DELETE_DATE IS NULL LIMIT 1) REPLY_CONTENTS
		FROM BOARD A
        	 LEFT OUTER JOIN MEMBER B ON A.CREATE_USER = B.MEMBER_ID
        	 LEFT OUTER JOIN STORE C ON A.STORE_ID = C.STORE_ID
		WHERE A.BOARD_TYPE = 'QNA'
		  AND A.DELETE_DATE IS NULL
          AND A.MEMBER_ID = @MEMBER_ID
		ORDER BY A.CREATE_DATE DESC
";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        command.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberId));
                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.RegistQnA)
            {
                #region RegistQnA
                string jsonData = requestModel.RequestMessage;

                QnaModel qnaData = JsonConvert.DeserializeObject<QnaModel>(jsonData);

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        // 별점 반영하기
                        string sql = $@"
        INSERT INTO BOARD
		(
			BOARD_TYPE
			,BOARD_NUM
			,TITLE
			,CONTENTS
			,NOTICE_YN
			,STORE_ID
			,CREATE_USER
            ,MEMBER_ID
		)
		VALUES
		(
			 @BOARD_TYPE
			,@BOARD_NUM
			,@TITLE
			,@CONTENTS
			,@NOTICE_YN
			,@STORE_ID
			,@CREATE_USER
            ,@MEMBER_ID
		)
		 ";

                        MySqlCommand command = new MySqlCommand(sql, connection);

                        command.Parameters.Add(new MySqlParameter("@BOARD_TYPE" , "QNA"));
                        command.Parameters.Add(new MySqlParameter("@BOARD_NUM"  , 1));
                        command.Parameters.Add(new MySqlParameter("@TITLE"      , qnaData.TITLE));
                        command.Parameters.Add(new MySqlParameter("@CONTENTS"   , qnaData.CONTENTS));
                        command.Parameters.Add(new MySqlParameter("@NOTICE_YN"  , "N"));
                        command.Parameters.Add(new MySqlParameter("@STORE_ID"   , qnaData.STORE_ID));
                        command.Parameters.Add(new MySqlParameter("@CREATE_USER", qnaData.MEMBER_ID));
                        command.Parameters.Add(new MySqlParameter("@MEMBER_ID"  , qnaData.MEMBER_ID));

                        int i = command.ExecuteNonQuery();

                        resultMessage += i.ToString();

                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = resultMessage + ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectNotice)
            {
                #region SelectNotice

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"
        SELECT 
			A.BOARD_ID
			,A.BOARD_NUM
			,A.TITLE
			,A.CONTENTS
			,A.NOTICE_YN
            ,A.CREATE_DATE
			,DATE_FORMAT(A.CREATE_DATE, '%Y-%m-%d %H:%i:%S') AS CREATE_DATE_STRING
			,CONCAT(B.MEMBER_NICKNAME, ' (', B.MEMBER_NAME, ')') AS MEMBER_NAME
            ,A.MEMBER_ID
			,IFNULL(C.STORE_NAME, '본사') AS STORE_NAME
            ,A.STORE_ID
		FROM BOARD A
        	 LEFT OUTER JOIN MEMBER B ON A.CREATE_USER = B.MEMBER_ID
        	 LEFT OUTER JOIN STORE C ON A.STORE_ID = C.STORE_ID
		WHERE A.BOARD_TYPE = 'NEWS'
		  AND A.DELETE_DATE IS NULL
		ORDER BY A.CREATE_DATE DESC
";

                        MySqlCommand command = new MySqlCommand(sql, connection);

                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.SelectEventCoupon)
            {
                #region SelectEventCoupon

                string memberId = requestModel.RequestMessage;

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        string sql = $@"
        SELECT 
            C.COUPON_NO
			,A.EVENT_COUPON_TYPE
			,(SELECT CODE_NAME FROM COMMON_CODE CC WHERE CC.CODE_GROUP = 'EVENT_COUPON_TYPE' AND CC.CODE_ID = A.EVENT_COUPON_TYPE AND CC.DELETE_DATE IS NULL) AS EVENT_CONDITION_TYPE_NM
			,A.ITEM_ID
			,B.ITEM_NAME
			,B.ITEM_PRICE
			,B.ITEM_IMAGE
			,A.DISCOUNT_TYPE
			,A.DISCOUNT_VALUE
			,A.DISCOUNT_LIMIT_PRICE
			,C.USEABLE_COUNT - C.USE_COUNT USEABLE_COUNT
			,DATE_FORMAT(A.USE_START_DATE, '%Y-%m-%d') AS E_USE_START_DATE
			,CASE WHEN A.USE_END_DATE IS NULL THEN '' ELSE DATE_FORMAT(A.USE_END_DATE, '%Y-%m-%d') END AS E_USE_END_DATE
            ,DATE_FORMAT(C.USE_START_DATE, '%Y-%m-%d') AS USE_START_DATE
			,CASE WHEN C.USE_END_DATE IS NULL THEN '' ELSE DATE_FORMAT(C.USE_END_DATE, '%Y-%m-%d') END AS USE_END_DATE
		FROM EVENT_COUPON A
		LEFT OUTER JOIN ITEM B ON A.ITEM_ID = B.ITEM_ID 
		JOIN COUPON C ON A.EVENT_COUPON_NO = C.EVENT_COUPON_NO
		WHERE A.DELETE_DATE IS NULL
		AND C.MEMBER_ID = @MEMBER_ID 
        AND (C.USEABLE_COUNT - C.USE_COUNT) > 0
		AND C.USE_START_DATE <= @DATE
		AND C.USE_END_DATE   >= @DATE";

                        MySqlCommand command = new MySqlCommand(sql, connection);
                        command.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberId));
                        command.Parameters.Add(new MySqlParameter("@DATE", DateTime.Now));

                        MySqlDataReader reader = command.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(reader);

                        reader.Close();
                        reader.Dispose();

                        resultMessage = JsonConvert.SerializeObject(dt);
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }
            else if (requestModel.RequestType == RequestType.UpdateToken)
            {
                #region UpdateToken

                // memberId,별명,폰번호
                string jsonData = JsonConvert.DeserializeObject<string>(requestModel.RequestMessage);

                string memberId = string.Empty;
                string token = string.Empty;

                try
                {

                    string[] infoDtas = jsonData.Split(',');

                    memberId = infoDtas[0];
                    token = infoDtas[1];
                }
                catch (Exception ex)
                {
                    return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                }
                finally
                {

                }

                using (MySqlConnection connection = new MySqlConnection(connectionString))
                {
                    try
                    {
                        connection.Open();

                        int i = -1;

                        string sql = @"
UPDATE member SET TOKEN = @TOKEN WHERE MEMBER_ID = @MEMBER_ID ";

                        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                        mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", memberId));
                        mySqlCommand.Parameters.Add(new MySqlParameter("@TOKEN", token));

                        i = mySqlCommand.ExecuteNonQuery();

                        resultMessage = i.ToString();
                    }
                    catch (Exception ex)
                    {
                        return new ResultModel() { ResultMessage = ex.ToString(), ResultCode = ResultCode.Fail };
                    }
                    finally
                    {
                        connection.Close();
                    }
                }

                #endregion
            }

            return new ResultModel() { ResultMessage = resultMessage, ResultDetailMessage = resultDetailMessage, ResultCode = ResultCode.Sucess };
        }

        #endregion


        // 결제완료 ==================================================================================================================================================================

        // 매장 : 브라운래빗 (imp02402838)
        #region OrderStatus_imp02402838 - 결제 완료 시 처리하는 로직입니다.

        /// <summary>
        /// 결제 완료 시 처리하는 로직입니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        //[HttpPost]
        [Route("OrderStatus_imp02402838")]
        public void OrderStatus_imp02402838([FromUri] IamPortResult iamPortResult)
        {
            string imp_key = "5528248102920909";
            string imp_secret = "hpD1tTi2qwVEZOQZ2Me6Hk2KArLSf7Jwxmy1gR3GnZknNanQXBWFVZpwqboFj8cQvKSLCVs0VMnIXKyV";

            OrderProcess(imp_key, imp_secret, iamPortResult.merchant_uid, iamPortResult.imp_uid);
        }
        #endregion

        // 매장 : 강준 (imp25453177)
        #region OrderStatus_imp25453177 - 결제 완료 시 처리하는 로직입니다.

        /// <summary>
        /// 결제 완료 시 처리하는 로직입니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        //[HttpPost]
        [Route("OrderStatus_imp25453177")]
        public void OrderStatus_imp25453177([FromUri] IamPortResult iamPortResult)
        {
            string imp_key = "2527412862401371";
            string imp_secret = "xMjYch5rQkdgut67Iy7RRUY7tnFPzox6bR4o9aPZu3fZxm6fVd7M85UBszq1YY89PdxYonYTqdSXzYdv";

            OrderProcess(imp_key, imp_secret, iamPortResult.merchant_uid, iamPortResult.imp_uid);
        }
        #endregion

        #region OrderProcess
        /// <summary>
        /// 결재완료처리 Process 입니다.
        /// </summary>
        /// <param name="imp_key"></param>
        /// <param name="imp_secret"></param>
        /// <param name="orderId"></param>
        /// <param name="payCode"></param>
        private void OrderProcess(string imp_key, string imp_secret, string merchant_uid, string imp_uid)
        {
            string orderId = merchant_uid;
            string payCode = imp_uid;

            #region Get Tocken

            TockenMessage tockenMessage = GetTockenPostData(new IamPortTocken() { imp_key = imp_key, imp_secret = imp_secret }, "https://api.iamport.kr/users/getToken");

            #endregion

            #region Get Order Status
            string tockenKey = tockenMessage.response.access_token;

            string url = "https://api.iamport.kr/payments/" + imp_uid;
            string responseText = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = 30 * 1000; // 30초
            request.Headers.Add("Authorization", "Bearer " + tockenKey); // 헤더 추가 방법

            using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
            {
                HttpStatusCode status = resp.StatusCode;
                Console.WriteLine(status);  // 정상이면 "OK"

                Stream respStream = resp.GetResponseStream();
                using (StreamReader sr = new StreamReader(respStream))
                {
                    responseText = sr.ReadToEnd();
                }
            }
            PayStatusMessage payStatusMessage = JsonConvert.DeserializeObject<PayStatusMessage>(responseText);
            string paystatus = payStatusMessage.response.status;
            string returnstatus = "falied";

            if (paystatus == "paid")
            {
                // SUCESS
                returnstatus = "PAID";
            }
            else
            {
                // CANCEL
                returnstatus = "CANCEL";
            }
            #endregion

            // 결제 이력을 위한 에러로그 메세지
            string errorMessage = "";
            errorMessage += "\r\n orderId :  " + orderId;

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    int i = -1;

                    MySqlCommand mySqlCommand = null;
                    string sql = "";
                    int stampCnt = 0;
                    // 결제가 정상적으로 된 경우 처리
                    if (returnstatus == "PAID")
                    {
                        DateTime nowDateTime = DateTime.Now;

                        #region 기존 주문 정보 가져오기

                        sql = @"
                        SELECT A.*, SUM(B.QUANTITY) ORDER_COUNT 
                          FROM `order` AS A, order_item B 
                         WHERE A.ORDER_ID = @ORDER_ID 
                           AND A.ORDER_ID = B.ORDER_ID ";

                        mySqlCommand = new MySqlCommand(sql, connection);
                        mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        errorMessage += "\r\n START order select ";
                        MySqlDataReader dataReader = mySqlCommand.ExecuteReader();
                        errorMessage += "\r\n END order select ";

                        DataTable dt = new DataTable();
                        dt.Load(dataReader);
                        dataReader.Close();

                        List<OrderModel> orderModelList = Function.CreateListFromTable<OrderModel>(dt);

                        errorMessage += "\r\n orderModelList Count : " + orderModelList.Count;

                        OrderModel orderModel = orderModelList[0];

                        errorMessage += "\r\n ORDER_ID :  " + orderModel.ORDER_ID;
                        errorMessage += "\r\n MEMBER_ID :  " + orderModel.MEMBER_ID;
                        errorMessage += "\r\n STORE_ID :  " + orderModel.STORE_ID;
                        errorMessage += "\r\n PRICE :  " + orderModel.PRICE;

                        DataRow dr = dt.Rows[0];
                        orderModel.ORDER_COUNT = double.Parse(dr["ORDER_COUNT"].ToString());

                        errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;
                        errorMessage += "\r\n COUPON_ITEM_ID :  " + orderModel.COUPON_ITEM_ID;

                        #endregion

                        // Store 정보는 카드 결재 취소에 필요한 IMPORT 정보 조회를 위함
                        #region Store 정보 Select

                        sql = @"
                            SELECT * FROM `store` WHERE STORE_ID = @STORE_ID ";

                        MySqlCommand storeCommand = new MySqlCommand(sql, connection);
                        storeCommand.Parameters.Add(new MySqlParameter("@STORE_ID", orderModel.STORE_ID));

                        errorMessage += "\r\n START store";

                        MySqlDataReader storeDataReader = storeCommand.ExecuteReader();

                        errorMessage += "\r\n END store";

                        DataTable dtStore = new DataTable();
                        dtStore.Load(storeDataReader);
                        storeDataReader.Close();

                        List<StoreModel> storeModelList = Function.CreateListFromTable<StoreModel>(dtStore);

                        errorMessage += $"\r\n storeModelList : {storeModelList.Count}";
                        
                        StoreModel storeModel = storeModelList[0];

                        errorMessage += $"\r\n storeModel.COUPON_USE_YN : {storeModel.COUPON_USE_YN}";

                        #endregion

                        // 쿠폰 사용 매장인 경우
                        if (storeModel.COUPON_USE_YN == "Y")
                        {
                            #region 쿠폰 기준 정보 가져오기

                            sql = @"
                        SELECT * FROM saving_standard";

                            mySqlCommand = new MySqlCommand(sql, connection);

                            errorMessage += "\r\n START saving_standard select ";
                            dataReader = mySqlCommand.ExecuteReader();
                            errorMessage += "\r\n END saving_standard select ";

                            dt = new DataTable();
                            dt.Load(dataReader);
                            dataReader.Close();

                            List<SavingStandard> savingStandardList = Function.CreateListFromTable<SavingStandard>(dt);

                            errorMessage += "\r\n savingStandardList Count : " + savingStandardList.Count;

                            #endregion

                            #region 쿠폰을 사용한 경우 - 스템프 대상 여부 확인 쿠폰 사용 시 쿠폰 사용 처리

                            // 나중에 쿠폰 적립 시 제외할 수량 - 스템프 적립기준이 QUANTITY 인 경우 필요
                            int exceptStampCount = 0;

                            // 스템프이 적용된 경우
                            if (!string.IsNullOrEmpty(orderModel.COUPON_NO))
                            {
                                #region 쿠폰 사용처리 - (USEABLE_COUNT -1) (USE_COUNT+1)
                                sql = @"
                            UPDATE coupon SET USE_COUNT = USE_COUNT+1 WHERE COUPON_NO = @COUPON_NO ";
                                // UPDATE coupon SET USEABLE_COUNT = USEABLE_COUNT-1, USE_COUNT = USE_COUNT+1 WHERE COUPON_NO = @COUPON_NO ";

                                mySqlCommand = new MySqlCommand(sql, connection);
                                mySqlCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", orderModel.COUPON_NO));

                                errorMessage += "\r\n START coupon update ";
                                i = mySqlCommand.ExecuteNonQuery();
                                errorMessage += "\r\n END coupon update ";
                                #endregion

                                // 특정 아이템에 적용된 경우
                                if (orderModel.COUPON_ITEM_ID != 0)
                                {
                                    // 현재 아이템이 쿠폰적용 대상인지 확인합니다.
                                    // 스템프 적용 대상이면 나중에 쿠폰 계산시 하나를 빼줘야하고 쿠폰대상이 아니라면 빼줄필요없다.
                                    sql = @"
SELECT *
FROM item
WHERE STAMP_YN = 'Y'  
  AND ITEM_ID = @ITEM_ID";
                                    mySqlCommand = new MySqlCommand(sql, connection);
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", orderModel.COUPON_ITEM_ID));

                                    errorMessage += "\r\n START STAMP_YN ";
                                    var existStampReader = mySqlCommand.ExecuteReader();
                                    errorMessage += "\r\n START STAMP_YN ";

                                    bool existStampItem = existStampReader.HasRows;
                                    existStampReader.Close();

                                    // 나중에 스템프 갯수에서 제외할 갯수입니다.
                                    if (existStampItem)
                                    {
                                        exceptStampCount = 1;
                                    }
                                }
                                else // 할인 및 금액 쿠폰을 먹인 경우
                                {
                                }
                            }

                            #endregion

                            // 최종 쿠폰 처리 갯수를 계산합니다.
                            int orderStampCount = 0;

                            // 스템프 발생 기준 QUANTOTY / PRICE
                            string stampSavingType = StandardValue.QUNATITY;
                            var stampSavingTypeItem = savingStandardList.Where(c => c.STANDARD_TYPE == StandardType.STAMP && c.STANDARD_CODE == StandardCode.SAVING_STANDARD).FirstOrDefault();
                            if (stampSavingTypeItem != null)
                            {
                                stampSavingType = stampSavingTypeItem.STANDARD_VALUE;
                            }

                            // 발행기준값
                            int stampDivideValue = 1;
                            var stampDivideValueItem = savingStandardList.Where(c => c.STANDARD_TYPE == StandardType.STAMP && c.STANDARD_CODE == StandardCode.STANDARD_VALUE).FirstOrDefault();
                            if (stampDivideValueItem != null)
                            {
                                stampDivideValue = int.Parse(stampDivideValueItem.STANDARD_VALUE);
                            }

                            if (stampSavingType == StandardValue.QUNATITY)
                            {
                                #region QUNATITY

                                // 스템프 적용 항목 갯수를 가져옵니다.
                                sql = @"
SELECT SUM(A.QUANTITY) ORDER_COUNT 
FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
WHERE A.ORDER_ID = @ORDER_ID";

                                mySqlCommand = new MySqlCommand(sql, connection);
                                mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                                errorMessage += "\r\n START order stamp ";
                                string orderCountString = mySqlCommand.ExecuteScalar()?.ToString();
                                errorMessage += "\r\n END order stamp ";

                                int stampCount = 0;
                                if (!string.IsNullOrEmpty(orderCountString))
                                {
                                    stampCount = int.Parse(orderCountString);
                                }

                                // 발생된 Stamp 갯수
                                orderStampCount = stampCount - exceptStampCount;

                                orderStampCount = orderStampCount / stampDivideValue;

                                #endregion
                            }
                            else
                            {
                                #region PRICE

                                orderStampCount = Convert.ToInt32(orderModel.PRICE) / stampDivideValue;

                                #endregion
                            }

                            stampCnt = orderStampCount;

                            #region 주문에 발생된 Stamp 수 처리 - order_stamp 테이블 Insert - 제거

                            //                            sql = $@"
                            //INSERT INTO `order_stamp` (`ORDER_ID`, `MEMBER_ID`, `COUNT`, `USE_YN`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                            //                          (@ORDER_ID,  @MEMBER_ID,  @STAMP_COUNT,   @USE_YN,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                            //                            mySqlCommand = new MySqlCommand(sql, connection);

                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@USE_YN", "N"));
                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            //                            mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            //                            errorMessage += "\r\n START order_stamp ";
                            //                            i += mySqlCommand.ExecuteNonQuery();
                            //                            errorMessage += "\r\n END order_stamp ";

                            #endregion

                            bool isCreateMemberStamp = false;

                            #region Stamp 테이블 조회 총 Stamp 구하기

                            // 스템프 적용 항목 갯수를 가져옵니다.
                            sql = @"
SELECT STAMP_COUNT
FROM `member_stamp` 
WHERE MEMBER_ID = @MEMBER_ID";

                            mySqlCommand = new MySqlCommand(sql, connection);
                            mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START select order_stamp ";
                            string memberStampCountString = mySqlCommand.ExecuteScalar()?.ToString();
                            errorMessage += "\r\n END select order_stamp ";

                            int memberStampCount = 0;
                            if (!string.IsNullOrEmpty(memberStampCountString))
                            {
                                memberStampCount = int.Parse(memberStampCountString);
                            }

                            if (string.IsNullOrEmpty(memberStampCountString))
                            {
                                isCreateMemberStamp = true;
                            }

                            // 기존 있던 Stamp 와 현재 발생된 Stamp 를 더함
                            int allStamp = orderStampCount + memberStampCount;

                            int stampByCoupon = 10;
                            var stampByCouponItem = savingStandardList.Where(c => c.STANDARD_TYPE == StandardType.COUPON && c.STANDARD_CODE == StandardCode.STAMP_QUANTITY).FirstOrDefault();
                            if (stampByCouponItem != null)
                            {
                                stampByCoupon = int.Parse(stampByCouponItem.STANDARD_VALUE);
                            }

                            int couponCount = 0;

                            if (allStamp >= stampByCoupon)
                            {
                                // 발행될 쿠폰 갯수
                                couponCount = allStamp / stampByCoupon;

                                // 남은 쿠폰 갯수
                                allStamp = allStamp % stampByCoupon;
                            }

                            #endregion

                            #region Coupon 발생

                            if (couponCount > 0)
                            {
                                for (int c = 1; c <= couponCount; c++)
                                {
                                    sql = $@"

	INSERT INTO COUPON
		(
			COUPON_NO
			,COUPON_TYPE
			,USEABLE_COUNT
			,USE_START_DATE
			,USE_END_DATE
			,MEMBER_ID
			,CREATE_USER
		)
		VALUES
		(
			 @COUPON_NO
			,@COUPON_TYPE
			,@USEABLE_COUNT
			,@USE_START_DATE
			,@USE_END_DATE
            ,@MEMBER_ID
            ,@CREATE_USER
		)
";

                                    int couponByDay = 365;
                                    var couponByDayItem = savingStandardList.Where(p => p.STANDARD_TYPE == StandardType.COUPON && p.STANDARD_CODE == StandardCode.USEABLE_DAYS).FirstOrDefault();
                                    if (couponByDayItem != null)
                                    {
                                        couponByDay = int.Parse(couponByDayItem.STANDARD_VALUE);
                                    }

                                    // 20210216121540_00003
                                    // yyMMddHHmmss#####
                                    string couponNo = DateTime.Now.ToString("yyMMddHHmmss") + c.ToString("D5");

                                    mySqlCommand = new MySqlCommand(sql, connection);

                                    mySqlCommand.Parameters.Add(new MySqlParameter("@COUPON_NO", couponNo));
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@COUPON_TYPE", "SAVING"));
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@USEABLE_COUNT", 1));
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@USE_START_DATE", nowDateTime.ToString("yyyy-MM-dd")));
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@USE_END_DATE", nowDateTime.AddDays(couponByDay).ToString("yyyy-MM-dd")));
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                                    mySqlCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", 1));

                                    errorMessage += "\r\n START COUPON insert = " + c.ToString();
                                    i += mySqlCommand.ExecuteNonQuery();
                                    errorMessage += "\r\n END COUPON insert = " + c.ToString();
                                }
                            }

                            #endregion

                            #region 남은 Stamp 수 Insert - member_stamp 테이블 Insert

                            if (isCreateMemberStamp)
                            {
                                // 신규  Insert
                                sql = $@"
INSERT INTO `member_stamp` (`MEMBER_ID`, `STAMP_COUNT`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@MEMBER_ID,  @STAMP_COUNT, @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                                mySqlCommand = new MySqlCommand(sql, connection);

                                mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", allStamp));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                                errorMessage += "\r\n START member_stamp insert";
                                i += mySqlCommand.ExecuteNonQuery();
                                errorMessage += "\r\n END member_stamp insert";
                            }
                            else
                            {
                                //UPDATE member SET MEMBER_NICKNAME = @NICKNAME WHERE MEMBER_ID = @MEMBER_ID ";
                                sql = $@"
UPDATE `member_stamp` 
   SET `STAMP_COUNT` = @STAMP_COUNT, `UPDATE_DATE` = @UPDATE_DATE, `UPDATE_USER` = @UPDATE_USER
 WHERE `MEMBER_ID` = @MEMBER_ID";

                                mySqlCommand = new MySqlCommand(sql, connection);

                                mySqlCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", allStamp));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                                mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                                errorMessage += "\r\n START member_stamp update";
                                i += mySqlCommand.ExecuteNonQuery();
                                errorMessage += "\r\n END member_stamp update";
                            }

                            #endregion
                        }

                        #region Pay 테이블 Insert

                        string paySql = $@"
INSERT INTO `pay` (`PAY_CODE`, `PAY_TYPE`, `PAY_PRICE`, `PAY_DATE`, `ORDER_ID`) VALUES
                  (@PAY_CODE,  @PAY_TYPE,  @PAY_PRICE,  @PAY_DATE,  @ORDER_ID) ";

                        MySqlCommand payCommand = new MySqlCommand(paySql, connection);

                        payCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_TYPE", "CARD"));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_PRICE", orderModel.PRICE));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_DATE", nowDateTime));
                        payCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        i += payCommand.ExecuteNonQuery();

                        #endregion

                        #region USP_KIOSK_WAITING_INS 호출하여 Waiting 상태로 만듭니다

                        // ORDER 테이블 ORDER_STATUS ='WAITING' 변경, STORE_ORDER 테이블 Insert
                        string orderWaitSql = "USP_KIOSK_WAITING_INS ";

                        MySqlCommand orderwaitCommand = new MySqlCommand(orderWaitSql, connection);
                        orderwaitCommand.CommandType = CommandType.StoredProcedure;

                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_STORE_ID", orderModel.STORE_ID));
                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_ORDER_ID", orderId));

                        i += orderwaitCommand.ExecuteNonQuery();

                        #endregion
                    }

                    #region order 테이블 status/StampCount update

                    sql = @"
                    UPDATE `order` 
                    SET 
                        ORDER_STATUS = @ORDER_STATUS,  
                        PAY_CODE     = @PAY_CODE, 
                        UPDATE_DATE  = @UPDATE_DATE, 
                        STAMP_CNT    = @STAMP_CNT 
                    WHERE ORDER_ID = @ORDER_ID ";

                    mySqlCommand = new MySqlCommand(sql, connection);
                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", returnstatus));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@STAMP_CNT", stampCnt));

                    errorMessage += "\r\n START order update";
                    i = mySqlCommand.ExecuteNonQuery();
                    errorMessage += "\r\n END order update ";

                    #endregion
                }
                catch (Exception ex)
                {
                    string errorfilename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                    File.WriteAllText($@"D:\IIS\Auosms\M\Order\{errorfilename}_Error.txt", $"imp_uid : {imp_uid}, merchant_uid : {merchant_uid} = {returnstatus} \r\nex : {errorMessage} \r\n{ex.ToString()}");
                }
                finally
                {
                    connection.Close();
                }
            }

            string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

            File.WriteAllText($@"D:\IIS\Auosms\M\Order\{filename}.txt", $"imp_uid : {imp_uid}, merchant_uid : {merchant_uid} = {returnstatus} Message : {errorMessage}");

        }
        #endregion

        // 결제취소 ==================================================================================================================================================================
        #region OrderCancel - 결제 취소 시 처리하는 로직입니다.

        /// <summary>
        /// 결제 취소 시 처리하는 로직입니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        //[HttpPost]
        [Route("OrderCancel")]
        public void OrderCancel([FromBody] dynamic value)
        {
            string resultMessage = string.Empty;
            string resultDetailMessage = string.Empty;

            try
            {
                string jsonValue = JsonConvert.SerializeObject(value);

                RequestModel requestModel = JsonConvert.DeserializeObject<RequestModel>(jsonValue);

                // imp_uid/취소사유
                string[] loginInfos = requestModel.RequestMessage.Split('∀');

                TockenMessage tockenMessage = GetTockenPostData(new IamPortTocken() { imp_key = "5528248102920909", imp_secret = "hpD1tTi2qwVEZOQZ2Me6Hk2KArLSf7Jwxmy1gR3GnZknNanQXBWFVZpwqboFj8cQvKSLCVs0VMnIXKyV" }, "https://api.iamport.kr/users/getToken");

                string tockenKey = tockenMessage.response.access_token;
                string impuid = loginInfos[0];
                string reason = loginInfos[1];

                string cancelResult = GetOrderCancelData(new OrderCancelData() { imp_uid = impuid, reason = reason }, tockenKey, "https://api.iamport.kr/payments/cancel");
            }
            catch (Exception ex)
            {
                //return new ResultModel() { ResultMessage = resultMessage + ex.ToString(), ResultCode = ResultCode.Fail };
            }

            // 기존의 주문 정보 조회가 필요할수도 있음 (참고코드)
            /*
            string tockenKey = tockenMessage.response.access_token;
            string orderId = iamPortResult.merchant_uid;
            string payCode = iamPortResult.imp_uid;
            string url = "https://api.iamport.kr/payments/" + iamPortResult.imp_uid;  //테스트 사이트
            string responseText = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = 30 * 1000; // 30초
            request.Headers.Add("Authorization", "Bearer " + tockenKey); // 헤더 추가 방법

            using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
            {
                HttpStatusCode status = resp.StatusCode;
                Console.WriteLine(status);  // 정상이면 "OK"

                Stream respStream = resp.GetResponseStream();
                using (StreamReader sr = new StreamReader(respStream))
                {
                    responseText = sr.ReadToEnd();
                }
            }
            PayStatusMessage payStatusMessage = JsonConvert.DeserializeObject<PayStatusMessage>(responseText);
            string paystatus = payStatusMessage.response.status;
             */

            string errorMessage = "";
            //errorMessage += "\r\n orderId :  " + orderId;

            #region SQL 작업
//            using (MySqlConnection connection = new MySqlConnection(connectionString))
//            {
//                try
//                {
//                    connection.Open();

//                    int i = -1;

//                    string sql = @"
//UPDATE `order` SET ORDER_STATUS = @ORDER_STATUS,  PAY_CODE = @PAY_CODE, UPDATE_DATE = @UPDATE_DATE WHERE ORDER_ID = @ORDER_ID ";

//                    MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

//                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
//                    mySqlCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
//                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", returnstatus));
//                    mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));

//                    errorMessage += "\r\n START sql ";
//                    i = mySqlCommand.ExecuteNonQuery();
//                    errorMessage += "\r\n END sql ";

//                    if (returnstatus == "PAID")
//                    {
//                        DateTime nowDateTime = DateTime.Now;

//                        #region 기준 주문 정보 가져오기
//                        string ordersql = @"
//SELECT A.*, SUM(B.QUANTITY) ORDER_COUNT FROM `order` AS A, order_item B WHERE A.ORDER_ID = @ORDER_ID AND A.ORDER_ID = B.ORDER_ID ";

//                        MySqlCommand storeidCommand = new MySqlCommand(ordersql, connection);
//                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

//                        errorMessage += "\r\n START storeidCommand ";

//                        MySqlDataReader dataReader = storeidCommand.ExecuteReader();

//                        errorMessage += "\r\n END storeidCommand ";

//                        DataTable dt = new DataTable();
//                        dt.Load(dataReader);
//                        dataReader.Close();

//                        DataRow dr = dt.Rows[0];

//                        errorMessage += "\r\n dt ORDER_ID :  " + dr["ORDER_ID"].ToString();
//                        errorMessage += "\r\n dt MEMBER_ID :  " + dr["MEMBER_ID"].ToString();
//                        errorMessage += "\r\n dt STORE_ID :  " + dr["STORE_ID"].ToString();
//                        errorMessage += "\r\n dt PRICE :  " + dr["PRICE"].ToString();
//                        errorMessage += "\r\n dt ORDER_COUNT :  " + dr["ORDER_COUNT"].ToString();
//                        errorMessage += "\r\n dt COUPON_ITEM_ID :  " + dr["COUPON_ITEM_ID"].ToString();

//                        List<OrderModel> orderModelList = Function.CreateListFromTable<OrderModel>(dt);

//                        errorMessage += "\r\n orderModelList Count : " + orderModelList.Count;

//                        OrderModel orderModel = orderModelList[0];

//                        errorMessage += "\r\n ORDER_ID :  " + orderModel.ORDER_ID;
//                        errorMessage += "\r\n MEMBER_ID :  " + orderModel.MEMBER_ID;
//                        errorMessage += "\r\n STORE_ID :  " + orderModel.STORE_ID;
//                        errorMessage += "\r\n PRICE :  " + orderModel.PRICE;
//                        errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;
//                        errorMessage += "\r\n COUPON_ITEM_ID :  " + orderModel.COUPON_ITEM_ID;

//                        errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;

//                        //                        ordersql = @"
//                        //SELECT SUM(QUANTITY) ORDER_COUNT FROM order_item WHERE ORDER_ID = @ORDER_ID ";

//                        int couponCount = 0;

//                        // 쿠폰이 적용된 경우
//                        if (orderModel.COUPON_ITEM_ID != 0)
//                        {
//                            // 현재 아이템이 쿠폰적용 대상인지 확인합니다.
//                            ordersql = @"
//SELECT *
//FROM item
//WHERE STAMP_YN='Y'  
//  AND ITEM_ID = @ITEM_ID";
//                            storeidCommand = new MySqlCommand(ordersql, connection);
//                            storeidCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", orderModel.COUPON_ITEM_ID));

//                            var existStampReader = storeidCommand.ExecuteReader();
//                            bool existStampItem = existStampReader.HasRows;
//                            existStampReader.Close();

//                            // 나중에 스템프 갯수에서 제외할 갯수입니다.
//                            if (existStampItem)
//                            {
//                                couponCount = 1;
//                            }

//                            ordersql = @"
//SELECT SUM(A.QUANTITY) ORDER_COUNT 
//FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
//WHERE A.ORDER_ID = @ORDER_ID";
//                        }
//                        else
//                        {
//                            ordersql = @"
//SELECT SUM(A.QUANTITY) ORDER_COUNT 
//FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
//WHERE A.ORDER_ID = @ORDER_ID";
//                        }

//                        storeidCommand = new MySqlCommand(ordersql, connection);
//                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

//                        errorMessage += "\r\n START storeidCommand ";

//                        string orderCount = storeidCommand.ExecuteScalar()?.ToString();
//                        if (string.IsNullOrEmpty(orderCount))
//                        {
//                            orderModel.ORDER_COUNT = 0;
//                        }
//                        else
//                        {
//                            orderModel.ORDER_COUNT = long.Parse(orderCount);
//                        }

//                        errorMessage += "\r\n ORDER_COUNT2 :  " + orderModel.ORDER_COUNT;

//                        #endregion

//                        #region Stamp 테이블 Insert

//                        double orderStampCount = orderModel.ORDER_COUNT - couponCount;

//                        string stampSql = $@"
//INSERT INTO `order_stamp` (`ORDER_ID`, `MEMBER_ID`, `COUNT`, `USE_YN`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
//                          (@ORDER_ID,  @MEMBER_ID,  @STAMP_COUNT,   @USE_YN,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

//                        MySqlCommand stampCommand = new MySqlCommand(stampSql, connection);

//                        stampCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
//                        stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
//                        stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
//                        stampCommand.Parameters.Add(new MySqlParameter("@USE_YN", "N"));
//                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
//                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
//                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
//                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

//                        errorMessage += "\r\n START stampCommand ";
//                        i += stampCommand.ExecuteNonQuery();
//                        errorMessage += "\r\n END stampCommand ";
//                        #endregion

//                        #region member_stamp 테이블 Insert
//                        stampSql = "SELECT STAMP_COUNT FROM member_stamp WHERE MEMBER_ID = @MEMBER_ID";

//                        storeidCommand = new MySqlCommand(stampSql, connection);
//                        storeidCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));

//                        errorMessage += "\r\n START member_stamp ";

//                        errorMessage += "\r\n MEMBER_ID : " + orderModel.MEMBER_ID;

//                        string stampCount = storeidCommand.ExecuteScalar()?.ToString();

//                        errorMessage += "\r\n STAMP_COUNT : " + stampCount ?? "";

//                        if (stampCount == null)
//                        {
//                            // 신규  Insert
//                            stampSql = $@"
//INSERT INTO `member_stamp` (`MEMBER_ID`, `STAMP_COUNT`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
//                          (@MEMBER_ID,  @STAMP_COUNT, @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

//                            stampCommand = new MySqlCommand(stampSql, connection);

//                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
//                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
//                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
//                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
//                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
//                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

//                            errorMessage += "\r\n START member_stamp insert";
//                            i += stampCommand.ExecuteNonQuery();
//                            errorMessage += "\r\n END member_stamp insert";
//                        }
//                        else
//                        {
//                            // 기존 수에 더하기
//                            int sumStampCount = int.Parse(stampCount) + (int)orderStampCount;

//                            if (couponCount != 0)
//                            {
//                                // 기존 수에서 10개를 뺀다.
//                                sumStampCount = sumStampCount - 10;
//                            }

//                            //UPDATE member SET MEMBER_NICKNAME = @NICKNAME WHERE MEMBER_ID = @MEMBER_ID ";
//                            stampSql = $@"
//UPDATE `member_stamp` 
//   SET `STAMP_COUNT` = @STAMP_COUNT, `UPDATE_DATE` = @UPDATE_DATE, `UPDATE_USER` = @UPDATE_USER
// WHERE `MEMBER_ID` = @MEMBER_ID";

//                            stampCommand = new MySqlCommand(stampSql, connection);

//                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", sumStampCount));
//                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
//                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
//                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

//                            errorMessage += "\r\n START member_stamp update";
//                            i += stampCommand.ExecuteNonQuery();
//                            errorMessage += "\r\n END member_stamp update";
//                        }

//                        #endregion

//                        #region Pay 테이블 Insert

//                        string paySql = $@"
//INSERT INTO `pay` (`PAY_CODE`, `PAY_TYPE`, `PAY_PRICE`, `PAY_DATE`, `ORDER_ID`) VALUES
//                  (@PAY_CODE,  @PAY_TYPE,  @PAY_PRICE,  @PAY_DATE,  @ORDER_ID) ";

//                        MySqlCommand payCommand = new MySqlCommand(paySql, connection);

//                        payCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
//                        payCommand.Parameters.Add(new MySqlParameter("@PAY_TYPE", "CARD"));
//                        payCommand.Parameters.Add(new MySqlParameter("@PAY_PRICE", orderModel.PRICE));
//                        payCommand.Parameters.Add(new MySqlParameter("@PAY_DATE", nowDateTime));
//                        payCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

//                        i += payCommand.ExecuteNonQuery();

//                        #endregion

//                        // ORDER 테이블 ORDER_STATUS ='WAITING' 변경, STORE_ORDER 테이블 Insert
//                        string orderWaitSql = "USP_KIOSK_WAITING_INS ";

//                        MySqlCommand orderwaitCommand = new MySqlCommand(orderWaitSql, connection);
//                        orderwaitCommand.CommandType = CommandType.StoredProcedure;

//                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_STORE_ID", orderModel.STORE_ID));
//                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_ORDER_ID", orderId));

//                        i += orderwaitCommand.ExecuteNonQuery();
//                    }
//                }
//                catch (Exception ex)
//                {
//                    string errorfilename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
//                    //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{errorfilename}_Error.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} \r\nex : {errorMessage} \r\n{ex.ToString()}");
//                }
//                finally
//                {
//                    connection.Close();
//                }
//            }
            #endregion

            string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

            //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{filename}.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} Message : {errorMessage}" );

            //return new PayResult() { status = returnstatus };
        }
        #endregion


        // ===========================================================================================================================================================================
        #region GetTockenPostData(object value, string url = null) - string

        /// <summary>
        /// WebApi 를 호출합니다.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private TockenMessage GetTockenPostData(object value, string url = null)
        {
            if (url == null) return null;

            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(url);
            // Set the Method property of the request to POST.
            request.Method = WebRequestMethods.Http.Post;
            // Create POST data and convert it to a byte array.
            string valueString = JsonConvert.SerializeObject(value);
            byte[] byteArray = Encoding.UTF8.GetBytes(valueString);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/json; charset=utf-8";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            System.Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return JsonConvert.DeserializeObject<TockenMessage>(responseFromServer);
        }

        #endregion

        #region GetOrderCancelData(object value, string url = null) - string

        /// <summary>
        /// WebApi 를 호출합니다.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        private string GetOrderCancelData(object value, string tockenKey, string url = null)
        {
            if (url == null) return null;

            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(url);

            // Set the Method property of the request to POST.
            request.Method = WebRequestMethods.Http.Post;
            //request.Headers.Add("Content-Type", "application/json");
            //request.Headers.Add("Authorization", tockenKey);
            request.Headers.Add("Authorization", "Bearer " + tockenKey); // 헤더 추가 방법

            // Create POST data and convert it to a byte array.
            string valueString = JsonConvert.SerializeObject(value);
            byte[] byteArray = Encoding.UTF8.GetBytes(valueString);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/json; charset=utf-8";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            System.Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return responseFromServer;
        }

        #endregion

        #region CardRegiste

        /// <summary>
        /// 요청에 대한 응답을 처리합니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        [HttpPost]
        [Route("CardRegiste")]
        public void CardRegiste([FromUri]IamPortResult iamPortResult)
        {
            try
            {
                string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

                //File.WriteAllText($@"D:\IIS\Auosms\M\Order\CardRegiste_{filename}.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid}, status : {iamPortResult.status ?? ""}");

                //return new PayResult() { status = returnstatus };
            }
            catch (Exception ex)
            {
                string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

                //File.WriteAllText($@"D:\IIS\Auosms\M\Order\CardRegiste_{filename}_Error.txt", $"Error Message : {ex.ToString()}");
            }
        }
        #endregion

        #region WebhookTest

        /// <summary>
        /// 요청에 대한 응답을 처리합니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("WebhookTest")]
        public void WebhookTest([FromUri]IamPortResult iamPortResult)
        {
            try
            {
                string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

                //File.WriteAllText($@"D:\IIS\Auosms\M\Order\WebhookTest_{filename}.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid}, status : {iamPortResult.status ?? ""}");

                //return new PayResult() { status = returnstatus };
            }
            catch(Exception ex)
            {
                string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

                //File.WriteAllText($@"D:\IIS\Auosms\M\Order\WebhookTest_{filename}_Error.txt", $"Error Message : {ex.ToString()}");
            }
        }
        #endregion

        #region Example
        // GET: api/Home
        public IEnumerable<string> Get(string value)
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Home/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Home
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Home/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Home/5
        public void Delete(int id)
        {
        }
        #endregion

        // 구버전
        #region OrderStatus - 결제 완료 시 처리하는 로직입니다.

        /// <summary>
        /// 결제 완료 시 처리하는 로직입니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        //[HttpPost]
        [Route("OrderStatus")]
        public void OrderStatus([FromUri] IamPortResult iamPortResult)
        {
            TockenMessage tockenMessage = GetTockenPostData(new IamPortTocken() { imp_key = "5528248102920909", imp_secret = "hpD1tTi2qwVEZOQZ2Me6Hk2KArLSf7Jwxmy1gR3GnZknNanQXBWFVZpwqboFj8cQvKSLCVs0VMnIXKyV" }, "https://api.iamport.kr/users/getToken");

            string tockenKey = tockenMessage.response.access_token;
            string orderId = iamPortResult.merchant_uid;
            string payCode = iamPortResult.imp_uid;
            string url = "https://api.iamport.kr/payments/" + iamPortResult.imp_uid;  //테스트 사이트
            string responseText = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = 30 * 1000; // 30초
            request.Headers.Add("Authorization", "Bearer " + tockenKey); // 헤더 추가 방법

            using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
            {
                HttpStatusCode status = resp.StatusCode;
                Console.WriteLine(status);  // 정상이면 "OK"

                Stream respStream = resp.GetResponseStream();
                using (StreamReader sr = new StreamReader(respStream))
                {
                    responseText = sr.ReadToEnd();
                }
            }
            PayStatusMessage payStatusMessage = JsonConvert.DeserializeObject<PayStatusMessage>(responseText);
            string paystatus = payStatusMessage.response.status;
            string returnstatus = "falied";

            if (paystatus == "paid")
            {
                // SUCESS
                returnstatus = "PAID";
            }
            else
            {
                // CANCEL
                returnstatus = "CANCEL";
            }

            /*
INSERT INTO `order` (`ORDER_ID`, `ORDER_DATE`, `STORE_ID`, `MEMBER_ID`, `PRICE`, `PAY_CODE`, `ARRIVE_DATE`, `ORDER_STATUS`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                    (@ORDER_ID,  @ORDER_DATE,  @STORE_ID,  @MEMBER_ID,  @PRICE,  @PAY_CODE,  @ARRIVE_DATE,  @ORDER_STATUS,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                        MySqlCommand orderCommand = new MySqlCommand(orderSql, connection);

                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_ID",     order.ORDER_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_DATE",   nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@STORE_ID",     order.STORE_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID",    memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@PRICE",        order.TOTAL_PRICE));
                        orderCommand.Parameters.Add(new MySqlParameter("@PAY_CODE",     payCode));
                        orderCommand.Parameters.Add(new MySqlParameter("@ARRIVE_DATE",  order.ARRIVE_DATE));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", payStatus));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE",  nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_USER",  memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE",  nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER",  memberID));
             */

            string errorMessage = "";
            errorMessage += "\r\n orderId :  " + orderId;

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    int i = -1;

                    string sql = @"
UPDATE `order` SET ORDER_STATUS = @ORDER_STATUS,  PAY_CODE = @PAY_CODE, UPDATE_DATE = @UPDATE_DATE WHERE ORDER_ID = @ORDER_ID ";

                    MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", returnstatus));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));

                    errorMessage += "\r\n START sql ";
                    i = mySqlCommand.ExecuteNonQuery();
                    errorMessage += "\r\n END sql ";

                    if (returnstatus == "PAID")
                    {
                        DateTime nowDateTime = DateTime.Now;

                        #region 기준 주문 정보 가져오기
                        string ordersql = @"
SELECT A.*, SUM(B.QUANTITY) ORDER_COUNT FROM `order` AS A, order_item B WHERE A.ORDER_ID = @ORDER_ID AND A.ORDER_ID = B.ORDER_ID ";

                        MySqlCommand storeidCommand = new MySqlCommand(ordersql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        errorMessage += "\r\n START storeidCommand ";

                        MySqlDataReader dataReader = storeidCommand.ExecuteReader();

                        errorMessage += "\r\n END storeidCommand ";

                        DataTable dt = new DataTable();
                        dt.Load(dataReader);
                        dataReader.Close();

                        DataRow dr = dt.Rows[0];

                        errorMessage += "\r\n dt ORDER_ID :  " + dr["ORDER_ID"].ToString();
                        errorMessage += "\r\n dt MEMBER_ID :  " + dr["MEMBER_ID"].ToString();
                        errorMessage += "\r\n dt STORE_ID :  " + dr["STORE_ID"].ToString();
                        errorMessage += "\r\n dt PRICE :  " + dr["PRICE"].ToString();
                        errorMessage += "\r\n dt ORDER_COUNT :  " + dr["ORDER_COUNT"].ToString();
                        errorMessage += "\r\n dt COUPON_ITEM_ID :  " + dr["COUPON_ITEM_ID"].ToString();

                        List<OrderModel> orderModelList = Function.CreateListFromTable<OrderModel>(dt);

                        errorMessage += "\r\n orderModelList Count : " + orderModelList.Count;

                        OrderModel orderModel = orderModelList[0];

                        errorMessage += "\r\n ORDER_ID :  " + orderModel.ORDER_ID;
                        errorMessage += "\r\n MEMBER_ID :  " + orderModel.MEMBER_ID;
                        errorMessage += "\r\n STORE_ID :  " + orderModel.STORE_ID;
                        errorMessage += "\r\n PRICE :  " + orderModel.PRICE;

                        orderModel.ORDER_COUNT = double.Parse(dr["ORDER_COUNT"].ToString());

                        errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;
                        errorMessage += "\r\n COUPON_ITEM_ID :  " + orderModel.COUPON_ITEM_ID;

                        //                        ordersql = @"
                        //SELECT SUM(QUANTITY) ORDER_COUNT FROM order_item WHERE ORDER_ID = @ORDER_ID ";

                        int couponCount = 0;

                        // 쿠폰이 적용된 경우
                        if (orderModel.COUPON_ITEM_ID != 0)
                        {
                            // 현재 아이템이 쿠폰적용 대상인지 확인합니다.
                            ordersql = @"
SELECT *
FROM item
WHERE STAMP_YN='Y'  
  AND ITEM_ID = @ITEM_ID";
                            storeidCommand = new MySqlCommand(ordersql, connection);
                            storeidCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", orderModel.COUPON_ITEM_ID));

                            var existStampReader = storeidCommand.ExecuteReader();
                            bool existStampItem = existStampReader.HasRows;
                            existStampReader.Close();

                            // 나중에 스템프 갯수에서 제외할 갯수입니다.
                            if (existStampItem)
                            {
                                couponCount = 1;
                            }

                            ordersql = @"
SELECT SUM(A.QUANTITY) ORDER_COUNT 
FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
WHERE A.ORDER_ID = @ORDER_ID";
                        }
                        else
                        {
                            ordersql = @"
SELECT SUM(A.QUANTITY) ORDER_COUNT 
FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
WHERE A.ORDER_ID = @ORDER_ID";
                        }

                        storeidCommand = new MySqlCommand(ordersql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        errorMessage += "\r\n START storeidCommand ";

                        string orderCount = storeidCommand.ExecuteScalar()?.ToString();
                        if (string.IsNullOrEmpty(orderCount))
                        {
                            orderModel.ORDER_COUNT = 0;
                        }
                        else
                        {
                            orderModel.ORDER_COUNT = long.Parse(orderCount);
                        }

                        errorMessage += "\r\n ORDER_COUNT2 :  " + orderModel.ORDER_COUNT;

                        #endregion

                        #region Stamp 테이블 Insert

                        double orderStampCount = orderModel.ORDER_COUNT - couponCount;

                        string stampSql = $@"
INSERT INTO `order_stamp` (`ORDER_ID`, `MEMBER_ID`, `COUNT`, `USE_YN`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@ORDER_ID,  @MEMBER_ID,  @STAMP_COUNT,   @USE_YN,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                        MySqlCommand stampCommand = new MySqlCommand(stampSql, connection);

                        stampCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                        stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                        stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                        stampCommand.Parameters.Add(new MySqlParameter("@USE_YN", "N"));
                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                        errorMessage += "\r\n START stampCommand ";
                        i += stampCommand.ExecuteNonQuery();
                        errorMessage += "\r\n END stampCommand ";
                        #endregion

                        #region member_stamp 테이블 Insert
                        stampSql = "SELECT STAMP_COUNT FROM member_stamp WHERE MEMBER_ID = @MEMBER_ID";

                        storeidCommand = new MySqlCommand(stampSql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));

                        errorMessage += "\r\n START member_stamp ";

                        errorMessage += "\r\n MEMBER_ID : " + orderModel.MEMBER_ID;

                        string stampCount = storeidCommand.ExecuteScalar()?.ToString();

                        errorMessage += "\r\n STAMP_COUNT : " + stampCount ?? "";

                        if (stampCount == null)
                        {
                            // 신규  Insert
                            stampSql = $@"
INSERT INTO `member_stamp` (`MEMBER_ID`, `STAMP_COUNT`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@MEMBER_ID,  @STAMP_COUNT, @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                            stampCommand = new MySqlCommand(stampSql, connection);

                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START member_stamp insert";
                            i += stampCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END member_stamp insert";
                        }
                        else
                        {
                            // 기존 수에 더하기
                            int sumStampCount = int.Parse(stampCount) + (int)orderStampCount;

                            if (couponCount != 0)
                            {
                                // 기존 수에서 10개를 뺀다.
                                sumStampCount = sumStampCount - 10;
                            }

                            //UPDATE member SET MEMBER_NICKNAME = @NICKNAME WHERE MEMBER_ID = @MEMBER_ID ";
                            stampSql = $@"
UPDATE `member_stamp` 
   SET `STAMP_COUNT` = @STAMP_COUNT, `UPDATE_DATE` = @UPDATE_DATE, `UPDATE_USER` = @UPDATE_USER
 WHERE `MEMBER_ID` = @MEMBER_ID";

                            stampCommand = new MySqlCommand(stampSql, connection);

                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", sumStampCount));
                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START member_stamp update";
                            i += stampCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END member_stamp update";
                        }

                        #endregion

                        #region Pay 테이블 Insert

                        string paySql = $@"
INSERT INTO `pay` (`PAY_CODE`, `PAY_TYPE`, `PAY_PRICE`, `PAY_DATE`, `ORDER_ID`) VALUES
                  (@PAY_CODE,  @PAY_TYPE,  @PAY_PRICE,  @PAY_DATE,  @ORDER_ID) ";

                        MySqlCommand payCommand = new MySqlCommand(paySql, connection);

                        payCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_TYPE", "CARD"));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_PRICE", orderModel.PRICE));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_DATE", nowDateTime));
                        payCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        i += payCommand.ExecuteNonQuery();

                        #endregion

                        // ORDER 테이블 ORDER_STATUS ='WAITING' 변경, STORE_ORDER 테이블 Insert
                        string orderWaitSql = "USP_KIOSK_WAITING_INS ";

                        MySqlCommand orderwaitCommand = new MySqlCommand(orderWaitSql, connection);
                        orderwaitCommand.CommandType = CommandType.StoredProcedure;

                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_STORE_ID", orderModel.STORE_ID));
                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_ORDER_ID", orderId));

                        i += orderwaitCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    string errorfilename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                    //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{errorfilename}_Error.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} \r\nex : {errorMessage} \r\n{ex.ToString()}");
                }
                finally
                {
                    connection.Close();
                }
            }

            string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

            //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{filename}.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} Message : {errorMessage}" );

            //return new PayResult() { status = returnstatus };
        }
        #endregion
        #region OrderStatus_imp02402838_old - 결제 완료 시 처리하는 로직입니다.

        /// <summary>
        /// 결제 완료 시 처리하는 로직입니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        //[HttpPost]
        [Route("OrderStatus_imp02402838_old")]
        public void OrderStatus_imp02402838_old([FromUri] IamPortResult iamPortResult)
        {
            TockenMessage tockenMessage = GetTockenPostData(new IamPortTocken() { imp_key = "5528248102920909", imp_secret = "hpD1tTi2qwVEZOQZ2Me6Hk2KArLSf7Jwxmy1gR3GnZknNanQXBWFVZpwqboFj8cQvKSLCVs0VMnIXKyV" }, "https://api.iamport.kr/users/getToken");

            string tockenKey = tockenMessage.response.access_token;
            string orderId = iamPortResult.merchant_uid;
            string payCode = iamPortResult.imp_uid;
            string url = "https://api.iamport.kr/payments/" + iamPortResult.imp_uid;  //테스트 사이트
            string responseText = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = 30 * 1000; // 30초
            request.Headers.Add("Authorization", "Bearer " + tockenKey); // 헤더 추가 방법

            using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
            {
                HttpStatusCode status = resp.StatusCode;
                Console.WriteLine(status);  // 정상이면 "OK"

                Stream respStream = resp.GetResponseStream();
                using (StreamReader sr = new StreamReader(respStream))
                {
                    responseText = sr.ReadToEnd();
                }
            }
            PayStatusMessage payStatusMessage = JsonConvert.DeserializeObject<PayStatusMessage>(responseText);
            string paystatus = payStatusMessage.response.status;
            string returnstatus = "falied";

            if (paystatus == "paid")
            {
                // SUCESS
                returnstatus = "PAID";
            }
            else
            {
                // CANCEL
                returnstatus = "CANCEL";
            }

            /*
INSERT INTO `order` (`ORDER_ID`, `ORDER_DATE`, `STORE_ID`, `MEMBER_ID`, `PRICE`, `PAY_CODE`, `ARRIVE_DATE`, `ORDER_STATUS`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                    (@ORDER_ID,  @ORDER_DATE,  @STORE_ID,  @MEMBER_ID,  @PRICE,  @PAY_CODE,  @ARRIVE_DATE,  @ORDER_STATUS,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                        MySqlCommand orderCommand = new MySqlCommand(orderSql, connection);

                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_ID",     order.ORDER_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_DATE",   nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@STORE_ID",     order.STORE_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID",    memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@PRICE",        order.TOTAL_PRICE));
                        orderCommand.Parameters.Add(new MySqlParameter("@PAY_CODE",     payCode));
                        orderCommand.Parameters.Add(new MySqlParameter("@ARRIVE_DATE",  order.ARRIVE_DATE));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", payStatus));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE",  nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_USER",  memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE",  nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER",  memberID));
             */

            string errorMessage = "";
            errorMessage += "\r\n orderId :  " + orderId;

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    int i = -1;

                    string sql = @"
UPDATE `order` SET ORDER_STATUS = @ORDER_STATUS,  PAY_CODE = @PAY_CODE, UPDATE_DATE = @UPDATE_DATE WHERE ORDER_ID = @ORDER_ID ";

                    MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", returnstatus));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));

                    errorMessage += "\r\n START sql ";
                    i = mySqlCommand.ExecuteNonQuery();
                    errorMessage += "\r\n END sql ";

                    // 결제가 정상적으로 된 경우 처리
                    if (returnstatus == "PAID")
                    {
                        DateTime nowDateTime = DateTime.Now;

                        #region 기준 주문 정보 가져오기
                        string ordersql = @"
SELECT A.*, SUM(B.QUANTITY) ORDER_COUNT FROM `order` AS A, order_item B WHERE A.ORDER_ID = @ORDER_ID AND A.ORDER_ID = B.ORDER_ID ";

                        MySqlCommand storeidCommand = new MySqlCommand(ordersql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        errorMessage += "\r\n START storeidCommand ";

                        MySqlDataReader dataReader = storeidCommand.ExecuteReader();

                        errorMessage += "\r\n END storeidCommand ";

                        DataTable dt = new DataTable();
                        dt.Load(dataReader);
                        dataReader.Close();

                        DataRow dr = dt.Rows[0];

                        errorMessage += "\r\n dt ORDER_ID :  " + dr["ORDER_ID"].ToString();
                        errorMessage += "\r\n dt MEMBER_ID :  " + dr["MEMBER_ID"].ToString();
                        errorMessage += "\r\n dt STORE_ID :  " + dr["STORE_ID"].ToString();
                        errorMessage += "\r\n dt PRICE :  " + dr["PRICE"].ToString();
                        errorMessage += "\r\n dt ORDER_COUNT :  " + dr["ORDER_COUNT"].ToString();
                        errorMessage += "\r\n dt COUPON_ITEM_ID :  " + dr["COUPON_ITEM_ID"].ToString();

                        List<OrderModel> orderModelList = Function.CreateListFromTable<OrderModel>(dt);

                        errorMessage += "\r\n orderModelList Count : " + orderModelList.Count;

                        OrderModel orderModel = orderModelList[0];

                        errorMessage += "\r\n ORDER_ID :  " + orderModel.ORDER_ID;
                        errorMessage += "\r\n MEMBER_ID :  " + orderModel.MEMBER_ID;
                        errorMessage += "\r\n STORE_ID :  " + orderModel.STORE_ID;
                        errorMessage += "\r\n PRICE :  " + orderModel.PRICE;

                        orderModel.ORDER_COUNT = double.Parse(dr["ORDER_COUNT"].ToString());

                        errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;
                        errorMessage += "\r\n COUPON_ITEM_ID :  " + orderModel.COUPON_ITEM_ID;

                        //                        ordersql = @"
                        //SELECT SUM(QUANTITY) ORDER_COUNT FROM order_item WHERE ORDER_ID = @ORDER_ID ";

                        int couponCount = 0;

                        // 쿠폰이 적용된 경우
                        if (orderModel.COUPON_ITEM_ID != 0)
                        {
                            // 현재 아이템이 쿠폰적용 대상인지 확인합니다.
                            ordersql = @"
SELECT *
FROM item
WHERE STAMP_YN='Y'  
  AND ITEM_ID = @ITEM_ID";
                            storeidCommand = new MySqlCommand(ordersql, connection);
                            storeidCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", orderModel.COUPON_ITEM_ID));

                            var existStampReader = storeidCommand.ExecuteReader();
                            bool existStampItem = existStampReader.HasRows;
                            existStampReader.Close();

                            // 나중에 스템프 갯수에서 제외할 갯수입니다.
                            if (existStampItem)
                            {
                                couponCount = 1;
                            }

                            ordersql = @"
SELECT SUM(A.QUANTITY) ORDER_COUNT 
FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
WHERE A.ORDER_ID = @ORDER_ID";
                        }
                        else
                        {
                            ordersql = @"
SELECT SUM(A.QUANTITY) ORDER_COUNT 
FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
WHERE A.ORDER_ID = @ORDER_ID";
                        }

                        storeidCommand = new MySqlCommand(ordersql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        errorMessage += "\r\n START storeidCommand ";

                        string orderCount = storeidCommand.ExecuteScalar()?.ToString();
                        if (string.IsNullOrEmpty(orderCount))
                        {
                            orderModel.ORDER_COUNT = 0;
                        }
                        else
                        {
                            orderModel.ORDER_COUNT = long.Parse(orderCount);
                        }

                        errorMessage += "\r\n ORDER_COUNT2 :  " + orderModel.ORDER_COUNT;

                        #endregion

                        #region Stamp 테이블 Insert

                        double orderStampCount = orderModel.ORDER_COUNT - couponCount;

                        string stampSql = $@"
INSERT INTO `order_stamp` (`ORDER_ID`, `MEMBER_ID`, `COUNT`, `USE_YN`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@ORDER_ID,  @MEMBER_ID,  @STAMP_COUNT,   @USE_YN,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                        MySqlCommand stampCommand = new MySqlCommand(stampSql, connection);

                        stampCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                        stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                        stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                        stampCommand.Parameters.Add(new MySqlParameter("@USE_YN", "N"));
                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                        errorMessage += "\r\n START stampCommand ";
                        i += stampCommand.ExecuteNonQuery();
                        errorMessage += "\r\n END stampCommand ";
                        #endregion

                        #region member_stamp 테이블 Insert
                        stampSql = "SELECT STAMP_COUNT FROM member_stamp WHERE MEMBER_ID = @MEMBER_ID";

                        storeidCommand = new MySqlCommand(stampSql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));

                        errorMessage += "\r\n START member_stamp ";

                        errorMessage += "\r\n MEMBER_ID : " + orderModel.MEMBER_ID;

                        string stampCount = storeidCommand.ExecuteScalar()?.ToString();

                        errorMessage += "\r\n STAMP_COUNT : " + stampCount ?? "";

                        if (stampCount == null)
                        {
                            // 신규  Insert
                            stampSql = $@"
INSERT INTO `member_stamp` (`MEMBER_ID`, `STAMP_COUNT`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@MEMBER_ID,  @STAMP_COUNT, @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                            stampCommand = new MySqlCommand(stampSql, connection);

                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START member_stamp insert";
                            i += stampCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END member_stamp insert";
                        }
                        else
                        {
                            // 기존 수에 더하기
                            int sumStampCount = int.Parse(stampCount) + (int)orderStampCount;

                            if (couponCount != 0)
                            {
                                // 기존 수에서 10개를 뺀다.
                                sumStampCount = sumStampCount - 10;
                            }

                            //UPDATE member SET MEMBER_NICKNAME = @NICKNAME WHERE MEMBER_ID = @MEMBER_ID ";
                            stampSql = $@"
UPDATE `member_stamp` 
   SET `STAMP_COUNT` = @STAMP_COUNT, `UPDATE_DATE` = @UPDATE_DATE, `UPDATE_USER` = @UPDATE_USER
 WHERE `MEMBER_ID` = @MEMBER_ID";

                            stampCommand = new MySqlCommand(stampSql, connection);

                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", sumStampCount));
                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START member_stamp update";
                            i += stampCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END member_stamp update";
                        }

                        #endregion

                        #region Pay 테이블 Insert

                        string paySql = $@"
INSERT INTO `pay` (`PAY_CODE`, `PAY_TYPE`, `PAY_PRICE`, `PAY_DATE`, `ORDER_ID`) VALUES
                  (@PAY_CODE,  @PAY_TYPE,  @PAY_PRICE,  @PAY_DATE,  @ORDER_ID) ";

                        MySqlCommand payCommand = new MySqlCommand(paySql, connection);

                        payCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_TYPE", "CARD"));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_PRICE", orderModel.PRICE));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_DATE", nowDateTime));
                        payCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        i += payCommand.ExecuteNonQuery();

                        #endregion

                        // ORDER 테이블 ORDER_STATUS ='WAITING' 변경, STORE_ORDER 테이블 Insert
                        string orderWaitSql = "USP_KIOSK_WAITING_INS ";

                        MySqlCommand orderwaitCommand = new MySqlCommand(orderWaitSql, connection);
                        orderwaitCommand.CommandType = CommandType.StoredProcedure;

                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_STORE_ID", orderModel.STORE_ID));
                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_ORDER_ID", orderId));

                        i += orderwaitCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    string errorfilename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                    //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{errorfilename}_Error.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} \r\nex : {errorMessage} \r\n{ex.ToString()}");
                }
                finally
                {
                    connection.Close();
                }
            }

            string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

            //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{filename}.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} Message : {errorMessage}" );

            //return new PayResult() { status = returnstatus };
        }
        #endregion
        #region OrderStatus_imp25453177_old - 결제 완료 시 처리하는 로직입니다.

        /// <summary>
        /// 결제 완료 시 처리하는 로직입니다.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        //[HttpPost]
        [Route("OrderStatus_imp25453177_old")]
        public void OrderStatus_imp25453177_old([FromUri] IamPortResult iamPortResult)
        {
            TockenMessage tockenMessage = GetTockenPostData(new IamPortTocken() { imp_key = "2527412862401371", imp_secret = "xMjYch5rQkdgut67Iy7RRUY7tnFPzox6bR4o9aPZu3fZxm6fVd7M85UBszq1YY89PdxYonYTqdSXzYdv" }, "https://api.iamport.kr/users/getToken");

            string tockenKey = tockenMessage.response.access_token;
            string orderId = iamPortResult.merchant_uid;
            string payCode = iamPortResult.imp_uid;
            string url = "https://api.iamport.kr/payments/" + iamPortResult.imp_uid;  //테스트 사이트
            string responseText = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = 30 * 1000; // 30초
            request.Headers.Add("Authorization", "Bearer " + tockenKey); // 헤더 추가 방법

            using (HttpWebResponse resp = (HttpWebResponse)request.GetResponse())
            {
                HttpStatusCode status = resp.StatusCode;
                Console.WriteLine(status);  // 정상이면 "OK"

                Stream respStream = resp.GetResponseStream();
                using (StreamReader sr = new StreamReader(respStream))
                {
                    responseText = sr.ReadToEnd();
                }
            }
            PayStatusMessage payStatusMessage = JsonConvert.DeserializeObject<PayStatusMessage>(responseText);
            string paystatus = payStatusMessage.response.status;
            string returnstatus = "falied";

            if (paystatus == "paid")
            {
                // SUCESS
                returnstatus = "PAID";
            }
            else
            {
                // CANCEL
                returnstatus = "CANCEL";
            }

            /*
INSERT INTO `order` (`ORDER_ID`, `ORDER_DATE`, `STORE_ID`, `MEMBER_ID`, `PRICE`, `PAY_CODE`, `ARRIVE_DATE`, `ORDER_STATUS`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                    (@ORDER_ID,  @ORDER_DATE,  @STORE_ID,  @MEMBER_ID,  @PRICE,  @PAY_CODE,  @ARRIVE_DATE,  @ORDER_STATUS,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                        MySqlCommand orderCommand = new MySqlCommand(orderSql, connection);

                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_ID",     order.ORDER_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_DATE",   nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@STORE_ID",     order.STORE_ID));
                        orderCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID",    memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@PRICE",        order.TOTAL_PRICE));
                        orderCommand.Parameters.Add(new MySqlParameter("@PAY_CODE",     payCode));
                        orderCommand.Parameters.Add(new MySqlParameter("@ARRIVE_DATE",  order.ARRIVE_DATE));
                        orderCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", payStatus));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE",  nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@CREATE_USER",  memberID));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE",  nowDateTime));
                        orderCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER",  memberID));
             */

            string errorMessage = "";
            errorMessage += "\r\n orderId :  " + orderId;

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                try
                {
                    connection.Open();

                    int i = -1;

                    string sql = @"
UPDATE `order` SET ORDER_STATUS = @ORDER_STATUS,  PAY_CODE = @PAY_CODE, UPDATE_DATE = @UPDATE_DATE WHERE ORDER_ID = @ORDER_ID ";

                    MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@ORDER_STATUS", returnstatus));
                    mySqlCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", DateTime.Now));

                    errorMessage += "\r\n START sql ";
                    i = mySqlCommand.ExecuteNonQuery();
                    errorMessage += "\r\n END sql ";

                    if (returnstatus == "PAID")
                    {
                        DateTime nowDateTime = DateTime.Now;

                        #region 기준 주문 정보 가져오기
                        string ordersql = @"
SELECT A.*, SUM(B.QUANTITY) ORDER_COUNT FROM `order` AS A, order_item B WHERE A.ORDER_ID = @ORDER_ID AND A.ORDER_ID = B.ORDER_ID ";

                        MySqlCommand storeidCommand = new MySqlCommand(ordersql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        errorMessage += "\r\n START storeidCommand ";

                        MySqlDataReader dataReader = storeidCommand.ExecuteReader();

                        errorMessage += "\r\n END storeidCommand ";

                        DataTable dt = new DataTable();
                        dt.Load(dataReader);
                        dataReader.Close();

                        DataRow dr = dt.Rows[0];

                        errorMessage += "\r\n dt ORDER_ID :  " + dr["ORDER_ID"].ToString();
                        errorMessage += "\r\n dt MEMBER_ID :  " + dr["MEMBER_ID"].ToString();
                        errorMessage += "\r\n dt STORE_ID :  " + dr["STORE_ID"].ToString();
                        errorMessage += "\r\n dt PRICE :  " + dr["PRICE"].ToString();
                        errorMessage += "\r\n dt ORDER_COUNT :  " + dr["ORDER_COUNT"].ToString();
                        errorMessage += "\r\n dt COUPON_ITEM_ID :  " + dr["COUPON_ITEM_ID"].ToString();

                        List<OrderModel> orderModelList = Function.CreateListFromTable<OrderModel>(dt);

                        errorMessage += "\r\n orderModelList Count : " + orderModelList.Count;

                        OrderModel orderModel = orderModelList[0];

                        errorMessage += "\r\n ORDER_ID :  " + orderModel.ORDER_ID;
                        errorMessage += "\r\n MEMBER_ID :  " + orderModel.MEMBER_ID;
                        errorMessage += "\r\n STORE_ID :  " + orderModel.STORE_ID;
                        errorMessage += "\r\n PRICE :  " + orderModel.PRICE;

                        orderModel.ORDER_COUNT = double.Parse(dr["ORDER_COUNT"].ToString());

                        errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;
                        errorMessage += "\r\n COUPON_ITEM_ID :  " + orderModel.COUPON_ITEM_ID;

                        errorMessage += "\r\n ORDER_COUNT :  " + orderModel.ORDER_COUNT;

                        //                        ordersql = @"
                        //SELECT SUM(QUANTITY) ORDER_COUNT FROM order_item WHERE ORDER_ID = @ORDER_ID ";

                        int couponCount = 0;

                        // 쿠폰이 적용된 경우
                        if (orderModel.COUPON_ITEM_ID != 0)
                        {
                            // 현재 아이템이 쿠폰적용 대상인지 확인합니다.
                            ordersql = @"
SELECT *
FROM item
WHERE STAMP_YN='Y'  
  AND ITEM_ID = @ITEM_ID";
                            storeidCommand = new MySqlCommand(ordersql, connection);
                            storeidCommand.Parameters.Add(new MySqlParameter("@ITEM_ID", orderModel.COUPON_ITEM_ID));

                            var existStampReader = storeidCommand.ExecuteReader();
                            bool existStampItem = existStampReader.HasRows;
                            existStampReader.Close();

                            // 나중에 스템프 갯수에서 제외할 갯수입니다.
                            if (existStampItem)
                            {
                                couponCount = 1;
                            }

                            ordersql = @"
SELECT SUM(A.QUANTITY) ORDER_COUNT 
FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
WHERE A.ORDER_ID = @ORDER_ID";
                        }
                        else
                        {
                            ordersql = @"
SELECT SUM(A.QUANTITY) ORDER_COUNT 
FROM order_item A INNER JOIN item B ON A.ITEM_ID = B.ITEM_ID AND B.STAMP_YN='Y'  
WHERE A.ORDER_ID = @ORDER_ID";
                        }

                        storeidCommand = new MySqlCommand(ordersql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        errorMessage += "\r\n START storeidCommand ";

                        string orderCount = storeidCommand.ExecuteScalar()?.ToString();
                        if (string.IsNullOrEmpty(orderCount))
                        {
                            orderModel.ORDER_COUNT = 0;
                        }
                        else
                        {
                            orderModel.ORDER_COUNT = long.Parse(orderCount);
                        }

                        errorMessage += "\r\n ORDER_COUNT2 :  " + orderModel.ORDER_COUNT;

                        #endregion

                        #region Stamp 테이블 Insert

                        double orderStampCount = orderModel.ORDER_COUNT - couponCount;

                        string stampSql = $@"
INSERT INTO `order_stamp` (`ORDER_ID`, `MEMBER_ID`, `COUNT`, `USE_YN`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@ORDER_ID,  @MEMBER_ID,  @STAMP_COUNT,   @USE_YN,  @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                        MySqlCommand stampCommand = new MySqlCommand(stampSql, connection);

                        stampCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));
                        stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                        stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                        stampCommand.Parameters.Add(new MySqlParameter("@USE_YN", "N"));
                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                        stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                        stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                        errorMessage += "\r\n START stampCommand ";
                        i += stampCommand.ExecuteNonQuery();
                        errorMessage += "\r\n END stampCommand ";
                        #endregion

                        #region member_stamp 테이블 Insert
                        stampSql = "SELECT STAMP_COUNT FROM member_stamp WHERE MEMBER_ID = @MEMBER_ID";

                        storeidCommand = new MySqlCommand(stampSql, connection);
                        storeidCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));

                        errorMessage += "\r\n START member_stamp ";

                        errorMessage += "\r\n MEMBER_ID : " + orderModel.MEMBER_ID;

                        string stampCount = storeidCommand.ExecuteScalar()?.ToString();

                        errorMessage += "\r\n STAMP_COUNT : " + stampCount ?? "";

                        if (stampCount == null)
                        {
                            // 신규  Insert
                            stampSql = $@"
INSERT INTO `member_stamp` (`MEMBER_ID`, `STAMP_COUNT`, `CREATE_DATE`, `CREATE_USER`, `UPDATE_DATE`, `UPDATE_USER`) VALUES
                          (@MEMBER_ID,  @STAMP_COUNT, @CREATE_DATE,  @CREATE_USER,  @UPDATE_DATE,  @UPDATE_USER) ";

                            stampCommand = new MySqlCommand(stampSql, connection);

                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", orderStampCount));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@CREATE_USER", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START member_stamp insert";
                            i += stampCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END member_stamp insert";
                        }
                        else
                        {
                            // 기존 수에 더하기
                            int sumStampCount = int.Parse(stampCount) + (int)orderStampCount;

                            if (couponCount != 0)
                            {
                                // 기존 수에서 10개를 뺀다.
                                sumStampCount = sumStampCount - 10;
                            }

                            //UPDATE member SET MEMBER_NICKNAME = @NICKNAME WHERE MEMBER_ID = @MEMBER_ID ";
                            stampSql = $@"
UPDATE `member_stamp` 
   SET `STAMP_COUNT` = @STAMP_COUNT, `UPDATE_DATE` = @UPDATE_DATE, `UPDATE_USER` = @UPDATE_USER
 WHERE `MEMBER_ID` = @MEMBER_ID";

                            stampCommand = new MySqlCommand(stampSql, connection);

                            stampCommand.Parameters.Add(new MySqlParameter("@STAMP_COUNT", sumStampCount));
                            stampCommand.Parameters.Add(new MySqlParameter("@MEMBER_ID", orderModel.MEMBER_ID));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_DATE", nowDateTime));
                            stampCommand.Parameters.Add(new MySqlParameter("@UPDATE_USER", orderModel.MEMBER_ID));

                            errorMessage += "\r\n START member_stamp update";
                            i += stampCommand.ExecuteNonQuery();
                            errorMessage += "\r\n END member_stamp update";
                        }

                        #endregion

                        #region Pay 테이블 Insert

                        string paySql = $@"
INSERT INTO `pay` (`PAY_CODE`, `PAY_TYPE`, `PAY_PRICE`, `PAY_DATE`, `ORDER_ID`) VALUES
                  (@PAY_CODE,  @PAY_TYPE,  @PAY_PRICE,  @PAY_DATE,  @ORDER_ID) ";

                        MySqlCommand payCommand = new MySqlCommand(paySql, connection);

                        payCommand.Parameters.Add(new MySqlParameter("@PAY_CODE", payCode));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_TYPE", "CARD"));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_PRICE", orderModel.PRICE));
                        payCommand.Parameters.Add(new MySqlParameter("@PAY_DATE", nowDateTime));
                        payCommand.Parameters.Add(new MySqlParameter("@ORDER_ID", orderId));

                        i += payCommand.ExecuteNonQuery();

                        #endregion

                        // ORDER 테이블 ORDER_STATUS ='WAITING' 변경, STORE_ORDER 테이블 Insert
                        string orderWaitSql = "USP_KIOSK_WAITING_INS ";

                        MySqlCommand orderwaitCommand = new MySqlCommand(orderWaitSql, connection);
                        orderwaitCommand.CommandType = CommandType.StoredProcedure;

                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_STORE_ID", orderModel.STORE_ID));
                        orderwaitCommand.Parameters.Add(new MySqlParameter("P_ORDER_ID", orderId));

                        i += orderwaitCommand.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    string errorfilename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
                    //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{errorfilename}_Error.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} \r\nex : {errorMessage} \r\n{ex.ToString()}");
                }
                finally
                {
                    connection.Close();
                }
            }

            string filename = DateTime.Now.ToString("yyyyMMddHHmmssffffff");

            //File.WriteAllText($@"D:\IIS\Auosms\M\Order\{filename}.txt", $"imp_uid : {iamPortResult.imp_uid}, merchant_uid : {iamPortResult.merchant_uid} = {returnstatus} Message : {errorMessage}" );

            //return new PayResult() { status = returnstatus };
        }
        #endregion
    }
}

