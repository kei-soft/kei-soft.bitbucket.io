﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI
{
    public class PayStatusInfo
    {
        public int amount { get; set; }
        public object apply_num { get; set; }
        public object bank_code { get; set; }
        public object bank_name { get; set; }
        public string buyer_addr { get; set; }
        public string buyer_email { get; set; }
        public string buyer_name { get; set; }
        public string buyer_postcode { get; set; }
        public string buyer_tel { get; set; }
        public int cancel_amount { get; set; }
        public List<object> cancel_history { get; set; }
        public object cancel_reason { get; set; }
        public List<object> cancel_receipt_urls { get; set; }
        public int cancelled_at { get; set; }
        public object card_code { get; set; }
        public object card_name { get; set; }
        public object card_number { get; set; }
        public int card_quota { get; set; }
        public object card_type { get; set; }
        public bool cash_receipt_issued { get; set; }
        public string channel { get; set; }
        public string currency { get; set; }
        public object custom_data { get; set; }
        public bool escrow { get; set; }
        public string fail_reason { get; set; }
        public int failed_at { get; set; }
        public string imp_uid { get; set; }
        public string merchant_uid { get; set; }
        public string name { get; set; }
        public int paid_at { get; set; }
        public string pay_method { get; set; }
        public string pg_id { get; set; }
        public string pg_provider { get; set; }
        public object pg_tid { get; set; }
        public string receipt_url { get; set; }
        public int started_at { get; set; }
        public string status { get; set; }
        public string user_agent { get; set; }
        public object vbank_code { get; set; }
        public int vbank_date { get; set; }
        public object vbank_holder { get; set; }
        public int vbank_issued_at { get; set; }
        public object vbank_name { get; set; }
        public object vbank_num { get; set; }
    }

    public class PayStatusMessage
    {
        public int code { get; set; }
        public object message { get; set; }
        public PayStatusInfo response { get; set; }
    }
}