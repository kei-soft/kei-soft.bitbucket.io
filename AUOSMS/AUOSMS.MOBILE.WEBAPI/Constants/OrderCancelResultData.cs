﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Constants
{
    public class OrderCancelResultData
    {
        public int code { get; set; }
        public string message { get; set; }
        public object Response { get; set; }
    }
}