﻿namespace AUOSMS.MOBILE.WEBAPI.Constants
{
    public class RequestType
    {
        /// <summary>
        /// 쿼리하고 결과를 리턴합니다.
        /// </summary>
        public static readonly string SelectQuery = "SelectQuery";

        /// <summary>
        /// CRUD 쿼리를 수행하고 영향받는 행을 리턴합니다.
        /// </summary>
        public static readonly string CrudQuery = "CrudQuery";

        /// <summary>
        /// Login 정보를 확인합니다.
        /// </summary>
        public static readonly string Login = "Login";

        /// <summary>
        /// Main 이미지를 반환합니다.
        /// </summary>
        public static readonly string SelectMainImage = "SelectMainImage";

        /// <summary>
        /// item 의 option 항목을 반환합니다.
        /// </summary>
        public static readonly string SelectItemOption = "SelectItemOption";

        /// <summary>
        /// 주문데이터를 처리합니다.
        /// </summary>
        public static readonly string OrderItem = "OrderItem";

        /// <summary>
        /// 가입을 처리합니다.
        /// </summary>
        public static readonly string SignUp = "SignUp";

        /// <summary>
        /// 취향정보를 처리합니다.
        /// </summary>
        public static readonly string Taste = "Taste";

        /// <summary>
        /// 마지막 주문 마스터 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectLastOrder = "SelectLastOrder";

        /// <summary>
        /// 전체 주문 마스터 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectAllOrder = "SelectAllOrder";

        /// <summary>
        /// 주문 아이템 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectOrderItem = "SelectOrderItem";

        /// <summary>
        /// 취향 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectTaste = "SelectTaste";

        /// <summary>
        /// 메뉴 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectMenu = "SelectMenu";

        /// <summary>
        /// 매장 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectStore = "SelectStore";

        /// <summary>
        /// 매장별 메뉴 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectStoreItem = "SelectStoreItem";

        /// <summary>
        /// 사용자 기준 스템프 개수를 반환합니다.
        /// </summary>
        public static readonly string SelectStampCount = "SelectStampCount";

        /// <summary>
        /// 추가 사용자 정보를 처리합니다.
        /// </summary>
        public static readonly string SignUpAddInfo = "SignUpAddInfo";

        /// <summary>
        /// 사용자 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectMemberInfo = "SelectMemberInfo";

        /// <summary>
        /// 옵션 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectOption = "SelectOption";

        /// <summary>
        /// 옵션 상세 정보를 반환합니다.
        /// </summary>
        public static readonly string SelectOptionDetail = "SelectOptionDetail";

        /// <summary>
        /// 별명 중복을 확인합니다.
        /// </summary>
        public static readonly string CheckNickName = "CheckNickName";

        /// <summary>
        /// 현재 주문의 상태를 반환합니다.
        /// </summary>
        public static readonly string SelectOrderStatus = "SelectOrderStatus";

        /// <summary>
        /// 스템프 쿠폰을 발행하기 위한 맴버 기준 스템프 갯수를 가져옵니다.
        /// </summary>
        public static readonly string SelectStampCuponCount = "SelectStampCuponCount";

        /// <summary>
        /// 주문 후 바로 주문 완료 처리합니다. 결제 금액이 0 인경우 강제 호출을 위함
        /// </summary>
        public static readonly string OrderComplete = "OrderComplete";

        /// <summary>
        /// 인증 문자를 발송합니다.
        /// </summary>
        public static readonly string SendSMS = "SendSMS";

        /// <summary>
        /// 인증 문자를 확인합니다.
        /// </summary>
        public static readonly string ConfirmSMS = "ConfirmSMS";

        /// <summary>
        /// 회원탈퇴를 진행합니다.
        /// </summary>
        public static readonly string DeleteMember = "DeleteMember";

        /// <summary>
        /// 자주주문한 메뉴를 조회합니다.
        /// </summary>
        public static readonly string SelectManyOrder = "SelectManyOrder";

        /// <summary>
        /// 평가를 반영합니다.
        /// </summary>
        public static readonly string UpdateScore = "UpdateScore";

        /// <summary>
        /// 주문을 취소합니다.
        /// </summary>
        public static readonly string CancelOrder = "CancelOrder";

        /// <summary>
        /// 쿠폰 정보를 가져옵니다.
        /// </summary>
        public static readonly string SelectCoupon = "SelectCoupon";

        /// <summary>
        /// 질의응답 정보를 가져옵니다.
        /// </summary>
        public static readonly string SelectQnA = "SelectQnA";

        /// <summary>
        /// 질의응답 정보를 등록합니다
        /// </summary>
        public static readonly string RegistQnA = "RegistQnA";

        /// <summary>
        /// 새소식 정보를 가져옵니다
        /// </summary>
        public static readonly string SelectNotice = "SelectNotice";

        /// <summary>
        /// 이벤트 쿠폰 정보를 가져옵니다
        /// </summary>
        public static readonly string SelectEventCoupon = "SelectEventCoupon";

        /// <summary>
        /// Token 을 갱신합니다.
        /// </summary>
        public static readonly string UpdateToken = "UpdateToken";
    }
}