﻿namespace AUOSMS.MOBILE.WEBAPI.Constants
{
    public class OrderCancelData
    {
        public string reason { get; set; }
        public string imp_uid { get; set; }
    }
}