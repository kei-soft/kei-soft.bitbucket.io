﻿namespace AUOSMS.MOBILE.WEBAPI.Constants
{
    /// <summary>
    /// 결과 코드입니다.
    /// </summary>
    public class ResultCode
    {
        /// <summary>
        /// 성공
        /// </summary>
        public static readonly string Sucess = "Sucess";
        /// <summary>
        /// 실패
        /// </summary>
        public static readonly string Fail = "Fail";
    }
}