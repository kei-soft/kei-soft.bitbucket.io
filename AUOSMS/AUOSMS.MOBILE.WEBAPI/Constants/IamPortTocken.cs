﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Constants
{
    #region IamPortTocken class
    public class IamPortTocken
    {
        public string imp_key { get; set; }
        public string imp_secret { get; set; }
    }
    #endregion
}