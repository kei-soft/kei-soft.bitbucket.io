﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI.Constants
{
    #region IamPortResult
    public class IamPortResult
    {
        public string imp_uid { get; set; }
        public string merchant_uid { get; set; }
        public string status { get; set; }
    }
    #endregion
}