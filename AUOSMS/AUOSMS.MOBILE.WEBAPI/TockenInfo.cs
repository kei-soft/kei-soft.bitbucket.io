﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AUOSMS.MOBILE.WEBAPI
{
    public class TockenInfo
    {
        public string access_token { get; set; }
        public int now { get; set; }
        public int expired_at { get; set; }
    }

    public class TockenMessage
    {
        public int code { get; set; }
        public object message { get; set; }
        public TockenInfo response { get; set; }
    }
}