﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<title>AUOSMS</title>

		<!--// Default - Login //-->
	
		<link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />

		<!-- Bootstrap Core CSS -->
		<link href="<%=Page.ResolveUrl("~") %>Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

		<!-- MetisMenu CSS -->
		<link href="<%=Page.ResolveUrl("~") %>Resources/metisMenu/metisMenu.min.css" rel="stylesheet" />

		<link href="<%=Page.ResolveUrl("~") %>Resources/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" />
		<link href="<%=Page.ResolveUrl("~") %>Resources/datatables-responsive/dataTables.responsive.css" rel="stylesheet" />

		<!-- Custom CSS -->
		<link href="<%=Page.ResolveUrl("~") %>Resources/sb-admin-2/css/sb-admin-2.min.css" rel="stylesheet" />

		<!-- Custom Fonts -->
		<link href="<%=Page.ResolveUrl("~") %>Resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<link href="<%=Page.ResolveUrl("~") %>css/common.css" rel="stylesheet" />
	</head>
	<body>
		
		<div id="wrapper">

			<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/Default.aspx">AUOSMS</a>
				</div>

				<%--<div class="navbar-default sidebar" role="navigation">
					<div class="sidebar-nav navbar-collapse">
						<ul class="nav" id="side-menu">
							<asp:Literal ID="litSideMenu" runat="server"></asp:Literal>
						</ul>
					</div>
				</div>--%>
			</nav>

			<!-- Page Content -->
			<%--<div id="page-wrapper">--%>
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">


							
							<div>
								<div class="row">
									<div class="col-md-4 col-md-offset-4">
										<div class="login-panel panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">로그인</h3>
											</div>
											<div class="panel-body">
												<form role="form">
													<fieldset>
														<div class="form-group">
															<input class="form-control" placeholder="ID" name="txtID" id="txtID" type="text" autofocus="autofocus" />
														</div>
														<div class="form-group">
															<input class="form-control" placeholder="Password" name="txtPassword" id="txtPassword" type="password" value="" />
														</div>
														<a class="btn btn-lg btn-success btn-block" id="btnLogin">로그인</a>
													</fieldset>
												</form>
											</div>
											<%--<div class="panel-footer text-center">
												<a href="UserRegister.aspx">관리자 등록</a>
												&nbsp; | &nbsp;
												<a href="Search.aspx">계정찾기</a>
											</div>--%>
										</div>
									</div>
								</div>
							</div>


						</div>
						<!-- /.col-lg-12 -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container-fluid -->
			<%--</div>--%>
			<!-- /#page-wrapper -->

		</div>

		<div id="loading_spinner" class="loading"><img id="loading_img" alt="loading" src="<%=Page.ResolveUrl("~") %>images/ajax-loader.gif" /></div>
		<script>
			var contextPath = "<%= Page.ResolveUrl("~") %>";
		</script>
		<!-- jQuery -->
		<script src="<%=Page.ResolveUrl("~") %>Resources/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="<%=Page.ResolveUrl("~") %>Resources/bootstrap/js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="<%=Page.ResolveUrl("~") %>Resources/metisMenu/metisMenu.min.js"></script>

		<script src="<%=Page.ResolveUrl("~") %>Resources/datatables/js/jquery.dataTables.min.js"></script>
		<script src="<%=Page.ResolveUrl("~") %>Resources/datatables-plugins/dataTables.bootstrap.min.js"></script>
		<script src="<%=Page.ResolveUrl("~") %>Resources/datatables-responsive/dataTables.responsive.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="<%=Page.ResolveUrl("~") %>Resources/sb-admin-2/js/sb-admin-2.min.js"></script>
		<script src="<%=Page.ResolveUrl("~") %>js/common.js"></script>
		e
		<script>
			$(document).ready(function () {
    			$("#btnLogin").click(function () {
    				var $e;

    				$e = $("#txtID");
    				if ($e.val() == "") {
    					alert("아이디를 입력하세요.");
    					$e.focus();
    					return;
    				}

    				$e = $("#txtPassword");
    				if ($e.val() == "") {
    					alert("비밀번호를 입력하세요.");
    					$e.focus();
    					return;
    				}

    				var params = {};
    				params.ID = $("#txtID").val();
					params.PW = $("#txtPassword").val();

					console.log(JSON.stringify(params));

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
    					async: true,
    					Command: "ChkManagerLogin",
    					Params: params,
    					SuccessCallBack: function (data) {
    						if (data) {
    							location.href = contextPath + "Page/Admin/View.aspx";
    						}
    						else {
    							alert("로그인에 실패하였습니다.");
    							$("#txtID").val("");
    							$("#txtPassword").val("");
    							$("#txtID").focus();
    						}
    					}
    				});

    			});

    			$("#txtID").keypress(function (e) {
    				if (e.keyCode == 13)
    					$("#txtPassword").focus();
    			});

    			$("#txtPassword").keypress(function (e) {
    				if (e.keyCode == 13)
    					$("#btnLogin").click();
				});
			});
		</script>
	</body>
</html>
