﻿using AUOSMS.ADMIN.UTIL.Authentication;

using System;
using System.Text;

public partial class Common_Admin : System.Web.UI.MasterPage
{
	protected string contextPath;

	protected void Page_Load(object sender, EventArgs e)
	{
		contextPath = Page.ResolveUrl("~");

		if (!IsPostBack)
			this.PageInit();
	}

	void PageInit()
	{
		this.litStoreName.Text = AUOSMSUser.GetStoreName();

		string url = Request.Url.AbsolutePath.ToLower();

		StringBuilder sbMenu = new StringBuilder();
		if (!AUOSMSUser.IsLogin())
		{
			Response.Redirect(contextPath + "Default.aspx");
		}
		else
		{
			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/admin/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Admin/View.aspx\"><i class=\"fa fa-user-md fa-fw\"></i> 관리자 정보</a></li>");

			sbMenu.AppendLine("<li><a style=\"cursor:pointer;\"><i class=\"fa fa-home fa-fw\"></i> 매장 관리<span class=\"fa arrow\"></span></a>");
			sbMenu.AppendLine("<ul class=\"nav nav-second-level collapse" + (url.StartsWith("/page/shop/") ? " in" : "") + "\">");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/shop/info.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Shop/Info.aspx\">매장 정보</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/shop/addorder.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Shop/AddOrder.aspx\">주문 하기</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/shop/order.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Shop/Order.aspx\">주문 처리 내역</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/shop/intro.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Shop/Intro.aspx\">INTRO 관리</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/shop/event.aspx") || url.StartsWith("/page/shop/eventdetail.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Shop/Event.aspx\">이벤트</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/shop/did.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Shop/DID.aspx\">DID 관리</a></li>");
			sbMenu.AppendLine("</ul>");
			sbMenu.AppendLine("</li>");

			sbMenu.AppendLine("<li><a style=\"cursor:pointer;\"><i class=\"fa fa-money fa-fw\"></i> 매출 관리<span class=\"fa arrow\"></span></a>");
			sbMenu.AppendLine("<ul class=\"nav nav-second-level collapse" + (url.StartsWith("/page/sale/") ? " in" : "") + "\">");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/sale/sale.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Sale/Sale.aspx\">매출 내역</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/sale/salebyitem.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Sale/SaleByItem.aspx\">메뉴별 매출 내역</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/sale/salereportbydayslist.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Sale/SaleReportByDaysList.aspx\">일자별 매출</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/sale/salereportbymonthlist.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Sale/SaleReportByMonthList.aspx\">월별 매출</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/sale/salereportbyreceiptslist.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Sale/SaleReportByReceiptsList.aspx\">영수증별 매출</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/sale/salereportbyitemtimelist.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Sale/SaleReportByItemTimeList.aspx\">시간대별 상품 매출</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/sale/salereportbyitemgrouplist.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Sale/SaleReportByItemGroupList.aspx\">상품별 매출</a></li>");
			sbMenu.AppendLine("</ul>");
			sbMenu.AppendLine("</li>");

			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/kiosk/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/KIOSK/List.aspx\"><i class=\"fa fa-building fa-fw\"></i> 키오스크 관리</a></li>");

			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/pos/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/POS/List.aspx\"><i class=\"fa fa-desktop fa-fw\"></i> 단말 관리</a></li>");

			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/menu/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Menu/List.aspx\"><i class=\"fa fa-home fa-fw\"></i> 메뉴 관리</a></li>");

			sbMenu.AppendLine("<li><a style=\"cursor:pointer;\"><i class=\"fa fa-list-alt fa-fw\"></i> 게시판<span class=\"fa arrow\"></span></a>");
			sbMenu.AppendLine("<ul class=\"nav nav-second-level collapse" + (url.StartsWith("/page/board/") ? " in" : "") + "\">");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/board/notice/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Board/Notice/List.aspx\">공지사항</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/board/channe/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Board/Channel/List.aspx\">소통채널</a></li>");
			sbMenu.AppendLine("</ul>");
			sbMenu.AppendLine("</li>");
		}
		this.litSideMenu.Text = sbMenu.ToString();
	}
}
