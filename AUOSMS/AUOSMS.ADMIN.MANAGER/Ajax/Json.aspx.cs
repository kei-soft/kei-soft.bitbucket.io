﻿using AUOSMS.ADMIN.UTIL;
using AUOSMS.ADMIN.UTIL.Authentication;
using AUOSMS.ADMIN.UTIL.DataBase;
using AUOSMS.ADMIN.UTIL.DataBase.DBAgent;

using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Configuration;

public partial class Ajax_Json : System.Web.UI.Page
{
	string connectionString = ConfigurationManager.ConnectionStrings["AUOSMS_MYSQL"].ConnectionString;

	protected void Page_Load(object sender, EventArgs e)
	{
		AJAX.Add3P3Header(Response);

		object jsonData;
		try
		{
			jsonData = AJAX.CheckReferrer(Request);
			if (jsonData == null)
			{
				LOG.Write(Request, "AJAX START - CMD : {0}, PARAMS : {1}", Request.Form["Command"], Request.Form["Params"]);

				switch (Request.Form["Command"])
				{
					case "ChkManagerLogin": // 로그인처리
						jsonData = ChkManagerLogin(JObject.Parse(Request.Form["Params"]));
						break;
					case "LogOut": // 로그아웃처리
						if (jsonData == null)
							jsonData = LogOut();
						break;
					case "GetCommonCodeList": // 공통코드 목록
						if (jsonData == null)
							jsonData = GetCommonCodeList(JObject.Parse(Request.Form["Params"]));
						break;
					#region 관리자
					case "ChkManagerPWD": // 기존비밀번호 검사
						if (jsonData == null)
							jsonData = ChkManagerPWD(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateManagerInfo": // 관리자 정보수정
						if (jsonData == null)
							jsonData = UpdateManagerInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateManagerPassword": // 관리자 비밀번호 변경
						if (jsonData == null)
							jsonData = UpdateManagerPassword(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetManagerInfo": // 관리자 정보
						if (jsonData == null)
							jsonData = GetManagerInfo();
						break;
					#endregion
					#region 매장
					case "GetStoreInfo": // 
						if (jsonData == null)
							jsonData = GetStoreInfo();
						break;
					case "UpdateStoreInfo": // 
						if (jsonData == null)
							jsonData = UpdateStoreInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "GetStoreSaleList": // 
						if (jsonData == null)
							jsonData = GetStoreSaleList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreOrderList": // 
						if (jsonData == null)
							jsonData = GetStoreOrderList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreSaleByItemList": // 
						if (jsonData == null)
							jsonData = GetStoreSaleByItemList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemList": // 
						if (jsonData == null)
							jsonData = GetItemList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemGroupList": // 
						if (jsonData == null)
							jsonData = GetItemGroupList();
						break;
					case "GetOrderItemGroupList": // 
						if (jsonData == null)
							jsonData = GetOrderItemGroupList();
						break;
					case "GetOrderMenuList": // 
						if (jsonData == null)
							jsonData = GetOrderMenuList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetOrderItemOptionList": // 
						if (jsonData == null)
							jsonData = GetOrderItemOptionList(JObject.Parse(Request.Form["Params"]));
						break;
					case "SaveOrderComplete": // 
						if (jsonData == null)
							jsonData = SaveOrderComplete(JObject.Parse(Request.Form["Params"]));
						break;
					#region Intro 관리
					case "GetIntroImageList": // 
						if (jsonData == null)
							jsonData = GetIntroImageList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetIntroImageInfo": // 
						if (jsonData == null)
							jsonData = GetIntroImageInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertIntroImage": // 
						if (jsonData == null)
							jsonData = InsertIntroImage(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "UpdateIntroImageInfo": // 
						if (jsonData == null)
							jsonData = UpdateIntroImageInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "DeleteIntroImage": // 
						if (jsonData == null)
							jsonData = DeleteIntroImage(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateIntroImageSort": // 
						if (jsonData == null)
							jsonData = UpdateIntroImageSort(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region DID 관리
					case "GetDIDImageList": // 
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetDIDImageList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetDIDImageInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetDIDImageInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertDIDImage": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertDIDImage(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "UpdateDIDImageInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateDIDImageInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "DeleteDIDImage": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteDIDImage(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateDIDImageSort": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateDIDImageSort(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region EVENT
					case "GetStoreEventList": // 
						if (jsonData == null)
							jsonData = GetStoreEventList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreEventInfo": // 
						if (jsonData == null)
							jsonData = GetStoreEventInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertStoreEvent": // 
						if (jsonData == null)
							jsonData = InsertStoreEvent(JObject.Parse(Request.Form["Params"]), Request.Files["Files1"], Request.Files["Files2"]);
						break;
					case "UpdateStoreEventInfo": // 
						if (jsonData == null)
							jsonData = UpdateStoreEventInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files1"], Request.Files["Files2"]);
						break;
					case "DeleteStoreEvent": // 
						if (jsonData == null)
							jsonData = DeleteStoreEvent(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#endregion
					#region KIOSK
					case "GetStoreKioskList": // 
						if (jsonData == null)
							jsonData = GetStoreKioskList();
						break;
					case "GetKIOSKInfo": // 
						if (jsonData == null)
							jsonData = GetKIOSKInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertKIOSK": // 
						if (jsonData == null)
							jsonData = InsertKIOSK(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateKIOSKInfo": //
						if (jsonData == null)
							jsonData = UpdateKIOSKInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteStoreKIOSK": // 
						if (jsonData == null)
							jsonData = DeleteStoreKIOSK(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region POS
					case "GetStorePosList": // 
						if (jsonData == null)
							jsonData = GetStorePosList();
						break;
					case "GetPOSInfo": // 
						if (jsonData == null)
							jsonData = GetPOSInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertPOS": // 
						if (jsonData == null)
							jsonData = InsertPOS(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdatePOSInfo": // 
						if (jsonData == null)
							jsonData = UpdatePOSInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteStorePOS": // 
						if (jsonData == null)
							jsonData = DeleteStorePOS(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region 메뉴관리
					case "GetStoreItemList": // 
						if (jsonData == null)
							jsonData = GetStoreItemList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreItemInfo": // 
						if (jsonData == null)
							jsonData = GetStoreItemInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateStoreItemLimit": // 
						if (jsonData == null)
							jsonData = UpdateStoreItemLimit(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateStoreItemSort": // 
						if (jsonData == null)
							jsonData = UpdateStoreItemSort(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteStoreItem": // 
						if (jsonData == null)
							jsonData = DeleteStoreItem(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreItemSearchList": // 
						if (jsonData == null)
							jsonData = GetStoreItemSearchList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemInfo": // 
						if (jsonData == null)
							jsonData = GetItemInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemOptionList": // 
						if (jsonData == null)
							jsonData = GetItemOptionList(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertItemStore": // 
						if (jsonData == null)
							jsonData = InsertItemStore(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region 매출관리
					case "GetSaleReportByDaysList": // 
						if (jsonData == null)
							jsonData = GetSaleReportByDaysList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByMonthList": // 
						if (jsonData == null)
							jsonData = GetSaleReportByMonthList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByReceiptsList": // 
						if (jsonData == null)
							jsonData = GetSaleReportByReceiptsList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByItemTimeList": // 
						if (jsonData == null)
							jsonData = GetSaleReportByItemTimeList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByItemGroupList": // 
						if (jsonData == null)
							jsonData = GetSaleReportByItemGroupList(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					default:
						jsonData = new { STATUS = "WrongCommand", MSG = string.Format("잘못된 명령어입니다 ({0})", Request.Form["Command"]) };
						break;
				}
			}
		}
		catch (Exception ex)
		{
			jsonData = new { STATUS = "SystemError", MSG = ex.ToString() };
		}

		LOG.Write(Request, "AJAX RESULT - {0}", JsonConvertor.SerializeObject(jsonData));
		AJAX.PrintJson(Response, jsonData);
	}

	object ChkManagerLogin(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_PASS", MySQLDataType.VARCHAR, 40, CommFunc.EncryptSHA1(Params.Value<string>("PW"))));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_GROUP", MySQLDataType.VARCHAR, 40, "50"));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_CHK", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		bool Result;
		if (dt.Rows.Count > 0)
		{
			if (string.IsNullOrEmpty(dt.Rows[0]["STORES"].ToString()))
			{
				return new { STATUS = "NoStoreManager", MSG = "매장관리접속 권한이 없습니다." };
			}
			else
			{
				Result = true;

				AUOSMSUser.Set(dt);
			}
		}
		else
		{
			Result = false;
		}

		return new { STATUS = "OK", RESULT = Result };
	}

	object LogOut()
	{
		if (AUOSMSUser.IsLogin())
		{
			AUOSMSUser.Expires();

			return new { STATUS = "OK", RESULT = true };
		}
		else
		{
			return new { STATUS = "OK", RESULT = false };
		}

	}

	object GetCommonCodeList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_CODE_GROUP", MySQLDataType.VARCHAR, 40, Params.Value<string>("CODE_GROUP")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_COMMON_CODE_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	#region 관리자
	object ChkManagerPWD(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_PASS", MySQLDataType.VARCHAR, 40, CommFunc.EncryptSHA1(Params.Value<string>("PW"))));

		int ipwCnt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_PWD_CHK", SQLParams))
		{
			ipwCnt = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = ipwCnt > 0 ? true : false };
	}

	object UpdateManagerInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_GROUP", MySQLDataType.VARCHAR, 40, string.Empty));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_INFO_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateManagerPassword(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_PASS", MySQLDataType.VARCHAR, 40, CommFunc.EncryptSHA1(Params.Value<string>("PW"))));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_PWD_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetManagerInfo()
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, AUOSMSUser.GetCode()));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}
	#endregion

	#region 매장관리
	object GetStoreInfo()
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/STORE/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, FILEPATH = imgPath } };
	}

	object UpdateStoreInfo(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/STORE/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_STORE_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_STORE_IMAGE", MySQLDataType.VARCHAR, 40, fileName));
		SQLParams.Add(new MySQLParamModel("P_POST_CODE", MySQLDataType.VARCHAR, 40, Params.Value<string>("PCODE")));
		SQLParams.Add(new MySQLParamModel("P_STATE", MySQLDataType.VARCHAR, 40, Params.Value<string>("STATE")));
		SQLParams.Add(new MySQLParamModel("P_CITY", MySQLDataType.VARCHAR, 40, Params.Value<string>("CITY")));
		SQLParams.Add(new MySQLParamModel("P_ADDRESS", MySQLDataType.VARCHAR, 40, Params.Value<string>("ADDR")));
		SQLParams.Add(new MySQLParamModel("P_ADDRESS_DETAIL", MySQLDataType.VARCHAR, 40, Params.Value<string>("ADDR_D")));
		SQLParams.Add(new MySQLParamModel("P_PHONE", MySQLDataType.VARCHAR, 40, Params.Value<string>("PHONE")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_ORDER_START_TIME", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDERSTART")));
		SQLParams.Add(new MySQLParamModel("P_ORDER_END_TIME", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDEREND")));
		SQLParams.Add(new MySQLParamModel("P_MEMO", MySQLDataType.VARCHAR, 100, Params.Value<string>("MEMO")));
		SQLParams.Add(new MySQLParamModel("P_LOC_LATITUDE", MySQLDataType.VARCHAR, 20, Params.Value<string>("LATITUDE")));
		SQLParams.Add(new MySQLParamModel("P_LOC_LONGITUDE", MySQLDataType.VARCHAR, 20, Params.Value<string>("LONGITUDE")));
		SQLParams.Add(new MySQLParamModel("P_BUSINESS_CODE", MySQLDataType.VARCHAR, 20, Params.Value<string>("BUSINESS_CODE")));
		SQLParams.Add(new MySQLParamModel("P_BUSINESS_OWNER", MySQLDataType.VARCHAR, 40, Params.Value<string>("BUSINESS_OWNER")));
		SQLParams.Add(new MySQLParamModel("P_PG_SIETCODE", MySQLDataType.VARCHAR, 100, Params.Value<string>("PG_SITECODE")));
		SQLParams.Add(new MySQLParamModel("P_PG_KEY", MySQLDataType.VARCHAR, 100, Params.Value<string>("PG_KEY")));
		SQLParams.Add(new MySQLParamModel("P_IMPORT_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("IMPORT_ID")));
		SQLParams.Add(new MySQLParamModel("P_IMPORT_PASS", MySQLDataType.VARCHAR, 40, Params.Value<string>("IMPORT_PASS")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetStoreSaleList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_SALE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), TOTAL_PRICE = ds.Tables[0].Rows[0]["TOTAL_PRICE"].ToString(), LIST = ds.Tables[1] } };
	}

	object GetStoreOrderList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_ORDER_STATUS", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDER_STATUS")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ORDER_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1] } };
	}

	object GetStoreSaleByItemList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_GROUP_CODE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_SALE_BY_ITEM_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL = ds.Tables[0], LIST = ds.Tables[1] } };
	}

	object GetItemList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 100, Params.Value<string>("GROUP_CODE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object GetItemGroupList()
	{
		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GROUP_LIST"))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetOrderItemGroupList()
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ORDER_ITEM_GROUP_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetOrderMenuList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 100, Params.Value<string>("GROUP_CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ORDER_MENU_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetOrderItemOptionList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ORDER_ITEM_OPTION_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object SaveOrderComplete(JObject Params)
	{
		string order_id = System.Guid.NewGuid().ToString();
		string pay_code = System.Guid.NewGuid().ToString();

		List<MySQLParamModel> SQLParams1 = new List<MySQLParamModel>();
		SQLParams1.Add(new MySQLParamModel("P_ORDER_ID", MySQLDataType.VARCHAR, 50, order_id));
		SQLParams1.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams1.Add(new MySQLParamModel("P_PRICE", MySQLDataType.INT32, 10, Params.Value<string>("TOTAL_PRICE")));
		SQLParams1.Add(new MySQLParamModel("P_PAY_CODE", MySQLDataType.VARCHAR, 50, pay_code));
		SQLParams1.Add(new MySQLParamModel("P_PAY_TYPE", MySQLDataType.VARCHAR, 40, Params.Value<string>("PAY_TYPE")));

		int Result = 0, Result2 = 0, Result3 = 0;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ORDER_PAY_INS", SQLParams1))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		if (Result > 0)
		{
			JArray items = (JArray)Params["ORDER_INFO"];
			foreach (JObject item in items)
			{
				List<MySQLParamModel> SQLParams2 = new List<MySQLParamModel>();
				SQLParams2.Add(new MySQLParamModel("P_ORDER_ID", MySQLDataType.VARCHAR, 50, order_id));
				SQLParams2.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, item.Value<string>("ITEM_ID")));
				SQLParams2.Add(new MySQLParamModel("P_QUANTITY", MySQLDataType.INT32, 10, item.Value<string>("ITEM_QUANTITY")));
				SQLParams2.Add(new MySQLParamModel("P_PRICE", MySQLDataType.INT32, 10, item.Value<string>("ITEM_TOTAL_PRICE")));
				SQLParams2.Add(new MySQLParamModel("P_ITEM_OPTION", MySQLDataType.VARCHAR, 4000, JsonConvertor.SerializeObject(item["OPTIONS"])));

				using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_ORDER_ITEM_INS", SQLParams2))
				{
					Result2 += Convert.ToInt32(dbAgent.ExecuteScalar());
				}
			}

			if (Result2 > 0)
			{
				List<MySQLParamModel> SQLParams3 = new List<MySQLParamModel>();
				SQLParams3.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
				SQLParams3.Add(new MySQLParamModel("P_ORDER_ID", MySQLDataType.VARCHAR, 50, order_id));

				using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_WAITING_INS", SQLParams3))
				{
					Result3 += Convert.ToInt32(dbAgent.ExecuteScalar());
				}
			}
		}

		return new { STATUS = "OK", RESULT = Result > 0 && Result2 > 0 && Result3 > 0 ? true : false };
	}

	#region Intro 관리
	object GetIntroImageList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object GetIntroImageInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, FILEPATH = imgPath } };
	}

	object InsertIntroImage(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 40, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateIntroImageInfo(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 100, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteIntroImage(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateIntroImageSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID1", MySQLDataType.INT32, 10, Params.Value<string>("ID1")));
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID2", MySQLDataType.INT32, 10, Params.Value<string>("ID2")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region DID 관리
	object GetDIDImageList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object GetDIDImageInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, FILEPATH = imgPath } };
	}

	object InsertDIDImage(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 40, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateDIDImageInfo(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 100, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteDIDImage(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateDIDImageSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID1", MySQLDataType.INT32, 10, Params.Value<string>("ID1")));
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID2", MySQLDataType.INT32, 10, Params.Value<string>("ID2")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region EVENT
	object GetStoreEventList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string itemImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";
		string eventImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], ITEMFILEPATH = itemImgPath, EVENTFILEPATH = eventImgPath } };
	}

	object GetStoreEventInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string itemImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";
		string eventImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, ITEMFILEPATH = itemImgPath, EVENTFILEPATH = eventImgPath } };
	}

	object InsertStoreEvent(JObject Params, System.Web.HttpPostedFile file1, System.Web.HttpPostedFile file2)
	{
		string gift_type = Params.Value<string>("GIFT_TYPE");
		string gift_code = string.Empty;
		string event_image = string.Empty;

		if (file1 != null)
		{
			event_image = file1.FileName;
			event_image = event_image.Substring(event_image.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
			event_image = CommFunc.GetFileName(dirPath, event_image);
			file1.SaveAs(dirPath + event_image);
		}

		if (gift_type.Equals("MENU"))
		{
			gift_code = Params.Value<string>("GIFT_CODE");
		}
		else
		{
			if (file2 != null)
			{
				gift_code = file2.FileName;
				gift_code = gift_code.Substring(gift_code.LastIndexOf("\\") + 1);
				string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
				gift_code = CommFunc.GetFileName(dirPath, gift_code);
				file2.SaveAs(dirPath + gift_code);
			}
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("EVENT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_INFO_IMAGE", MySQLDataType.VARCHAR, 100, event_image));
		SQLParams.Add(new MySQLParamModel("P_EVENT_START_DATE", MySQLDataType.DATE, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_END_DATE", MySQLDataType.DATE, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_TYPE", MySQLDataType.VARCHAR, 100, gift_type));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_CODE", MySQLDataType.VARCHAR, 100, gift_code));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("GIFT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_TYPE", MySQLDataType.VARCHAR, 100, Params.Value<string>("CONDITION_TYPE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_VALUE", MySQLDataType.INT32, 10, Params.Value<string>("CONDITION_VALUE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateStoreEventInfo(JObject Params, System.Web.HttpPostedFile file1, System.Web.HttpPostedFile file2)
	{
		string gift_type = Params.Value<string>("GIFT_TYPE");
		string gift_code = string.Empty;
		string event_image = string.Empty;

		if (file1 != null)
		{
			event_image = file1.FileName;
			event_image = event_image.Substring(event_image.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
			event_image = CommFunc.GetFileName(dirPath, event_image);
			file1.SaveAs(dirPath + event_image);
		}

		if (gift_type.Equals("MENU"))
		{
			gift_code = Params.Value<string>("GIFT_CODE");
		}
		else
		{
			if (file2 != null)
			{
				gift_code = file2.FileName;
				gift_code = gift_code.Substring(gift_code.LastIndexOf("\\") + 1);
				string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
				gift_code = CommFunc.GetFileName(dirPath, gift_code);
				file2.SaveAs(dirPath + gift_code);
			}
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("EVENT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_INFO_IMAGE", MySQLDataType.VARCHAR, 100, event_image));
		SQLParams.Add(new MySQLParamModel("P_EVENT_START_DATE", MySQLDataType.DATE, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_END_DATE", MySQLDataType.DATE, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_TYPE", MySQLDataType.VARCHAR, 100, gift_type));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_CODE", MySQLDataType.VARCHAR, 100, gift_code));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("GIFT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_TYPE", MySQLDataType.VARCHAR, 100, Params.Value<string>("CONDITION_TYPE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_VALUE", MySQLDataType.INT32, 10, Params.Value<string>("CONDITION_VALUE")));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStoreEvent(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion
	#endregion

	#region 키오스크 관리
	object GetStoreKioskList()
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_KIOSK_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetKIOSKInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_INFO_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertKIOSK(JObject Params)
	{
		string kiosk_id = System.Guid.NewGuid().ToString();

		//return new { STATUS = "OK", RESULT = string.Format("{0}, {1}, {2}", store_id, kiosk_id, Params.Value<string>("NAME")) };

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, kiosk_id));
		SQLParams.Add(new MySQLParamModel("P_KIOSK_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateKIOSKInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_KIOSK_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_INFO_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStoreKIOSK(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("KIOSK_ID")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_KIOSK_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region 단말 관리
	object GetStorePosList()
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_POS_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetPOSInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_INFO_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertPOS(JObject Params)
	{
		string pos_id = System.Guid.NewGuid().ToString();

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, pos_id));
		SQLParams.Add(new MySQLParamModel("P_POS_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdatePOSInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_POS_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_INFO_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStorePOS(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("POS_ID")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_POS_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region 메뉴 관리
	object GetStoreItemList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 100, Params.Value<string>("GROUP_CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { LIST = dt, FILEPATH = imgPath } };
	}

	object GetStoreItemInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object UpdateStoreItemLimit(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_LIMIT_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("LIMIT_YN")));
		SQLParams.Add(new MySQLParamModel("P_LIMIT_QUANTITY", MySQLDataType.INT32, 10, Params.Value<string>("LIMIT_QUANTITY")));
		SQLParams.Add(new MySQLParamModel("P_SOLDOUT_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("SOLDOUT_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_STORE_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateStoreItemSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID1", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID1")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID2", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID2")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStoreItem(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetStoreItemSearchList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 100, Params.Value<string>("GROUP_CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_SEARCH_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetItemInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, FILEPATH = imgPath } };
	}

	object GetItemOptionList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_OPTION_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertItemStore(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_STORE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region 매출관리
	object GetSaleReportByDaysList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_DAYS", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByMonthList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_START_MON", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_MON")));
		SQLParams.Add(new MySQLParamModel("P_END_MON", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_MON")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_MONTH", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByReceiptsList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_ORDER_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDER_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_RECEIPTS", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByItemTimeList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_ITEM_TIME", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByItemGroupList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_ITEM_GROUP", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}
	#endregion

}