﻿<%@ Control Language="C#" ClassName="ITEMNewForm" %>


<script>
	var ItemImageFile = new Object();

	$(document).ready(function () {
		$("#btnSave").click(function () {
			ITEMNewForm.save();
		});

		$("#imgItemImage").click(function () {
			$("#selectImage").click();
		});

		$("#selectImage").on("change", function (e) {
			var file = $(this)[0].files[0];
			if (!file.type.match("image/.*")) {
				alert("이미지파일만 선택가능");
			}
			else {
				ItemImageFile = file;
				var reader = new FileReader();
				reader.onload = function (e) {
					$("#imgItemImage").attr("src", e.target.result);
				}
				reader.readAsDataURL(ItemImageFile);
			}
		});
	});

	var ITEMNewForm = {
		saveCallback: null,
		groupCode: null,
		itemID: null,
		open: function (item_id, group_code, saveCallback) {
			this.saveCallback = saveCallback;
			this.itemID = item_id;
			this.groupCode = group_code;
			
			if (this.itemID == null) {
				$("#divNewItem h4.modal-title").text("메뉴 추가");
				$("#imgItemImage").attr("src", contextPath + "images/no_image.png");
				ItemImageFile = new Object();
				$("#txtItemName").val("");
				$("#txtItemEngName").val("");
				$("#txtItemPrice").val("");
				$("#txtItemDesc").val("");
			}
			else {
				$("#divNewItem h4.modal-title").text("메뉴 수정");
				$("#imgItemImage").attr("src", imgPath + ItemInfo.ITEM_IMAGE);
				ItemImageFile = new Object();
				$("#txtItemName").val(ItemInfo.ITEM_NAME);
				$("#txtItemEngName").val(ItemInfo.ITEM_ENG_NAME);
				$("#txtItemPrice").val(String(ItemInfo.ITEM_PRICE).currencyFormat());
				$("#txtItemDesc").val(ItemInfo.ITEM_DESC);
			}

			$("#divNewItem").modal();
		},
		close: function () {
			$("#divNewItem").modal("hide");
		},
		validation: function () {
			var $e;

			$e = $("#txtItemName");
			if ($e.val() == "") {
				alert("메뉴 이름을 입력하세요.");
				$e.focus();
				return false;
			}

			$e = $("#txtItemPrice");
			if ($e.val() == "") {
				alert("가격을 입력하세요.");
				$e.focus();
				return false;
			}

			if (!confirm("메뉴를 저장하시겠습니까?")) {
				return false;
			}

			return true;

		},
		save: function () {
			if (this.validation()) {
				var cmd = "";
				var params = {};
				if (this.itemID == null) {
					cmd = "InsertItem";
				}
				else {
					cmd = "UpdateItemInfo";
					params.ID = this.itemID;
				}
				params.NAME = $("#txtItemName").val();
				params.ENG_NAME = $("#txtItemEngName").val();
				params.DESC = $("#txtItemDesc").val();
				params.PRICE = removeCommas($("#txtItemPrice").val());

				do_cmd.ajax_file({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: cmd,
					Params: params,
					Files: ItemImageFile,
					SuccessCallBack: function (data) {
						if (data > 0) {
							if (ITEMNewForm.groupCode != null) {

								var params2 = {};
								params2.GROUP_CODE = ITEMNewForm.groupCode;
								params2.ITEM_ID = data

								do_cmd.ajax({
									URL: contextPath + "Ajax/Json.aspx",
									async: true,
									Command: "InsertItemCategory",
									Params: params2,
									SuccessCallBack: function (data2) {
										if (data2) {
											alert("완료되었습니다.");
											ITEMNewForm.close();
											ITEMNewForm.saveCallback();
										}
										else {
											alert("등록 오류!");
										}
									}
								});
							}
							else {
								alert("완료되었습니다.");
								ITEMNewForm.close();
								ITEMNewForm.saveCallback();
							}
						}
						else {
							alert("등록 오류!");
						}
					}
				});
			}
		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divNewItem">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body" style="min-height:500px;">
				<form>
					<div class="form-group text-center">
						<img src="<%=Page.ResolveUrl("~") %>images/no_image.png" style="width:50%; cursor: pointer;" alt="클릭하여 이미지 선택" id="imgItemImage" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" />
						<input type="file" id="selectImage" name="selectImage" style="display: none;" />
						<p class="text-primary">이미지를 클릭하여 사진을 선택하세요.</p>
					</div>
					<div class="form-group">
						<label for="txtStoreName">메뉴명 : </label>
						<input type="text" class="form-control" placeholder="메뉴명" id="txtItemName" />
					</div>
					<div class="form-group">
						<label for="txtItemEngName">영문명 : </label>
						<input type="text" class="form-control" placeholder="영문명" id="txtItemEngName" />
					</div>
					<div class="form-group">
						<label for="txtItemPrice">가격 : </label>
						<input type="text" class="form-control numberOnly" placeholder="가격" id="txtItemPrice"  />
					</div>
					<%--<div class="form-group" id="divOptionList">
						<div class="panel panel-default">
							<div class="panel-heading">
								옵션
								<div class="pull-right">
									<div class="btn-group">
										<button type="button" class="btn btn-primary btn-xs" id="btnOptonSearch">추가</button>
									</div>
								</div>
							</div>

							<div class="panel-body">
								<div class="list-group" style="margin-bottom: 0px;">
									<div class="list-group-item">
										컵선택 - 머그컵(0원), 개인컵(0원), 일회용컵(0원)
										<div class="pull-right">
											<a style="cursor:pointer;" title="삭제"><span class="fa fa-trash-o fa-fw"></span></a>
										</div>
									</div>
									<div class="list-group-item">
										컵선택 - 머그컵(0원), 개인컵(0원), 일회용컵(0원)
										<div class="pull-right">
											<a style="cursor:pointer;" title="삭제"><span class="fa fa-trash-o fa-fw"></span></a>
										</div>
									</div>
									<div class="list-group-item">
										컵선택 - 머그컵(0원), 개인컵(0원), 일회용컵(0원)
										<div class="pull-right">
											<a style="cursor:pointer;" title="삭제"><span class="fa fa-trash-o fa-fw"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>--%>
					<div class="form-group">
						<label for="txtItemDesc">Description : </label>
						<textarea class="form-control" id="txtItemDesc" style="height:80px;"></textarea>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>