﻿<%@ Control Language="C#" ClassName="StoreNewForm" %>
<%@ Register Src="~/Include/PostCode.ascx" TagPrefix="uc1" TagName="PostCode" %>


<script>
	var StoreImageFile = new Object();

	$(document).ready(function () {
		$("#btnSave").click(function () {
			StoreNewForm.save();
		});

		$("#imgStoreImage").click(function () {
			$("#selectImage").click();
		});

		$("#selectImage").on("change", function (e) {
			var file = $(this)[0].files[0];
			if (!file.type.match("image/.*")) {
				alert("이미지파일만 선택가능");
			}
			else {
				StoreImageFile = file;
				var reader = new FileReader();
				reader.onload = function (e) {
					$("#imgStoreImage").attr("src", e.target.result);
				}
				reader.readAsDataURL(StoreImageFile);
			}
		});
	});

	var StoreNewForm = {
		saveCallback: null,
		open: function (saveCallback) {
			this.saveCallback = saveCallback;

			$("#imgStoreImage").attr("src", contextPath + "images/no_image.png");
			StoreImageFile = new Object();
			$("#txtStoreName").val("");
			$("#txtPostCode").val("");
			$("#txtState").val("");
			$("#txtCity").val("");
			$("#txtAddress").val("");
			$("#txtAddressDetail").val("");
			$("#txtPhone").val("");

			$("#divNewStore").modal();
		},
		close: function () {
			$("#divNewStore").modal("hide");
		},
		validation: function () {
			var $e;

			$e = $("#txtStoreName");
			if ($e.val() == "") {
				alert("매장 이름을 선택하세요.");
				$e.focus();
				return false;
			}

			$e = $("#txtPostCode");
			if ($e.val() == "") {
				alert("주소를 검색하세요.");
				$("#btnSearchAddr").click();
				return false;
			}

			$e = $("#txtAddressDetail");
			if ($e.val() == "") {
				alert("상세주소를 입력하세요.");
				$e.focus();
				return false;
			}

			$e = $("#txtPhone");
			if ($e.val() == "") {
				alert("연락처를 입력하세요.");
				$e.focus();
				return false;
			}

			if (!confirm("매장을 등록하시겠습니까?")) {
				return false;
			}

			return true;

		},
		save: function () {
			if (this.validation()) {
				var params = {};
				params.NAME = $("#txtStoreName").val();
				params.PCODE = $("#txtPostCode").val();
				params.STATE = $("#txtState").val();
				params.CITY = $("#txtCity").val();
				params.ADDR = $("#txtAddress").val();
				params.ADDR_D = $("#txtAddressDetail").val();
				params.PHONE = $("#txtPhone").val();

				//console.log(JSON.stringify(params)); return;

				do_cmd.ajax_file({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "InsertStore",
					Params: params,
					Files: StoreImageFile,
					SuccessCallBack: function (data) {
						if (data) {
							alert("등록이 완료되었습니다.");
							StoreNewForm.close();
							StoreNewForm.saveCallback();
						}
						else {
							alert("등록 오류!");
						}
					}
				});
			}
		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divNewStore">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">매장 추가</h4>
			</div>
			<div class="modal-body" style="min-height:500px;">
				<form>
					<div class="form-group text-center">
						<img src="<%=Page.ResolveUrl("~") %>images/no_image.png" style="width:50%; cursor: pointer;" alt="클릭하여 이미지 선택" id="imgStoreImage" />
						<input type="file" id="selectImage" name="selectImage" style="display: none;" />
						<p class="text-primary">이미지를 클릭하여 사진을 선택하세요.</p>
					</div>
					<div class="form-group">
						<label for="txtStoreName">매장명 : </label>
						<input type="text" class="form-control" placeholder="매장명" id="txtStoreName" />
					</div>
					<div class="form-group">
						<label for="btnSearchAddr">주소 : <button type="button" class="btn btn-primary btn-xs" id="btnSearchAddr">주소 검색</button></label>
						<input type="text" class="form-control readonly" placeholder="우편번호" id="txtPostCode" />
						<input type="text" class="form-control readonly" placeholder="시도" id="txtState" />
						<input type="text" class="form-control readonly" placeholder="구군" id="txtCity" />
						<input type="text" class="form-control readonly" placeholder="주소" id="txtAddress" />
						<input type="text" class="form-control" placeholder="상세주소" id="txtAddressDetail" />
					</div>
					<div class="form-group">
						<label for="txtPhone">전화번호 : </label>
						<input type="text" class="form-control" placeholder="전화번호" id="txtPhone" data-chk="false" />
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>

<uc1:PostCode runat="server" ID="PostCode" />
