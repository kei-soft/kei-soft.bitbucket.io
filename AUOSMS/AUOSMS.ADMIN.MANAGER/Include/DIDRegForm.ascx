﻿<%@ Control Language="C#" ClassName="DIDRegForm" %>

<script>
	var DIDImageFile = new Object();

	$(document).ready(function () {
		$("#btnSave").click(function () {
			DIDRegForm.save();
		});

		$("#imgDIDImage").click(function () {
			$("#selectDIDImage").click();
		});

		$("#selectDIDImage").on("change", function (e) {
			var file = $(this)[0].files[0];
			if (!file.type.match("image/.*")) {
				alert("이미지파일만 선택가능");
			}
			else {
				DIDImageFile = file;
				var reader = new FileReader();
				reader.onload = function (e) {
					$("#imgDIDImage").attr("src", e.target.result);
				}
				reader.readAsDataURL(DIDImageFile);
			}
		});

	});

	var DIDRegForm = {
		saveCallback: null,
		DID_id: null,
		open: function (DID_id, saveCallback) {
			this.saveCallback = saveCallback;
			this.DID_id = DID_id;

			DIDImageFile = new Object();

			if (this.DID_id == null) {
				$("#imgDIDImage").attr("src", contextPath + "images/no_image.png");
				$("#selUseYN").val("Y");
				$("#divDIDImage h4.modal-title").text("IMAGE 추가");
			}
			else {
				var params = {};
				params.ID = this.DID_id;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "GetDIDImageInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data.INFO.length > 0) {
							$("#imgDIDImage").attr("src", data.FILEPATH + data.INFO[0].IMAGE_NAME);
							$("#selUseYN").val(data.INFO[0].USE_YN);
						}
						else {
							alert("조회 오류");
						}
					}
				});
			}

			$("#divDIDImage").modal();
		},
		close: function () {
			$("#divDIDImage").modal("hide");
		},
		save: function () {
			var cmd = "";
			var params = {};
			if (this.DID_id == null) {
				cmd = "InsertDIDImage";
			}
			else {
				cmd = "UpdateDIDImageInfo";
				params.ID = this.DID_id;
			}
			params.USE_YN = $("#selUseYN").val();

			do_cmd.ajax_file({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: cmd,
				Params: params,
				Files: DIDImageFile,
				SuccessCallBack: function (data) {
					if (data) {
						DIDRegForm.close()
						DIDRegForm.saveCallback();
					}
					else {
						alert("오류!");
					}
				}
			});

		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divDIDImage">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body" style="min-height:500px;">
				<form>
					<div class="form-group text-center">
						<img src="<%=Page.ResolveUrl("~") %>images/no_image.png" style="width:100%; cursor: pointer;" alt="클릭하여 이미지 선택" id="imgDIDImage" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" />
						<input type="file" id="selectDIDImage" name="selectDIDImage" style="display: none;" />
						<p class="text-primary">이미지를 클릭하여 사진을 선택하세요.</p>
					</div>
					<div class="form-group">
						<label for="selUseYN">사용유무 : </label>
						<select class="form-control" id="selUseYN">
							<option value="Y">Y</option>
							<option value="N">N</option>
						</select>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>