﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Board.ascx.cs" Inherits="Include_Board_Board" %>
<%@ Register Src="~/Include/Board/FileProgress.ascx" TagPrefix="uc1" TagName="FileProgress" %>
<%@ Register Src="~/Include/Board/FileDownload.ascx" TagPrefix="uc1" TagName="FileDownload" %>
<%@ Register Src="~/Include/Board/FileUpload.ascx" TagPrefix="uc1" TagName="FileUpload" %>



<link href="<%=Page.ResolveUrl("~") %>Include/Board/Board.css" rel="stylesheet" />
<style>
	.non_display {
		display:none;
	}
	.pagination a[data-page] {
		cursor: pointer;
	}
</style>

<%--<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>--%>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<%--<script src="<%=Page.ResolveUrl("~") %>Resources/ckeditor/ckeditor.js"></script>--%>

<script>
	var BoardInfo = {
		AjaxUrl: "<%=Page.ResolveUrl("~") %>Include/Board/Board.aspx",
		ImgUploadUrl: "<%=Page.ResolveUrl("~") %>Include/Board/BoardImage.aspx",
		BoardType: "<%= this.Board_Type %>",
		Page_Size: <%= this.Page_Size %>,
		Store_Search_YN: "<%= this.Store_Search_YN %>",
		LIST_TYPE: "<%= this.LIST_TYPE %>",
		WRITABLE: "<%= this.WRITABLE %>",
		NOTICE_YN: "<%= this.NOTICE_YN %>",
		REPLY_CNT: <%= this.REPLY_CNT %>,
		FILE_SIZE: <%= this.FILE_SIZE %>,
		REPLY_WRITABLE: "<%= this.REPLY_WRITABLE %>",
		BOARD_ID: null,
		BOARD_FOLDER: "",
		BOARD_REPLY_NUM: null,
		BOARD_REPLY_FOLDER: ""
	};
</script>

<script src="<%=Page.ResolveUrl("~") %>Include/Board/Board.js"></script>
<uc1:FileProgress runat="server" ID="FileProgress" />
<div id="viewList" class="boardViewArea non_display">

	<div class="panel panel-default form-inline" style="padding: 5px; text-align: right; margin-top: 5px;">
		<select class="form-inline form-control" id="selSearchStore" style="width: 150px;"></select>
		<select class="form-inline form-control" id="selSearchCondition" style="width: 100px;">
			<option value="NAME">작성자</option>
			<option value="TITLE">제목</option>
		</select>
		<input class="form-control" id="txtSearchKeyword" style="width: 200px;" placeholder="검색" />
		<a class="btn btn-success" id="btnSearch">조회</a>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblBoardList">
		<thead></thead>
		<tbody></tbody>
	</table>

	<div class="text-center">
		<ul class="pagination"></ul>
	</div>

	<div class="text-right" style="margin: 10px 0px;">
		<a class="btn btn-primary" id="btnNewBoard">글쓰기</a>
	</div>

</div>

<div id="viewDetail" class="boardViewArea non_display">

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			<span id="vwDetailTitle">제목 타이틀입니다.</span>
			<div class="pull-right" style="color:gray; font-size: 12px;">
				<i class="fa fa-user fa-fw"></i><i id="vwDetailManager">관리자</i>  <i class="fa fa-clock-o fa-fw"></i><i id="vwDetailDate">2020-03-20 21:09:54</i>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body" id="vwDetailContents" style="min-height: 400px;"></div>
		<!-- /.panel-body -->
	</div>

	<uc1:FileDownload runat="server" ID="FileDownload" />

	<div class="text-right" style="margin: 10px 0px;">
		<button type="button" class="btn btn-primary" id="btnDetailToEdit">수정</button>
		<button type="button" class="btn btn-danger" id="btnDetailToDelete">삭제</button>
		<button type="button" class="btn btn-default" id="btnDetailToList">목록</button>
	</div>

	<div class="panel panel-default non_display" id="divReplyArea">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i> 답글
		</div>
		<div class="panel-body" id="divReplyBody">
			<ul class="chat"></ul>
		</div>
	</div>

	<div id="divReplyForm" class="non_display">
        <textarea class="form-control" id="txtReplyContents"></textarea>
		<div class="text-right" style="margin: 3px 0px;">
			<div class="btn-group">
				<button type="button" class="btn btn-primary btn-xs" id="btnReplySave">저장</button>
				<button type="button" class="btn btn-default btn-xs" id="btnReplyCancel">취소</button>
			</div>
		</div>
	</div>

</div>

<div id="viewForm" class="boardViewArea non_display">

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading" id="divFormTitle"></div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<form role="form">
                <div class="form-group input-group">
					<span class="input-group-addon">제목</span>
					<input type="text" class="form-control" placeholder="제목" id="txtTitle" />
				</div>
				<div class="form-group input-group" id="divNoticeGroup">
					<span class="input-group-addon">공지</span>
					<select class="form-control" id="selNoticeYN">
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
				</div>
				<textarea class="form-control" id="txtContents"></textarea>
				
				<uc1:FileUpload runat="server" ID="FileUpload" />
            </form>
		</div>
		<!-- /.panel-body -->
	</div>

	<div class="text-right">
		<a class="btn btn-primary" id="btnSave">저장</a>
		<a class="btn btn-primary" id="btnCancel">취소</a>
	</div>

</div>