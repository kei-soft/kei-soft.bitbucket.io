﻿<%@ Control Language="C#" ClassName="FileDownload" %>


<style type="text/css">
	#FileDownListArea td.emptyFile {height: 100px;}
	#FileDownListArea td, #FileDownListArea th {padding: 3px 0px;}
	#FileDownListArea a {cursor: pointer;}
</style>

<script type="text/javascript">	
	var FileDownController = {
		id : "#FileDownListArea",
		progCnt : 0,
		fileCnt : 0,
		jobNo : "",
		fileListUrl : "",
		fileList : null,
		completeCallback: null,
		download : function($file) {
			if ($file == undefined || $file == null) {
				this.setFileList();
			}
			else {
				this.fileList = new Object();
				this.fileList[$file.parent("td").prev("td").find("input[type='checkbox']").val()] = $file.text();
			}
			
			this.fileCnt = this.fileCount();
			if (this.fileCnt == 0) {
				alert("파일을 선택해 주세요.");
				return;
			}
			
			this.progCnt = 0;
			FileProgressController.init("FILE DOWNLOAD", this.fileCnt);
			
			setTimeout(function() {
        		FileProgressController.open();
        		FileDownController.download_file();
        	}, 200);
		},
		download_file : function() {
			var firstKey = "";
			for(var key in this.fileList) {
				firstKey = key;
				break;
			}
			
			if (firstKey != "") {
				$('#ProgressBarPopup').hide();
            	FileDownController.progCnt ++;
            	
            	FileProgressController.setFileCount(FileDownController.progCnt + " / " + FileDownController.fileCnt);
            	FileProgressController.setFileName(FileDownController.fileList[firstKey]);
            	FileProgressController.setFileProgress("0");
				
				var xhr = new XMLHttpRequest();
				xhr.open('POST', BoardInfo.AjaxUrl, true);
				xhr.responseType = 'blob';
				
				xhr.onprogress = function(pe) {
					console.log('progress');
					if (pe.lengthComputable) {
						var perc = Math.round((pe.loaded / pe.total) * 100);
                      	perc = perc > 100 ? 100 : perc;
                      	FileProgressController.setFileProgress(perc);
					}
				};
				
				xhr.onload = function(e) {
					if (this.status == 200) {
						var blob = this.response;
						if (navigator.appVersion.toString().indexOf('.NET') > 0) {
							window.navigator.msSaveBlob(blob, FileDownController.fileList[firstKey]);
		                	setTimeout(function() {
		            			delete FileDownController.fileList[firstKey];
		                		FileDownController.download_file();
	                    	}, 200);
						}
						else {
							var a = document.createElement("a");
							var blobUrl = window.URL.createObjectURL(new Blob([blob], {type: blob.type}));
							document.body.appendChild(a);
							a.style = "display: none";
							a.href = blobUrl;
							a.download = FileDownController.fileList[firstKey];
							a.click();
						}
					}
				};
				
				xhr.send("Command=download_file&folder=" + BoardInfo.BOARD_FOLDER + "&file=" + FileDownController.fileList[firstKey]);
			}
			else {
				FileProgressController.close();
			}
		},
		setFileList : function () {
			this.fileList = new Object();
			var $chk = $(this.id + " > tbody > tr > td > input[type='checkbox']:checked");
			$chk.each(function() {
				FileDownController.fileList[$(this).val()] = $(this).parent("td").next("td").text();
			});
		},
		fileCount : function() {
			var count = 0;
			for(var key in this.fileList) {
				count++;
			}
			return count;
		},
		displayFileList : function() {
			$(FileDownController.id + " .fileChkAll").prop("checked", false);
			var params = {};
			params.BOARD_FOLDER = BoardInfo.BOARD_FOLDER;

			do_cmd.ajax({
				URL: BoardInfo.AjaxUrl,
				async: true,
				Params: params,
				Command: "GetFileList",
				SuccessCallBack: function (data) {
					if (data.length > 0) {
						var strHtml = "";
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							//strHtml += "<td class=\"text-center\"><input type=\"checkbox\" value=\"" + data[i].NAME + "\" /></td>";
							strHtml += "<td class=\"text-left\" style=\"text-indent: 10px;\"><a>" + data[i].NAME + "</a></td>";
							strHtml += "<td class=\"text-center\">" + FileDownController.toFileSize(parseInt(data[i].SIZE)) + "</td>";
							strHtml += "</tr>";
						}
						$(FileDownController.id + " > tbody").html(strHtml);
						$("#divFileDownArea").show();
					}
					else {
						strHtml += "<tr><td colspan=\"3\" class=\"emptyFile text-center\">No Files</td></tr>";
						$(FileDownController.id + " > tbody").html(strHtml);
						$("#divFileDownArea").hide();
					}
				}
			});
		},
		toFileSize : function(num) {
			var size_type = "";
			var file_size = 0;
			if (num >= 1024 * 1024) { // MB
				size_type = "MB";
				file_size = Math.round(num / (1024 * 1024));
			}
			else if (num >= 1024) { // KB
				size_type = "KB";
				file_size = Math.round(num / 1024);
			}
			else { // Byte
				size_type = "Byte";
				file_size = num;
			}
			return file_size.toLocaleString() + size_type;
		}
	};
	
	$(function() {
		$(FileDownController.id + " .fileChkAll").on("click", function(){
			var bChekced = $(this).prop("checked");
			$(FileDownController.id + " > tbody input[type='checkbox']").each(function() {
				if (!$(this).prop("disabled")) {
					$(this).prop("checked", bChekced);
				}
			});
		});
		
		$(FileDownController.id + " > tbody").on("click", "tr > td > a", function () {
			FileDownPost(contextPath + "Include/Board/Board.aspx", "AjaxTest", "download_file", $(this).text()); return;
			FileDownController.download($(this));
		});
		
		$("#fileDown").on("click", function() {
			FileDownController.download();
		});
	});

	function FileDownPost(url, target, command, file) {
	var $frm = $("<form>").attr({
		"id": "frmPopup", "method": "post", "action": url, "target": target
	});

	$("<input>").attr({ "type": "hidden", "value": command, "name": "Command" }).appendTo($frm);
	$("<input>").attr({ "type": "hidden", "value": BoardInfo.BOARD_FOLDER, "name": "folder" }).appendTo($frm);
	$("<input>").attr({ "type": "hidden", "value": file, "name": "file" }).appendTo($frm);

	//window.open("", target, "");
	$("body").append($frm);

	$frm.submit();

	$frm.remove();

	return false;
}
</script>

<div class="panel panel-default non_display" style="margin-top: 20px;" id="divFileDownArea">
	<div class="panel-heading">
		첨부파일
		<div class="pull-right">
			<div class="btn-group">
				<%--<button type="button" class="btn btn-success btn-xs" id="fileDown">DOWNLOAD</button>--%>
			</div>
		</div>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">

		<%--<div class="list-group" id="FileDownListArea">
			<a class="list-group-item" style="cursor:pointer;">
				<i class="fa fa-file fa-fw"></i> New Comment
				<span class="pull-right text-muted small"><em>4 minutes ago</em></span>
			</a>
		</div>--%>

		<table class="table table-striped table-bordered table-hover" id="FileDownListArea">
			<colgroup>
				<%--<col style="width:30px;"/><!-- 체크박스 -->--%>
				<col /><!-- File Name -->
				<col style="width: 100px;"/><!-- File Size -->
			</colgroup>
			<thead>
				<tr>
					<%--<th class="text-center"><input type="checkbox" name="fileChkAll" class="fileChkAll" /></th>--%>
					<th class="text-center">File Name</th>
					<th class="text-center">File Size</th>
				</tr>
			</thead>
			<tbody><tr><td colspan="3" class="emptyFile text-center" style="vertical-align: middle;">No Files</td></tr></tbody>
		</table>

	</div>
	<!-- /.panel-body -->
</div>
