﻿<%@ Control Language="C#" ClassName="FileUpload" %>

<style type="text/css">
	#FileUploadListArea td.emptyFile {height: 100px; color: silver;}
	
	#FileUploadListArea td, #FileUploadListArea th {padding: 3px 0px;}}
</style>

<script type="text/javascript">	
	var FileUploadController = {
		id : "#FileUploadListArea",
		progCnt : 0,
		fileCnt : 0,
		maxSize : BoardInfo.FILE_SIZE * (1024 * 1024),
		fileOver : false,
		uploadUrl : "",
		fileList : new Object(),
		editFileList: [],
		delFileList: [],
		completeCallback: null,
		upload : function(completeCallback) {
			this.completeCallback = completeCallback;
			if (this.fileCnt == 0) {
				//alert("파일을 선택해 주세요");
				FileUploadController.completeCallback();
				return;
			}
			
			var totalSize = this.totalSize();
			if (this.fileOver) {
				alert("파일용량 초과\nMAX : " + this.toFileSize(this.maxSize));
				return;
			}

			this.progCnt = 0;
			FileProgressController.init("FILE UPLOAD", this.fileCnt);
			
			setTimeout(function() {
        		FileProgressController.open();
        		FileUploadController.upload_file();
        	}, 200);
		},
		upload_file : function() {
			var firstKey = "";
			for(var key in this.fileList) {
				firstKey = key;
				break;
			}
			
			if (firstKey != "") {
	        	var formdata = new FormData();
	        	formdata.append("Command", "upload_file");
	        	formdata.append("folder", BoardInfo.BOARD_FOLDER);
	        	formdata.append("uploadFile", this.fileList[firstKey]);
				
				$.ajax({
					type: "POST",
					url : BoardInfo.AjaxUrl,
	                dataType : "json",
	                processData : false,
	                contentType : false,
	                data : formdata,
	                beforeSend : function() {
	                	$('#ProgressBarPopup').hide();
	                	FileUploadController.progCnt ++;
	                	
	                	FileProgressController.setFileCount(FileUploadController.progCnt + " / " + FileUploadController.fileCnt);
	                	FileProgressController.setFileName(FileUploadController.fileList[firstKey].name);
	                	FileProgressController.setFileProgress("0");
	                },
	                xhr : function() {
	                	var xhr = $.ajaxSettings.xhr();
	                	
	                	xhr.upload.onprogress = function(event){
	                      	var perc = Math.round((event.loaded / event.total) * 100);
	                      	perc = perc > 100 ? 100 : perc;
	                      	FileProgressController.setFileProgress(perc);
	                     };
	                     return xhr ;
	                },
	                success : function(data){
						if (data.STATUS == "OK") {
							FileProgressController.setTotalProgress(Math.round((FileUploadController.progCnt / FileUploadController.fileCnt) * 100));
							delete FileUploadController.fileList[firstKey];
							setTimeout(function () {
								FileUploadController.upload_file();
							}, 200);
						}
	                	else {
	                		alert("Error!!!!");
	                		FileProgressController.close();
	                	}
	                },
	                error : function(e) {
	                	alert(e);
		              	//alert("System Error 관리자에게 문의하시기 바랍니다.");
		 	            console.log(e);
		 	           FileProgressController.close();
	                }
				});
			}
			else {
				this.displayFileList();
				FileProgressController.close();
				FileUploadController.completeCallback();
			}
		},
		fileCount : function() {
			var count = 0;
			for(var key in this.fileList) {
				count++;
			}
			return count;
		},
		displayFileList : function() {
			var strHtml = "";
			for (var i = 0; i < this.editFileList.length; i++) {
				if (this.editFileList[i] != undefined) {
					strHtml += "<tr>";
					strHtml += "<td class=\"text-center\"><input type=\"checkbox\" data-type=\"E\" data-index=\"" + i + "\" /></td>";
					strHtml += "<td class=\"text-left\" style=\"text-indent: 10px;\">" + this.editFileList[i].NAME + "</td>";
					strHtml += "<td class=\"text-center\">" + this.toFileSize(this.editFileList[i].SIZE) + "</td>";
					strHtml += "</tr>";
				}
			}

			for(var key in this.fileList) {
				strHtml += "<tr>";
				strHtml += "<td class=\"text-center\"><input type=\"checkbox\" data-type=\"N\" /></td>";
				strHtml += "<td class=\"text-left\" style=\"text-indent: 10px;\">" + this.fileList[key].name + "</td>";
				strHtml += "<td class=\"text-center\">" + this.toFileSize(this.fileList[key].size) + "</td>";
				strHtml += "</tr>";
			}
			
			if (strHtml == "") {
				strHtml += "<tr><td colspan=\"3\" class=\"emptyFile text-center\" style=\"vertical-align: middle;\">Drag File</td></tr>";
			}
			
			$(this.id + " > tbody").html(strHtml);
			var totalSize = this.totalSize();	
			this.fileOver = totalSize > this.maxSize;
			
			$("#fileTotalSize").html("[" + this.toFileSize(totalSize) + " / " + this.toFileSize(this.maxSize) + "]" + (this.fileOver ? " <b style=\"color:red;\"> - 파일용량 초과</b>" : ""));

			console.log(FileUploadController.editFileList);
			console.log(FileUploadController.delFileList);
			

			this.fileCnt = this.fileCount();
		},
		addFiles : function(files) {
			for (var i = 0; i < files.length; i++) {
				if (!(files[i].name in this.fileList)) {
					this.fileList[files[i].name] = files[i];
				}
			}

			this.displayFileList();
		},
		totalSize : function() {
			var total = 0;
			for (var i = 0; i < this.editFileList.length; i++) {
				if (this.editFileList[i] != undefined)
					total += this.editFileList[i].SIZE;
			}
			for (var key in this.fileList) {
				total += this.fileList[key].size;
			}
			return total;
		},
		toFileSize : function(num) {
			var size_type = "";
			var file_size = 0;
			if (num >= 1024 * 1024) { // MB
				size_type = " MB";
				file_size = Math.round(num / (1024 * 1024));
			}
			else if (num >= 1024) { // KB
				size_type = " KB";
				file_size = Math.round(num / 1024);
			}
			else { // Byte
				size_type = " Byte";
				file_size = num;
			}
			return file_size.toLocaleString() + size_type;
		},
		deleteFiles: function (callback) {
			if (this.delFileList.length > 0) {
				var params = {};
				params.ATTACH_FOLDER = BoardInfo.BOARD_FOLDER;
				params.FileNames = this.delFileList;

				do_cmd.ajax({
					URL: BoardInfo.AjaxUrl,
					async: true,
					Params: params,
					Command: "UpdateBoardFile",
					SuccessCallBack: function (data) {

						if (data) {
							if (callback) callback();
						}

					}
				});
			}
			else {
				if (callback) callback();
			}
		}
	};

	$(function() {
		$(FileUploadController.id).on("dragover", function(e) {
			e.preventDefault();
			e.stopPropagation();
		});

		$(FileUploadController.id).on("dragenter", function(e) {
			e.preventDefault();
			e.stopPropagation();
		});

		$(FileUploadController.id).on("drop", function(e) {
			if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length) {
				e.preventDefault();
				e.stopPropagation();
				
				FileUploadController.addFiles(e.originalEvent.dataTransfer.files);
				$(document).focus();
			}
		});
		
		$("#fileSelect").on("click", function() {
			$("#selectFiles").click();
		});
		
		$("#selectFiles").on("change", function() {
			FileUploadController.addFiles($(this)[0].files);
		});
		
		$("#fileDelete").on("click", function() {
			var $chk = $(FileUploadController.id + " > tbody > tr > td > input[type='checkbox']:checked");
			
			if ($chk.length == 0) {
				alert("파일을 선택해 주세요.");
				return;
			}
			
			$chk.each(function () {
				if ($(this).data("type") == "N") {
					delete FileUploadController.fileList[$(this).parent("td").next("td").text()];
				}
				else if ($(this).data("type") == "E") {
					var index = parseInt($(this).data("index"));
					var file = {};
					file.NAME = FileUploadController.editFileList[index].NAME;
					FileUploadController.delFileList.push(file);
					delete FileUploadController.editFileList[index];
				}
			});
			FileUploadController.displayFileList();
		});
		
		$(FileUploadController.id + " .chkAll").on("click", function(){
			var bChekced = $(this).prop("checked");
			$(FileUploadController.id + " > tbody input[type='checkbox']").each(function() {
				if (!$(this).prop("disabled")) {
					$(this).prop("checked", bChekced);
				}
			});
		});
		
		$("#fileTotalSize").html("[" + FileUploadController.toFileSize(0) + " / " + FileUploadController.toFileSize(FileUploadController.maxSize) + "]");
	});
</script>

<input type="file" id="selectFiles" name="selectFiles" style="display: none;" multiple />
<div class="panel panel-default" style="margin-top: 20px;">
	<div class="panel-heading">
		파일첨부 <span id="fileTotalSize"></span>
		<div class="pull-right">
			<div class="btn-group">
				<button type="button" class="btn btn-primary btn-xs" id="fileSelect">파일첨부</button>
				<button type="button" class="btn btn-danger btn-xs" id="fileDelete">삭제</button>
			</div>
		</div>
	</div>
	<!-- /.panel-heading -->
	<div class="panel-body">

		<table class="table table-striped table-bordered table-hover" id="FileUploadListArea">
			<colgroup>
				<col style="width:30px;"/><!-- 체크박스 -->
				<col /><!-- File Name -->
				<col style="width: 100px;"/><!-- File Size -->
			</colgroup>
			<thead>
				<tr>
					<th class="text-center"><input type="checkbox" name="chkAll" class="chkAll" /></th>
					<th class="text-center">File Name</th>
					<th class="text-center">File Size</th>
				</tr>
			</thead>
			<tbody><tr><td colspan="3" class="emptyFile text-center" style="vertical-align: middle;">Drag File</td></tr></tbody>
		</table>

	</div>
	<!-- /.panel-body -->
</div>

