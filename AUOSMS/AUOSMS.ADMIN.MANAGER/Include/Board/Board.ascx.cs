﻿using System.ComponentModel;

public partial class Include_Board_Board : System.Web.UI.UserControl
{
	[Category("Board_Type"), Description("게시판 유형(NOTICE:공지사항, CHANNEL:소통채널)")]
	public string Board_Type
	{
		get;set;
	}

	[Category("Page_Size"), Description("한페이지 게시물 수")]
	public string Page_Size
	{
		get;set;
	}

	[Category("Store_Search_YN"), Description("매장 검색이 가능여부값(Y/N)")]
	public string Store_Search_YN
	{
		get; set;
	}

	[Category("LIST_TYPE"), Description("전체목록인지 본매장글목록인지 구분값(ALL:전체, SELF:상점글만)")]
	public string LIST_TYPE
	{
		get; set;
	}

	[Category("WRITABLE"), Description("글쓰기 가능 여부(Y/N)")]
	public string WRITABLE
	{
		get; set;
	}

	[Category("NOTICE_YN"), Description("공지입력가능여부(Y/N)")]
	public string NOTICE_YN
	{
		get; set;
	}

	[Category("REPLY_CNT"), Description("답글갯수")]
	public string REPLY_CNT
	{
		get; set;
	}

	[Category("FILE_SIZE"), Description("파일업로드 용량(MB)")]
	public string FILE_SIZE
	{
		get; set;
	}

	[Category("REPLY_WRITABLE"), Description("댓글입력가능여부(Y/N)")]
	public string REPLY_WRITABLE
	{
		get; set;
	}
}