﻿using AUOSMS.ADMIN.UTIL;
using AUOSMS.ADMIN.UTIL.Authentication;
using AUOSMS.ADMIN.UTIL.DataBase;
using AUOSMS.ADMIN.UTIL.DataBase.DBAgent;

using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System.Web.Configuration;

public partial class Include_Board_Board : System.Web.UI.Page
{
	string connectionString = ConfigurationManager.ConnectionStrings["AUOSMS_MYSQL"].ConnectionString;

	protected void Page_Load(object sender, EventArgs e)
	{
		AJAX.Add3P3Header(Response);

		object jsonData;
		bool bAjax = true;
		try
		{
			jsonData = AJAX.CheckReferrer(Request);
			if (jsonData == null)
			{
				LOG.Write(Request, "AJAX START - CMD : {0}, PARAMS : {1}", Request.Form["Command"], Request.Form["Params"]);

				switch (Request.Form["Command"])
				{
					case "GetNewGUID":
						jsonData = GetNewGUID();
						break;
					case "upload_file":
						jsonData = FileUpload(Request.Form["folder"], Request.Files["uploadFile"]);
						break;
					case "download_file":
						FileDownload(Request.Form["folder"], Request.Form["file"]);
						bAjax = false;
						break;
					case "GetFileList":
						jsonData = GetFileList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreList":
						jsonData = GetStoreList();
						break;
					case "GetBoardList":
						jsonData = GetBoardList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetBoardGet":
						jsonData = GetBoardGet(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertBoard":
						jsonData = InsertBoard(JObject.Parse(Request.Form["Params"]), Request.Form["Contents"]);
						break;
					case "UpdateBoardFile":
						jsonData = UpdateBoardFile(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateBoard":
						jsonData = UpdateBoard(JObject.Parse(Request.Form["Params"]), Request.Form["Contents"]);
						break;
					case "DeleteBoard":
						jsonData = DeleteBoard(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetBoardReplyList":
						jsonData = GetBoardReplyList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetBoardReplyGet":
						jsonData = GetBoardReplyGet(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertBoardReply":
						jsonData = InsertBoardReply(JObject.Parse(Request.Form["Params"]), Request.Form["Contents"]);
						break;
					case "UpdateBoardReply":
						jsonData = UpdateBoardReply(JObject.Parse(Request.Form["Params"]), Request.Form["Contents"]);
						break;
					case "DeleteBoardReply":
						jsonData = DeleteBoardReply(JObject.Parse(Request.Form["Params"]));
						break;
					default:
						jsonData = new { STATUS = "WrongCommand", MSG = string.Format("잘못된 명령어입니다 ({0})", Request.Form["Command"]) };
						break;
				}
			}
		}
		catch (Exception ex)
		{
			jsonData = new { STATUS = "SystemError", MSG = ex.ToString() };
		}

		if (bAjax)
		{
			LOG.Write(Request, "AJAX RESULT - {0}", JsonConvertor.SerializeObject(jsonData));
			AJAX.PrintJson(Response, jsonData);
		}
	}

	object GetStoreList()
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 10, "1000"));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, "0"));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { LIST = ds.Tables[1] } };
	}

	object GetNewGUID()
	{
		return new { STATUS = "OK", RESULT = Guid.NewGuid().ToString() };
	}

	object FileUpload(string folder, System.Web.HttpPostedFile uploads)
	{
		string file = Path.GetFileName(uploads.FileName);
		string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/BOARD/FILES/" + folder + "/");

		DirectoryInfo dir = new DirectoryInfo(dirPath);
		if (!dir.Exists)
			dir.Create();

		uploads.SaveAs(dirPath + file);

		return new { STATUS = "OK" };
	}

	void FileDownload(string folder, string file)
	{
		string filePath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/BOARD/FILES/" + folder + "/" + file);
		//Response.ClearContent();
		//Response.Clear();
		//Response.ContentType = "text/plain";
		//Response.AddHeader("Content-Disposition", "attachment; filename=" + file + ";");
		//Response.TransmitFile(filePath);
		//Response.Flush();
		//Response.End();

		Response.Clear();
		Response.ContentType = "application/octet-stream";
		Response.AddHeader("content-Disposition", "attachment; filename=" + Server.UrlPathEncode(file));
		Response.WriteFile(filePath);
		Response.End();
	}

	object GetFileList(JObject Params)
	{
		string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/BOARD/FILES/" + Params.Value<string>("BOARD_FOLDER") + "/");
		DirectoryInfo dir = new DirectoryInfo(dirPath);
		if (dir.Exists)
		{
			var files = from f in dir.GetFiles()
						orderby f.CreationTime descending
						select new { NAME = f.Name, SIZE = f.Length };

			return new { STATUS = "OK", RESULT = files };
		}
		else
		{
			return new { STATUS = "OK", RESULT = new List<FileInfo>() };
		}
	}

	object GetBoardList(JObject Params)
	{
		string store_id = "0";
		if (Params.ContainsKey("STORE_ID"))
		{
			if (Params.Value<string>("STORE_ID").Equals("SELF"))
				store_id = AUOSMSUser.GetStoreID();
			else
				store_id = Params.Value<string>("STORE_ID");
		}

		string name = string.Empty, title = string.Empty;
		if (Params.Value<string>("CONDITION").Equals("NAME"))
		{
			name = Params.Value<string>("KEYWORD");
			title = string.Empty;
		}
		else if (Params.Value<string>("CONDITION").Equals("TITLE"))
		{
			name = string.Empty;
			title = Params.Value<string>("KEYWORD");
		}
		else
		{
			name = string.Empty;
			title = string.Empty;
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_BOARD_TYPE", MySQLDataType.VARCHAR, 40, Params.Value<string>("BOARD_TYPE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, store_id));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_NAME", MySQLDataType.VARCHAR, 10, name));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_TITLE", MySQLDataType.VARCHAR, 10, title));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1] } };
	}

	object GetBoardGet(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));

		DataTable dt;
		bool Editable = false;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_GET", SQLParams))
		{
			dt = dbAgent.DataTable();

			if (dt.Rows.Count > 0)
			{
				Editable = (dt.Rows[0]["CREATE_USER"].ToString() == AUOSMSUser.GetCode()) || (AUOSMSUser.GetLevel() >= 99);
			}
		}

		return new { STATUS = "OK", RESULT = new { LIST = dt, EDIT = Editable } };
	}

	object InsertBoard(JObject Params, string Contents)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_TYPE", MySQLDataType.VARCHAR, 40, Params.Value<string>("BOARD_TYPE")));
		SQLParams.Add(new MySQLParamModel("P_TITLE", MySQLDataType.VARCHAR, 200, Params.Value<string>("TITLE")));
		SQLParams.Add(new MySQLParamModel("P_CONTENTS", MySQLDataType.TEXT, 20000, Contents));
		SQLParams.Add(new MySQLParamModel("P_NOTICE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("NOTICE_YN")));
		SQLParams.Add(new MySQLParamModel("P_ATTACH_FOLDER", MySQLDataType.VARCHAR, 40, Params.Value<string>("ATTACH_FOLDER")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateBoardFile(JObject Params)
	{
		string folder = Params.Value<string>("ATTACH_FOLDER");
		JArray files = (JArray)Params["FileNames"];
		foreach (JObject file in files)
		{
			string filePath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/BOARD/FILES/" + folder + "/" + file.Value<string>("NAME"));
			FileInfo f = new FileInfo(filePath);
			if (f.Exists)
			{
				f.Delete();
			}
		}

		return new { STATUS = "OK", RESULT = true };
	}

	object UpdateBoard(JObject Params, string Contents)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));
		SQLParams.Add(new MySQLParamModel("P_TITLE", MySQLDataType.VARCHAR, 200, Params.Value<string>("TITLE")));
		SQLParams.Add(new MySQLParamModel("P_CONTENTS", MySQLDataType.TEXT, 20000, Contents));
		SQLParams.Add(new MySQLParamModel("P_NOTICE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("NOTICE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteBoard(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetBoardReplyList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_REPLY_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		dt.Columns.Add(new DataColumn("EDIT"));
		foreach(DataRow dr in dt.Rows)
		{
			dr["EDIT"] = (dr["CREATE_USER"].ToString() == AUOSMSUser.GetCode()) || (AUOSMSUser.GetLevel() >= 99) ? "Y" : "N";
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetBoardReplyGet(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));
		SQLParams.Add(new MySQLParamModel("P_REPLY_NUM", MySQLDataType.INT32, 10, Params.Value<string>("REPLY_NUM")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_REPLY_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertBoardReply(JObject Params, string Contents)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));
		SQLParams.Add(new MySQLParamModel("P_CONTENTS", MySQLDataType.TEXT, 20000, Contents));
		SQLParams.Add(new MySQLParamModel("P_ATTACH_FOLDER", MySQLDataType.VARCHAR, 40, Params.Value<string>("ATTACH_FOLDER")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, AUOSMSUser.GetStoreID()));
		SQLParams.Add(new MySQLParamModel("P_P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_REPLY_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateBoardReply(JObject Params, string Contents)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));
		SQLParams.Add(new MySQLParamModel("P_REPLY_NUM", MySQLDataType.INT32, 10, Params.Value<string>("REPLY_NUM")));
		SQLParams.Add(new MySQLParamModel("P_CONTENTS", MySQLDataType.TEXT, 20000, Contents));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_REPLY_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteBoardReply(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_BOARD_ID", MySQLDataType.INT32, 10, Params.Value<string>("BOARD_ID")));
		SQLParams.Add(new MySQLParamModel("P_REPLY_NUM", MySQLDataType.INT32, 10, Params.Value<string>("REPLY_NUM")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_BOARD_REPLY_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
}