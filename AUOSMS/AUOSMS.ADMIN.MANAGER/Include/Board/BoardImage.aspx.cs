﻿using AUOSMS.ADMIN.UTIL;

using System;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;

public partial class Include_Board_BoardImage : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		AJAX.Add3P3Header(Response);

		Response.ContentType = "application/json";
		HttpPostedFile uploads = Request.Files["upload"];
		string CKEditorFuncNum = Request["CKEditorFuncNum"];
		string folder = Request.QueryString["folder"];
		string file = Path.GetFileName(uploads.FileName);
		string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/BOARD/IMAGES/" + folder + "/");

		DirectoryInfo dir = new DirectoryInfo(dirPath);
		if (!dir.Exists)
			dir.Create();

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/BOARD/IMAGES/" + folder + "/";
		uploads.SaveAs(dirPath + file);

		AJAX.PrintJson(Response, new { uploaded = "1", fileName = file, url = imgPath + file });
	}
}