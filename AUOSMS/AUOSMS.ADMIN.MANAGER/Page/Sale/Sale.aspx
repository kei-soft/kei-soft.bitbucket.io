﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">매출 내역</h1>

	<div class="panel panel-default" style="padding: 5px; text-align: right; margin-top: 5px;">
		매출일자 : 
		<input class="form-control datepicker readonly" name="txtFR_DATE" id="txtFR_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd") %>">
		~
		<input class="form-control datepicker readonly" name="txtTO_DATE" id="txtTO_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.ToString("yyyy-MM-dd") %>">
		<a class="btn btn-success" id="btnSearch">조회</a>
		<a class="btn btn-success" id="btnToExcel">Excel</a>
	</div>

	<div class="text-right" style="margin-bottom:5px;">
		<i class="fa fa-caret-right fa-fw"></i>총
		<span id="spTotalCnt">0건</span>
		&nbsp; &nbsp;
		<i class="fa fa-krw fa-fw"></i>
		<span id="spTotalPrice">0원</span>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblSaleList">
		<thead>
			<tr>
				<%--<th class="text-center">결제번호</th>--%>
				<th class="text-center">결제일시</th>
				<th class="text-center">결제타입</th>
				<th class="text-center">결제금액</th>
				<th class="text-center">주문방법</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<%--<div class="text-center">
		<ul class="pagination pagination-sm">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
		</ul>
	</div>--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.PAGE = 0;
				SearchInfo.START_DATE = $("#txtFR_DATE").val();
				SearchInfo.END_DATE = $("#txtTO_DATE").val();
				GetSaleList();
			});

			$("#btnToExcel").click(function () {
				do_cmd.excelExport("tblSaleList", "매출내역");
			});

			GetSaleList();
		});

		function GetSaleList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreSaleList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							//strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_CODE + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_DATE + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_TYPE + "</td>";
							strHtml += "<td class=\"text-right\">" + String(data.LIST[i].PAY_PRICE).currencyFormat() + "원</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_PATH + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"4\" class=\"text-center\">매출내역이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#spTotalCnt").text(String(data.TOTAL_CNT).currencyFormat() + "건");
					$("#spTotalPrice").text(String(data.TOTAL_PRICE).currencyFormat() + "원");

					$("#tblSaleList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 1000;
			SearchInfo.START_DATE = $("#txtFR_DATE").val();
			SearchInfo.END_DATE = $("#txtTO_DATE").val();
		}
	</script>
</asp:Content>

