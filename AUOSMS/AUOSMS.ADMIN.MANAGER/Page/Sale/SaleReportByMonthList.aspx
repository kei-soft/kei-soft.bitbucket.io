﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
	<style>
		table.report_table th, td{
			padding:8px 3px !important;
			vertical-align: middle !important;
		}
		table.report_table tr.total{
			font-weight: bold;
			background-color: antiquewhite;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">월별매출 종합</h1>

	<div class="panel panel-default form-inline" style="padding: 5px; text-align: right; margin-top: 5px;">
		매출년월 :
		<select class="form-inline form-control" style="width: 80px;" id="selStartYear">
			<option value="2020">2020</option>
			<option value="2021">2021</option>
			<option value="2022">2022</option>
			<option value="2023">2023</option>
			<option value="2024">2024</option>
		</select>년
		<select class="form-inline form-control" style="width: 80px;" id="selStartMonth">
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
		</select>년
		~
		<select class="form-inline form-control" style="width: 80px;" id="selEndYear">
			<option value="2020">2020</option>
			<option value="2021">2021</option>
			<option value="2022">2022</option>
			<option value="2023">2023</option>
			<option value="2024">2024</option>
		</select>년
		<select class="form-inline form-control" style="width: 80px;" id="selEndMonth">
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
		</select>년
		<a class="btn btn-success" id="btnSearch">조회</a>
		<a class="btn btn-success" id="btnToExcel">Excel</a>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover report_table" id="tblSaleList">
		<thead>
			<tr>
				<th class="text-center" rowspan="2">년-월</th>
				<th class="text-center" rowspan="2">영업일수</th>
				<th class="text-center" colspan="7">매출현황</th>
				<th class="text-center" colspan="2">고객</th>
				<th class="text-center" colspan="7">결제수단</th>
				<th class="text-center" colspan="5">할인내역</th>
			</tr>
			<tr>
				<th class="text-center">총매출</th>
				<th class="text-center">총할인</th>
				<th class="text-center">실매출</th>
				<th class="text-center">가액</th>
				<th class="text-center">부가세</th>
				<th class="text-center">영수건수</th>
				<th class="text-center">영수단가</th>

				<th class="text-center">고객수</th>
				<th class="text-center">객단가</th>
				
				<th class="text-center">결제합계</th>
				<th class="text-center">신용카드</th>
				<th class="text-center">현금</th>
				<th class="text-center">회원포인트</th>
				<th class="text-center">모바일쿠폰</th>
				<th class="text-center">선불카드</th>
				<th class="text-center">온라인결제</th>

				<th class="text-center">일반</th>
				<th class="text-center">서비스</th>
				<th class="text-center">제휴</th>
				<th class="text-center">쿠폰</th>
				<th class="text-center">모바일회원</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.START_MON = $("#selStartYear").val() + "-" + $("#selStartMonth").val();
				SearchInfo.END_MON = $("#selEndYear").val() + "-" + $("#selEndMonth").val();
				GetSaleList();
			});

			$("#btnToExcel").click(function () {
				do_cmd.excelExport("tblSaleList", "월별매출 종합");
			});

			GetSaleList();
		});

		function GetSaleList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetSaleReportByMonthList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						var tot_sale_day = tot_price = tot_dc_price = tot_real_price = tot_ori_price = tot_vat_price = tot_receipt_count = tot_receipt_unit_cost = tot_customer_count = tot_customer_unit_cost = tot_total_price = tot_card_price = tot_cash_price = 0;
						for (var i = 0; i < data.length; i++) {
							tot_sale_day += data[i].SALE_DAY;
							tot_price += data[i].PRICE;
							tot_dc_price += data[i].DC_PRICE;
							tot_real_price += data[i].REAL_PRICE;
							tot_ori_price += data[i].ORI_PRICE;
							tot_vat_price += data[i].VAT_PRICE;
							tot_receipt_count += data[i].RECEIPT_COUNT;
							tot_receipt_unit_cost += data[i].RECEIPT_UNIT_COST;
							tot_customer_count += data[i].CUSTOMER_COUNT;
							tot_customer_unit_cost += data[i].CUSTOMER_UNIT_COST;
							tot_total_price += data[i].TOTAL_PRICE;
							tot_card_price += data[i].CARD_PRICE;
							tot_cash_price += data[i].CASH_PRICE;

							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].PAY_MONTH + "</td>"; // 년-월
							strHtml += "<td class=\"text-center\">" + data[i].SALE_DAY + "</td>"; // 영업일수
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE).currencyFormat() + "</td>"; // 매출현황/총매출
							strHtml += "<td class=\"text-center\">" + String(data[i].DC_PRICE).currencyFormat() + "</td>"; // 매출현황/총할인
							strHtml += "<td class=\"text-center\">" + String(data[i].REAL_PRICE).currencyFormat() + "</td>"; // 매출현황/실매출
							strHtml += "<td class=\"text-center\">" + String(data[i].ORI_PRICE).currencyFormat() + "</td>"; // 매출현황/가액
							strHtml += "<td class=\"text-center\">" + String(data[i].VAT_PRICE).currencyFormat() + "</td>"; // 매출현황/부가세
							strHtml += "<td class=\"text-center\">" + data[i].RECEIPT_COUNT + "</td>"; // 매출현황/영수건수
							strHtml += "<td class=\"text-center\">" + String(data[i].RECEIPT_UNIT_COST).currencyFormat() + "</td>"; // 매출현황/영수단가
							strHtml += "<td class=\"text-center\">" + data[i].CUSTOMER_COUNT + "</td>"; // 고객/고객수
							strHtml += "<td class=\"text-center\">" + String(data[i].CUSTOMER_UNIT_COST).currencyFormat() + "</td>"; // 고객/객단가
							strHtml += "<td class=\"text-center\">" + String(data[i].TOTAL_PRICE).currencyFormat() + "</td>"; // 결제수단/결제합계
							strHtml += "<td class=\"text-center\">" + String(data[i].CARD_PRICE).currencyFormat() + "</td>"; // 결제수단/신용카드
							strHtml += "<td class=\"text-center\">" + String(data[i].CASH_PRICE).currencyFormat() + "</td>"; // 결제수단/현금
							strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/회원포인트
							strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/모바일쿠폰
							strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/선불카드
							strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/온라인결제
							strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/일반
							strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/서비스
							strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/제휴
							strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/쿠폰
							strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/모바일회원
							strHtml += "</tr>";
						}

						strHtml += "<tr class=\"total\">";
						strHtml += "<td class=\"text-center\">합계</td>"; // 합계
						strHtml += "<td class=\"text-center\">" + tot_sale_day + "</td>"; // 영업일수
						strHtml += "<td class=\"text-center\">" + String(tot_price).currencyFormat() + "</td>"; // 매출현황/총매출
						strHtml += "<td class=\"text-center\">" + String(tot_dc_price).currencyFormat() + "</td>"; // 매출현황/총할인
						strHtml += "<td class=\"text-center\">" + String(tot_real_price).currencyFormat() + "</td>"; // 매출현황/실매출
						strHtml += "<td class=\"text-center\">" + String(tot_ori_price).currencyFormat() + "</td>"; // 매출현황/가액
						strHtml += "<td class=\"text-center\">" + String(tot_vat_price).currencyFormat() + "</td>"; // 매출현황/부가세
						strHtml += "<td class=\"text-center\">" + tot_receipt_count + "</td>"; // 매출현황/영수건수
						strHtml += "<td class=\"text-center\">" + String(tot_receipt_unit_cost).currencyFormat() + "</td>"; // 매출현황/영수단가
						strHtml += "<td class=\"text-center\">" + tot_customer_count + "</td>"; // 고객/고객수
						strHtml += "<td class=\"text-center\">" + String(tot_customer_unit_cost).currencyFormat() + "</td>"; // 고객/객단가
						strHtml += "<td class=\"text-center\">" + String(tot_total_price).currencyFormat() + "</td>"; // 결제수단/결제합계
						strHtml += "<td class=\"text-center\">" + String(tot_card_price).currencyFormat() + "</td>"; // 결제수단/신용카드
						strHtml += "<td class=\"text-center\">" + String(tot_cash_price).currencyFormat() + "</td>"; // 결제수단/현금
						strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/회원포인트
						strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/모바일쿠폰
						strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/선불카드
						strHtml += "<td class=\"text-center\">0</td>"; // 결제수단/온라인결제
						strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/일반
						strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/서비스
						strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/제휴
						strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/쿠폰
						strHtml += "<td class=\"text-center\">0</td>"; // 할인내역/모바일회원
						strHtml += "</tr>";
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"23\" class=\"text-center\">매출내역이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblSaleList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			$("#selStartYear").val("<%=DateTime.Today.Year.ToString()%>");
			$("#selStartMonth").val("01");
			$("#selEndYear").val("<%=DateTime.Today.Year.ToString()%>");
			$("#selEndMonth").val("<%=string.Format("{0:00}", DateTime.Today.Month)%>");
			
			SearchInfo.START_MON = $("#selStartYear").val() + "-" + $("#selStartMonth").val();
			SearchInfo.END_MON = $("#selEndYear").val() + "-" + $("#selEndMonth").val();
		}
	</script>
</asp:Content>

