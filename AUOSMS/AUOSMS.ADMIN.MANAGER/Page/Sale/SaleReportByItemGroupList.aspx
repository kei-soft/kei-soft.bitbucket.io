﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
	<style>
		table.report_table th, td{
			padding:8px 3px !important;
			vertical-align: middle !important;
		}
		table.report_table tr.total{
			font-weight: bold;
			background-color: antiquewhite;
		}
		table.report_table tr.sub_total{
			background-color: azure;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">상품별 매출 상세 현황</h1>

	<div class="panel panel-default" style="padding: 5px; text-align: right; margin-top: 5px;">
		매출일자 : 
		<input class="form-control datepicker readonly" name="txtFR_DATE" id="txtFR_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd") %>">
		~
		<input class="form-control datepicker readonly" name="txtTO_DATE" id="txtTO_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.ToString("yyyy-MM-dd") %>">
		<a class="btn btn-success" id="btnSearch">조회</a>
		<a class="btn btn-success" id="btnToExcel">Excel</a>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover report_table" id="tblSaleList">
		<thead>
			<tr>
				<th class="text-center" rowspan="2">No.</th>
				<th class="text-center" rowspan="2">대분류</th>
				<th class="text-center" rowspan="2">상품코드</th>
				<th class="text-center" rowspan="2">상품명</th>
				<th class="text-center" rowspan="2">수량</th>
				<th class="text-center" rowspan="2">총매출액</th>
				<th class="text-center" rowspan="2">총할인액</th>
				<th class="text-center" rowspan="2">실매출액</th>
				<th class="text-center" colspan="5">할인</th>
			</tr>
			<tr>
				<th class="text-center">일반</th>
				<th class="text-center">서비스</th>
				<th class="text-center">제휴</th>
				<th class="text-center">쿠폰</th>
				<th class="text-center">모바일</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.START_DATE = $("#txtFR_DATE").val();
				SearchInfo.END_DATE = $("#txtTO_DATE").val();
				GetSaleList();
			});

			$("#btnToExcel").click(function () {
				do_cmd.excelExport("tblSaleList", "상품별 매출 상세 현황");
			});

			GetSaleList();
		});

		function GetSaleList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetSaleReportByItemGroupList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					var tot_quantity = tot_total_price = 0;
					var group_quantity = group_total_price = 0;
					var cur_group_code = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							tot_quantity += data[i].QUANTITY;
							tot_total_price += data[i].TOTAL_PRICE;

							group_quantity += data[i].QUANTITY;
							group_total_price += data[i].TOTAL_PRICE;

							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + (i + 1) + "</td>"; // No.
							if (cur_group_code != data[i].ITEM_GROUP_CODE) {
								cur_group_code = data[i].ITEM_GROUP_CODE;
								strHtml += "<td class=\"text-center\" rowspan=\"" + data[i].ITEM_COUNT + "\">" + data[i].ITEM_GROUP_NAME + "</td>"; // 대분류
							}
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_ID + "</td>"; // 상품코드
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_NAME + "</td>"; // 상품명
							strHtml += "<td class=\"text-center\">" + data[i].QUANTITY + "</td>"; // 수량
							strHtml += "<td class=\"text-center\">" + String(data[i].TOTAL_PRICE).currencyFormat() + "</td>"; // 총매출액
							strHtml += "<td class=\"text-center\">0</td>"; // 총할인액
							strHtml += "<td class=\"text-center\">" + String(data[i].TOTAL_PRICE).currencyFormat() + "</td>"; // 실매출액
							strHtml += "<td class=\"text-center\">0</td>"; // 할인/일반
							strHtml += "<td class=\"text-center\">0</td>"; // 할인/서비스
							strHtml += "<td class=\"text-center\">0</td>"; // 할인/제휴
							strHtml += "<td class=\"text-center\">0</td>"; // 할인/쿠폰
							strHtml += "<td class=\"text-center\">0</td>"; // 할인/모바일
							strHtml += "</tr>";

							if (data[i + 1] == null || data[i + 1].ITEM_GROUP_CODE != data[i].ITEM_GROUP_CODE) {
								strHtml += "<tr class=\"sub_total\">";
								strHtml += "<td class=\"text-center\" colspan=\"4\">소계 : " + data[i].ITEM_GROUP_NAME + "</td>";
								strHtml += "<td class=\"text-center\">" + String(group_quantity).currencyFormat() + "</td>"; // 수량
								strHtml += "<td class=\"text-center\">" + String(group_total_price).currencyFormat() + "</td>"; // 총매출액
								strHtml += "<td class=\"text-center\">0</td>"; // 총할인액
								strHtml += "<td class=\"text-center\">" + String(group_total_price).currencyFormat() + "</td>"; // 실매출액
								strHtml += "<td class=\"text-center\">0</td>"; // 할인/일반
								strHtml += "<td class=\"text-center\">0</td>"; // 할인/서비스
								strHtml += "<td class=\"text-center\">0</td>"; // 할인/제휴
								strHtml += "<td class=\"text-center\">0</td>"; // 할인/쿠폰
								strHtml += "<td class=\"text-center\">0</td>"; // 할인/모바일
								strHtml += "</tr>";

								group_quantity = group_total_price = 0;
							}
						}
						
						strHtml += "<tr class=\"total\">";
						strHtml += "<td class=\"text-center\" colspan=\"4\">합계</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_quantity).currencyFormat() + "</td>"; // 수량
						strHtml += "<td class=\"text-center\">" + String(tot_total_price).currencyFormat() + "</td>"; // 총매출액
						strHtml += "<td class=\"text-center\">0</td>"; // 총할인액
						strHtml += "<td class=\"text-center\">" + String(tot_total_price).currencyFormat() + "</td>"; // 실매출액
						strHtml += "<td class=\"text-center\">0</td>"; // 할인/일반
						strHtml += "<td class=\"text-center\">0</td>"; // 할인/서비스
						strHtml += "<td class=\"text-center\">0</td>"; // 할인/제휴
						strHtml += "<td class=\"text-center\">0</td>"; // 할인/쿠폰
						strHtml += "<td class=\"text-center\">0</td>"; // 할인/모바일
						strHtml += "</tr>";
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"13\" class=\"text-center\">매출내역이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblSaleList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.START_DATE = $("#txtFR_DATE").val();
			SearchInfo.END_DATE = $("#txtTO_DATE").val();
		}
	</script>
</asp:Content>

