﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">메뉴별 매출 내역</h1>

	<div class="panel panel-default form-inline" style="padding: 5px; text-align: right; margin-top: 5px;">
		메뉴그룹 :
		<select class="form-inline form-control" style="width: 150px;" id="selItemGroup"></select>
		&nbsp; &nbsp;
		메뉴 :
		<select class="form-inline form-control" style="width: 150px;" id="selItem"></select>
		&nbsp; &nbsp;
		매출일자 : 
		<input class="form-control datepicker readonly" name="txtFR_DATE" id="txtFR_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd") %>">
		~
		<input class="form-control datepicker readonly" name="txtTO_DATE" id="txtTO_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.ToString("yyyy-MM-dd") %>">
		<a class="btn btn-success" id="btnSearch">조회</a>
		<a class="btn btn-success" id="btnToExcel">Excel</a>
	</div>

	<div class="text-right" style="margin-bottom:5px;">
		<i class="fa fa-caret-right fa-fw"></i>총
		<span id="spTotalCnt">0건</span>
		&nbsp; &nbsp;
		<i class="fa fa-caret-right fa-fw"></i>수량
		<span id="spTotalQuantity">0개</span>
		&nbsp; &nbsp;
		<i class="fa fa-krw fa-fw"></i>
		<span id="spTotalPrice">0원</span>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblSaleList">
		<thead>
			<tr>
				<%--<th class="text-center">결제번호</th>--%>
				<th class="text-center">메뉴명</th>
				<th class="text-center">결제금액</th>
				<th class="text-center">결제수량</th>
				<th class="text-center">결제수단</th>
				<th class="text-center">주문방법</th>
				<th class="text-center">결제일시</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<%--<div class="text-center">
		<ul class="pagination pagination-sm">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
		</ul>
	</div>--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.PAGE = 0;
				SearchInfo.START_DATE = $("#txtFR_DATE").val();
				SearchInfo.END_DATE = $("#txtTO_DATE").val();
				SearchInfo.ITEM_GROUP_CODE = $("#selItemGroup").val();
				SearchInfo.ITEM_ID = $("#selItem").val();
				GetSaleList();
			});

			$("#selItemGroup").change(function () {
				if ($(this).val == "0") {
					$("#selItem").html("<option value=\"0\">선택</option>");
				}
				else {
					var params = {};
					params.PAGE_SIZE = 100;
					params.PAGE = 0;
					params.GROUP_CODE = $(this).val();

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "GetItemList",
						Params: params,
						SuccessCallBack: function (data) {
							var strHtml = "<option value=\"0\">선택</option>";
							if (data.LIST.length > 0) {
								for (var i = 0; i < data.LIST.length; i++) {
									strHtml += "<option value=\"" + data.LIST[i].ITEM_ID + "\">" + data.LIST[i].ITEM_NAME + "</option>";
								}
							}

							$("#selItem").html(strHtml);
						}
					});
				}
			});

			$("#btnToExcel").click(function () {
				do_cmd.excelExport("tblSaleList", "메뉴별 매출내역");
			});

			GetItemGroupList(function () {
				InitSearchInfo();
				GetSaleList();
			});
		});

		function GetItemGroupList(callback) {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemGroupList",
				SuccessCallBack: function (data) {
					var strHtml = "<option value=\"0\">선택</option>";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<option value=\"" + data[i].ITEM_GROUP_CODE + "\">" + data[i].ITEM_GROUP_NAME + "</option>";
						}
					}

					$("#selItemGroup").html(strHtml);
					$("#selItem").html("<option value=\"0\">선택</option>");
					if (callback) callback();
				}
			});
		}

		function GetSaleList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreSaleByItemList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							//strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_CODE + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].ITEM_NAME + "</td>";
							strHtml += "<td class=\"text-right\">" + String(data.LIST[i].PRICE).currencyFormat() + "원</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].QUANTITY + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_TYPE + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_PATH + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_DATE + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"6\" class=\"text-center\">매출내역이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#spTotalCnt").text(String(data.TOTAL[0].TOTAL_CNT).currencyFormat() + "건");
					$("#spTotalQuantity").text(String(data.TOTAL[0].TOTAL_QUANTITY).currencyFormat() + "개");
					$("#spTotalPrice").text(String(data.TOTAL[0].TOTAL_PRICE).currencyFormat() + "원");

					$("#tblSaleList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 1000;
			SearchInfo.START_DATE = $("#txtFR_DATE").val();
			SearchInfo.END_DATE = $("#txtTO_DATE").val();
			SearchInfo.ITEM_GROUP_CODE = $("#selItemGroup").val();
			SearchInfo.ITEM_ID = $("#selItem").val();
		}
	</script>
</asp:Content>

