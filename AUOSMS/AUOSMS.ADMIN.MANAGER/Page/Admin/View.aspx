﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			관리자 정보
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-info btn-xs" id="btnToEdit">수정</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<form role="form">
                <fieldset>
                    <div class="form-group">
						<label>관리자 이름</label>
						<input class="form-control readonly" id="vwManagerName" type="text" value="">
                    </div>
                    <div class="form-group">
						<label>관리자 아이디</label>
						<input class="form-control readonly" id="vwManagerID" type="text" value="">
                    </div>
                </fieldset>
            </form>
		</div>
		<!-- /.panel-body -->
	</div>

	<!-- 정보 변경 -->
	<div class="modal fade" role="dialog" id="divChangeManagerInfo">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">관리자 수정</h4>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="txtName">관리자이름 : </label>
							<input type="text" class="form-control" placeholder="NAME" id="txtName" />
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnManagerInfoSave">저장</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>
	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var ManagerInfo;
		$(document).ready(function () {
			$("#btnToEdit").click(function () {
				$("#txtName").val(ManagerInfo.MANAGER_NAME);
				$("#divChangeManagerInfo").modal();
			});
			$("#btnManagerInfoSave").click(function () {
				var $e;
				$e = $("#txtName");
				if ($e.val() == "") {
					alert("관리자 이름을 선택하세요.");
					$e.focus();
					return false;
				}

				var params = {};
				params.NAME = $e.val();

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "UpdateManagerInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							alert("관리자 정보가 변경되었습니다.");
							$("#divChangeManagerInfo").modal("hide");
							GetMangerInfo();
						}
						else {
							alert("오류!");
						}
					}
				});
			});

			GetMangerInfo();
		});

		function GetMangerInfo() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetManagerInfo",
				SuccessCallBack: function (data) {
					if (data.length > 0) {
						ManagerInfo = data[0];
						$("#vwManagerName").val(ManagerInfo.MANAGER_NAME);
						$("#vwManagerID").val(ManagerInfo.MANAGER_ID);
					}
					else {
						alert("잘못된 관리자 코드입니다.");
					}
				}
			});

		}

	</script>
</asp:Content>

