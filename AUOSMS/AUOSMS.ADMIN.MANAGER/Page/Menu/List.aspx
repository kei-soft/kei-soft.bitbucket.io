﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">메뉴 관리</h1>

	<br /><br />	
	<div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-file-text fa-fw"></i> 메뉴관리
            <div class="pull-right">
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                        <span id="spSelectedCategory" data-category="">Actions</span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu" id="ulCategory"></ul>
					<button type="button" class="btn btn-primary btn-xs" style="margin-left: 5px;" id="btnItemSearch">메뉴 추가</button>
                </div>
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            
			<div class="list-group" id="dvStoreItemList"></div>
            <!-- /.list-group -->

        </div>
        <!-- /.panel-body -->
    </div>
	
	<!-- 메뉴 목록 -->
	<div class="modal fade" role="dialog" id="divItemSearchList">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">메뉴 목록</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblItemSearchList">
						<colgroup>
							<col />
							<col style="width:120px;" />
							<col style="width:100px;" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">메뉴명</th>
								<th class="text-center">가격</th>
								<th class="text-center">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>
	
	<!-- 메뉴 목록 -->
	<div class="modal fade" role="dialog" id="divItemInfo">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">메뉴정보</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">

					<img src="#" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="max-width:200px;" id="vwItemImage" />
					<br /><br />
					<dl>
						<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">상품군</p></dt>
						<dd>
							<blockquote id="vwGroup"></blockquote>
						</dd>
						<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">메뉴명</p></dt>
						<dd>
							<blockquote id="vwItemName"></blockquote>
						</dd>
						<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">영문명</p></dt>
						<dd>
							<blockquote id="vwEngName"></blockquote>
						</dd>
						<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">가격</p></dt>
						<dd>
							<blockquote id="vwPrice"></blockquote>
						</dd>
						<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">Description</p></dt>
						<dd>
							<blockquote id="vwDesc"></blockquote>
						</dd>
						<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">OPTION</p></dt>
						<dd>
							<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblIOptionList">
								<thead>
									<tr>
										<th class="text-center">옵션명</th>
										<th class="text-center">상세옵션</th>
										<th class="text-center">필수여부</th>
										<th class="text-center">사용여부</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</dd>
					</dl>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" role="dialog" id="dvItemLimitForm">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">한정수량여부 수정</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<form>
						<div class="form-group">
							<label for="txtLimitItemName">아이템명 : </label>
							<input type="text" class="form-control" placeholder="아이템명" id="txtLimitItemName" />
						</div>
						<div class="form-group">
							<label for="selLimitYN">수량한정여부 : </label>
							<select class="form-control" id="selLimitYN">
								<option value="N">N</option>
								<option value="Y">Y</option>
							</select>
						</div>
						<div class="form-group" id="dvLimitQuantityArea">
							<label for="txtLimtQuantity">한정수량 : </label>
							<input type="text" class="spinner" style="width: 50px;" id="txtLimtQuantity" /> 개
						</div>
						<div class="form-group">
							<label for="selSoldoutYN">품절여부 : </label>
							<select class="form-control" id="selSoldoutYN">
								<option value="N">N</option>
								<option value="Y">Y</option>
							</select>
						</div>

					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnLimitSave">저장</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		$(document).ready(function () {
			$("#ulCategory").on("click", "a.select_category", function () {
				$("#spSelectedCategory").text($(this).text());
				$("#spSelectedCategory").data("category", $(this).data("category"));

				GetStoreItemList();
			});

			$("#btnItemSearch").click(function () {
				GetIemSearchList(null);
			});
			$("#tblItemSearchList > tbody").on("click", "button.select_item", function () {
				var params = {};
				params.ITEM_ID = $(this).data("item");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "InsertItemStore",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetIemSearchList(function () {
								GetStoreItemList();
							});
						}
						else {
							alert("오류!");
						}
					}
				});

			});

			$("#dvStoreItemList").on("click", "a.item_up", function () {
				var $curGroup = $(this).parents("div.list-group-item");
				var curIdx = $("#dvStoreItemList > div.list-group-item").index($curGroup);

				if (curIdx != 0) {
					ChangeItemSort($curGroup.data("item"), $curGroup.prev("div.list-group-item").data("item"));
				}
			});
			$("#dvStoreItemList").on("click", "a.item_down", function () {
				var $curGroup = $(this).parents("div.list-group-item");
				var curIdx = $("#dvStoreItemList > div.list-group-item").index($curGroup);

				if (curIdx != $("#dvStoreItemList > div.list-group-item").length - 1) {
					ChangeItemSort($curGroup.data("item"), $curGroup.next("div.list-group-item").data("item"));
				}
			});
			$("#dvStoreItemList").on("click", "a.item_delete", function () {
				if (confirm("메뉴를 삭제하시겠습니까?")) {
					var params = {};
					params.ITEM_ID = $(this).parents("div.list-group-item").data("item");

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteStoreItem",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								GetStoreItemList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			$("#dvStoreItemList").on("click", "a.view_item", function () {
				GetItemInfo($(this).parents("div.list-group-item").data("item"));
			});
			$("#dvStoreItemList").on("click", "button.item_limit", function () {
				ItemLimitForm.open($(this).parents("div.list-group-item").data("item"), function () { GetStoreItemList(); });
			});

			$("#selLimitYN").change(function () {
				if ($(this).val() == "Y") {
					$("#dvLimitQuantityArea").show();
				}
				else {
					$("#dvLimitQuantityArea").hide();
				}
			});

			$("#btnLimitSave").click(function () {
				ItemLimitForm.save();
			});


			GetItemGroupList(function () {
				GetStoreItemList();
			});
		});

		function GetItemGroupList(callback) {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemGroupList",
				SuccessCallBack: function (data) {
					if (data.length > 0) {
						$("#spSelectedCategory").text(data[0].ITEM_GROUP_NAME);
						$("#spSelectedCategory").data("category", data[0].ITEM_GROUP_CODE);

						var strHtml = "";
						for (var i = 0; i < data.length; i++) {
							strHtml += "<li><a class=\"select_category\" style=\"cursor: pointer; \" data-category=\"" + data[i].ITEM_GROUP_CODE + "\">" + data[i].ITEM_GROUP_NAME + "</a></li>";
						}
						$("#ulCategory").html(strHtml);
						if (callback) callback();
					}
				}
			});
		}

		function GetStoreItemList() {
			var params = {};
			params.GROUP_CODE = $("#spSelectedCategory").data("category");
			// 1.28em
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreItemList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<div class=\"list-group-item\" data-item=\"" + data.LIST[i].ITEM_ID + "\">";
							strHtml += "	<img src=\"" + data.FILEPATH + data.LIST[i].ITEM_IMAGE + "\" onerror=\"this.src='" + contextPath + "images/no_image.png';\" style=\"width:2.28em; height:2.28em;\" />";
							if (data.LIST[i].RECOMMEND_YN == "Y") {
								strHtml += "<i class=\"fa fa-star\" style=\"color: yellow;\"></i>";
							}
							strHtml += "	<a style=\"cursor:pointer;\" class=\"view_item\">" + data.LIST[i].ITEM_NAME + "[" + String(data.LIST[i].ITEM_PRICE).currencyFormat() + "원]</a>";
							if (data.LIST[i].LIMIT_YN == "Y") {
								strHtml += "<span class=\"text-primary\"> - 한정수량 " + String(data.LIST[i].LIMIT_QUANTITY).currencyFormat() + "개</span>";
								strHtml += "<span class=\"text-danger\">, 남은수량 " + String(data.LIST[i].REMAINING_QUANTITY).currencyFormat() + "개</span>";
							}
							strHtml += "	<div class=\"pull-right\">";
							strHtml += "		<div class=\"btn-group\">";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"item_up\" title=\"올리기\"><span class=\"fa fa-arrow-up fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"item_down\" title=\"내리기\"><span class=\"fa fa-arrow-down fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"item_delete\" title=\"삭제\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
							if(data.LIST[i].SOLDOUT_YN == "Y") {
								strHtml += "			<button class=\"btn btn-light btn-xs\" style=\"margin-right: 5px; cursor: default;\">SOLDOUT</button>";
							}
							strHtml += "			<button class=\"btn btn-success btn-xs item_limit\" type=\"button\">수량한정</button>";
							strHtml += "		</div>";
							strHtml += "	</div>";
							strHtml += "</div>";
						}
					}
					else {
						strHtml += "<div class=\"list-group-item text-center\">등록된 메뉴가 없습니다.</div>";
					}

					$("#dvStoreItemList").html(strHtml);
				}
			});
		}

		function ChangeItemSort(id1, id2) {
			var params = {};
			params.ITEM_ID1 = id1;
			params.ITEM_ID2 = id2;

			//alert(JSON.stringify(params)); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "UpdateStoreItemSort",
				Params: params,
				SuccessCallBack: function (data) {
					if (data) {
						GetStoreItemList();
					}
					else {
						alert("오류!");
					}
				}
			});
		}

		function GetIemSearchList(callback) {
			var params = {};
			params.GROUP_CODE = $("#spSelectedCategory").data("category");

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreItemSearchList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_NAME + "</td>"
							strHtml += "<td class=\"\">" + String(data[i].ITEM_PRICE).currencyFormat() + "</td>"
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs select_item\" data-item=\"" + data[i].ITEM_ID + "\">선택</button></td>"
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">선택할 메뉴가 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblItemSearchList > tbody").html(strHtml);
					if (callback) callback();
				}
			});
			$("#divItemSearchList").modal();
		}

		function GetItemInfo(itemID) {
			var params = {};
			params.ID = itemID;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerInfo", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						ItemInfo = data.INFO[0];
						imgPath = data.FILEPATH;
						$("#viewTitle > span").text(ItemInfo.ITEM_NAME);
						$("#vwItemImage").attr("src", imgPath + ItemInfo.ITEM_IMAGE);
						$("#vwGroup").text(ItemInfo.GROUP_NAME);
						$("#vwItemName").text(ItemInfo.ITEM_NAME);
						$("#vwEngName").text(ItemInfo.ITEM_ENG_NAME);
						$("#vwPrice").text(String(ItemInfo.ITEM_PRICE).currencyFormat() + "원");
						$("#vwDesc").text(ItemInfo.ITEM_DESC);

						GetOptionList(itemID);
					}
					else {
						alert("잘못된 메뉴 코드입니다.");
						location.href = "LIst.aspx";
					}
				}
			});

		}

		function GetOptionList(itemID) {
			var params = {};
			params.ID = itemID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemOptionList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr data-option=\"" + data[i].OPTION_CODE + "\">";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].OPTION_NAME + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].DETAIL_NAME + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].ESSENTIAL + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].USE_YN + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"4\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblIOptionList > tbody").html(strHtml);

					$("#divItemInfo").modal();
				}
			});
		}

		var ItemLimitForm = {
			saveCallback: null,
			item_id: null,
			open: function (item_id, saveCallback) {
				this.item_id = item_id;
				this.saveCallback = saveCallback;

				var params = {};
				params.ITEM_ID = this.item_id;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "GetStoreItemInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data.length > 0) {
							$("#txtLimitItemName").val(data[0].ITEM_NAME);
							$("#selLimitYN").val(data[0].LIMIT_YN);
							$("#txtLimtQuantity").val(String(data[0].LIMIT_QUANTITY).currencyFormat());
							if (data[0].LIMIT_YN == "Y")
								$("#dvLimitQuantityArea").show();
							else
								$("#dvLimitQuantityArea").hide();
							$("#selSoldoutYN").val(data[0].SOLDOUT_YN);

							$("#dvItemLimitForm").modal();
						}
						else {
							alert("잘못된 경로입니다.");
						}
					}
				});
			},
			close: function () {
				$("#dvItemLimitForm").modal("hide");
			},
			save: function () {
				var params = {};
				params.ITEM_ID = this.item_id;
				params.LIMIT_YN = $("#selLimitYN").val();
				params.LIMIT_QUANTITY = removeCommas($("#txtLimtQuantity").val());
				params.SOLDOUT_YN = $("#selSoldoutYN").val();

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "UpdateStoreItemLimit",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							ItemLimitForm.close();
							ItemLimitForm.saveCallback();
						}
						else {
							alert("오류");
						}
					}
				});

			}
		};
	</script>
</asp:Content>

