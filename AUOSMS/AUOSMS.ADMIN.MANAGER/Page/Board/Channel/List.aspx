﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/Board/Board.ascx" TagPrefix="uc1" TagName="Board" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">소통채널</h1>

	<uc1:Board runat="server" ID="Board" 
		Board_Type="CHANNEL" 
		Page_Size="10" 
		Store_Search_YN="N" 
		LIST_TYPE="SELF"
		WRITABLE="Y"
		REPLY_WRITABLE="N"
		NOTICE_YN="N"
		REPLY_CNT="1"
		FILE_SIZE="1"
	/>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
</asp:Content>

