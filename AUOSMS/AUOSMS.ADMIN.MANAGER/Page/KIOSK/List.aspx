﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/KIOSKNewForm.ascx" TagPrefix="uc1" TagName="KIOSKNewForm" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">키오스크 관리</h1>

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			KIOSK 관리
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-xs" id="btnToNewKIOSK">KIOSK등록</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblKIOSKList">
				<colgroup>
					<col />
					<col />
					<col />
					<col style="width:100px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="text-center">ID</th>
						<th class="text-center">키오스크명</th>
						<th class="text-center">등록일시</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.panel-body -->
	</div>

	<uc1:KIOSKNewForm runat="server" ID="KIOSKNewForm" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		$(document).ready(function () {
			$("#tblKIOSKList").on("click", "button.delete_kiosk", function () {
				if (confirm("삭제 하시겠습니까?")) {
					var params = {};
					params.KIOSK_ID = $(this).data("kiosk");

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteStoreKIOSK",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("KIOSK가 삭제 되었습니다.");
								GetStoreKIOSKList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			
			$("#tblKIOSKList").on("click", "button.edit_kiosk", function () {
				KIOSKNewForm.open($(this).data("kiosk"), function () {
					GetStoreKIOSKList();
				});
			});

			$("#btnToNewKIOSK").click(function () {
				KIOSKNewForm.open(null, function () {
					GetStoreKIOSKList();
				});
			});

			GetStoreKIOSKList();
		});

		function GetStoreKIOSKList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreKioskList",
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].KIOSK_ID + "</td>"
							strHtml += "<td class=\"text-center\">" + data[i].KIOSK_NAME + "</td>"
							strHtml += "<td class=\"text-center\">" + data[i].CREATE_DATE + "</td>"
							strHtml += "<td class=\"text-center\">"
							strHtml += "<button class=\"btn btn-success btn-xs edit_kiosk\" data-kiosk=\"" + data[i].KIOSK_ID + "\">수정</button>";
							strHtml += "<button class=\"btn btn-danger btn-xs delete_kiosk\" data-kiosk=\"" + data[i].KIOSK_ID + "\">삭제</button>";
							strHtml += "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"4\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblKIOSKList > tbody").html(strHtml);
				}
			});
		}
	</script>
</asp:Content>

