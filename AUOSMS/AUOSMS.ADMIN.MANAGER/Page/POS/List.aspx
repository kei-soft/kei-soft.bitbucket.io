﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/POSNewForm.ascx" TagPrefix="uc1" TagName="POSNewForm" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">단말 관리</h1>

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			매장 단말 관리
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-xs" id="btnToNewPOS">단말등록</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblPOSList">
				<colgroup>
					<col />
					<col />
					<col />
					<col style="width:100px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="text-center">ID</th>
						<th class="text-center">단말기명</th>
						<th class="text-center">등록일시</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.panel-body -->
	</div>

	<uc1:POSNewForm runat="server" ID="POSNewForm" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		$(document).ready(function () {
			$("#tblPOSList").on("click", "button.delete_pos", function () {
				if (confirm("삭제 하시겠습니까?")) {
					var params = {};
					params.POS_ID = $(this).data("pos");

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteStorePOS",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("단말이 삭제 되었습니다.");
								GetStorePOSList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			
			$("#tblPOSList").on("click", "button.edit_pos", function () {
				POSKNewForm.open($(this).data("pos"), function () {
					GetStorePOSList();
				});
			});

			$("#btnToNewPOS").click(function () {
				POSKNewForm.open(null, function () {
					GetStorePOSList();
				});
			});

			GetStorePOSList();
		});

		function GetStorePOSList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStorePosList",
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].POS_ID + "</td>"
							strHtml += "<td class=\"text-center\">" + data[i].POS_NAME + "</td>"
							strHtml += "<td class=\"text-center\">" + data[i].CREATE_DATE + "</td>"
							strHtml += "<td class=\"text-center\">"
							strHtml += "<button class=\"btn btn-success btn-xs edit_pos\" data-pos=\"" + data[i].POS_ID + "\">수정</button>";
							strHtml += "<button class=\"btn btn-danger btn-xs delete_pos\" data-pos=\"" + data[i].POS_ID + "\">삭제</button>";
							strHtml += "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"4\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblPOSList > tbody").html(strHtml);
				}
			});
		}
	</script>
</asp:Content>

