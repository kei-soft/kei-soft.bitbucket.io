﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/EventRegForm.ascx" TagPrefix="uc1" TagName="EventRegForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">이벤트</h1>

	<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblEventList">
		<colgroup>
			<col style="width: 150px;" />
			<col />
			<col style="width: 180px;" />
			<col style="width: 180px;" />
			<col style="width: 150px;" />
			<col style="width: 80px;" />
			<col style="width: 80px;" />
		</colgroup>
		<thead>
			<tr>
				<th class="text-center" colspan="2">이벤트명</th>
				<th class="text-center">기간</th>
				<th class="text-center">사은품</th>
				<th class="text-center">제공기준</th>
				<th class="text-center">사용유무</th>
				<th class="text-center">상태</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-primary" id="btnNewEvent">이벤트 등록</a>
	</div>

	<uc1:EventRegForm runat="server" ID="EventRegForm" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			$("#btnNewEvent").click(function () {
				EventRegForm.open(null, function () { GetEventList(); });
			});

			GetEventList();
		});

		function GetEventList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreEventList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "	<td style=\"vertical-align:middle; text-align:center;\">";
							strHtml += "		<img src=\"" + data.EVENTFILEPATH + data.LIST[i].EVENT_INFO_IMAGE + "\" style=\"max-width: 150px;\" onError=\"this.src='" + contextPath + "images/no_image.png'\" />";
							strHtml += "	</td>";
							strHtml += "	<td style=\"vertical-align:middle;\"><a href=\"EventDetail.aspx?event_id=" + data.LIST[i].EVENT_ID + "\">" + data.LIST[i].EVENT_NAME + "</a></td>";
							strHtml += "	<td style=\"vertical-align:middle; text-align:center;\">" + data.LIST[i].EVENT_START_DATE + " ~ " + data.LIST[i].EVENT_END_DATE + "</td>";
							strHtml += "	<td style=\"vertical-align:middle; text-align:center;\">";
							strHtml += "		<img src=\"" + (data.LIST[i].EVENT_GIFT_TYPE == "MENU" ? data.ITEMFILEPATH : data.EVENTFILEPATH) + data.LIST[i].EVENT_IMAGE + "\" style=\"max-width: 150px;\" onError=\"this.src='" + contextPath + "images/no_image.png'\" />";
							strHtml += "		<br />";
							strHtml += "		" + data.LIST[i].EVENT_GIFT_NAME;
							strHtml += "	</td>";
							strHtml += "	<td style=\"vertical-align:middle; text-align:center;\">" + (data.LIST[i].EVENT_CONDITION_TYPE == "QUANTITY" ? "음료" + data.LIST[i].EVENT_CONDITION_VALUE + "잔당" : data.LIST[i].EVENT_CONDITION_VALUE + "원이상") + "</td>";
							strHtml += "	<td style=\"vertical-align:middle; text-align:center;\">" + data.LIST[i].USE_YN + "</td>";
							strHtml += "	<td style=\"vertical-align:middle; text-align:center;\">" + (data.LIST[i].EVENT_STATUS == 1 ? "진행중" : (data.LIST[i].EVENT_STATUS == 2 ? "시작전" : "종료")) + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"7\" class=\"text-center\">등록된 이벤트가 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblEventList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 100;
		}

	</script>
</asp:Content>

