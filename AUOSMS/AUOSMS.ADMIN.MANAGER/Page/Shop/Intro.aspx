﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/IntroRegForm.ascx" TagPrefix="uc1" TagName="IntroRegForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">INTRO 관리</h1>

	<div class="list-group" id="divIntroList"></div>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-primary" id="btnNewIntro">이미지 추가</a>
	</div>

	<uc1:IntroRegForm runat="server" ID="IntroRegForm" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		$(document).ready(function () {
			$("#btnNewIntro").click(function () {
				IntroRegForm.open(null, function () { GetIntroImageList(); });
			});
			
			$("#divIntroList").on("click", "a.intro_up", function () {
				var $intro = $(this).parents("div.list-group-item");
				var currentIndex = $("#divIntroList div.list-group-item").index($intro);

				if (currentIndex == 0) {
					return;
				}

				var intro_id1 = $intro.data("id");
				var intro_id2 = $intro.prev("div.list-group-item").data("id");

				UpdateIntroImageSort(intro_id1, intro_id2);
			});
			$("#divIntroList").on("click", "a.intro_down", function () {
				var $intro = $(this).parents("div.list-group-item");
				var intro_count = $("#divIntroList div.list-group-item").length;
				var currentIndex = $("#divIntroList div.list-group-item").index($intro);

				if (intro_count <= currentIndex + 1) {
					return;
				}

				var intro_id1 = $intro.data("id");
				var intro_id2 = $intro.next("div.list-group-item").data("id");

				UpdateIntroImageSort(intro_id1, intro_id2);
			});
			$("#divIntroList").on("click", "a.intro_edit", function () {
				IntroRegForm.open($(this).parents("div.list-group-item").data("id"), function () { GetIntroImageList() });
			});
			$("#divIntroList").on("click", "a.intro_delete", function () {
				if (!confirm("이미지를 삭제 하시겠습니까?")) {
					return;
				}
				
				var params = {};
				params.ID = $(this).parents("div.list-group-item").data("id");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "DeleteIntroImage",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetIntroImageList();
						}
						else {
							alert("오류");
						}
					}
				});
			});
			
			GetIntroImageList();
		});

		function GetIntroImageList() {
			var params = {};
			params.PAGE_SIZE = 1000;
			params.PAGE = 0;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Params: params,
				Command: "GetIntroImageList",
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<div class=\"list-group-item\" data-id=\"" + data.LIST[i].IMAGE_ID + "\"" + (data.LIST[i].USE_YN == "N" ? " style=\"background-color: silver;\"" : "") + ">";
							strHtml += "	<img src=\"" + data.FILEPATH + data.LIST[i].IMAGE_NAME + "\" style=\"width: 200px;\"  onError=\"this.src='" + contextPath + "images/no_image.png'\" />";
							strHtml += "	<div class=\"pull-right\">";
							strHtml += "		<div class=\"btn-group\">";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"intro_up\" title=\"올리기\"><span class=\"fa fa-arrow-up fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"intro_down\" title=\"내리기\"><span class=\"fa fa-arrow-down fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"intro_edit\" title=\"수정\"><span class=\"fa fa-pencil fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"intro_delete\" title=\"삭제\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
							strHtml += "		</div>";
							strHtml += "	</div>";
							strHtml += "</div>";
						}
					}
					else {
						strHtml += "<div class=\"list-group-item text-center\">";
						strHtml += "		등록된 이미지가 없습니다.";
						strHtml += "</div>";
					}

					$("#divIntroList").html(strHtml);
				}
			});
		}

		function UpdateIntroImageSort(id1, id2) {
			var params = {};
			params.ID1 = id1;
			params.ID2 = id2;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "UpdateIntroImageSort",
				Params: params,
				SuccessCallBack: function (data) {
					if (data) {
						GetIntroImageList();
					}
					else {
						alert("오류!");
					}
				}
			});
		}
	</script>
</asp:Content>