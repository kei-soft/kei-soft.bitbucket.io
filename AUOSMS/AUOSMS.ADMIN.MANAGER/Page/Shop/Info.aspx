﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">매장 정보</h1>

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			매장 정보
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-info btn-xs" id="btnToEdit">수정</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<img src="#" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="max-width:200px;" id="vwStoreImage" />
			<br /><br />
			<div class="row">
				<dl class="col-md-6">
					<dt>
						<p class="bg-success" style="text-indent: 10px; font-size:14px;">매장명</p>
					</dt>
					<dd>
						<blockquote id="vwStoreName"></blockquote>
					</dd>
				</dl>
				<dl class="col-md-3">
					<dt>
						<p class="bg-success" style="text-indent: 10px; font-size:14px;">사업자번호</p>
					</dt>
					<dd>
						<blockquote id="vwBusinessCode"></blockquote>
					</dd>
				</dl>
				<dl class="col-md-3">
					<dt>
						<p class="bg-success" style="text-indent: 10px; font-size:14px;">대표자명</p>
					</dt>
					<dd>
						<blockquote id="vwBusinessOwner"></blockquote>
					</dd>
				</dl>
				<dl class="col-md-12">
					<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">주소</p></dt>
					<dd>
						<blockquote id="vwStoreAddr"></blockquote>
					</dd>
					<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">전화번호</p></dt>
					<dd>
						<blockquote id="vwPhone"></blockquote>
					</dd>
					<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">주문가능시간</p></dt>
					<dd>
						<blockquote id="vwOrderTime"></blockquote>
					</dd>
					<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">좌표</p></dt>
					<dd>
						<blockquote id="vwLocation"></blockquote>
					</dd>
				</dl>
				<dl class="col-md-6">
					<dt>
						<p class="bg-success" style="text-indent: 10px; font-size:14px;">PG SITE CODE/KEY</p>
					</dt>
					<dd>
						<blockquote id="vwPGSiteCodeKey"></blockquote>
					</dd>
				</dl>
				<dl class="col-md-6">
					<dt>
						<p class="bg-success" style="text-indent: 10px; font-size:14px;">Im Port 관리자 계정</p>
					</dt>
					<dd>
						<blockquote id="vwImPortIDPass"></blockquote>
					</dd>
				</dl>
				<dl class="col-md-12">
					<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">MEMO</p></dt>
					<dd>
						<blockquote id="vwMemo"></blockquote>
					</dd>
				</dl>

			</div>
		</div>
		<!-- /.panel-body -->
	</div>

	<!-- 매장 정보 변경 -->
	<div class="modal fade" role="dialog" id="divChangeStoreInfo">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">매장 수정</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<form>
						<div class="form-group text-center">
							<img src="#" style="width:50%; cursor: pointer;" alt="클릭하여 이미지 선택" id="imgStoreImage" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" />
							<input type="file" id="selectImage" name="selectImage" style="display: none;" />
							<p class="text-primary">이미지를 클릭하여 사진을 선택하세요.</p>
						</div>
						<div class="form-group">
							<label for="txtStoreName">매장명 : </label>
							<input type="text" class="form-control" placeholder="매장명" id="txtStoreName" />
						</div>
						<div class="form-group form-inline">
							<label for="txtMemo">사업자 정보 : </label>
							<br />
							사업자번호 <input type="text" class="form-control" placeholder="사업자번호" style="width:200px;" id="txtBusinessCode" />
							&nbsp; &nbsp;
							대표자명 <input type="text" class="form-control" placeholder="대표자명" style="width:200px;" id="txtBusinessOwner" />
						</div>
						<div class="form-group">
							<label for="btnSearchAddr">주소 : <button type="button" class="btn btn-primary btn-xs" id="btnSearchAddr">주소 검색</button></label>
							<input type="text" class="form-control readonly" placeholder="우편번호" id="txtPostCode" />
							<input type="text" class="form-control readonly" placeholder="시도" id="txtState" />
							<input type="text" class="form-control readonly" placeholder="구군" id="txtCity" />
							<input type="text" class="form-control readonly" placeholder="주소" id="txtAddress" />
							<input type="text" class="form-control" placeholder="상세주소" id="txtAddressDetail" />
						</div>
						<div class="form-group">
							<label for="txtPhone">전화번호 : </label>
							<input type="text" class="form-control" placeholder="전화번호" id="txtPhone" data-chk="false" />
						</div>
						<div class="form-group form-inline">
							<label for="txtMemo">주문가능시간 : </label>
							<br />
							<select class="form-control bind_time" style="width:80px;" id="selStartTime"></select> : <select class="form-control bind_min" style="width:80px;" id="selStartMin"></select>
							~
							<select class="form-control bind_time" style="width:80px;" id="selEndTime"></select> : <select class="form-control bind_min" style="width:80px;" id="selEndMin"></select>
						</div>
						<div class="form-group form-inline">
							<label for="txtMemo">좌표 : </label>
							<br />
							위도 <input type="text" class="form-control" placeholder="위도" style="width:200px;" id="txtLocLatitude" />
							&nbsp; &nbsp;
							경도 <input type="text" class="form-control" placeholder="위도" style="width:200px;" id="txtLocLongitude" />
						</div>
						<div class="form-group form-inline">
							<label for="txtMemo">PG 정보 : </label>
							<br />
							SITE CODE <input type="text" class="form-control" placeholder="SITE CODE" style="width:200px;" id="txtPGSiteCode" />
							&nbsp; &nbsp;
							SITE KEY <input type="text" class="form-control" placeholder="SITE KEY" style="width:200px;" id="txtPGSiteKey" />
						</div>
						<div class="form-group form-inline">
							<label for="txtMemo">Im Port 관리자 계정 : </label>
							<br />
							ID <input type="text" class="form-control" placeholder="ID" style="width:200px;" id="txtImPortID" />
							&nbsp; &nbsp;
							PW <input type="text" class="form-control" placeholder="PW" style="width:200px;" id="txtImPortPass" />
						</div>
						<div class="form-group">
							<label for="txtMemo">MEMO : </label>
							<input type="text" class="form-control" placeholder="MEMO" id="txtMemo" />
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnSave">저장</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var StoreInfo;
		var imgPath;
		var StoreImageFile = new Object();

		$(document).ready(function () {
			$("#btnToEdit").click(function () {
				$("#imgStoreImage").attr("src", imgPath + StoreInfo.STORE_IMAGE);
				$("#txtStoreName").val(StoreInfo.STORE_NAME);
				$("#txtBusinessCode").val(StoreInfo.BUSINESS_CODE);
				$("#txtBusinessOwner").val(StoreInfo.BUSINESS_OWNER);
				$("#txtPostCode").val(StoreInfo.POST_CODE);
				$("#txtState").val(StoreInfo.STATE);
				$("#txtCity").val(StoreInfo.CITY);
				$("#txtAddress").val(StoreInfo.ADDRESS);
				$("#txtAddressDetail").val(StoreInfo.ADDRESS_DETAIL);
				$("#txtPhone").val(StoreInfo.PHONE);
				$("#selStartTime").val(StoreInfo.ORDER_START_TIME.split(":")[0]);
				$("#selStartMin").val(StoreInfo.ORDER_START_TIME.split(":")[1]);
				$("#selEndTime").val(StoreInfo.ORDER_END_TIME.split(":")[0]);
				$("#selEndMin").val(StoreInfo.ORDER_END_TIME.split(":")[1]);
				$("#txtLocLatitude").val(StoreInfo.LOC_LATITUDE);
				$("#txtLocLongitude").val(StoreInfo.LOC_LONGITUDE);
				$("#txtPGSiteCode").val(StoreInfo.PG_SIETCODE);
				$("#txtPGSiteKey").val(StoreInfo.PG_KEY);
				$("#txtImPortID").val(StoreInfo.IMPORT_ID);
				$("#txtImPortPass").val(StoreInfo.IMPORT_PASS);
				$("#txtMemo").val(StoreInfo.MEMO);

				$("#divChangeStoreInfo").modal();
			});
			$("#imgStoreImage").click(function () {
				$("#selectImage").click();
			});
			$("#selectImage").on("change", function (e) {
				var file = $(this)[0].files[0];
				if (!file.type.match("image/.*")) {
					alert("이미지파일만 선택가능");
				}
				else {
					StoreImageFile = file;
					var reader = new FileReader();
					reader.onload = function (e) {
						$("#imgStoreImage").attr("src", e.target.result);
					}
					reader.readAsDataURL(StoreImageFile);
				}
			});
			$("#btnSave").click(function () {
				if (validation()) {
					var params = {};
					params.NAME = $("#txtStoreName").val();
					params.BUSINESS_CODE = $("#txtBusinessCode").val();
					params.BUSINESS_OWNER = $("#txtBusinessOwner").val();
					params.PCODE = $("#txtPostCode").val();
					params.STATE = $("#txtState").val();
					params.CITY = $("#txtCity").val();
					params.ADDR = $("#txtAddress").val();
					params.ADDR_D = $("#txtAddressDetail").val();
					params.PHONE = $("#txtPhone").val();
					params.ORDERSTART = $("#selStartTime").val() + ":" + $("#selStartMin").val();
					params.ORDEREND = $("#selEndTime").val() + ":" + $("#selEndMin").val();
					params.LATITUDE = $("#txtLocLatitude").val();
					params.LONGITUDE = $("#txtLocLongitude").val();
					params.PG_SITECODE = $("#txtPGSiteCode").val();
					params.PG_KEY = $("#txtPGSiteKey").val();
					params.IMPORT_ID = $("#txtImPortID").val();
					params.IMPORT_PASS = $("#txtImPortPass").val();
					params.MEMO = $("#txtMemo").val();

					do_cmd.ajax_file({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "UpdateStoreInfo",
						Params: params,
						Files: StoreImageFile,
						SuccessCallBack: function (data) {
							if (data) {
								alert("수정 되었습니다.");
								$("#divChangeStoreInfo").modal("hide");
								GetStoreInfo();
							}
							else {
								alert("수정 오류!");
							}
						}
					});

				}
			});

			GetStoreInfo();
		});

		function GetStoreInfo() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreInfo",
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						StoreInfo = data.INFO[0];
						imgPath = data.FILEPATH;
						$("#viewTitle > span").text(StoreInfo.STORE_NAME);
						$("#vwStoreImage").attr("src", imgPath + StoreInfo.STORE_IMAGE);
						$("#vwStoreName").html(StoreInfo.STORE_NAME);
						$("#vwBusinessCode").html(StoreInfo.BUSINESS_CODE);
						$("#vwBusinessOwner").html(StoreInfo.BUSINESS_OWNER);
						$("#vwStoreAddr").html("(" + StoreInfo.POST_CODE + ")<br />" + StoreInfo.ADDRESS + " " + StoreInfo.ADDRESS_DETAIL);
						$("#vwPhone").html(StoreInfo.PHONE);
						$("#vwOrderTime").html(StoreInfo.ORDER_START_TIME + " ~ " + StoreInfo.ORDER_END_TIME);
						$("#vwLocation").html("위도 : " + StoreInfo.LOC_LATITUDE + ", 경도 : " + StoreInfo.LOC_LONGITUDE);
						$("#vwPGSiteCodeKey").html("SITE CODE : " + StoreInfo.PG_SIETCODE + " &nbsp; &nbsp; &nbsp; &nbsp; SITE KEY : " + StoreInfo.PG_KEY);
						$("#vwImPortIDPass").html("ID : " + StoreInfo.IMPORT_ID + " &nbsp; &nbsp; &nbsp; &nbsp; PW : " + StoreInfo.IMPORT_PASS);
						$("#vwMemo").html(StoreInfo.MEMO);
					}
					else {
						alert("잘못된 매장 코드입니다.");
						location.href = "./LIst.aspx";
					}
				}
			});

		}

		function validation() {
			var $e;

			$e = $("#txtStoreName");
			if ($e.val() == "") {
				alert("매장 이름을 선택하세요.");
				$e.focus();
				return false;
			}

			$e = $("#txtPostCode");
			if ($e.val() == "") {
				alert("주소를 검색하세요.");
				$("#btnSearchAddr").click();
				return false;
			}

			$e = $("#txtAddressDetail");
			if ($e.val() == "") {
				alert("상세주소를 입력하세요.");
				$e.focus();
				return false;
			}

			$e = $("#txtPhone");
			if ($e.val() == "") {
				alert("연락처를 입력하세요.");
				$e.focus();
				return false;
			}

			if (!confirm("매장을 수정하시겠습니까?")) {
				return false;
			}

			return true;
		}
	</script>
</asp:Content>

