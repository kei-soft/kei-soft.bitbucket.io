﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">주문 추가</h1>

	<div class="panel panel-default">
		<div class="panel-heading">
			주문 추가
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-success btn-xs" id="btnAddMenu">메뉴선택</button>
				</div>
			</div>
		</div>

		<!-- /.panel-heading -->
		<div class="panel-body" id="divOrderList">
			<div class="list-group"></div>
		</div>

		<div class="panel-footer text-right">
			총 <label id="lblTotalPrice">0</label>원
		</div>
	<!-- /.panel-body -->
	</div>

	<div class="text-right form-inline" style="margin-bottom: 20px;">
		<div class="form-group input-group" style="width: 200px;">
			<span class="input-group-addon">결재수단</span>
			<select class="form-control common_code" data-group="PAY_TYPE" data-select="" id="selPayType"></select>
		</div>
		<a class="btn btn-primary" id="btnOrderComplete">주문하기</a>
		<a class="btn btn-danger" id="btnOrderCancel">취소</a>
	</div>
	
	<!-- 메뉴 추가 -->
	<div class="modal fade" role="dialog" id="divMenuArea">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">메뉴 선택</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					
					<div id="dvItemArea">
						<ul class="nav nav-tabs" style="margin-bottom: 20px;" id="dvCategoryTabs"></ul>
						<div class="list-group" id="dvItemList"></div>
					</div>

					<div id="dvOptionArea">

						<div class="panel panel-default">
							<div class="panel-heading">
								메뉴1 옵션선택
							</div>
							<div class="panel-body"></div>
							<div class="panel-footer text-right">
								<button type="button" class="btn btn-primary" id="btnSelecteMenu">메뉴선택</button>
								<button type="button" class="btn btn-danger" id="btnToMenu">취소</button>
							</div>
						</div>

					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var selected_item = {};
		var item_list = [];
		var total_price;

		$(document).ready(function () {
			$("#btnAddMenu").click(function () {
				openMenuArea();
			});

			$("#dvCategoryTabs").on("click", "li > a", function () {
				if (!$(this).parent("li").hasClass("active")) {
					$("#dvCategoryTabs > li.active").removeClass("active");
					$(this).parent("li").addClass("active");
					
					GetMenuList();
				}
			});

			$("#dvItemList").on("click", "button", function () {
				var $parents = $(this).parents("div.list-group-item");
				selected_item.ITEM_ID = $(this).data("id");
				selected_item.ITEM_NAME = $parents.find("span.name").text() + "(" + $parents.find("span.price").text() + "원)";
				selected_item.ITEM_PRICE = parseInt($parents.find("span.price").text().replace(/,/gi, ""));
				selected_item.ITEM_QUANTITY = 1;

				GetOptionList();
			});

			$("#btnToMenu").click(function () {
				$("#dvItemArea").show();
				$("#dvOptionArea").hide();
			});

			$("#btnSelecteMenu").click(function () {
				selected_item.OPTIONS = new Array();

				$("#dvOptionArea div.panel-body div.form-group").each(function () {
					var option = new Object();
					option.OPTION_NAME = $(this).find("span.input-group-addon").text(); // OPTION_NAME
					option.OPTION_DETAIL_PRICE = parseInt($(this).find("select").val()); // OPTION_DETAIL_PRICE
					option.OPTION_DETAIL_NAME = $(this).find("select > option:selected").text(); // OPTION_DETAIL_NAME
					option.OPTION_DETAIL_NAME = option.OPTION_DETAIL_NAME.substr(0, option.OPTION_DETAIL_NAME.indexOf(" ("));
					selected_item.OPTIONS.push(option);
				});
				
				item_list.push(JSON.parse(JSON.stringify(selected_item)));
				
				DisplayOrderItemList();
				$("#dvItemArea").show();
				$("#dvOptionArea").hide();
			});

			$("#divOrderList > div.list-group").on("click", "div.list-group-item a.plus", function () {
				var idx = parseInt($(this).data("idx"));
				var quantity = item_list[idx].ITEM_QUANTITY;
				if (quantity < 99) {
					item_list[idx].ITEM_QUANTITY = quantity + 1;
					DisplayOrderItemList();
				}
			});

			$("#divOrderList > div.list-group").on("click", "div.list-group-item a.minus", function () {
				var idx = parseInt($(this).data("idx"));
				var quantity = item_list[idx].ITEM_QUANTITY;
				if (quantity > 1) {
					item_list[idx].ITEM_QUANTITY = quantity - 1;
					DisplayOrderItemList();
				}
			});

			$("#divOrderList > div.list-group").on("click", "div.list-group-item a.delete", function () {
				if (!confirm("삭제하시겠습니까?")) return;
				var idx = parseInt($(this).data("idx"));
				if (item_list.length > 0) {
					item_list.splice(idx, 1);
					DisplayOrderItemList();
				}
			});

			$("#btnOrderComplete").click(function () {
				if (confirm("선택한 메뉴를 주문하시겠습니까?")) {
					var params = {};
					params.PAY_TYPE = $("#selPayType").val();
					params.TOTAL_PRICE = total_price;
					params.ORDER_INFO = item_list;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "SaveOrderComplete",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								location.href = "Order.aspx";
							}
							else {
								alert("주문오류!!!");
							}
						}
					});
				}
			});

			$("#btnOrderCancel").click(function () {
				if (confirm("주문을 취소하시겠습니까?")) {
					location.href = "Order.aspx";
				}
			});


			GetItemGroupList();
		});

		function openMenuArea() {
			$("#dvCategoryTabs > li.active").removeClass("active");
			$("#dvCategoryTabs > li").eq(0).addClass("active");

			GetMenuList();
			$("#dvItemArea").show();
			$("#dvOptionArea").hide();
			$("#divMenuArea").modal();
		}

		function GetItemGroupList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetOrderItemGroupList",
				SuccessCallBack: function (data) {
					if (data.length > 0) {
						var strHtml = "";
						for (var i = 0; i < data.length; i++) {
							strHtml += "<li><a style=\"cursor: pointer; \" data-category=\"" + data[i].ITEM_GROUP_CODE + "\">" + data[i].ITEM_GROUP_NAME + "</a></li>";
						}

						$("#dvCategoryTabs").html(strHtml);

						openMenuArea();
					}
				}
			});
		}

		function GetMenuList() {
			var params = {};
			params.GROUP_CODE = $("#dvCategoryTabs > li.active > a").data("category");
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Params: params,
				Command: "GetOrderMenuList",
				SuccessCallBack: function (data) {
					if (data.length > 0) {
						var strHtml = "";
						for (var i = 0; i < data.length; i++) {
							strHtml += "<div class=\"list-group-item\">";
							strHtml += "	<span class=\"name\">" + data[i].ITEM_NAME + "</span>";
							strHtml += "	<div class=\"pull-right\">";
							strHtml += "		<span class=\"price\">" + String(data[i].ITEM_PRICE).currencyFormat() + "</span>원";
							strHtml += "		<button type=\"button\" class=\"btn btn-success btn-xs\" data-id=\"" + data[i].ITEM_ID + "\">선택</button>";
							strHtml += "	</div>";
							strHtml += "</div>";
						}

						$("#dvItemList").html(strHtml);
					}
				}
			});
		}

		function GetOptionList() {
			var params = {};
			params.ID = selected_item.ITEM_ID;
			
			$("#dvItemArea").hide();

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Params: params,
				Command: "GetOrderItemOptionList",
				SuccessCallBack: function (data) {
					$("#dvOptionArea div.panel-heading").html(selected_item.ITEM_NAME);
					if (data.length > 0) {
						var strHtml = "";
						for (var i = 0; i < data.length; i++) {
							strHtml += "<div class=\"form-group input-group\">";
							strHtml += "	<span class=\"input-group-addon\">" + data[i].OPTION_NAME + "</span>";
							var details = data[i].DETAIL_NAME.split(',');
							strHtml += "	<select class=\"form-control\">";
							for (var j = 0; j < details.length; j++) {
								var detail = details[j].split('|');
								strHtml += "		<option value=\"" + detail[1] + "\">" + detail[0] + " (" + String(detail[1]).currencyFormat() + "원)</option>";
							}
							strHtml += "	</select>";
							strHtml += "</div>";
						}

						$("#dvOptionArea div.panel-body").html(strHtml);
					}

					$("#dvOptionArea").show();
				}
			});
		}

		function DisplayOrderItemList() {
			var strHtml = "";
			total_price = 0;
			for (var i = 0; i < item_list.length; i++) {
				var price = item_list[i].ITEM_PRICE;
				strHtml += "<div class=\"list-group-item\">";
				strHtml += "	<span>";
				strHtml += "		" + item_list[i].ITEM_NAME;
				strHtml += "		<small>";
				var options = "";
				for (var j = 0; j < item_list[i].OPTIONS.length; j++) {
					options += ", " + item_list[i].OPTIONS[j].OPTION_NAME + " : " + item_list[i].OPTIONS[j].OPTION_DETAIL_NAME + "(" + String(item_list[i].OPTIONS[j].OPTION_DETAIL_PRICE).currencyFormat() + ")";
					price += item_list[i].OPTIONS[j].OPTION_DETAIL_PRICE;
				}
				options = options.length > 1 ? options.substr(2) : "";
				price = price * item_list[i].ITEM_QUANTITY;
				strHtml += "			" + options;
				strHtml += "		</small>";
				strHtml += "	</span>";
				strHtml += "	<div class=\"pull-right\">";
				strHtml += "		<span style=\"margin-right: 50px;\">" + String(price).currencyFormat() + "원</span>";
				strHtml += "		<div class=\"btn-group\">";
				strHtml += "			<a style=\"cursor:pointer;\" class=\"plus\" title=\"Plus\" data-idx=\"" + i + "\"><span class=\"fa fa-plus fa-fw\"></span></a>";
				strHtml += "			<span style=\"padding: 2px 10px; text-align: center; border: 1px solid silver;\">" + item_list[i].ITEM_QUANTITY + "</span>";
				strHtml += "			<a style=\"cursor:pointer;\" class=\"minus\" title=\"Minus\" data-idx=\"" + i + "\"><span class=\"fa fa-minus fa-fw\"></span></a>";
				strHtml += "			&nbsp;";
				strHtml += "			<a style=\"cursor:pointer;\" class=\"delete\" title=\"Delete\" data-idx=\"" + i + "\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
				strHtml += "		</div>";
				strHtml += "	</div>";
				strHtml += "</div>";

				item_list[i].ITEM_TOTAL_PRICE = price;
				total_price += price;
			}

			$("#divOrderList > div.list-group").html(strHtml);
			$("#lblTotalPrice").text(String(total_price).currencyFormat());
			console.log(item_list);
		}

	</script>
</asp:Content>