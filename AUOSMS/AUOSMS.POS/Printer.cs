﻿using AUOSMS.POS.Models;
using System;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;

namespace AUOSMS.POS
{
	/// <summary>
	/// 프린터
	/// </summary>
	/// <remarks>
	/// - SAM4S GIANT-100 : VID=1C8A, PID=3A0E
	/// - Serial 대신 USB로 연결
	/// </remarks>
	internal static class Printer
	{
		/// <summary>
		/// 연결 상태 확인
		/// </summary>
		/// <returns></returns>
		public static bool IsConnected()
		{
			bool isConnected = false;

			string query = "SELECT * FROM Win32_PnPEntity WHERE DeviceID LIKE '%VID_1C8A&PID_3A0E%'";
			try
			{
				using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", query))
				{
					foreach (ManagementObject queryObj in searcher.Get())
					{
						isConnected = true;
						break;
					}
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("Exception :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}

			return isConnected;
		}

		/// <summary>
		/// 매장용 주문서 인쇄
		/// </summary>
		/// <param name="orderNo"></param>
		/// <param name="orderDate"></param>
		/// <param name="orderETA"></param>
		/// <param name="orderItems"></param>
		public static void Print(OrderData orderData)
		{
			byte[] cmdCenter = new byte[] { 0x1B, 0x61, 0x01 };
			byte[] cmdLeft = new byte[] { 0x1B, 0x61, 0x00 };
			byte[] cmdBig = new byte[] { 0x1D, 0x21, ((byte)0xF0 & ((byte)0x01 << 4) | ((byte)0x0F & (byte)(0x01))) };
			byte[] cmdNormal = new byte[] { 0x1D, 0x21, 0x00 };
			byte[] cmdCut = new byte[] { 0x1D, 0x56, 0x1 };

			string line = "------------------------------------------";
			string endLine = "\n  \n";

			string orderNo = $"[{Properties.Resources.OrderNo} : {orderData.ORDER_NO}]";
			string orderDate = $"{Properties.Resources.OrderDateTime} : {orderData.ORDER_DATE} {orderData.ORDER_TIME}";
			string orderETA = $"{Properties.Resources.OrderETA} : {orderData.ARRIVE_DATE}";

			string orderItem = String.Empty;
			foreach (var item in orderData.ORDER_ITEM)
			{
				orderItem += $"{Properties.Resources.OrderItem} : {item.ITEM_NAME}\n";
				orderItem += $"{Properties.Resources.OrderCount} : {item.QUANTITY}\n";
				orderItem += $"{Properties.Resources.OrderOption} : {item.ITEM_OPTION}\n";
				orderItem += $"{line}\n";
			}

			string title = $"\n{orderNo}{endLine}";
			string body = $"{orderDate}\n{orderETA}\n{line}\n{orderItem}{endLine}{endLine}";

			byte[] docTitle = Encoding.Default.GetBytes(title);
			byte[] docBody = Encoding.Default.GetBytes(body);
			byte[] printDoc = new byte[cmdCenter.Length + cmdBig.Length + docTitle.Length + cmdLeft.Length + cmdNormal.Length + docBody.Length + cmdCut.Length];

			int offset = 0;
			Buffer.BlockCopy(cmdCenter, 0, printDoc, offset, cmdCenter.Length);
			offset += cmdCenter.Length;
			Buffer.BlockCopy(cmdBig, 0, printDoc, offset, cmdBig.Length);
			offset += cmdBig.Length;
			Buffer.BlockCopy(docTitle, 0, printDoc, offset, docTitle.Length);
			offset += docTitle.Length;
			Buffer.BlockCopy(cmdLeft, 0, printDoc, offset, cmdLeft.Length);
			offset += cmdLeft.Length;
			Buffer.BlockCopy(cmdNormal, 0, printDoc, offset, cmdNormal.Length);
			offset += cmdNormal.Length;
			Buffer.BlockCopy(docBody, 0, printDoc, offset, docBody.Length);
			offset += docBody.Length;
			Buffer.BlockCopy(cmdCut, 0, printDoc, offset, cmdCut.Length);

			IntPtr pDocument = Marshal.AllocCoTaskMem(printDoc.Length);
			Marshal.Copy(printDoc, 0, pDocument, printDoc.Length);
			SendBytesToPrinter(pDocument, printDoc.Length);
			Marshal.FreeCoTaskMem(pDocument);
		}

		/// <summary>
		/// DOCINFOA
		/// </summary>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
		public class DOCINFOA
		{
			[MarshalAs(UnmanagedType.LPStr)]
			public string pDocName;
			[MarshalAs(UnmanagedType.LPStr)]
			public string pOutputFile;
			[MarshalAs(UnmanagedType.LPStr)]
			public string pDataType;
		}

		#region DllImport to Control the Printer

		[DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

		[DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		public static extern bool ClosePrinter(IntPtr hPrinter);

		[DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

		[DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		public static extern bool EndDocPrinter(IntPtr hPrinter);

		[DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		public static extern bool StartPagePrinter(IntPtr hPrinter);

		[DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		public static extern bool EndPagePrinter(IntPtr hPrinter);

		[DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
		public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

		#endregion

		/// <summary>
		/// 프린터로 데이터 전송
		/// </summary>
		/// <param name="pBytes">포인터</param>
		/// <param name="dwCount">총 바이트 수</param>
		/// <returns></returns>
		private static bool SendBytesToPrinter(IntPtr pBytes, Int32 dwCount)
		{
			Int32 dwError = 0, dwWritten = 0;
			IntPtr hPrinter = new IntPtr(0);
			DOCINFOA di = new DOCINFOA();
			bool bSuccess = false; // Assume failure unless you specifically succeed.

			di.pDocName = "ORDER";
			di.pDataType = "RAW";

			// Open the printer.
			if (OpenPrinter(AppConfig.PrinterName.Normalize(), out hPrinter, IntPtr.Zero))
			{
				// Start a document.
				if (StartDocPrinter(hPrinter, 1, di))
				{
					// Start a page.
					if (StartPagePrinter(hPrinter))
					{
						// Write your bytes.
						bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
						EndPagePrinter(hPrinter);
					}
					EndDocPrinter(hPrinter);
				}
				ClosePrinter(hPrinter);
			}
			// If you did not succeed, GetLastError may give more information
			// about why not.
			if (bSuccess == false)
			{
				dwError = Marshal.GetLastWin32Error();
			}
			return bSuccess;
		}
	}
}
