﻿namespace AUOSMS.POS.Models
{
	/// <summary>
	/// 주문 아이템 모델
	/// </summary>
	public class OrderItem
	{
		public string ORDER_ID { get; set; }
		public string ITEM_NAME { get; set; }
		public string QUANTITY { get; set; }
		public string PRICE { get; set; }
		public string ITEM_OPTION { get; set; }
	}
}
