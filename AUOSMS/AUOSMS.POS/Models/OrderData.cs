﻿using System.Collections.Generic;

namespace AUOSMS.POS.Models
{
	/// <summary>
	/// 주문 정보 모델
	/// </summary>
	public class OrderData
	{
		public string ORDER_ID { get; set; }
		public string ORDER_DATE { get; set; }
		public string ORDER_TIME { get; set; }
		public string ORDER_NO { get; set; }
		public string ORDER_STATUS { get; set; }
		public string MADE { get; set; }
		public string SERVED { get; set; }
		public string TOTAL_PRICE { get; set; }
		public string MEMBER_NICKNAME { get; set; }
		public string ARRIVE_DATE { get; set; }
		public List<OrderItem> ORDER_ITEM { get; set; } = new List<OrderItem>();
	}
}
