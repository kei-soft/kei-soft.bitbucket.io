﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Win = System.Windows.Forms;

namespace AUOSMS.POS
{
	/// <summary>
	/// 매장단말 DID 윈도우
	/// </summary>
	public partial class DidWindow : Window
    {
		#region Fields

		/// <summary>
		/// 픽업 화면 영역 그리드
		/// </summary>
		private UniformGrid PickupPanel = null;

		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		public DidWindow()
        {
            InitializeComponent();

			this.Loaded += DidWindow_Loaded;
			this.Closed += DidWindow_Closed;
			this.StateChanged += DidWindow_StateChanged;
			this.PreviewKeyDown += DidWindow_PreviewKeyDown;
		}

		/// <summary>
		/// 윈도우 로드
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DidWindow_Loaded(object sender, RoutedEventArgs e)
		{
			AppCommon.Log("DID - OPEN");

			if (Win.Screen.AllScreens.Count() > 1)
			{
				Win.Screen screen = Win.Screen.AllScreens[1];
				this.Left = screen.WorkingArea.Left;
				this.Top = screen.WorkingArea.Top;
				this.WindowState = WindowState.Maximized;
			}

			this.PickupPanel = VisualTreeHelper.GetChild(VisualTreeHelper.GetChild(VisualTreeHelper.GetChild(this.PickupOrdersItemsControl, 0), 0), 0) as UniformGrid;
		}

		/// <summary>
		/// 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DidWindow_Closed(object sender, EventArgs e)
		{
			AppCommon.Log("DID - CLOSE");
		}

		/// <summary>
		/// 윈도우 상태 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DidWindow_StateChanged(object sender, EventArgs e)
		{
			this.WindowStyle = (this.WindowState == WindowState.Maximized) ? WindowStyle.None : WindowStyle.SingleBorderWindow;
		}

		/// <summary>
		/// 키보딩 처리
		/// : ESC키 입력시 윈도우 상태를 기본값으로 변경
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DidWindow_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				this.WindowState = WindowState.Normal;
			}
		}

		/// <summary>
		/// 데이터 바인딩
		/// </summary>
		/// <param name="pickups"></param>
		/// <param name="waitings"></param>
		public void SetBindingData(List<string> pickups, List<string> waitings)
		{
			if (pickups == null) return;

			int itemCount = pickups.Count;
			if (itemCount == 1)
			{
				this.PickupPanel.Columns = 1;
			}
			else if (itemCount >= 2 && itemCount <= 4)
			{
				this.PickupPanel.Columns = 2;
			}
			else if (itemCount >= 5 && itemCount <= 9)
			{
				this.PickupPanel.Columns = 3;
			}
			else
			{
				this.PickupPanel.Columns = 4;
			}

			try
			{
				IEnumerable<string> pickups_old = this.PickupOrdersItemsControl.ItemsSource.Cast<string>();
				IEnumerable<string> newPickups = pickups.Except(pickups_old);

				for (int i = 0; i < newPickups.Count(); i++)
				{
					SoundPlayer soundPlayer = new SoundPlayer(Properties.Resources.Dingdong);
					soundPlayer.Play();
				}
			}
			catch { }

			this.PickupOrdersItemsControl.ItemsSource = pickups;
			this.WaitingOrdersItemsControl.ItemsSource = waitings;
		}
	}
}
