﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AUOSMS.POS.Converters
{
	/// <summary>
	/// String.IsNullOrEmpty 값인 경우 입력 파라미터로 대체 반환 컨버터
	/// </summary>
	public class ParamInsteadNullConverter : IValueConverter
	{
		/// <summary>
		/// 컨버트
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return String.IsNullOrEmpty(value.ToString()) ? parameter : value;
		}

		/// <summary>
		/// 컨버트 콜백
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
