﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AUOSMS.POS.Converters
{
	/// <summary>
	/// 문자열 존재 여부에 따른 부울값 반환 컨버터
	/// </summary>
	public class HasStringConverter : IValueConverter
	{
		/// <summary>
		/// 컨버트
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool hasString = !String.IsNullOrEmpty(value.ToString());
			bool reverse = (parameter == null) ? false : Boolean.Parse(parameter.ToString());

			return reverse ? !hasString : hasString;
		}

		/// <summary>
		/// 컨버트 콜백
		/// </summary>
		/// <param name="value"></param>
		/// <param name="targetType"></param>
		/// <param name="parameter"></param>
		/// <param name="culture"></param>
		/// <returns></returns>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
