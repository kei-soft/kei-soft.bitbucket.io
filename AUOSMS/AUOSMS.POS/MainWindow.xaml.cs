﻿using AUOSMS.POS.Models;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AUOSMS.POS
{
	/// <summary>
	/// 매장단말 메인 윈도우
	/// </summary>
	public partial class MainWindow : Window
	{
		#region Fields

		/// <summary>
		/// 서비스 제공 미완료 주문 목록
		/// </summary>
		private List<OrderData> OrderWaiting = new List<OrderData>();

		/// <summary>
		/// 서비스 제공 완료 주문 목록
		/// </summary>
		private List<OrderData> OrderServed = new List<OrderData>();

		/// <summary>
		/// DID 윈도우
		/// </summary>
		private DidWindow DidWindow = null;

		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();

			this.Loaded += MainWindow_Loaded;
			this.Closed += MainWindow_Closed;

			this.DidToggleButton.Checked += DidToggleButton_Checked;
			this.DidToggleButton.Unchecked += DidToggleButton_Unchecked;
			this.LogoImageButton.PreviewMouseDown += LogoImageButton_PreviewMouseDown;
		}

		/// <summary>
		/// 윈도우 로드
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MainWindow_Loaded(object sender, RoutedEventArgs e)
		{
			AppCommon.Log("MAIN - OPEN");

			if (!AppCommon.IsOnline())
			{
				MessageBox.Show(Properties.Resources.NoNetwork, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
				this.Close();
				return;
			}

			this.DisplayCurrentTime();
			this.CheckHW();
			this.GetPosInfo();
			this.UpdateOrderData();
		}

		/// <summary>
		/// 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MainWindow_Closed(object sender, EventArgs e)
		{
			AppCommon.Log("MAIN - CLOSE");
		}

		/// <summary>
		/// 현재 시간 표시
		/// </summary>
		private async void DisplayCurrentTime()
		{
			this.CurrentTimeTextBlock.Dispatcher.Invoke(() => {
				this.CurrentTimeTextBlock.Text = $"{DateTime.Now:yyyy-MM-dd\nHH:mm:ss}";
			});
			await Task.Delay(1000);
			this.DisplayCurrentTime();
		}

		/// <summary>
		/// 장비 연결 상태 확인
		/// </summary>
		private void CheckHW()
		{
			while (!Printer.IsConnected())
			{
				AppCommon.Log("PRINTER :: NO");
				if (MessageBox.Show(Properties.Resources.NoPrinter, Properties.Resources.Error, MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.No)
				{
					this.Close();
					return;
				}
			}
			AppCommon.Log("PRINTER :: OK");
		}

		/// <summary>
		/// 매장단말 관련 정보 확인
		/// </summary>
		private void GetPosInfo()
		{
			try
			{
				List<MySqlParameter> parameters = new List<MySqlParameter>();
				parameters.Add(new MySqlParameter("P_POS_ID", AppConfig.PosID));

				using (DataTable dt = AppCommon.GetDataTable("USP_POS_INFO_GET", parameters))
				{
					AppConfig.StoreID = (int)dt.Rows[0]["STORE_ID"];
					AppConfig.StoreName = dt.Rows[0]["STORE_NAME"].ToString();
				}
			}
			catch (Exception)
			{
				MessageBox.Show(Properties.Resources.App_NoPosID, Properties.Resources.Error, MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
				AppCommon.Log("POS INFO :: STORE_ID={0}, STORE_NAME={1}", AppConfig.StoreID, AppConfig.StoreName);
			}
		}

		/// <summary>
		/// 옵션 정보 JSON 형식 문자열을 일반 문자 형식으로 변환 : OPTION_NAME, OPTION_DETAIL_NAME, OPTION_DETAIL_PRICE
		/// </summary>
		/// <param name="jsonString"></param>
		/// <returns></returns>
		private string ConvertJsonToText(string jsonString)
		{
			string plainText = String.Empty;

			List<Dictionary<string, string>> jsonList = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(jsonString);
			if (jsonList.Count > 0)
			{
				List<string> optionList = new List<string>();
				foreach (var jsonLine in jsonList)
				{
					optionList.Add(String.Join(":", jsonLine["OPTION_NAME"], jsonLine["OPTION_DETAIL_NAME"]));
				}
				plainText = String.Join(", ", optionList);
			}

			return plainText;
		}

		/// <summary>
		/// 주문 정보 확인
		/// </summary>
		private async void UpdateOrderData()
		{
			try
			{
				//AppCommon.Log("ORDER DATA UPDATE form SERVER - BEGIN");

				List<MySqlParameter> parameters = new List<MySqlParameter>();
				parameters.Add(new MySqlParameter("P_STORE_ID", AppConfig.StoreID));
				parameters.Add(new MySqlParameter("P_ORDER_DATE", DateTime.Today.ToShortDateString()));

				using (DataTable dt = AppCommon.GetDataTable("USP_POS_ORDER_LST", parameters))
				{
					AppCommon.OrderDataList.Clear();

					foreach (DataRow row in dt.Rows)
					{
						OrderItem item = new OrderItem
						{
							ORDER_ID = row["ORDER_ID"].ToString(),
							ITEM_NAME = row["ITEM_NAME"].ToString(),
							QUANTITY = row["QUANTITY"].ToString(),
							PRICE = String.Format("{0:#,#}", row["PRICE"]),
							ITEM_OPTION = this.ConvertJsonToText(row["ITEM_OPTION"].ToString())
						};

						var checker = AppCommon.OrderDataList.SingleOrDefault(x => x.ORDER_ID == row["ORDER_ID"].ToString());
						if (checker == null)
						{
							OrderData data = new OrderData
							{
								ORDER_ID = row["ORDER_ID"].ToString(),
								ORDER_DATE = String.Format("{0:yyyy-MM-dd}", row["ORDER_DATE"]),
								ORDER_TIME = row["ORDER_TIME"].ToString(),
								ORDER_NO = row["ORDER_NO"].ToString(),
								ORDER_STATUS = row["ORDER_STATUS"].ToString(),
								MADE = String.Format("{0:HH:mm:ss}", row["MADE"]),
								SERVED = String.Format("{0:HH:mm:ss}", row["SERVED"]),
								TOTAL_PRICE = String.Format("{0:#,#}", row["TOTAL_PRICE"]),
								MEMBER_NICKNAME = row["MEMBER_NICKNAME"].ToString(),
								ARRIVE_DATE = String.IsNullOrEmpty(row["ARRIVE_DATE"].ToString()) ? "-" : String.Format("{0:yyyy-MM-dd HH:mm:ss}", row["ARRIVE_DATE"])
							};

							data.ORDER_ITEM.Add(item);
							AppCommon.OrderDataList.Add(data);
						}
						else
						{
							checker.ORDER_ITEM.Add(item);
						}
					}
				}

				this.SetBindingData();

				//AppCommon.Log("ORDER DATA UPDATE form SERVER - END");

				foreach (var order in AppCommon.OrderDataList)
				{
					if (order.ORDER_STATUS == ServiceStatus.WAITING.ToString())
					{
						this.SetMakingOrder(order);
					}
				}
			}
			catch (Exception x)
			{
				AppCommon.Log("ORDER DATA UPDATE FAIL :: {0} ==>\r\n{1}", x.Message, x.StackTrace);
			}
			finally
			{
				await Task.Delay(AppConfig.SeverPollingInterval * 1000);
				this.UpdateOrderData();
			}
		}

		/// <summary>
		/// 데이터 바인딩
		/// </summary>
		private void SetBindingData()
		{
			this.OrderWaiting = AppCommon.OrderDataList.Where(x => String.IsNullOrEmpty(x.SERVED)).ToList();
			this.OrderServed = AppCommon.OrderDataList.Where(x => !String.IsNullOrEmpty(x.SERVED)).ToList();

			this.WaitingCountTextBlock.Text = $": {this.OrderWaiting.Count}";
			this.ServedCountTextBlock.Text = $": {this.OrderServed.Count}";

			this.OrderWaitingListView.ItemsSource = this.OrderWaiting;
			this.OrderServedListView.ItemsSource = this.OrderServed;

			if (this.DidWindow != null)
			{
				List<string> pickups = AppCommon.OrderDataList.Where(x => !String.IsNullOrEmpty(x.MADE) && String.IsNullOrEmpty(x.SERVED))
															  .OrderByDescending(x => x.MADE)
															  .Select(x => x.ORDER_NO).ToList();
				List<string> waitings = AppCommon.OrderDataList.Where(x => x.ORDER_STATUS == ServiceStatus.MAKING.ToString())
															   .OrderBy(x => x.ORDER_NO)
															   .Select(x => x.ORDER_NO).ToList();
				this.DidWindow.SetBindingData(pickups, waitings);
			}
		}

		/// <summary>
		/// 주문서 출력 및 DB 업데이트
		/// </summary>
		/// <param name="orderData"></param>
		private void SetMakingOrder(OrderData orderData)
		{
			// Print
			AppCommon.Log("ORDER PRINT :: {0}", orderData.ORDER_ID);
			Printer.Print(orderData);

			// DB Update
			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_ORDER_ID", orderData.ORDER_ID));

			int result = Convert.ToInt32(AppCommon.GetDataScalar("USP_POS_ORDER_UPD_MAKING", parameters)); 

			if (result == 1)
			{
				AppCommon.Log("ORDER DATA UPDATE to SERVER :: [MAKING] {0}", orderData.ORDER_ID);

				// Local Update
				var selectedOrder = AppCommon.OrderDataList.Single(x => x.ORDER_ID == orderData.ORDER_ID);
				selectedOrder.MADE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				selectedOrder.ORDER_STATUS = ServiceStatus.MAKING.ToString();
			}
			else
			{
				AppCommon.Log("ORDER DATA UPDATE to SERVER - ERROR({1}) :: [MAKING] {0}", orderData.ORDER_ID, result);
			}
		}

		/// <summary>
		/// 제조 완료 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MadeButton_Click(object sender, RoutedEventArgs e)
		{
			Button button = sender as Button;
			string orderID = button.Tag.ToString();

			if (String.IsNullOrEmpty(orderID)) return;

			if (MessageBox.Show($"{Properties.Resources.OrderMade} {Properties.Resources.ConfirmQuestion}",
								Properties.Resources.Information, MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No) return;

			// DB Update
			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_ORDER_ID", orderID));

			int result = Convert.ToInt32(AppCommon.GetDataScalar("USP_POS_ORDER_UPD_MADE", parameters));

			if (result == 1)
			{
				AppCommon.Log("ORDER DATA UPDATE to SERVER :: [MADE] {0}", orderID);

				// Local Update
				var selectedOrder = AppCommon.OrderDataList.Single(x => x.ORDER_ID == orderID);
				selectedOrder.MADE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				selectedOrder.ORDER_STATUS = ServiceStatus.MADE.ToString();

				this.SetBindingData();
			}
			else
			{
				AppCommon.Log("ORDER DATA UPDATE to SERVER - ERROR({1}) :: [MADE] {0}", orderID, result);
			}
		}

		/// <summary>
		/// 제공 완료 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void ServedButton_Click(object sender, RoutedEventArgs e)
		{
			Button button = sender as Button;
			string orderID = button.Tag.ToString();

			if (String.IsNullOrEmpty(orderID)) return;

			if (MessageBox.Show($"{Properties.Resources.OrderServed} {Properties.Resources.ConfirmQuestion}",
								Properties.Resources.Information, MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No) return;

			// DB Update
			List<MySqlParameter> parameters = new List<MySqlParameter>();
			parameters.Add(new MySqlParameter("P_ORDER_ID", orderID));

			int result = Convert.ToInt32(AppCommon.GetDataScalar("USP_POS_ORDER_UPD_SERVED", parameters));

			if (result == 1)
			{
				AppCommon.Log("ORDER DATA UPDATE to SERVER :: [SERVED] {0}", orderID);

				// Local Update
				var selectedOrder = AppCommon.OrderDataList.Single(x => x.ORDER_ID == orderID);
				selectedOrder.SERVED = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				selectedOrder.ORDER_STATUS = ServiceStatus.SERVED.ToString();

				this.SetBindingData();
			}
			else
			{
				AppCommon.Log("ORDER DATA UPDATE to SERVER - ERROR({1}) :: [SERVED] {0}", orderID, result);
			}
		}

		/// <summary>
		/// DID 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DidToggleButton_Checked(object sender, RoutedEventArgs e)
		{
			if (this.DidWindow == null)
			{
				this.DidWindow = new DidWindow
				{
					Owner = this,
					WindowStartupLocation = WindowStartupLocation.CenterOwner
				};
				this.DidWindow.Closed += Did_Closed;
				this.DidWindow.Show();
				this.SetBindingData();
			}
		}

		/// <summary>
		/// DID 버튼 선택 해제
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DidToggleButton_Unchecked(object sender, RoutedEventArgs e)
		{
			foreach (Window did in this.OwnedWindows)
			{
				if (did is DidWindow)
				{
					did.Close();
				}
			}
		}

		/// <summary>
		/// DID 윈도우 종료
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Did_Closed(object sender, EventArgs e)
		{
			this.DidWindow = null;
			this.DidToggleButton.IsChecked = false;
		}

		/// <summary>
		/// 로고 버튼 선택
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LogoImageButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			Process.Start($"{AppConfig.ServerBaseUrl}/STORE/");
		}
	}
}
