﻿using System;
using System.IO;

namespace AUOSMS.POS
{
	/// <summary>
	/// 앱 설정 클래스
	/// </summary>
	internal static class AppConfig
	{
		/// <summary>
		/// 앱 명칭
		/// </summary>
		public static readonly string AppName = "AUOSMS_POS";

		/// <summary>
		/// 앱 실행 위치
		/// : cf. C:\Program Files (x86)\AUOSMS_POS
		/// </summary>
		public static readonly string StartupPath = AppDomain.CurrentDomain.BaseDirectory;

		/// <summary>
		/// 관련 데이터 기본 경로
		/// : cf. C:\ProgramData\AUOSMS_POS
		/// </summary>
		private static readonly string BaseDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), AppConfig.AppName);

		/// <summary>
		/// 로그 파일 경로
		/// </summary>
		public static readonly string LogDirPath = Path.Combine(AppConfig.BaseDataPath, "LOG");

		/// <summary>
		/// 서버 URL
		/// </summary>
		public static readonly string ServerBaseUrl = "http://211.57.201.122:8200";

		/// <summary>
		/// DB 커넥션 스트링
		/// </summary>
		public static readonly string DBConnectionString = "SERVER=211.57.201.122;DATABASE=AUOSMS;UID=auosms_user;PASSWORD=auosms20201!";

		/// <summary>
		/// 단말 ID (from INI)
		/// : cf. c72e7e07-a99a-4656-8f39-869b4bea4e1b
		/// </summary>
		public static string PosID = String.Empty;

		/// <summary>
		/// 서버 폴링 시간(초) (from INI)
		/// : cf. 3
		/// </summary>
		public static int SeverPollingInterval = 3;

		/// <summary>
		/// 설치된 프린터 이름
		/// : cf. SAM4S GIANT-100
		/// </summary>
		public static string PrinterName = String.Empty;

		/// <summary>
		/// 매장 이름
		/// </summary>
		public static string StoreName = String.Empty;

		/// <summary>
		/// 매장 코드
		/// </summary>
		public static int StoreID = -1;
	}
}
