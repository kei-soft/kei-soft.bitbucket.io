﻿$(document).ready(function () {
	do_cmd.elementDisabled($(".readonly"));

	$("body").on("keyup", "input:text.numberOnly, input:text.spinner", function () {
		$(this).val(addCommas($(this).val().replace(/[^0-9]/g, "")));
	});

	$(".datepicker").datepicker({
		showOn: "both",
		buttonImage: contextPath + "images/icon_calendar.gif",
		buttonImageOnly: true,
		dateFormat: "yy-mm-dd",
		monthNamesShort: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
		showMonthAfterYear: true,
		changeMonth: true,
		changeYear: true
	})
	.css("display", "inline-block")
		.next("img").hide();

	$(".spinner").spinner({
		min: 1,
		max: 100,
		step: 1
	});
	
	$("select.common_code").each(function () {
		BindCommCodeSelect($(this));
	});

	$("select.bind_time").each(function () {
		BindTimeSelect($(this), "TIME");
	});

	$("select.bind_min").each(function () {
		BindTimeSelect($(this), "MIN");
	});
	
});

var do_cmd = {
	ajax: function (opt) {
		$.ajax({
			url: opt.URL,
			//contentType: "application/json; charset=UTF-8",
			method: "POST",
			async: opt.async,
			data: { Command: opt.Command, Params: JSON.stringify(opt.Params), Files: opt.Files },
			dataType: "json",
			beforeSend: function () {
				do_cmd.loading_spinner(true);
			},
			success: function (result) {
				if (result.STATUS == "OK") {
					if (opt.SuccessCallBack) opt.SuccessCallBack(result.RESULT);
				}
				else {
					alert(result.MSG);
				}
			},
			error: function (err) {
				console.log(err);
				alert(err);
			},
			complete: function () {
				do_cmd.loading_spinner(false);
			}
		});
	},
	ajax_file: function (opt) {
		var formdata = new FormData();
		formdata.append("Command", opt.Command);
		formdata.append("Params", JSON.stringify(opt.Params));
		formdata.append("Files", opt.Files);

		$.ajax({
			url: opt.URL,
			dataType: "json",
			type: "POST",
			processData: false,
			contentType: false,
			data: formdata,
			beforeSend: function () {
				do_cmd.loading_spinner(true);
			},
			success: function (result) {
				if (result.STATUS == "OK") {
					if (opt.SuccessCallBack) opt.SuccessCallBack(result.RESULT);
				}
				else {
					alert(result.MSG);
				}
			},
			error: function (err) {
				console.log(err);
				alert(err);
			},
			complete: function () {
				do_cmd.loading_spinner(false);
			}
		});
	},
	loading_spinner: function (visibility) {
		if (visibility) {
			$("#loading_spinner").show();
		}
		else {
			$("#loading_spinner").hide();
		}
	},
	elementDisabled: function ($obj) {
		$obj.focus(function () {
			$(this).blur();
		});
		$obj.css("background-color", "#E6E5E5");
	},
	currencyFormat: function (strNumber) {
		strNumber = strNumber.replace(",", "");

		if (isNaN(strNumber)) {
			return strNumber;
		}

		var negative = false;
		if (parseInt(strNumber) < 0) {
			negative = true;
			strNumber = strNumber.replace(/-/, "");
		}

		if (parseInt(strNumber) < 1000) {
			return (negative ? "-" : "") + strNumber;
		}
		else {
			var strInt, strFloat;
			if (strNumber.indexOf(".") == -1) {
				strInt = strNumber;
				strFloat = 0;
			}
			else {
				var temp = strNumber.split(".");
				strInt = temp[0];
				strFloat = temp[1];
			}

			var n = strInt.length;
			var reverseString = ""
			var checkedString = "";
			var k = (n + parseInt(n / 3));
			var j = 0;
			var checked = false;

			for (var i = k - 1; i >= 0; i--) {
				if (checked) {
					reverseString += ",";
					checked = false;
				}
				else {
					n--;
					reverseString += strNumber.charAt(n);
					j++;
					checked = ((j % 3) == 0) ? true : false;
				}
			}

			for (var i = k - 1; i >= 0; i--) {
				checkedString += reverseString.charAt(i);
			}
			checkedString = (checkedString.charAt(0) == ",") ? checkedString.substr(1) : checkedString;

			return (negative ? "-" : "") + ((strFloat == 0) ? checkedString : checkedString + "." + strFloat);
		}
	},
	excelExport: function (tableID, fileName) {
		$("#" + tableID).table2excel({
			exclude: ".noExl",
			name: "Excel Document Name",
			filename: fileName + '.xls', //확장자를 여기서 붙여줘야한다.
			fileext: ".xls",
			exclude_img: true,
			exclude_links: true,
			exclude_inputs: true
		});
	}
};

String.format = function () {
	var theString = arguments[0];

	for (var i = 1; i < arguments.length; i++) {
		var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
		theString = theString.replace(regEx, arguments[i]);
	}

	return theString;
}

String.prototype.currencyFormat = function () {
	var strNumber = this;

	strNumber = strNumber.replace(",", "");

	if (isNaN(strNumber)) {
		return strNumber;
	}

	var negative = false;
	if (parseInt(strNumber) < 0) {
		negative = true;
		strNumber = strNumber.replace(/-/, "");
	}

	if (parseInt(strNumber) < 1000) {
		return (negative ? "-" : "") + strNumber;
	}
	else {
		var strInt, strFloat;
		if (strNumber.indexOf(".") == -1) {
			strInt = strNumber;
			strFloat = 0;
		}
		else {
			var temp = strNumber.split(".");
			strInt = temp[0];
			strFloat = temp[1];
		}

		var n = strInt.length;
		var reverseString = ""
		var checkedString = "";
		var k = (n + parseInt(n / 3));
		var j = 0;
		var checked = false;

		for (var i = k - 1; i >= 0; i--) {
			if (checked) {
				reverseString += ",";
				checked = false;
			}
			else {
				n--;
				reverseString += strNumber.charAt(n);
				j++;
				checked = ((j % 3) == 0) ? true : false;
			}
		}

		for (var i = k - 1; i >= 0; i--) {
			checkedString += reverseString.charAt(i);
		}
		checkedString = (checkedString.charAt(0) == ",") ? checkedString.substr(1) : checkedString;

		return (negative ? "-" : "") + ((strFloat == 0) ? checkedString : checkedString + "." + strFloat);
	}
}

String.prototype.lPad = function (len, chr) {
	var rtnStr = this;

	if (this.length >= len)
		return rtnStr;

	for (var i = 0; i < len - this.length; i++) {
		rtnStr = chr + rtnStr;
	}

	return rtnStr;
}

function BindCommCodeSelect($obj) {
	$obj.html("");
	if ($obj.data("select") == "kor") {
		$obj.append("<option value=\"\">선택</option>");
	}
	if ($obj.data("select") == "eng") {
		$obj.append("<option value=\"\">SELECT</option>");
	}

	var params = {};
	params.CODE_GROUP = $obj.data("group");

	do_cmd.ajax({
		URL: contextPath + "Ajax/Json.aspx",
		async: true,
		Command: "GetCommonCodeList",
		Params: params,
		SuccessCallBack: function (data) {
			if (data.length > 0) {
				for (var i = 0; i < data.length; i++) {
					$obj.append("<option value=\"" + data[i].CODE_ID + "\">" + data[i].CODE_NAME+"</option>");
				}
			}
		}
	});

	//ajaxTestPopup(contextPath + "/Ajax/Json.aspx", "AjaxTest", "GetCommonCodeList", params);

}

function BindTimeSelect($obj, gubun) {
	$obj.html("");
	var endNum = gubun == "TIME" ? 23 : 59;

	for (var i = 0; i <= endNum; i++) {
		var time = i < 10 ? "0" + String(i) : String(i);
		$obj.append("<option value=\"" + time + "\">" + time + "</option>");
	}
}

function GetItemGroupList(callback) {
	do_cmd.ajax({
		URL: contextPath + "Ajax/Json.aspx",
		async: true,
		Command: "GetItemGroupList",
		SuccessCallBack: function (data) {
			callback(data);
		}
	});
}

function displayPageNavi(pageIndex, pageSize, naviCount, dataSize) {
	pageIndex = pageIndex - 1;
	var temp = Math.floor(pageIndex / naviCount);  // 현재페이지에 보여줄 페이지수를 나눈값을 저장
	var cnt = (temp * naviCount); // 나눈값에 다시 보여줄 페이지수를 곱하여 나눈 몫을 구함
	var Paging = "";  // 페이징부분에 보여줄 문자열 변수

	if (dataSize % pageSize != 0) {
		dataSize = Math.floor(dataSize / pageSize);
		dataSize = dataSize + 1;
	}
	else {
		dataSize = dataSize / pageSize;
	}

	Paging = "";
	// 현재페이지가 한번에 보여줄 페이지수보다 큰 경우에 앞페이지로 넘어가는 Prev버튼을 보여줌
	if (temp > 0) {
		Paging += "<label for='previousPageUnit'><span id='previousPageUnit' class='btn_first pagenavi' alt='previousPageUnit' name='btnFirst' data-page='1'></span></label>";
		Paging += "<label for='previousPage'><span id='previousPage' class='btn_prev pagenavi' alt='previousPage' name='btnPrev' data-page='" + (pageIndex - naviCount + 1) + "'></span></label>";
	}
	else {
		Paging += "<label for='previousPageUnit'><span id='previousPageUnit' class='btn_first' alt='previousPageUnit' name='btnFirst'></span></label>";
		Paging += "<label for='previousPage'><span id='previousPage' class='btn_prev' alt='previousPage' name='btnPrev'></span></label>";
	}

	// 페이지를 표시갯수만큼 보여줌
	Paging += "<strong>";
	var i;
	for (i = cnt + 1; i <= cnt + naviCount; i++) {
		if (i <= dataSize) {
			if (i == pageIndex + 1)
				Paging += "<strong>" + i + "</strong>";
			else
				Paging += "<a data-page='" + i + "' class='pagenavi'>" + i + "</a>";
		}
	}
	Paging += "</strong>";

	if (dataSize >= i) {
		Paging += "<label for='nextPage'><span id='nextPage' class='btn_next pagenavi' alt='nextPage' name='btnNext' data-page='" + i + "'></span></label>";
		Paging += "<label for='nextPageUnit'><span id='nextPageUnit' class='btn_last pagenavi' alt='nextPageUnit' name='btnLast' data-page='" + dataSize + "'></span></label>";
	}
	else {
		Paging += "<label for='nextPage'><span id='nextPage' class='btn_next' alt='nextPage' name='btnNext'></span></label>";
		Paging += "<label for='nextPageUnit'><span id='nextPageUnit' class='btn_last' alt='nextPageUnit' name='btnLast'></span></label>";
	}

	return Paging;
}

// 팝업 오픈
function fctPopupOpen(url, target, data, option) {

	var $frm = jQuery("<form>").attr({
		"id": "frmPopup", "method": "post", "action": url, "target": target
	});

	// var data = [{ "ky": "hdfName", "vl": "Kim" }, { "ky": "hdfAge", "vl": "25" }, { "ky": "hdfSex", "vl": "F"}];


	for (var i = 0; i < data.length; i++) {
		jQuery("<input>").attr({ "type": "hidden", "value": data[i].vl, "name": data[i].ky }).appendTo($frm);
	}

	window.open("", target, option);
	jQuery("body").append($frm);

	$frm.submit();

	$frm.remove();

	return false;
}

function ajaxTestPopup(url, target, command, data) {
	var $frm = $("<form>").attr({
		"id": "frmPopup", "method": "post", "action": url, "target": target
	});

	$("<input>").attr({ "type": "hidden", "value": command, "name": "Command" }).appendTo($frm);
	$("<input>").attr({ "type": "hidden", "value": JSON.stringify(data), "name": "Params" }).appendTo($frm);

	window.open("", target, "");
	$("body").append($frm);

	$frm.submit();

	$frm.remove();

	return false;
}

function inputDigit() {
	var key = window.event.keyCode;
	alert(key);

	if (key >= 48 && key <= 57) {
		// 자판 숫자
	}
	else if (key >= 96 && key <= 105) {
		// 키패드 숫자
	}
	else if (key == 190) {
		// 자판 .
	}
	else if (key == 110) {
		// 키패드 .
	}
	else if (key == 144) {
		// Num Lock
	}
	else if (key == 46) {
		// Delete
	}
	else if (key == 8) {
		// Back Space
	}
	else if (key >= 37 && key <= 40) {
		// 방향키
	}
	else if (key == 9) {
		// Tab
	}
	else {
		window.event.returnValue = false;
	}
}

//3자리 단위마다 콤마 생성
function addCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//모든 콤마 제거
function removeCommas(x) {
	if (!x || x.length == 0) return "";
	else return x.split(",").join("");
}

function getDBWeekday(day) {
	var str = "";
	switch (day) {
		case 0:
			str = "월";
			break;
		case 1:
			str = "화";
			break;
		case 2:
			str = "수";
			break;
		case 3:
			str = "목";
			break;
		case 4:
			str = "금";
			break;
		case 5:
			str = "토";
			break;
		case 6:
			str = "일";
			break;
	}

	return str;
}