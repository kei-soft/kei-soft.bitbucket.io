﻿<%@ Control Language="C#" ClassName="OptionForm" %>


<script>
	var OptionImageFile = new Object();

	$(document).ready(function () {
		$("#btnSave").click(function () {
			OptionForm.save();
		});

		$("#imgOptionImage").click(function () {
			$("#selectImage").click();
		});

		$("#selectImage").on("change", function (e) {
			var file = $(this)[0].files[0];
			if (!file.type.match("image/.*")) {
				alert("이미지파일만 선택가능");
			}
			else {
				OptionImageFile = file;
				var reader = new FileReader();
				reader.onload = function (e) {
					$("#imgOptionImage").attr("src", e.target.result);
				}
				reader.readAsDataURL(OptionImageFile);
			}
		});

		$("#btnOptionDetailAdd").click(function () {
			var $detail = $(OptionForm.DetailObject());
			$("#divOptionDetailPanel").append($detail);
		});

		$("#divOptionDetailPanel").on("click", "button.option_detail_up", function () {
			var $formGroup = $(this).parents("form.form_option_detail");
			var currentIndex = $("#divOptionDetailPanel form.form_option_detail").index($formGroup);

			if (currentIndex == 0) {
				return;
			}

			$formGroup.prev("form.form_option_detail").before($formGroup);
		});

		$("#divOptionDetailPanel").on("click", "button.option_detail_down", function () {
			var $formGroup = $(this).parents("form.form_option_detail");
			var group_count = $("#divOptionDetailPanel form.form_option_detail").length;
			var currentIndex = $("#divOptionDetailPanel form.form_option_detail").index($formGroup);

			if (group_count <= currentIndex + 1) {
				return;
			}

			$formGroup.next("form.form_option_detail").after($formGroup);

		});

		$("#divOptionDetailPanel").on("click", "button.option_detail_minus", function () {
			if (confirm("삭제 하시겠습니까?"))
				$(this).parents("form.form_option_detail").remove();
		});

	});

	var OptionForm = {
		saveCallback: null,
		optionCode: null,
		groupCode: null,
		open: function (option_code, group_code, saveCallback) {
			this.saveCallback = saveCallback;
			this.optionCode = option_code;
			this.groupCode = group_code;
			
			if (this.optionCode == null) { // 새로입력
				$("#divOptionForm h4.modal-title").text("메뉴옵션 등록");
				
				$("#imgOptionImage").attr("src", contextPath + "images/no_image.png");
				OptionImageFile = new Object();
				$("#txtOptionName").val("");
				$("#selEssential").val("Y");
				$("#setUseYN").val("Y");
				$("#divOptionDetailPanel").html("");
				
				$("#divOptionForm").modal();
			}
			else { // 수정입력
				$("#divOptionForm h4.modal-title").text("메뉴옵션 수정");

				var params = {};
				params.OPTION_CODE = this.optionCode;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "GetOptionInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data.INFO.length > 0) {
							$("#txtOptionName").val(data.INFO[0].OPTION_NAME);
							$("#imgOptionImage").attr("src", data.FILEPATH + data.INFO[0].OPTION_IMAGE);
							ItemImageFile = new Object();
							$("#selEssential").val(data.INFO[0].ESSENTIAL);
							$("#setUseYN").val(data.INFO[0].USE_YN);
							$("#divOptionDetailPanel").html("");

							for (var i = 0; i < data.LIST.length; i++) {
								var $detail = $(OptionForm.DetailObject());
								$detail.find("input.txtDetailName").val(data.LIST[i].OPTION_DETAIL_NAME);
								$detail.find("input.txtExtraCost").val(String(data.LIST[i].EXTRA_COST).currencyFormat());								
								$detail.find("select.selUseYN").val(data.LIST[i].USE_YN);

								$("#divOptionDetailPanel").append($detail);
							}

							$("#divOptionForm").modal();
						}
						else {
							alert("error");
						}
					}
				});
			}

		},
		close: function () {
			$("#divOptionForm").modal("hide");
		},
		validation: function () {
			var $e;

			$e = $("#txtOptionName");
			if ($e.val() == "") {
				alert("옵션명을 입력하세요.");
				$e.focus();
				return false;
			}

			if ($("#divOptionDetailPanel > form.form_option_detail").length < 1) {
				alert("상세옵션을 등록하시기 바랍니다.");
				return false;
			}

			for (var i = 0; i < $("#divOptionDetailPanel > form.form_option_detail").length; i++) {
				var $form = $("#divOptionDetailPanel > form.form_option_detail").eq(i);

				$e = $form.find("input.txtDetailName");
				if ($e.val() == "") {
					alert("상세이름을 입력하세요.");
					$e.focus();
					return false;
				}

				$e = $form.find("input.txtExtraCost");
				if ($e.val() == "") {
					alert("가격을 입력하세요.");
					$e.focus();
					return false;
				}
			}

			if (!confirm("옵션을 저장하시겠습니까?")) {
				return false;
			}

			return true;

		},
		save: function () {
			if (this.validation()) {
				var params = {};
				params.CATEGORY = this.groupCode;
				params.NAME = $("#txtOptionName").val();
				params.ESSENTIAL = $("#selEssential").val();
				params.USE_YN = $("#setUseYN").val();
				params.DETAILS = [];

				for (var i = 0; i < $("#divOptionDetailPanel > form.form_option_detail").length; i++) {
					var $form = $("#divOptionDetailPanel > form.form_option_detail").eq(i);

					var details = {};
					details.NAME = $form.find("input.txtDetailName").val();
					details.EXTRA_COST = removeCommas($form.find("input.txtExtraCost").val());
					details.USE_YN = $form.find("select.selUseYN").val();

					params.DETAILS.push(details);
				}

				var cmd = "";
				if (this.optionCode == null) { // 새로입력
					cmd = "InsertOption";
				}
				else { // 수정입력
					cmd = "UpdateOption";
					params.CODE = this.optionCode;
				}

				//console.log(JSON.stringify(params)); alert(JSON.stringify(params)); return;

				do_cmd.ajax_file({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: cmd,
					Params: params,
					Files: OptionImageFile,
					SuccessCallBack: function (data) {
						if (data > 0) {
							alert("저장되었습니다.");
							OptionForm.close();
							OptionForm.saveCallback();
						}
						else {
							alert("등록 오류!");
						}
					}
				});
			}
		},
		DetailObject: function () {
			var strHtml = "";
			strHtml += "<form class=\"form-inline form_option_detail\" style=\"padding:5px;\">";
			strHtml += "";
			strHtml += "	<div class=\"form-group input-group\" style=\"width: 200px;\">";
			strHtml += "		<span class=\"input-group-addon\">이름</span>";
			strHtml += "		<input type=\"text\" class=\"form-control txtDetailName\" placeholder=\"이름\"/>";
			strHtml += "	</div>";
			strHtml += "	";
			strHtml += "	<div class=\"form-group input-group\" style=\"width: 200px;\">";
			strHtml += "		<span class=\"input-group-addon\">가격</span>";
			strHtml += "		<input type=\"text\" class=\"form-control numberOnly txtExtraCost\" placeholder=\"가격\"  />";
			strHtml += "	</div>";
			strHtml += "	";
			strHtml += "	<div class=\"form-group input-group\" style=\"width: 200px;\">";
			strHtml += "		<span class=\"input-group-addon\">사용여부</span>";
			strHtml += "		<select class=\"form-control selUseYN\">";
			strHtml += "			<option value=\"Y\">Y</option>";
			strHtml += "			<option value=\"N\">N</option>";
			strHtml += "		</select>";
			strHtml += "	</div>";
			strHtml += "";
			strHtml += "	<div class=\"form-group input-group pull-right\" style=\"padding:6px;\">";
			strHtml += "		<button type=\"button\" class=\"btn btn-primary btn-xs option_detail_up\"><span class=\"fa fa-arrow-up fa-fw\"></span></button>";
			strHtml += "		<button type=\"button\" class=\"btn btn-primary btn-xs option_detail_down\"><span class=\"fa fa-arrow-down fa-fw\"></span></button>";
			strHtml += "		<button type=\"button\" class=\"btn btn-danger btn-xs option_detail_minus\"><span class=\"fa fa-minus-square-o fa-fw\"></span></button>";
			strHtml += "	</div>";
			strHtml += "";
			strHtml += "</form>";

			return strHtml;
		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divOptionForm">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">메뉴 옵션</h4>
			</div>
			<div class="modal-body" style="min-height:500px;">
				<form>
					<div class="form-group text-center">
						<img src="<%=Page.ResolveUrl("~") %>images/no_image.png" style="width:50%; cursor: pointer;" alt="클릭하여 이미지 선택" id="imgOptionImage" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" />
						<input type="file" id="selectImage" name="selectImage" style="display: none;" />
						<p class="text-primary">이미지를 클릭하여 사진을 선택하세요.</p>
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">옵션명</span>
						<input type="text" class="form-control" placeholder="옵션명" id="txtOptionName" />
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">필수여부</span>
						<select class="form-control" id="selEssential">
							<option value="Y">Y</option>
							<option value="N">N</option>
						</select>
					</div>
					<div class="form-group input-group">
						<span class="input-group-addon">사용여부</span>
						<select class="form-control" id="setUseYN">
							<option value="Y">Y</option>
							<option value="N">N</option>
						</select>
					</div>
				</form>

				<div class="panel panel-default" style="margin-top: 20px;">
					<div class="panel-heading">
						상세 옵션
						<div class="pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-info btn-xs" id="btnOptionDetailAdd"><span class="fa fa-plus-square-o fa-fw"></span></button>
							</div>
						</div>
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body" style="padding:5px;" id="divOptionDetailPanel"></div>
					<!-- /.panel-body -->
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>