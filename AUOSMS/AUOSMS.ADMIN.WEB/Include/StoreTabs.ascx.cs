﻿using System;
using System.ComponentModel;
using System.Text;

public partial class Include_StoreTabs : System.Web.UI.UserControl
{
	private string category = string.Empty;

	[Category("Category"), Description("카테고리 (View, Menu, Sale, Order, Intro, Event, DID)")]
	public string Catergory
	{
		get { return this.category; }
		set { this.category = value; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			string newLine = Environment.NewLine;
			string shop_id = Request.QueryString["shop_id"];

			string[,] arrTabList = new string[,]
			{
				{ "View","매장 정보" }
				,{ "Menu","메뉴 관리"}
				,{ "Sale","매출 내역"}
				,{ "Order","주문 내역"}
				//,{ "SaleByItem","메뉴별 매출 내역"}
				,{ "Intro","INTRO 관리"}
				,{ "Event","이벤트"}
				,{ "DID","DID관리"}
			};

			StringBuilder sbTabs = new StringBuilder();
			for(int i = 0; i < arrTabList.GetLength(0); i++)
			{
				string id = arrTabList[i, 0];
				string name= arrTabList[i, 1];

				if (this.category.Equals(id))
				{
					sbTabs.AppendFormat("<li class=\"active\"><a>{0}</a></li>{1}", name, newLine);
				}
				else
				{
					sbTabs.AppendFormat("<li><a href=\"{0}.aspx?shop_id={1}\">{2}</a></li>{3}", id, shop_id, name, newLine);
				}
			}

			this.litTabList.Text = sbTabs.ToString();
		}
	}
}