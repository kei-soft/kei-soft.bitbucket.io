﻿<%@ Control Language="C#" ClassName="ManagerNewForm" %>

<script>
	$(document).ready(function () {
		$("#txtID").blur(function () {
			ManagerNewForm.chkId($(this).val());
		});

		$("#btnSave").click(function () {
			ManagerNewForm.save();
		});
	});

	var ManagerNewForm = {
		saveCallback: null,
		open: function (saveCallback) {
			this.saveCallback = saveCallback;

			$("#txtName").val("");
			$("#selGroup option:eq(0)").attr("selected", "selected");
			$("#txtID").val("");
			$("#txtID").data("chk", false);
			$("#txtPWD").val("");
			$("#txtPWDConfirm").val("");

			$("#divNewNamager").modal();
		},
		close: function () {
			$("#divNewNamager").modal("hide");
		},
		setIDMsg: function (msg, color) {
			$("#spIDMsg").css("color", color);
			$("#spIDMsg").text(msg);
		},
		chkId: function (id) {
			if (id.length < 5) {
				//alert("아이디는 다섯자 이상 입력하세요.");
				this.setIDMsg(" : 아이디는 다섯자 이상 입력하세요.", "red");
				$("#txtID").data("chk", false);
			}
			else {
				var params = {};
				params.ID = id;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "ChkManagerID",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							//alert("사용 가능한 아이디입니다.");
							ManagerNewForm.setIDMsg(" : 사용 가능한 아이디입니다.", "blue");
							$("#txtID").data("chk", true);
						}
						else {
							//alert("이미 사용중인 아이디입니다.");
							ManagerNewForm.setIDMsg(" : 이미 사용중인 아이디입니다.", "red");
							$("#txtID").data("chk", false);
						}
					}
				});
			}
		},
		validation: function () {
			var $e, $e2;

			$e = $("#txtID");
			if (!$e.data("chk")) {
				alert("아이디 중복검사가 되지 않았습니다.");
				$e.focus();
				return false;
			}

			$e = $("#txtName");
			if ($e.val() == "") {
				alert("관리자 이름을 선택하세요.");
				$e.focus();
				return false;
			}

			$e = $("#selGroup");
			if ($e.val() == "") {
				alert("관리자 권한을 선택하세요.");
				$e.focus();
				return false;
			}

			$e = $("#txtPWD");
			if ($e.val().length < 4) {
				alert("패스워드를 4자 이상 입력하세요.");
				$e.focus();
				return false;
			}

			$e2 = $("#txtPWDConfirm");
			if ($e.val() != $e2.val()) {
				alert("패스워드가 맞지 않습니다.");
				$e2.focus();
				return false;
			}

			if (!confirm("관리자를 등록하시겠습니까?")) {
				return false;
			}

			return true;

		},
		save: function () {
			if (this.validation()) {
				var params = {};
				params.GROUP = $("#selGroup").val();
				params.ID = $("#txtID").val();
				params.PW = $("#txtPWD").val();
				params.NAME = $("#txtName").val();

				//console.log(JSON.stringify(params)); return;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "InsertManager",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							alert("등록이 완료되었습니다.");
							ManagerNewForm.close();
							ManagerNewForm.saveCallback();
						}
						else {
							alert("등록 오류!");
						}
					}
				});
			}
		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divNewNamager">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">관리자 생성</h4>
			</div>
			<div class="modal-body" style="min-height:500px;">
				<form>
					<div class="form-group">
						<label for="txtName">관리자이름</label>
						<input type="text" class="form-control" placeholder="NAME" id="txtName" />
					</div>
					<div class="form-group">
						<label for="selGroup">관리자권한</label>
						<select class="form-control common_code" data-group="MANAGER_GROUP" data-select="eng" id="selGroup"></select>
					</div>
					<div class="form-group">
						<label for="txtID">아이디</label> <span id="spIDMsg" class="text-danger"></span>
						<input type="text" class="form-control" placeholder="ID" id="txtID" data-chk="false" />
					</div>
					<div class="form-group">
						<label for="txtPWD">비밀번호</label>
						<input type="password" class="form-control" placeholder="PASSWORD" id="txtPWD" />
					</div>
					<div class="form-group">
						<label for="txtPWDConfirm">비밀번호 확인</label>
						<input type="password" class="form-control" placeholder="CONFIRM PASSWORD" id="txtPWDConfirm" />
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>