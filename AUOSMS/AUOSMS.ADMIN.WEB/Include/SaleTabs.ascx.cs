﻿using System;
using System.ComponentModel;
using System.Text;

public partial class Include_SaleTabs : System.Web.UI.UserControl
{
	private string category = string.Empty;

	[Category("Category"), Description("카테고리 (Sale, SaleByItem, SaleReportByDaysList, SaleReportByMonthList, SaleReportByReceiptsList, SaleReportByItemTimeList, SaleReportByItemGroupList)")]
	public string Catergory
	{
		get { return this.category; }
		set { this.category = value; }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			string newLine = Environment.NewLine;
			string shop_id = Request.QueryString["shop_id"];

			string[,] arrTabList = new string[,]
			{
				{ "Sale","매출 내역"}
				,{ "SaleByItem","메뉴별 매출 내역"}
				,{ "SaleReportByDaysList","일자별 매출"}
				,{ "SaleReportByMonthList","월별 매출"}
				,{ "SaleReportByReceiptsList","영수증별 매출"}
				,{ "SaleReportByItemTimeList","시간대별 상품 매출"}
				,{ "SaleReportByItemGroupList","상품별 매출"}
			};

			StringBuilder sbTabs = new StringBuilder();
			for (int i = 0; i < arrTabList.GetLength(0); i++)
			{
				string id = arrTabList[i, 0];
				string name = arrTabList[i, 1];

				if (this.category.Equals(id))
				{
					sbTabs.AppendFormat("<li class=\"active\"><a>{0}</a></li>{1}", name, newLine);
				}
				else
				{
					sbTabs.AppendFormat("<li><a href=\"{0}.aspx?shop_id={1}\">{2}</a></li>{3}", id, shop_id, name, newLine);
				}
			}

			this.litTabList.Text = sbTabs.ToString();
		}
	}
}