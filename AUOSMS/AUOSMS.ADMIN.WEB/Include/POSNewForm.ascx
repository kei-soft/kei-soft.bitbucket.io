﻿<%@ Control Language="C#" ClassName="POSNewForm" %>

<script>
	$(document).ready(function () {
		$("#btnPOSSave").click(function () {
			POSKNewForm.save();
		});
	});

	var POSKNewForm = {
		saveCallback: null,
		store_id: null,
		pos_id: null,
		open: function (store_id, pos_id, saveCallback) {
			this.saveCallback = saveCallback;
			this.store_id = store_id;
			this.pos_id = pos_id;

			if (pos_id == null) {
				$("#txtPOSName").val("");

				$("#divNewPOS").modal();
				$("#txtPOSName").focus();

			}
			else {
				var params = {};
				params.ID = this.pos_id;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "GetPOSInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data.length > 0) {
							$("#txtPOSName").val(data[0].POS_NAME);

							$("#divNewPOS").modal();
							$("#txtPOSName").focus();
						}
						else {
							alert("잘못된 매장 코드입니다.");
						}
					}
				});
			}
		},
		close: function () {
			$("#divNewPOS").modal("hide");
		},
		validation: function () {
			var $e;

			$e = $("#txtPOSName");
			if ($e.val() == "") {
				alert("단말기 이름을 입력하세요.");
				$e.focus();
				return false;
			}

			if (!confirm("단말을 저장하시겠습니까?")) {
				return false;
			}

			return true;

		},
		save: function () {
			if (this.validation()) {
				var params = {};
				params.NAME = $("#txtPOSName").val();
				if (this.store_id != null)
					params.STORE_ID = this.store_id;

				var cmd = "";
				if (this.pos_id == null) {
					cmd = "InsertPOS";
				}
				else {
					cmd = "UpdatePOSInfo";
					params.ID = this.pos_id;
				}

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: cmd,
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							alert("저장되었습니다.");
							POSKNewForm.close();
							POSKNewForm.saveCallback();
						}
						else {
							alert("등록 오류!");
						}
					}
				});
			}
		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divNewPOS">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">단말 생성</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="txtPOSName">단말기 이름 : </label>
						<input type="text" class="form-control" placeholder="NAME" id="txtPOSName" />
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnPOSSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>