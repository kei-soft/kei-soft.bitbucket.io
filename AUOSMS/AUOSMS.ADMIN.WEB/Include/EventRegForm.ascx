﻿<%@ Control Language="C#" ClassName="EventRegForm" %>

<script>
	var GiftImage = new Object();
	var EventImage = new Object();

	$(document).ready(function () {
		$("#ulCategory").on("click", "a.select_category", function () {
			$("#spSelectedCategory").text($(this).text());
			$("#spSelectedCategory").data("category", $(this).data("category"));

			EventRegForm.displayMenu();
		});

		$("#dvStoreItemList").on("click", "button.btnSelectItem", function () {
			var $parent = $(this).parents("div.list-group-item");
			$("#spItemID").text($parent.find("span.spItemID").text());
			$("#imgItemImage").attr("src", $parent.find("img").attr("src"));
			$("#spItemName").text($parent.find("span.spItemName").text());

			$("#dvSelectedMenuArea").show();
			$("#dvSelectMenuArea").hide();
		});

		$("#selGiftType").change(function () {
			if ($(this).val() == "MENU") {
				$("#dvMenuArea").show();
				$("#dvGiftArea").hide();
			}
			else {
				$("#dvMenuArea").hide();
				$("#dvGiftArea").show();
			}
		});

		$("#selConditionType").change(function () {
			if ($(this).val() == "QUANTITY") {
				$("#dvQuantityArea").show();
				$("#dvPriceArea").hide();
			}
			else {
				$("#dvQuantityArea").hide();
				$("#dvPriceArea").show();
			}
		});

		$("#imgEventImage").click(function () {
			$("#selectEventImage").click();
		});

		$("#selectEventImage").on("change", function (e) {
			var file = $(this)[0].files[0];
			if (!file.type.match("image/.*")) {
				alert("이미지파일만 선택가능");
			}
			else {
				EventImage = file;
				var reader = new FileReader();
				reader.onload = function (e) {
					$("#imgEventImage").attr("src", e.target.result);
				}
				reader.readAsDataURL(EventImage);
			}
		});


		$("#btnSelectGiftImage").click(function () {
			$("#selectGiftImage").click();
		});

		$("#selectGiftImage").change(function () {
			var file = $(this)[0].files[0];
			if (!file.type.match("image/.*")) {
				alert("이미지파일만 선택가능");
			}
			else {
				GiftImage = file;
				var reader = new FileReader();
				reader.onload = function (e) {
					$("#imgGiftImage").attr("src", e.target.result);
				}
				reader.readAsDataURL(GiftImage);
			}
		});

		$("#btnEventSave").click(function () {
			EventRegForm.save();
		});
	});

	var EventRegForm = {
		saveCallback: null,
		event_id: null,
		store_id: null,
		open: function (event_id, store_id, saveCallback) {
			this.saveCallback = saveCallback;
			this.event_id = event_id;
			this.store_id = store_id;

			GiftImage = new Object();
			EventImage = new Object();

			EventRegForm.displayCategory();

			if (this.event_id == null) {
				$("#imgEventImage").attr("src", contextPath + "images/no_image.png");

				$("#divEventForm h4.modal-title").text("이벤트 등록");
				$("#txtEventName").val("");
				$("#txtStartDate").val("<%= DateTime.Today.ToString("yyyy-MM-dd") %>");
				$("#txtEndDate").val("");

				$("#selGiftType").val("MENU");

				$("#dvMenuArea").show();
				$("#spItemID").text("");
				$("#imgItemImage").attr("src", contextPath + "images/no_image.png");
				$("#spItemName").text("메뉴를 선택하세요.");

				$("#dvGiftArea").hide();
				$("#imgGiftImage").attr("src", contextPath + "images/no_image.png");
				$("#txtGiftName").text("");

				$("#selConditionType").val("QUANTITY");
				
				$("#dvQuantityArea").show();
				$("#txtQuantityValue").val("1");
				
				$("#dvPriceArea").hide();
				$("#txtPriceValue").val("10,000");
				
				$("#selUseYN").val("Y");

				$("#divEventForm").modal();
			}
			else {
				var params = {};
				params.ID = this.event_id;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "GetStoreEventInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data.INFO.length > 0) {
							$("#divEventForm h4.modal-title").text("이벤트 수정");
							
							$("#imgEventImage").attr("src", data.EVENTFILEPATH + data.INFO[0].EVENT_INFO_IMAGE);

							$("#txtEventName").val(data.INFO[0].EVENT_NAME);
							$("#txtStartDate").val(data.INFO[0].EVENT_START_DATE);
							$("#txtEndDate").val(data.INFO[0].EVENT_END_DATE);

							$("#selGiftType").val(data.INFO[0].EVENT_GIFT_TYPE);
							if (data.INFO[0].EVENT_GIFT_TYPE == "MENU") {
								$("#dvMenuArea").show();
								$("#spItemID").text("[" + data.INFO[0].EVENT_GIFT_CODE.lPad(5, "0") + "]");
								$("#imgItemImage").attr("src", data.ITEMFILEPATH + data.INFO[0].EVENT_IMAGE);
								$("#spItemName").text(data.INFO[0].EVENT_GIFT_NAME);

								$("#dvGiftArea").hide();
								$("#imgGiftImage").attr("src", contextPath + "images/no_image.png");
								$("#txtGiftName").text("");
							}
							else {
								$("#dvMenuArea").hide();
								$("#spItemID").text("");
								$("#imgItemImage").attr("src", contextPath + "images/no_image.png");
								$("#spItemName").text("메뉴를 선택하세요.");

								$("#dvGiftArea").show();
								$("#imgGiftImage").attr("src", data.EVENTFILEPATH + data.INFO[0].EVENT_IMAGE);
								$("#txtGiftName").val(data.INFO[0].EVENT_GIFT_NAME);
							}

							$("#selConditionType").val(data.INFO[0].EVENT_CONDITION_TYPE);
							if (data.INFO[0].EVENT_CONDITION_TYPE == "QUANTITY") {
								$("#dvQuantityArea").show();
								$("#txtQuantityValue").val(data.INFO[0].EVENT_CONDITION_VALUE);

								$("#dvPriceArea").hide();
								$("#txtPriceValue").val("10,000");
							}
							else {
								$("#dvQuantityArea").hide();
								$("#txtQuantityValue").val("1");

								$("#dvPriceArea").show();
								$("#txtPriceValue").val(String(data.INFO[0].EVENT_CONDITION_VALUE).currencyFormat());
							}

							$("#selUseYN").val(data.INFO[0].USE_YN);

							$("#divEventForm").modal();

						}
						else {
							alert("조회 오류");
						}
					}
				});
			}
		},
		close: function () {
			$("#divEventForm").modal("hide");
		},
		displayCategory: function () {
			//var params = {};
			//params.STORE_ID = this.store_id;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				//Params: params,
				//Command: "GetOrderItemGroupList",
				Command: "GetItemGroupList",
				SuccessCallBack: function (data) {
					if (data.length > 0) {
						$("#spSelectedCategory").text(data[0].ITEM_GROUP_NAME);
						$("#spSelectedCategory").data("category", data[0].ITEM_GROUP_CODE);

						var strHtml = "";
						for (var i = 0; i < data.length; i++) {
							strHtml += "<li><a class=\"select_category\" style=\"cursor: pointer; \" data-category=\"" + data[i].ITEM_GROUP_CODE + "\">" + data[i].ITEM_GROUP_NAME + "</a></li>";
						}
						$("#ulCategory").html(strHtml);
						EventRegForm.displayMenu();
					}
				}
			});
		},
		displayMenu: function () {
			var params = {};
			//params.STORE_ID = this.store_id;
			params.PAGE_SIZE = 10000;
			params.PAGE = 0;
			params.GROUP_CODE = $("#spSelectedCategory").data("category");
			//aleret(JSON.stringify(params)); return;
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				//Command: "GetOrderMenuList",
				Command: "GetItemList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<div class=\"list-group-item\" data-item=\"" + data.LIST[i].ITEM_ID + "\">";
							strHtml += "	<img src=\"" + data.FILEPATH + data.LIST[i].ITEM_IMAGE + "\" onerror=\"this.src='" + contextPath + "images/no_image.png';\" style=\"width:2.28em; height:2.28em;\" />";
							strHtml += "	<span class=\"spItemID\">[" + String(data.LIST[i].ITEM_ID).lPad(5, "0") + "]</span> <span class=\"spItemName\">" + data.LIST[i].ITEM_NAME + "</span>";
							strHtml += "	<div class=\"pull-right\">";
							strHtml += "		<span class=\"price\">" + String(data.LIST[i].ITEM_PRICE).currencyFormat() + "</span>원";
							strHtml += "		<button type=\"button\" class=\"btn btn-success btn-xs btnSelectItem\">선택</button>";
							strHtml += "	</div>";
							strHtml += "</div>";
						}
					}
					else {
						strHtml += "<div class=\"list-group-item text-center\">등록된 메뉴가 없습니다.</div>";
					}

					$("#dvStoreItemList").html(strHtml);
				}
			});
		},
		validation: function () {
			var $e;

			$e = $("#txtEventName");
			if ($e.val() == "") {
				alert("이벤트명을 입력하세요.");
				$e.focus();
				return false;
			}

			if ($("#selGiftType").val() == "MENU") {
				$e = $("#spItemID");
				if ($e.text() == "") {
					alert("메뉴를 선택하세요.");
					return false;
				}
			}
			else {
				if (this.event_id == null && GiftImage.name == undefined) {
					alert("사은품 이미지를 선택하세요.");
					return false;
				}
				
				$e = $("#txtGiftName");
				if ($e.val() == "") {
					alert("사은품명을 입력하세요.");
					$e.focus();
					return false;
				}
			}
			
			if ($("#selConditionType").val() == "QUANTITY") {
				$e = $("#txtQuantityValue");
				if ($e.val() == "") {
					alert("제공기준을 입력하세요.");
					$e.focus();
					return false;
				}
			}
			else {
				$e = $("#txtPriceValue");
				if ($e.val() == "") {
					alert("제공기준을 입력하세요.");
					$e.focus();
					return false;
				}
			}

			if (!confirm("이벤트 내용을 저장하시겠습니까?")) {
				return false;
			}

			return true;
		},
		save: function () {
			if (this.validation()) {
				var cmd = "";
				var params = {};
				if (this.event_id == null) {
					cmd = "InsertStoreEvent";
					params.STORE_ID = this.store_id;
				}
				else {
					cmd = "UpdateStoreEventInfo";
					params.ID = this.event_id;
				}

				params.EVENT_NAME = $("#txtEventName").val();
				params.START_DATE = $("#txtStartDate").val();
				params.END_DATE = $("#txtEndDate").val() == "" ? null : $("#txtEndDate").val();

				params.GIFT_TYPE = $("#selGiftType").val();
				if ($("#selGiftType").val() == "MENU") {
					params.GIFT_CODE = String(parseInt($("#spItemID").text().replace("[", "").replace("]", "")));
					params.GIFT_NAME = $("#spItemName").text();
				}
				else {
					params.GIFT_NAME = $("#txtGiftName").val();
				}

				params.CONDITION_TYPE = $("#selConditionType").val();
				if ($("#selConditionType").val() == "QUANTITY") {
					params.CONDITION_VALUE = $("#txtQuantityValue").val();
				}
				else {
					params.CONDITION_VALUE = removeCommas($("#txtPriceValue").val());
				}

				params.USE_YN = $("#selUseYN").val();

				EventRegForm.ajax_file({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: cmd,
					Params: params,
					Files1: EventImage,
					Files2: GiftImage,
					SuccessCallBack: function (data) {
						if (data) {
							alert("저장되었습니다.");
							EventRegForm.close();
							EventRegForm.saveCallback();
						}
						else {
							alert("등록 오류!");
						}
					}
				});
			}
		},
		ajax_file: function (opt) {
			var formdata = new FormData();
			formdata.append("Command", opt.Command);
			formdata.append("Params", JSON.stringify(opt.Params));
			formdata.append("Files1", opt.Files1);
			formdata.append("Files2", opt.Files2);

			$.ajax({
				url: opt.URL,
				dataType: "json",
				type: "POST",
				processData: false,
				contentType: false,
				data: formdata,
				beforeSend: function () {
					do_cmd.loading_spinner(true);
				},
				success: function (result) {
					if (result.STATUS == "OK") {
						if (opt.SuccessCallBack) opt.SuccessCallBack(result.RESULT);
					}
					else {
						alert(result.MSG);
					}
				},
				error: function (err) {
					console.log(err);
					alert(err);
				},
				complete: function () {
					do_cmd.loading_spinner(false);
				}
			});
		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divEventForm">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body" style="min-height:500px;">
				<form>
					<div class="form-group text-center">
						<img  src="X" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="width:50%; cursor: pointer;" alt="클릭하여 이미지 선택" id="imgEventImage" />
						<input type="file" id="selectEventImage" name="selectEventImage" style="display: none;" />
						<p class="text-primary">이미지를 클릭하여 사진을 선택하세요.</p>
					</div>
					<div class="form-group">
						<label for="txtEventName">이벤트명 : </label>
						<input type="text" class="form-control" placeholder="이벤트명" id="txtEventName" />
					</div>
					<div class="form-group">
						<label for="txtStartDate">기간 : </label>
						<input class="form-control datepicker readonly" name="txtStartDate" id="txtStartDate" type="text" style="width: 100px;" />
						~
						<input class="form-control datepicker readonly" name="txtEndDate" id="txtEndDate" type="text" style="width: 100px;" />
					</div>
					<div class="form-group">
						<label for="selGiftType">사은품 구분 : </label>
						<select class="form-control common_code" data-group="EVENT_GIFT_TYPE" id="selGiftType"></select>
					</div>
					<div class="form-group" id="dvMenuArea">
						<label for="selGiftType">메뉴선택 : </label>
						
						<div class="list-group-item" style="margin-top: 5px;">
							<img src="X" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="width:2.28em; height:2.28em;" id="imgItemImage" />
							<span id="spItemID"></span> <span id="spItemName">메뉴를 선택하세요.</span>
						</div>

						<div>

							<div class="panel panel-default">
								<div class="panel-heading">
									<i class="fa fa-file-text fa-fw"></i> 메뉴선택
									<div class="pull-right">
										<div class="btn-group">
											<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
												<span id="spSelectedCategory" data-category="">Actions</span>
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu pull-right" role="menu" id="ulCategory"></ul>
										</div>
									</div>
								</div>
								<!-- /.panel-heading -->
								<div class="panel-body">
            
									<div class="list-group" id="dvStoreItemList"></div>
									<!-- /.list-group -->

								</div>
								<!-- /.panel-body -->
							</div>

						</div>
					</div>
					<div class="form-group" id="dvGiftArea">
						<label for="selGiftType">사은품선택 : </label>
						<button type="button" class="btn btn-primary btn-xs" id="btnSelectGiftImage">사은품 이미지 선택</button>
						
						<div class="list-group-item" style="margin-top: 5px;">
							<img src="X" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="width:2.28em; height:2.28em;" id="imgGiftImage" class="pull-left" />
							<input type="file" id="selectGiftImage" name="selectGiftImage" style="display: none;" />
							<input type="text" class="form-control" style="width: 200px; margin-left: 50px;" placeholder="사은품명" id="txtGiftName" />
						</div>
					</div>
					<div class="form-group">
						<label for="selGiftType">제공방법 : </label>
						<select class="form-control common_code" data-group="EVENT_CONDITION_TYPE" id="selConditionType"></select>
					</div>
					<div class="form-group">
						<label for="selGiftType">제공기준 : </label>
						<div id="dvQuantityArea">
							음료 <input type="text" class="spinner" style="width: 50px;" id="txtQuantityValue" /> 잔당
						</div>
						<div id="dvPriceArea">
							<input type="text" class="form-control numberOnly pull-left" style="width: 150px;" id="txtPriceValue" /> <span style="display: block; padding: 6px;"> 원 이상</span>
						</div>
					</div>
					<div class="form-group">
						<label for="selUseYN">사용유무 : </label>
						<select class="form-control" id="selUseYN">
							<option value="Y">Y</option>
							<option value="N">N</option>
						</select>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnEventSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>