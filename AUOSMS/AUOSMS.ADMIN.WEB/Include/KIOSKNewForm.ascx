﻿<%@ Control Language="C#" ClassName="KIOSKNewForm" %>

<script>
	$(document).ready(function () {
		$("#btnKIOSKSave").click(function () {
			KIOSKNewForm.save();
		});
	});

	var KIOSKNewForm = {
		saveCallback: null,
		store_id: null,
		kiosk_id: null,
		open: function (store_id, kiosk_id, saveCallback) {
			this.saveCallback = saveCallback;
			this.store_id = store_id;
			this.kiosk_id = kiosk_id;

			if (kiosk_id == null) {
				$("#txtKIOSKName").val("");

				$("#divNewKIOSK").modal();
				$("#txtKIOSKName").focus();

			}
			else {
				var params = {};
				params.ID = this.kiosk_id;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "GetKIOSKInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data.length > 0) {
							$("#txtKIOSKName").val(data[0].KIOSK_NAME);

							$("#divNewKIOSK").modal();
							$("#txtKIOSKName").focus();
						}
						else {
							alert("잘못된 매장 코드입니다.");
						}
					}
				});
			}
		},
		close: function () {
			$("#divNewKIOSK").modal("hide");
		},
		validation: function () {
			var $e;

			$e = $("#txtKIOSKName");
			if ($e.val() == "") {
				alert("KIOSK 이름을 입력하세요.");
				$e.focus();
				return false;
			}

			if (!confirm("KIOSK를 저장하시겠습니까?")) {
				return false;
			}

			return true;

		},
		save: function () {
			if (this.validation()) {
				var params = {};
				params.NAME = $("#txtKIOSKName").val();
				if (this.store_id != null)
					params.STORE_ID = this.store_id;

				var cmd = "";
				if (this.kiosk_id == null) {
					cmd = "InsertKIOSK";
				}
				else {
					cmd = "UpdateKIOSKInfo";
					params.ID = this.kiosk_id;
				}

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: cmd,
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							alert("저장되었습니다.");
							KIOSKNewForm.close();
							KIOSKNewForm.saveCallback();
						}
						else {
							alert("등록 오류!");
						}
					}
				});
			}
		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divNewKIOSK">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">KIOSK 생성</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="txtKIOSKName">KIOSK 이름 : </label>
						<input type="text" class="form-control" placeholder="NAME" id="txtKIOSKName" />
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnKIOSKSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>