﻿<%@ Control Language="C#" ClassName="IntroRegForm" %>

<script>
	var IntroImageFile = new Object();

	$(document).ready(function () {
		$("#btnSave").click(function () {
			IntroRegForm.save();
		});

		$("#imgIntroImage").click(function () {
			$("#selectIntroImage").click();
		});

		$("#selectIntroImage").on("change", function (e) {
			var file = $(this)[0].files[0];
			if (!file.type.match("image/.*")) {
				alert("이미지파일만 선택가능");
			}
			else {
				IntroImageFile = file;
				var reader = new FileReader();
				reader.onload = function (e) {
					$("#imgIntroImage").attr("src", e.target.result);
				}
				reader.readAsDataURL(IntroImageFile);
			}
		});

	});

	var IntroRegForm = {
		saveCallback: null,
		intro_id: null,
		store_id: null,
		open: function (intro_id, store_id, saveCallback) {
			this.saveCallback = saveCallback;
			this.intro_id = intro_id;
			this.store_id = store_id;

			IntroImageFile = new Object();

			if (this.intro_id == null) {
				$("#imgIntroImage").attr("src", contextPath + "images/no_image.png");
				$("#selUseYN").val("Y");
				$("#divIntroImage h4.modal-title").text("IMAGE 추가");
			}
			else {
				var params = {};
				params.ID = this.intro_id;

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "GetIntroImageInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data.INFO.length > 0) {
							$("#imgIntroImage").attr("src", data.FILEPATH + data.INFO[0].IMAGE_NAME);
							$("#selUseYN").val(data.INFO[0].USE_YN);
						}
						else {
							alert("조회 오류");
						}
					}
				});
			}

			$("#divIntroImage").modal();
		},
		close: function () {
			$("#divIntroImage").modal("hide");
		},
		save: function () {
			var cmd = "";
			var params = {};
			if (this.intro_id == null) {
				cmd = "InsertIntroImage";
				params.STORE_ID = this.store_id;
			}
			else {
				cmd = "UpdateIntroImageInfo";
				params.ID = this.intro_id;
			}
			params.USE_YN = $("#selUseYN").val();

			do_cmd.ajax_file({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: cmd,
				Params: params,
				Files: IntroImageFile,
				SuccessCallBack: function (data) {
					if (data) {
						IntroRegForm.close()
						IntroRegForm.saveCallback();
					}
					else {
						alert("오류!");
					}
				}
			});

		}
	};
</script>

<!-- Modal -->
<div class="modal fade" role="dialog" id="divIntroImage">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body" style="min-height:500px;">
				<form>
					<div class="form-group text-center">
						<img src="<%=Page.ResolveUrl("~") %>images/no_image.png" style="width:100%; cursor: pointer;" alt="클릭하여 이미지 선택" id="imgIntroImage" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" />
						<input type="file" id="selectIntroImage" name="selectIntroImage" style="display: none;" />
						<p class="text-primary">이미지를 클릭하여 사진을 선택하세요.</p>
					</div>
					<div class="form-group">
						<label for="selUseYN">사용유무 : </label>
						<select class="form-control" id="selUseYN">
							<option value="Y">Y</option>
							<option value="N">N</option>
						</select>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnSave">저장</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
			</div>
		</div>

	</div>
</div>