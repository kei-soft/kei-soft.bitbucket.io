﻿<%@ Control Language="C#" ClassName="PostCode" %>

<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script>
	$(document).ready(function () {
		$("#btnSearchAddr").click(function () {
			new daum.Postcode({
				oncomplete: function (data) {
					$("#txtPostCode").val(data.zonecode);
					$("#txtState").val(data.sido);
					$("#txtCity").val(data.sigungu);
					$("#txtAddress").val(data.roadAddress);
					$("#txtAddressDetail").focus();
				}
			}).open();
		});
	});
</script>