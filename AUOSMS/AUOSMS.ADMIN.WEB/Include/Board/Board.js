﻿var SearchInfo = {};

$(document).ready(function () {
	SetInit();
	SetEvents();
});

function SetInit() {
	// 매장검색 표시여부
	if (BoardInfo.Store_Search_YN == "Y") {
		$("#selSearchStore").show();
		getStoreListBind();
	}
	else {
		$("#selSearchStore").hide();
	}

	// 글쓰기 가능여부
	if (BoardInfo.WRITABLE == "Y") {
		$("#btnNewBoard").show();
	}
	else {
		$("#btnNewBoard").hide();
	}

	// 초기 검색조건값
	SearchInfo.PAGE_SIZE = BoardInfo.Page_Size;
	SearchInfo.PAGE = 1;
	SearchInfo.BOARD_TYPE = BoardInfo.BoardType;
	SearchInfo.STORE_ID = "0";
	SearchInfo.CONDITION = "NAME";
	SearchInfo.KEYWORD = "";

	// 목록 헤더
	var strHtml = "<tr>";
	strHtml += "<th class=\"text-center\" style=\"width: 80px;\">No</th>";
	strHtml += "<th class=\"text-center\">제목</th>";
	strHtml += "<th class=\"text-center\" style=\"width: 200px;\">작성자</th>";
	if (BoardInfo.BoardType == "CHANNEL") {
		strHtml += "<th class=\"text-center\" style=\"width: 120px;\">매장</th>";
		strHtml += "<th class=\"text-center\" style=\"width: 100px;\">답변여부</th>";
	}
	strHtml += "<th class=\"text-center\" style=\"width: 150px;\">작성일시</th>";
	strHtml += "</tr>";
	$("#tblBoardList > thead").html(strHtml);

	// 글쓰기 버튼
	if (BoardInfo.WRITABLE == "Y")
		$("#btnNewBoard").show();
	else
		$("#btnNewBoard").hide();

	viewBoardList();
	//viewBoardNewForm();
	//viewBoardDetail();
}

function SetEvents() {
	$("#btnSearch").click(function () {
		SearchInfo.STORE_ID = BoardInfo.LIST_TYPE == "SELF" ? "SELF" : (BoardInfo.Store_Search_YN == "Y" ? $("#selSearchStore").val() : "0");
		SearchInfo.CONDITION = $("#selSearchCondition").val();
		SearchInfo.KEYWORD = $("#txtSearchKeyword").val();
		SearchInfo.PAGE = 1;
		getBoardList();
	});

	$("#tblBoardList > tbody").on("click", "a[data-board]", function () {
		BoardInfo.BOARD_ID = $(this).data("board");
		viewBoardDetail();
	});

	$("ul.pagination").on("click", "a[data-page]", function () {
		SearchInfo.PAGE = parseInt($(this).data("page"));
		getBoardList();
	});

	$("#btnNewBoard").click(function () {
		if (BoardInfo.WRITABLE == "Y") {
			viewBoardNewForm();
		}
	});

	$("#btnSave").click(function () {
		if (validation()) {
			if (BoardInfo.BOARD_ID == null) {
				FileUploadController.upload(function () {
					var params = {};
					params.BOARD_TYPE = BoardInfo.BoardType;
					params.TITLE = $("#txtTitle").val();
					params.NOTICE_YN = $("#selNoticeYN").val();
					params.ATTACH_FOLDER = BoardInfo.BOARD_FOLDER;

					board_reg({
						Command: "InsertBoard",
						Params: params,
						Contents: CKEDITOR.instances.txtContents.getData(),
						//Contents: "",
						async: true,
						SuccessCallBack: function (data) {
							if (data) {
								SearchInfo.PAGE = 1;
								SearchInfo.KEYWORD_NAME = "";
								SearchInfo.KEYWORD_TITLE = "";
								SearchInfo.STORE_ID = "0";
								viewBoardList();
							}
						}
					});
				});
			}
			else {
				FileUploadController.deleteFiles(function () {
					FileUploadController.upload(function () {
						var params = {};
						params.BOARD_ID = BoardInfo.BOARD_ID;
						params.TITLE = $("#txtTitle").val();
						params.NOTICE_YN = $("#selNoticeYN").val();

						board_reg({
							Command: "UpdateBoard",
							Params: params,
							Contents: CKEDITOR.instances.txtContents.getData(),
							async: true,
							SuccessCallBack: function (data) {
								if (data) {
									viewBoardDetail();
								}
							}
						});
					});
				});
			}
		}
	});

	$("#btnCancel").click(function () {
		if (BoardInfo.BOARD_ID == null) {
			viewBoardList();
		}
		else {
			viewBoardDetail();
		}
	});

	$("#btnDetailToEdit").click(function () {
		viewBoardEditForm();
	});

	$("#btnDetailToDelete").click(function () {
		if (confirm("게시글을 삭제하시겠습니까?")) {
			var params = {};
			params.BOARD_ID = BoardInfo.BOARD_ID;

			do_cmd.ajax({
				URL: BoardInfo.AjaxUrl,
				async: true,
				Params: params,
				Command: "DeleteBoard",
				SuccessCallBack: function (data) {

					viewBoardList();

				}
			});
		}
	});

	$("#btnDetailToList").click(function () {
		viewBoardList();
	});

	$("#btnReplySave").click(function () {
		if (CKEDITOR.instances.txtReplyContents.getData() == "") {
			alert("댓글 내용을 입력하세요.");
			CKEDITOR.instances.txtReplyContents.focus();
			return false;
		}

		var command = "";
		var params = {};
		params.BOARD_ID = BoardInfo.BOARD_ID;
		if (BoardInfo.BOARD_REPLY_NUM == null) {
			command = "InsertBoardReply";
			params.ATTACH_FOLDER = BoardInfo.BOARD_REPLY_FOLDER;
		}
		else {
			command = "UpdateBoardReply";
			params.REPLY_NUM = BoardInfo.BOARD_REPLY_NUM;
		}

		board_reg({
			Command: command,
			Params: params,
			Contents: CKEDITOR.instances.txtReplyContents.getData(),
			async: true,
			SuccessCallBack: function (data) {
				getBoardReplyInfo();
			}
		});
	});
	$("#btnReplyCancel").click(function () {
		getBoardReplyInfo();
	});

	$("#divReplyBody").on("click", "a.reply_edit", function () {
		BoardInfo.BOARD_REPLY_NUM = $(this).data("reply");

		var params = {};
		params.BOARD_ID = BoardInfo.BOARD_ID;
		params.REPLY_NUM = BoardInfo.BOARD_REPLY_NUM;

		do_cmd.ajax({
			URL: BoardInfo.AjaxUrl,
			async: true,
			Params: params,
			Command: "GetBoardReplyGet",
			SuccessCallBack: function (data) {

				if (data.length > 0) {
					CKEDITOR.config.width = "100%";
					CKEDITOR.config.height = 100;
					CKEDITOR.replace('txtReplyContents', {
						filebrowserUploadUrl: BoardInfo.ImgUploadUrl + "?folder=" + data[0].ATTACH_FOLDER
					});

					CKEDITOR.instances.txtReplyContents.setData(data[0].CONTENTS);

					$("#btnReplyCancel").show();
					$("#divReplyForm").show();
					$("#divReplyArea").hide();
				}

			}
		});
	});
	$("#divReplyBody").on("click", "a.reply_delete", function () {
		if (confirm("댓글을 삭제하시겠습니까?")) {
			var params = {};
			params.BOARD_ID = BoardInfo.BOARD_ID;
			params.REPLY_NUM = $(this).data("reply");

			do_cmd.ajax({
				URL: BoardInfo.AjaxUrl,
				async: true,
				Params: params,
				Command: "DeleteBoardReply",
				SuccessCallBack: function (data) {

					getBoardReplyInfo();

				}
			});

		}
	});
}

function viewBoardList() {
	BoardInfo.BOARD_ID = null;

	$("#viewDetail").hide();
	$("#viewForm").hide();
	$("#viewList").show();
	getBoardList();
}

function getBoardList() {
	if (BoardInfo.Store_Search_YN == "Y") $("#selSearchStore").val(SearchInfo.STORE_ID);
	$("#selSearchCondition").val(SearchInfo.CONDITION);
	$("#txtSearchKeyword").val(SearchInfo.KEYWORD);

	//alert(JSON.stringify(SearchInfo)); return;

	do_cmd.ajax({
		URL: BoardInfo.AjaxUrl,
		async: true,
		Params: SearchInfo,
		Command: "GetBoardList",
		SuccessCallBack: function (data) {
			var strHtml = "";
			if (data.LIST.length > 0) {
				for (var i = 0; i < data.LIST.length; i++) {
					//strHtml += "<tr" + (data.LIST[i].NOTICE_YN == "Y" ? " style=\"font-weight: bold;\"" : "") + ">";
					strHtml += "<tr>";
					strHtml += "<td class=\"text-center\">" + (data.LIST[i].NOTICE_YN == "Y" ? "공지" : data.LIST[i].ROWNO) + "</td>";
					strHtml += "<td class=\"text-left\"><a data-board=\"" + data.LIST[i].BOARD_ID + "\" style=\"cursor:pointer;\">" + data.LIST[i].TITLE + "</a></td>";
					strHtml += "<td class=\"text-center\">" + data.LIST[i].MANAGER_NAME + "</td>";
					if (BoardInfo.BoardType == "CHANNEL") {
						strHtml += "<td class=\"text-center\">" + (data.LIST[i].NOTICE_YN == "Y" ? "본사" : data.LIST[i].STORE_NAME) + "</td>";
						strHtml += "<td class=\"text-center\">" + (data.LIST[i].NOTICE_YN == "Y" ? "-" : (data.LIST[i].REPLY_CNT > 0 ? "답변완료" : "미답변")) + "</td>";
					}
					strHtml += "<td class=\"text-center\">" + data.LIST[i].CREATE_DATE + "</th>";
					strHtml += "</tr>";
				}
			}
			else {
				strHtml += "<tr>";
				strHtml += "<td colspan=\"" + (BoardInfo.BoardType == "CHANNEL" ? "6" : "4") + "\" class=\"text-center\">글목록이 없습니다.</td>";
				strHtml += "</tr>";
			}

			$("ul.pagination").html(displayPageNavi(SearchInfo.PAGE, SearchInfo.PAGE_SIZE, 5, parseInt(data.TOTAL_CNT)));

			$("#tblBoardList > tbody").html(strHtml);
		}
	});
}


function viewBoardNewForm() {
	if (BoardInfo.WRITABLE != "Y") {
		viewBoardList();
	}

	$("#viewDetail").hide();
	$("#viewList").hide();

	FileUploadController.fileList = new Object();
	FileUploadController.editFileList = [];
	FileUploadController.delFileList = [];
	FileUploadController.displayFileList();

	BoardInfo.BOARD_ID = null;

	$("#divFormTitle").text("새글 작성");
	$("#txtTitle").val("");
	$("#selNoticeYN").val("N");

	getNewGUID(function (data) {
		if (BoardInfo.NOTICE_YN == "Y") {
			$("#divNoticeGroup").show();
			$("#selNoticeYN").val("Y");
		}
		else
			$("#divNoticeGroup").hide();

		BoardInfo.BOARD_FOLDER = data;

		CKEDITOR.config.width = "100%";
		CKEDITOR.config.height = 500;
		CKEDITOR.replace('txtContents', {
			filebrowserUploadUrl: BoardInfo.ImgUploadUrl + "?folder=" + BoardInfo.BOARD_FOLDER
		});
		CKEDITOR.instances.txtContents.setData();

		$("#viewForm").show();
		$("#txtTitle").focus();
	});
}

function viewBoardEditForm() {
	$("#viewDetail").hide();
	$("#viewList").hide();

	var params = {};
	params.BOARD_ID = BoardInfo.BOARD_ID;

	do_cmd.ajax({
		URL: BoardInfo.AjaxUrl,
		async: true,
		Params: params,
		Command: "GetBoardGet",
		SuccessCallBack: function (data) {
			if (data.LIST.length > 0) {
				BoardInfo.BOARD_FOLDER = data.LIST[0].ATTACH_FOLDER;

				$("#divFormTitle").text("글 수정");
				$("#txtTitle").val(data.LIST[0].TITLE);

				$("#selNoticeYN").val(data.LIST[0].NOTICE_YN);

				if (BoardInfo.NOTICE_YN == "Y")
					$("#divNoticeGroup").show();
				else
					$("#divNoticeGroup").hide();

				CKEDITOR.config.width = "100%";
				CKEDITOR.config.height = 500;
				CKEDITOR.replace('txtContents', {
					filebrowserUploadUrl: BoardInfo.ImgUploadUrl + "?folder=" + BoardInfo.BOARD_FOLDER
				});
				CKEDITOR.instances.txtContents.setData(data.LIST[0].CONTENTS);

				params.BOARD_FOLDER = BoardInfo.BOARD_FOLDER;

				do_cmd.ajax({
					URL: BoardInfo.AjaxUrl,
					async: true,
					Params: params,
					Command: "GetFileList",
					SuccessCallBack: function (files) {

						FileUploadController.fileList = new Object();
						FileUploadController.editFileList = files;
						FileUploadController.delFileList = [];						

						FileUploadController.displayFileList();

						$("#viewForm").show();
						
					}
				});
			}
			else {
				alert("잘못된 접근입니다.");
				viewBoardList();
			}

		}
	});
}

function validation() {
	if ($("#txtTitle").val() == "") {
		alert("제목을 입력하세요.");
		$("#txtTitle").focus();
		return false;
	}
	
	if (CKEDITOR.instances.txtContents.getData() == "") {
		alert("내용을 입력하세요.");
		CKEDITOR.instances.txtContents.focus();
		return false;
	}

	return true;
}


function viewBoardDetail() {
	$("#viewDetail").show();
	$("#viewForm").hide();
	$("#viewList").hide();

	getBoardDetail();
}

function getBoardDetail() {
	var params = {};
	params.BOARD_ID = BoardInfo.BOARD_ID;

	do_cmd.ajax({
		URL: BoardInfo.AjaxUrl,
		async: true,
		Params: params,
		Command: "GetBoardGet",
		SuccessCallBack: function (data) {
			if (data.LIST.length > 0) {
				$("#vwDetailTitle").text(data.LIST[0].TITLE);
				$("#vwDetailManager").text(data.LIST[0].MANAGER_NAME);
				$("#vwDetailDate").text(data.LIST[0].CREATE_DATE);
				$("#vwDetailContents").html(data.LIST[0].CONTENTS);
				BoardInfo.BOARD_FOLDER = data.LIST[0].ATTACH_FOLDER;

				FileDownController.displayFileList();

				if (data.EDIT) {
					$("#btnDetailToEdit").show();
					$("#btnDetailToDelete").show();
				}
				else {
					$("#btnDetailToEdit").hide();
					$("#btnDetailToDelete").hide();
				}

				getBoardReplyInfo();
			}
			else {
				alert("잘못된 접근입니다.");
				viewBoardList();
			}
			
		}
	});
}

function getBoardReplyInfo() {
	BoardInfo.BOARD_REPLY_NUM = null;
	if (BoardInfo.REPLY_CNT > 0) {
		var params = {};
		params.BOARD_ID = BoardInfo.BOARD_ID;

		do_cmd.ajax({
			URL: BoardInfo.AjaxUrl,
			async: true,
			Params: params,
			Command: "GetBoardReplyList",
			SuccessCallBack: function (data) {
				var strHtml = "";
				if (data.length > 0) {
					for (var i = 0; i < data.length; i++) {
						strHtml += "<li class=\"clearfix\">";
						strHtml += "	<div class=\"chat-body clearfix\">";
						strHtml += "		<div class=\"header\">";
						strHtml += "			<strong class=\"primary-font\">" + data[i].MANAGER_NAME + "</strong>";
						if (data[i].EDIT == "Y") {
							strHtml += "			<a style=\"cursor:pointer;\" class=\"reply_edit\" data-reply=\"" + data[i].REPLY_NUM + "\"><span class=\"fa fa-pencil fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"reply_delete\" data-reply=\"" + data[i].REPLY_NUM + "\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
						}
						strHtml += "			<small class=\"pull-right text-muted\">";
						strHtml += "				<i class=\"fa fa-clock-o fa-fw\"></i> " + data[i].CREATE_DATE;
						strHtml += "			</small>";
						strHtml += "		</div>";
						strHtml += "		<div style=\"margin: 10px 0px;\">";
						strHtml += data[i].CONTENTS;
						strHtml += "		</div>";
						strHtml += "	</div>";
						strHtml += "</li>";
					}
				}
				else {
					strHtml += "<li class=\"clearfix\">";
					strHtml += "	<div class=\"chat-body clearfix\">";
					strHtml += "		<div class=\"text-center\">";
					strHtml += "			댓글이 없습니다...";
					strHtml += "		</div>";
					strHtml += "	</div>";
					strHtml += "</li>";
				}

				$("#divReplyBody > ul.chat").html(strHtml);
				$("#divReplyArea").show();

				// 댓글입력창 보여주기
				if (BoardInfo.REPLY_CNT > data.length && BoardInfo.REPLY_WRITABLE == "Y") {
					getNewGUID(function (data) {
						BoardInfo.BOARD_REPLY_FOLDER = data;

						CKEDITOR.config.width = "100%";
						CKEDITOR.config.height = 100;
						CKEDITOR.replace('txtReplyContents', {
							filebrowserUploadUrl: BoardInfo.ImgUploadUrl + "?folder=" + BoardInfo.BOARD_REPLY_FOLDER
						});

						CKEDITOR.instances.txtReplyContents.setData();
						$("#btnReplyCancel").hide();
						$("#divReplyForm").show();
					});
				}
				else {
					$("#divReplyForm").hide();
				}
			}
		});
	}
	else {
		$("#divReplyArea").hide();
		$("#divReplyForm").hide();
	}
}


function getNewGUID(callback) {
	do_cmd.ajax({
		URL: BoardInfo.AjaxUrl,
		async: true,
		Command: "GetNewGUID",
		SuccessCallBack: function (data) {
			if (callback) callback(data);
		}
	});
}

function getStoreListBind() {
	do_cmd.ajax({
		URL: BoardInfo.AjaxUrl,
		async: true,
		Command: "GetStoreList",
		SuccessCallBack: function (data) {
			var strHtml = "<option value=\"0\">매장선택</option>";
			if (data.LIST.length > 0) {
				for (var i = 0; i < data.LIST.length; i++) {
					strHtml += "<option value=\"" + data.LIST[i].STORE_ID + "\">" + data.LIST[i].STORE_NAME + "</option>";
				}
			}

			$("#selSearchStore").html(strHtml);
		}
	});
}

function viewNonDisplay() {
	$("#div.boardViewArea").each(function () {
		$(this).hide();
	});
}

function displayPageNavi(pageIndex, pageSize, naviCount, dataSize) {
	pageIndex = pageIndex - 1;
	var temp = Math.floor(pageIndex / naviCount);  // 현재페이지에 보여줄 페이지수를 나눈값을 저장
	var cnt = (temp * naviCount); // 나눈값에 다시 보여줄 페이지수를 곱하여 나눈 몫을 구함
	var Paging = "";  // 페이징부분에 보여줄 문자열 변수

	if (dataSize % pageSize != 0) {
		dataSize = Math.floor(dataSize / pageSize);
		dataSize = dataSize + 1;
	}
	else {
		dataSize = dataSize / pageSize;
	}

	Paging = "";
	// 현재페이지가 한번에 보여줄 페이지수보다 큰 경우에 앞페이지로 넘어가는 Prev버튼을 보여줌
	if (temp > 0) {
		Paging += "<li><a data-page=\"" + (pageIndex - naviCount + 1) + "\"><i class=\"fa fa-chevron-left fa-fw\"></i></a></li>";
	}
	else {
		Paging += "<li><a><i class=\"fa fa-chevron-left fa-fw\"></i></a></li>";
	}

	// 페이지를 표시갯수만큼 보여줌
	var i;
	for (i = cnt + 1; i <= cnt + naviCount; i++) {
		if (i <= dataSize) {
			Paging += "<li" + (i == pageIndex + 1 ? " class=\"active\"" : "") + "><a data-page=\"" + i + "\">" + i + "</a></li>";
		}
	}

	if (dataSize >= i) {
		Paging += "<li><a data-page=\"" + dataSize + "\"><i class=\"fa fa-chevron-right fa-fw\"></i></a></li>";
	}
	else {
		Paging += "<li><a><i class=\"fa fa-chevron-right fa-fw\"></i></a></li>";
	}

	return Paging;
}

var board_reg = function (opt) {
	var formdata = new FormData();
	formdata.append("Command", opt.Command);
	formdata.append("Params", JSON.stringify(opt.Params));
	formdata.append("Contents", opt.Contents);

	$.ajax({
		url: BoardInfo.AjaxUrl,
		method: "POST",
		async: opt.async,
		data: formdata,
		processData: false,
		contentType: false,
		dataType: "json",
		beforeSend: function () {
			do_cmd.loading_spinner(true);
		},
		success: function (result) {
			if (result.STATUS == "OK") {
				if (opt.SuccessCallBack) opt.SuccessCallBack(result.RESULT);
			}
			else {
				alert(result.MSG);
			}
		},
		error: function (err) {
			console.log(err);
			alert(err);
		},
		complete: function () {
			do_cmd.loading_spinner(false);
		}
	});
};