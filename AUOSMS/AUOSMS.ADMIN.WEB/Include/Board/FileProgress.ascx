﻿<%@ Control Language="C#" ClassName="FileProgress" %>

<style type="text/css">
	#fileProgressPage {
		height: 100%;
		left: 0px;
		position: fixed;
		_position:absolute; 
		top: 0px;
		width: 100%;
		display: none;
		z-index: 4001;
	}
	
	#fileProgressPage .fileProgressArea {
		position:absolute; 
		top:50%;
		left:50%;
		margin-left: -200px;
		margin-top: -75px;
		z-index: 2000;
		background-color: #fff;
		border: 1px solid gray;
		width: 400px;
		height: 150px;
		border-radius: 5px;
	}
	
	#fileProgressPage .fileProgressArea .fileTitle {
		display: block;
		width: 100%;
		height: 30px;
		text-align: center;
		line-height: 30px;
		background-color: rgb(76, 99, 197);
		color: #fff; font-weight: bold;
	}
	
	#fileProgressPage .fileProgressArea .fileProgressBar {
		padding: 5px;
	}
	
	#fileProgressPage .fileProgressArea span {
		font-size: 10px;
	}
	
	#fileProgressPage .fileProgressArea .spFileCount {
		display: block;
		height: 12px;
		margin-top: 10px;
		text-indent: 10px;
	}
	
	#fileProgressPage .fileProgressArea .spFileName {
		display: block;
		height: 12px;
		margin-top: 10px;
		text-indent: 10px;
		width: 100%; overflow: hidden;
	}
	
	#fileProgressPage .fileProgressArea .progressBg {
		width: 100%;
		height: 20px;
		border: 1px solid silver;
		margin: 5px 0px;
		border-radius: 5px;
	}
	
	#fileProgressPage .progressBg .progressBar {
		display: block;
		height: 100%;
		width: 0%;
		color: #fff; text-align: center; line-height: 20px;
		overflow: hidden;
		background-color: rgb(131, 165, 193);
	}
</style>

<script type="text/javascript">
	var FileProgressController = {
		id : "#fileProgressPage",
		init : function(title, total) {
			$(this.id).find(".fileTitle").text(title);
			$(this.id).find(".spFileCount").text("0 / " + total);
			$(this.id).find("#progressBarTotal").css("width", "0%");
			$(this.id).find(".spFileName").text("");
			$(this.id).find("#progressBarFile").css("width", "0%");
			$(this.id).find("#progressBarFile").text("");
		},
		setFileCount : function(val) { $(this.id).find(".spFileCount").text(val); },
		setFileName : function(val) { $(this.id).find(".spFileName").text(val); },
		setTotalProgress : function(val) { $(this.id).find("#progressBarTotal").css("width", val + "%"); },
		setFileProgress : function(val) {
			$(this.id).find("#progressBarFile").css("width", val + "%");
			$(this.id).find("#progressBarFile").text(val + "%");
		},
		open : function() {
			$(this.id).show();
		},
		close : function() {
			$(this.id).hide();
		}
	};
</script>

<div id="fileProgressPage">
	<div class="fileProgressArea">
		<label class="fileTitle"></label>
		<div class="fileProgressBar">
			<span class="spFileCount"></span>		
			<div class="progressBg">
				<span class="progressBar" id="progressBarTotal"></span>
			</div>
			
			<span class="spFileName"></span>
			<div class="progressBg">
				<span class="progressBar" id="progressBarFile">60%</span>
			</div>
		</div>
	</div>
</div>