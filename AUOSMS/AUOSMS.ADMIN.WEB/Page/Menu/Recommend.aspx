﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" AutoEventWireup="true" CodeFile="Recommend.aspx.cs" Inherits="Page_Menu_Recommend" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	<h1 class="page-header">추천 메뉴 관리</h1>

	<ul class="nav nav-tabs nav-justified" style="margin-bottom: 20px;">
		<asp:Literal runat="server" ID="litTabs"></asp:Literal>
	</ul>
	
    <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblItemList">
		<colgroup>
			<col style="width: 100px;" />
			<col style="width: 250px;" />
			<col style="width: 250px;" />
			<col />
			<col style="width: 150px;" />
			<col style="width: 80px;" />
		</colgroup>
		<thead>
			<tr>
				<th class="text-center"></th>
				<th class="text-center">메뉴명</th>
				<th class="text-center">영문명</th>
				<th class="text-center">메뉴설명</th>
				<th class="text-center">가격</th>
				<th class="text-center">추천제거</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-success" id="btnAddItem">추천메뉴 추가</a>
	</div>
	
	<!-- 메뉴 목록 -->
	<div class="modal fade" role="dialog" id="divItemSearchList">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">메뉴 검색</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblItemSearchList">
						<colgroup>
							<col style="width:200px;" />
							<col />
							<col style="width:100px;" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">메뉴이름</th>
								<th class="text-center">메뉴그룹</th>
								<th class="text-center">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();

			$("#btnAddItem").click(function () {
				GetItemSearchList();
			});
			$("#tblItemSearchList").on("click", "button.select_item", function () {
				if (confirm("추가 하시겠습니까?")) {
					var params = {};
					params.ITEM_ID = $(this).data("item");
					params.RECOMMEND_YN = "Y";

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "UpdateItemRecommend",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("추천메뉴가 추가 되었습니다.");
								$("#divItemSearchList").modal("hide");
								GetItemList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});

			$("#tblItemList").on("click", "button.delete_item", function () {
				if (confirm("해당 추천메뉴를 제거 하시겠습니까?")) {
					var params = {};
					params.ITEM_ID = $(this).data("item");
					params.RECOMMEND_YN = "N";

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "UpdateItemRecommend",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("추천메뉴가 삭제 되었습니다.");
								GetItemList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			

			GetItemList();
		});

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 1000;
		}

		function GetItemList() {
			SearchInfo.RECOMMEND_YN = "Y";
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemRecommendList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\"><img src=\"" + data.FILEPATH + data.LIST[i].ITEM_IMAGE + "\" style=\"width: 80px; height: 80px;\" onError=\"this.src='" + contextPath + "images/no_image.png'\" /></td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\"><a href=\"View.aspx?item_id=" + data.LIST[i].ITEM_ID + "&group=" + SearchInfo.GROUP_CODE + "\">" + data.LIST[i].ITEM_NAME + "</a> <i class=\"fa fa-star\" style=\"color: yellow;\"></i></td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].ITEM_ENG_NAME + "</td>";
							//strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].GROUP_NAME.replace(/,/gi, "<br />") + "</td>";
							strHtml += "<td class=\"text-left\" style=\"vertical-align:middle\">" + data.LIST[i].ITEM_DESC + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + String(data.LIST[i].ITEM_PRICE).currencyFormat() + "원</td>";
							//strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].CREATE_DATE.substr(0, 10) + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\"><button class=\"btn btn-danger btn-xs delete_item\" data-item=\"" + data.LIST[i].ITEM_ID + "\">제거</button></td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"7\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblItemList > tbody").html(strHtml);
				}
			});
		}

		function GetItemSearchList() {
			SearchInfo.RECOMMEND_YN = "N";
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemRecommendList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].ITEM_NAME + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].GROUP_NAME + "</td>";
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs select_item\" data-item=\"" + data.LIST[i].ITEM_ID + "\">선택</button></td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">선택할 메뉴가 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblItemSearchList > tbody").html(strHtml);
				}
			});
			$("#divItemSearchList").modal();
		}

	</script>
</asp:Content>