﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Menu_List" %>
<%@ Register Src="~/Include/ITEMNewForm.ascx" TagPrefix="uc1" TagName="ITEMNewForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	<h1 class="page-header">메뉴관리</h1>

	<ul class="nav nav-tabs nav-justified" style="margin-bottom: 20px;">
		<asp:Literal runat="server" ID="litTabs"></asp:Literal>
	</ul>
	
    <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblItemList">
		<colgroup>
			<col style="width: 100px;" />
			<col style="width: 250px;" />
			<col style="width: 250px;" />
			<col />
			<col style="width: 150px;" />
			<col style="width: 80px;" />
		</colgroup>
		<thead>
			<tr>
				<th class="text-center"></th>
				<th class="text-center">메뉴명</th>
				<th class="text-center">영문명</th>
				<th class="text-center">메뉴설명</th>
				<th class="text-center">가격</th>
				<th class="text-center">그룹제거</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-primary" id="btnNewItem">메뉴등록</a>
		<a class="btn btn-success" id="btnAddItem">메뉴추가</a>
	</div>
	
	<!-- 메뉴 목록 -->
	<div class="modal fade" role="dialog" id="divItemSearchList">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">메뉴 검색</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblItemSearchList">
						<colgroup>
							<col style="width:200px;" />
							<col />
							<col style="width:100px;" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">메뉴이름</th>
								<th class="text-center">메뉴그룹</th>
								<th class="text-center">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

	<uc1:ITEMNewForm runat="server" ID="ITEMNewForm" />

	
	
	<!-- 옵션 추가 목록 -->
	<div class="modal fade" role="dialog" id="divOptionSearchList">
		<div class="modal-dialog" style="width: 700px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">옵션검색</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					
					<div class="panel-group" id="accoOptionSearchList"></div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();

			$("#btnNewItem").click(function () {
				ITEMNewForm.open(null, SearchInfo.GROUP_CODE, function () {
					InitSearchInfo();
					GetItemList();
				});
			});

			$("#btnAddItem").click(function () {
				GetItemSearchList();
			});
			$("#tblItemSearchList").on("click", "button.select_item", function () {
				if (confirm("추가 하시겠습니까?")) {
					var params = {};
					params.ITEM_ID = $(this).data("item");
					params.GROUP_CODE = SearchInfo.GROUP_CODE;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "InsertItemCategory",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("메뉴가 추가 되었습니다.");
								$("#divItemSearchList").modal("hide");
								GetItemList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});

			$("#tblItemList").on("click", "button.delete_item", function () {
				if (confirm("해당 메뉴를 그룹에서 제거 하시겠습니까?")) {
					var params = {};
					params.GROUP_CODE = SearchInfo.GROUP_CODE;
					params.ITEM_ID = $(this).data("item");

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteItemCategory",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("메뉴가 해당 카테고리에서 삭제 되었습니다.");
								GetItemList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});

			$("#btnOptonSearch").click(function () {
				GetOptionSearchList();
			});

			GetItemList();
		});

		function GetItemList() {
			//ajaxTestPopup(contextPath + "Ajax/Json.aspx", "AjaxTest", "GetStoreList", SearchInfo); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\"><img src=\"" + data.FILEPATH + data.LIST[i].ITEM_IMAGE + "\" style=\"width: 80px; height: 80px;\" onError=\"this.src='" + contextPath + "images/no_image.png'\" /></td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">";
							strHtml += "<a href=\"View.aspx?item_id=" + data.LIST[i].ITEM_ID + "&group=" + SearchInfo.GROUP_CODE + "\">" + data.LIST[i].ITEM_NAME + "</a>";
							if (data.LIST[i].RECOMMEND_YN == "Y") {
								strHtml += " <i class=\"fa fa-star\" style=\"color: yellow;\"></i>";
							}
							else {
								strHtml += " <i class=\"fa fa-star-o\" style=\"color: silver;\"></i>";
							}
							
							strHtml += "</td>";


							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].ITEM_ENG_NAME + "</td>";
							//strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].GROUP_NAME.replace(/,/gi, "<br />") + "</td>";
							strHtml += "<td class=\"text-left\" style=\"vertical-align:middle\">" + data.LIST[i].ITEM_DESC + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + String(data.LIST[i].ITEM_PRICE).currencyFormat() + "원</td>";
							//strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].CREATE_DATE.substr(0, 10) + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\"><button class=\"btn btn-danger btn-xs delete_item\" data-item=\"" + data.LIST[i].ITEM_ID + "\">제거</button></td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"7\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblItemList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 1000;
			SearchInfo.GROUP_CODE = <%= OPTION_GROUP_CODE %>;
		}

		function GetItemSearchList() {
			var params = {};
			params.GROUP_CODE = SearchInfo.GROUP_CODE;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerStoreSearchList", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemSearchList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_NAME + "</td>"
							strHtml += "<td class=\"text-center\">" + data[i].GROUP_NAME + "</td>"
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs select_item\" data-item=\"" + data[i].ITEM_ID + "\">선택</button></td>"
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">선택할 관리자가 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblItemSearchList > tbody").html(strHtml);
				}
			});
			$("#divItemSearchList").modal();
		}

		function GetOptionSearchList() {
			var params = {};
			params.ID = 0;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetOptionSearchList",
				Params: params,
				SuccessCallBack: function (data) {
					var option_group = "";
					var strHtml = "";
					var collapseID = 0;
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							if (option_group != data[i].GROUP_CODE) {
								option_group = data[i].GROUP_CODE;
								if (i != 0) {
									strHtml += "		</div>";
									strHtml += "	</div>";
									strHtml += "</div>";
								}

								collapseID++;

								strHtml += "<div class=\"panel panel-default\">";
								strHtml += "	<div class=\"panel-heading\">";
								strHtml += "		<h4 class=\"panel-title\">";
								strHtml += "			<a data-toggle=\"collapse\" data-parent=\"#accoOptionSearchList\" href=\"#collapseID" + collapseID + "\">" + data[i].GROUP_NAME + "</a>";
								strHtml += "		</h4>";
								strHtml += "	</div>";
								strHtml += "	<div id=\"collapseID" + collapseID + "\" class=\"panel-collapse collapse" + (i == 0 ? " in" : "") + "\">";
								strHtml += "		<div class=\"panel-body\">";
							}

							strHtml += "			<div class=\"list-group-item\">";
							strHtml += "				" + data[i].OPTION_NAME + " - " + data[i].DETAIL_NAME;
							strHtml += "				<div class=\"pull-right\">";
							strHtml += "					<div class=\"btn-group\">";
							strHtml += "						<button type=\"button\" class=\"btn btn-primary btn-xs btn_option_add\" data-option=\"" + data[i].OPTION_CODE + "\">선택</button>";
							strHtml += "					</div>";
							strHtml += "				</div>";
							strHtml += "			</div>";
						}

						strHtml += "		</div>";
						strHtml += "	</div>";
						strHtml += "</div>";
					}

					$("#accoOptionSearchList").html(strHtml);
				}
			});
			
			$("#divOptionSearchList").modal();
		}

	</script>
</asp:Content>