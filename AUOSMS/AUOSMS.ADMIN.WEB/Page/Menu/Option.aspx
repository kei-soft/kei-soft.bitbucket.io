﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" AutoEventWireup="true" CodeFile="Option.aspx.cs" Inherits="Page_Menu_Option" %>

<%@ Register Src="~/Include/OptionForm.ascx" TagPrefix="uc1" TagName="OptionForm" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<h2 id="viewTitle"><span></span><small> 메뉴 옵션관리</small></h2>

	<ul class="nav nav-tabs nav-justified" style="margin-bottom: 20px;">
		<asp:Literal runat="server" ID="litTabs"></asp:Literal>
	</ul>
	
    <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblIOptionList">
		<colgroup>
			<col style="width: 200px;" />
			<col />
			<col style="width: 100px;" />
			<col style="width: 100px;" />
			<col style="width: 100px;" />
		</colgroup>
		<thead>
			<tr>
				<th class="text-center">옵션명</th>
				<th class="text-center">상세옵션</th>
				<th class="text-center">필수여부</th>
				<th class="text-center">사용여부</th>
				<th class="text-center"></th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-primary" id="btnNewOption">메뉴옵션등록</a>
	</div>

	<uc1:OptionForm runat="server" ID="OptionForm" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();

			$("#btnNewOption").click(function () {
				OptionForm.open(null, SearchInfo.GROUP_CODE, function () {
					GetOptionList();
				});
			});

			$("#tblIOptionList").on("click", "a.option_edit", function () {
				OptionForm.open($(this).data("option"), SearchInfo.GROUP_CODE, function () {
					GetOptionList();
				});
			});

			$("#tblIOptionList").on("click", "a.option_delete", function () {
				if (!confirm("삭제하시겠습니까?")) {
					return false;
				}

				var params = {};
				params.OPTION_CODE = $(this).data("option");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "DeleteOption",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							alert("삭제되었습니다.");
							GetOptionList();
						}
						else {
							alert("Error");
						}
					}
				});
			});

			GetOptionList();
		});

		function GetOptionList() {
			//alert(JSON.stringify(SearchInfo)); return;
			//ajaxTestPopup(contextPath + "Ajax/Json.aspx", "AjaxTest", "GetOptionList", SearchInfo); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetOptionList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td style=\"vertical-align:middle\">";
							
							strHtml += "	<img src=\"" + data.FILEPATH + data.LIST[i].OPTION_IMAGE + "\" onerror=\"this.src='" + contextPath + "images/no_image.png';\" style=\"width:2.28em; height:2.28em;\" />";
							strHtml += "	" + data.LIST[i].OPTION_NAME;

							strHtml += "</td>";

							//strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].OPTION_NAME + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].DETAIL_NAME + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].ESSENTIAL + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].USE_YN + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">";
							strHtml += "<a style=\"cursor:pointer;\" class=\"option_edit\" data-option=\"" + data.LIST[i].OPTION_CODE + "\" title=\"수정\"><span class=\"fa fa-pencil fa-fw\"></span></a>";
							strHtml += "<a style=\"cursor:pointer;\" class=\"option_delete\"  data-option=\"" + data.LIST[i].OPTION_CODE + "\" title=\"삭제\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
							strHtml += "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"5\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblIOptionList > tbody").html(strHtml);
				}
			});

		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 1000;
			SearchInfo.GROUP_CODE = "<%= OPTION_GROUP_CODE %>";
		}

	</script>
</asp:Content>