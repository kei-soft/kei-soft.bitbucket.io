﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>
<%@ Register Src="~/Include/ITEMNewForm.ascx" TagPrefix="uc1" TagName="ITEMNewForm" %>


<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<h2 id="viewTitle"><span></span><small> 메뉴관리</small></h2>

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			메뉴 정보
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-info btn-xs" id="btnToEdit">수정</button>
					<button type="button" class="btn btn-danger btn-xs" id="btnDelete">삭제</button>
					<button type="button" class="btn btn-primary btn-xs" id="btnToList">목록</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<img src="#" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="max-width:200px;" id="vwItemImage" />
			<br /><br />
			<dl>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">상품군</p></dt>
				<dd>
					<blockquote id="vwGroup"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">메뉴명</p></dt>
				<dd>
					<blockquote id="vwItemName"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">영문명</p></dt>
				<dd>
					<blockquote id="vwEngName"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">가격</p></dt>
				<dd>
					<blockquote id="vwPrice"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">영양정보</p></dt>
				<dd>
					<blockquote id="vwNutrition"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">원산지정보</p></dt>
				<dd>
					<blockquote id="vwOrigin"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">알레르기정보</p></dt>
				<dd>
					<blockquote id="vwAllergie"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">Description</p></dt>
				<dd>
					<blockquote id="vwDesc"></blockquote>
				</dd>
			</dl>
		</div>
		<!-- /.panel-body -->
	</div>

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			옵션관리
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-warning btn-xs" id="btnOptionSearch">옵션검색</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			 <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblIOptionList">
					<colgroup>
						<col style="width: 200px;" />
						<col />
						<col style="width: 100px;" />
						<col style="width: 100px;" />
						<col style="width: 100px;" />
					</colgroup>
					<thead>
						<tr>
							<th class="text-center">옵션명</th>
							<th class="text-center">상세옵션</th>
							<th class="text-center">필수여부</th>
							<th class="text-center">사용여부</th>
							<th class="text-center"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
		</div>
		<!-- /.panel-body -->
	</div>

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			사용 매장
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-warning btn-xs" id="btnStoreSearch">매장검색</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblStoreList">
				<thead>
					<tr>
						<th class="text-center">상점명</th>
						<th class="text-center">주소</th>
						<th class="text-center">삭제</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.panel-body -->
	</div>

	
	
	<!-- 옵션 추가 목록 -->
	<div class="modal fade" role="dialog" id="divOptionSearchList">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">옵션검색</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					
					<div class="panel-group" id="accoOptionSearchList"></div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>
	
	<!-- 매장 목록 -->
	<div class="modal fade" role="dialog" id="divStoreSearchList">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">매장검색</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblStoreSearchList">
						<colgroup>
							<col style="width:200px;" />
							<col />
							<col style="width:100px;" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">상점명</th>
								<th class="text-center">주소</th>
								<th class="text-center">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

	<uc1:ITEMNewForm runat="server" ID="ITEMNewForm" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var ITEM_ID = "<%= Request.QueryString["item_id"]%>";
		var ItemInfo;
		var imgPath;
		$(document).ready(function () {
			$("#btnToEdit").click(function () {
				ITEMNewForm.open(ITEM_ID, null, function () {
					GetItemInfo();
				});
			});
			
			$("#btnDelete").click(function () {
				if (confirm("메뉴를 삭제하시겠습니까?")) {
					var params = {};
					params.ID = ITEM_ID;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteItem",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("삭제 되었습니다.");
								location.href = "LIst.aspx?group=<%= Request.QueryString["group"] %>";
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			
			$("#btnToList").click(function () {
				location.href = "LIst.aspx?group=<%= Request.QueryString["group"] %>";
			});

			$("#btnOptionSearch").click(function () {
				GetOptionSearchList();
			});
			$("#accoOptionSearchList").on("click", "button.btn_option_add", function () {
				var params = {};
				params.ITEM_ID = ITEM_ID;
				params.OPTION_CODE = $(this).data("option");

				var $parent = $(this).parents("div.list-group-item");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "InsertItemOption",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetOptionList();
							//GetOptionSearchList();
							//$("#divOptionSearchList").modal("hide");
							$parent.remove();

						}
						else {
							alert("오류!");
						}
					}
				});

			});
			$("#tblIOptionList > tbody").on("click", "a.option_up", function () {
				var $curTr = $(this).parents("tr");
				var curIdx = $("#tblIOptionList > tbody > tr").index($curTr);

				if (curIdx != 0) {
					ChangeOptionSort($curTr.data("option"), $curTr.prev("tr").data("option"));
				}
			});
			$("#tblIOptionList > tbody").on("click", "a.option_down", function () {
				var $curTr = $(this).parents("tr");
				var curIdx = $("#tblIOptionList > tbody > tr").index($curTr);

				if (curIdx != $("#tblIOptionList > tbody > tr").length - 1) {
					ChangeOptionSort($curTr.data("option"), $curTr.next("tr").data("option"));
				}
			});
			$("#tblIOptionList > tbody").on("click", "a.option_delete", function () {
				if (confirm("옵션을 삭제하시겠습니까?")) {
					var params = {};
					params.ITEM_ID = ITEM_ID;
					params.OPTION_CODE = $(this).parents("tr").data("option");

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteItemOptionSort",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								GetOptionList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});

			$("#btnStoreSearch").click(function () {
				GetStoreSearchList();
			});
			$("#tblStoreSearchList").on("click", "button.select_store", function () {
				if (confirm("선택 하시겠습니까?")) {
					var params = {};
					params.STORE_ID = $(this).data("store");
					params.ITEM_ID = ITEM_ID;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "InsertItemStore",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								$("#divStoreSearchList").modal("hide");
								GetItemStoreList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			$("#tblStoreList").on("click", "button.delete_store", function () {
				if (confirm("삭제 하시겠습니까?")) {
					var params = {};
					params.STORE_ID = $(this).data("store");
					params.ITEM_ID = ITEM_ID;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteItemStore",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("매장이 삭제 되었습니다.");
								GetItemStoreList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});

			GetItemInfo();
			GetOptionList();
			GetItemStoreList();
		});
		

		function GetItemInfo() {
			var params = {};
			params.ID = ITEM_ID;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerInfo", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						ItemInfo = data.INFO[0];
						imgPath = data.FILEPATH;
						$("#viewTitle > span").text(ItemInfo.ITEM_NAME);
						$("#vwItemImage").attr("src", imgPath + ItemInfo.ITEM_IMAGE);
						$("#vwGroup").text(ItemInfo.GROUP_NAME);
						$("#vwItemName").text(ItemInfo.ITEM_NAME);
						$("#vwEngName").text(ItemInfo.ITEM_ENG_NAME);
						$("#vwPrice").text(String(ItemInfo.ITEM_PRICE).currencyFormat() + "원");
						$("#vwDesc").text(ItemInfo.ITEM_DESC);
						$("#vwNutrition").text(ItemInfo.NUTRITION);
						$("#vwOrigin").text(ItemInfo.ORIGIN);
						$("#vwAllergie").text(ItemInfo.ALLERGIE);
					}
					else {
						alert("잘못된 메뉴 코드입니다.");
						location.href = "LIst.aspx";
					}
				}
			});

		}

		function GetOptionList() {
			var params = {};
			params.ID = ITEM_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemOptionList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr data-option=\"" + data[i].OPTION_CODE + "\">";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].OPTION_NAME + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].DETAIL_NAME + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].ESSENTIAL + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data[i].USE_YN + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">";
							strHtml += "<a style=\"cursor:pointer;\" class=\"option_up\" title=\"올리기\"><span class=\"fa fa-arrow-up fa-fw\"></span></a>";
							strHtml += "<a style=\"cursor:pointer;\" class=\"option_down\" title=\"내리기\"><span class=\"fa fa-arrow-down fa-fw\"></span></a>";
							strHtml += "<a style=\"cursor:pointer;\" class=\"option_delete\" title=\"삭제\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
							strHtml += "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"5\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblIOptionList > tbody").html(strHtml);
				}
			});

		}

		function GetItemStoreList() {
			var params = {};
			params.ITEM_ID = ITEM_ID;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerInfo", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemStoreList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].STORE_NAME + "</td>"
							strHtml += "<td class=\"\">" + data[i].ADDRESS + " " + data[i].ADDRESS_DETAIL + "</td>"
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs delete_store\" data-store=\"" + data[i].STORE_ID + "\">삭제</button></td>"
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblStoreList > tbody").html(strHtml);
				}
			});
		}

		function GetOptionSearchList() {
			var params = {};
			params.ID = ITEM_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetOptionSearchList",
				Params: params,
				SuccessCallBack: function (data) {
					var option_group = "";
					var strHtml = "";
					var collapseID = 0;
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							if (option_group != data[i].GROUP_CODE) {
								option_group = data[i].GROUP_CODE;
								if (i != 0) {
									strHtml += "		</div>";
									strHtml += "	</div>";
									strHtml += "</div>";
								}

								collapseID++;

								strHtml += "<div class=\"panel panel-default\">";
								strHtml += "	<div class=\"panel-heading\">";
								strHtml += "		<h4 class=\"panel-title\">";
								strHtml += "			<a data-toggle=\"collapse\" data-parent=\"#accoOptionSearchList\" href=\"#collapseID" + collapseID + "\">" + data[i].GROUP_NAME + "</a>";
								strHtml += "		</h4>";
								strHtml += "	</div>";
								strHtml += "	<div id=\"collapseID" + collapseID + "\" class=\"panel-collapse collapse" + (i == 0 ? " in" : "") + "\">";
								strHtml += "		<div class=\"panel-body\">";
							}

							strHtml += "			<div class=\"list-group-item\">";
							strHtml += "				" + data[i].OPTION_NAME + " - " + data[i].DETAIL_NAME;
							strHtml += "				<div class=\"pull-right\">";
							strHtml += "					<div class=\"btn-group\">";
							strHtml += "						<button type=\"button\" class=\"btn btn-primary btn-xs btn_option_add\" data-option=\"" + data[i].OPTION_CODE + "\">선택</button>";
							strHtml += "					</div>";
							strHtml += "				</div>";
							strHtml += "			</div>";
						}

						strHtml += "		</div>";
						strHtml += "	</div>";
						strHtml += "</div>";
					}

					$("#accoOptionSearchList").html(strHtml);
				}
			});
			
			$("#divOptionSearchList").modal();
		}

		function ChangeOptionSort(code1, code2) {
			var params = {};
			params.ITEM_ID = ITEM_ID;
			params.OPTION_CODE1 = code1;
			params.OPTION_CODE2 = code2;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "UpdaeItemOptionSort",
				Params: params,
				SuccessCallBack: function (data) {
					if (data) {
						GetOptionList();
					}
					else {
						alert("오류!");
					}
				}
			});
		}

		function GetStoreSearchList() {
			var params = {};
			params.ITEM_ID = ITEM_ID;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerStoreSearchList", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemStoreSearchList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].STORE_NAME + "</td>"
							strHtml += "<td class=\"\">" + data[i].ADDRESS + " " + data[i].ADDRESS_DETAIL + "</td>"
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs select_store\" data-store=\"" + data[i].STORE_ID + "\">선택</button></td>"
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">선택할 매장이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblStoreSearchList > tbody").html(strHtml);
				}
			});
			$("#divStoreSearchList").modal();
		}
	</script>
</asp:Content>

