﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
	<style>
		#divCategoryList div.list-group-item:hover{
			background-color: azure;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<h2 id="viewTitle"><span></span><small> 메뉴 카테고리</small></h2>

	<div class="panel panel-default">
		<div class="panel-heading">
			카테고리 관리
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-xs" id="btnToNew">추가</button>
				</div>
			</div>
		</div>

		<!-- /.panel-heading -->
		<div class="panel-body" id="divCategoryList">
			<div class="list-group"></div>
		</div>
	<!-- /.panel-body -->
	</div>


	<!-- Modal -->
	<div class="modal fade" role="dialog" id="divCategoryForm">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">카테고리</h4>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="txtItemGroupName">카테고리 이름 : </label>
							<input type="text" class="form-control" placeholder="카테고리 이름" id="txtItemGroupName" />
						</div>
						<div class="form-group">
							<label for="selNoOption">옵션없음 : </label>
							<select class="form-control" id="selNoOption">
								<option value="N">N</option>
								<option value="Y">Y</option>
							</select>
						</div>
						<div class="form-group">
							<label for="selUseYN">사용여부 : </label>
							<select class="form-control" id="selUseYN">
								<option value="Y">Y</option>
								<option value="N">N</option>
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnCategorySave">저장</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		$(document).ready(function () {
			$("#btnToNew").click(function () {
				CategoryForm.Open(null, "", "N", "Y");
			});

			
			$("#divCategoryList").on("click", "a.category_up", function () {
				var $group = $(this).parents("div.list-group-item");
				var currentIndex = $("#divCategoryList div.list-group-item").index($group);

				if (currentIndex == 0) {
					return;
				}

				var group_code1 = $group.data("group");
				var group_code2 = $group.prev("div.list-group-item").data("group");

				UpdaeItemGroupSort(group_code1, group_code2);
			});
			$("#divCategoryList").on("click", "a.category_down", function () {
				var $group = $(this).parents("div.list-group-item");
				var group_count = $("#divCategoryList div.list-group-item").length;
				var currentIndex = $("#divCategoryList div.list-group-item").index($group);

				if (group_count <= currentIndex + 1) {
					return;
				}

				var group_code1 = $group.data("group");
				var group_code2 = $group.next("div.list-group-item").data("group");

				UpdaeItemGroupSort(group_code1, group_code2);
			});
			$("#divCategoryList").on("click", "a.category_edit", function () {
				CategoryForm.Open(
					$(this).parents("div.list-group-item").data("group"),
					$(this).parents("div.list-group-item").find("span.spName").text(),
					$(this).parents("div.list-group-item").find("span.spNooption").text(),
					$(this).parents("div.list-group-item").find("span.spUseYN").text()
				);
			});
			$("#divCategoryList").on("click", "a.category_delete", function () {
				if (!confirm("카테고리를 삭제 하시겠습니까?")) {
					return;
				}
				
				var params = {};
				params.GROUP_CODE = $(this).parents("div.list-group-item").data("group");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "DeleteItemGroup",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetItemGroupList();
						}
						else {
							alert("해당 카테고리에 메뉴가 있어 삭제할 수 없습니다.");
						}
					}
				});
			});

			$("#btnCategorySave").click(function () {
				CategoryForm.Save();
			});

			GetItemGroupList();
		});

		function GetItemGroupList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetItemGroupList",
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<div class=\"list-group-item\" data-group=\"" + data[i].ITEM_GROUP_CODE + "\">";
							strHtml += "	<span class=\"spName\">" + data[i].ITEM_GROUP_NAME + "</span> <small> - 옵션없음 : <span class=\"spNooption\">" + data[i].NOOPTION + "</span>, 노출여부 : <span class=\"spUseYN\">" + data[i].USE_YN + "</span></small>";
							strHtml += "	<div class=\"pull-right\">";
							strHtml += "		<div class=\"btn-group\">";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"category_up\" title=\"올리기\"><span class=\"fa fa-arrow-up fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"category_down\" title=\"내리기\"><span class=\"fa fa-arrow-down fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"category_edit\" title=\"수정\"><span class=\"fa fa-pencil fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"category_delete\" title=\"삭제\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
							strHtml += "		</div>";
							strHtml += "	</div>";
							strHtml += "</div>";
						}
					}
					else {
						strHtml += "<div class=\"list-group-item text-center\">";
						strHtml += "		등록된 카테고리가 없습니다.";
						strHtml += "</div>";
					}

					$("#divCategoryList > div.list-group").html(strHtml);
				}
			});
		}

		function UpdaeItemGroupSort(code1, code2) {
			var params = {};
			params.GROUP_CODE1 = code1;
			params.GROUP_CODE2 = code2;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "UpdaeItemGroupSort",
				Params: params,
				SuccessCallBack: function (data) {
					if (data) {
						GetItemGroupList();
					}
					else {
						alert("오류!");
					}
				}
			});
		}

		var CategoryForm = {
			ItemGroupCode: null,
			Open: function (code, name, nooption, useyn) {
				if (code == null)
					$("#btnCategorySave").text("저장");
				else
					$("#btnCategorySave").text("수정");

				this.ItemGroupCode = code;
				$("#txtItemGroupName").val(name);
				$("#selNoOption").val(nooption);
				$("#selUseYN").val(useyn);
				$("#divCategoryForm").modal();
			},
			close: function () {
				$("#divCategoryForm").modal("hide");
			},
			Save: function () {
				var $txt = $("#txtItemGroupName");
				if ($txt.val() == "") {
					alert("카테고리 이름을 입력하세요");
					$txt.focus();
					return;
				}

				var params = {};
				params.GROUP_NAME = $txt.val();
				params.NOOPTION = $("#selNoOption").val();
				params.USE_YN = $("#selUseYN").val();
				var cmd = "";
				if (this.ItemGroupCode == null) { // 
					cmd = "InsertItemGroup";
				}
				else {
					cmd = "UpdateItemGroup";
					params.GROUP_CODE = this.ItemGroupCode;
				}

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: cmd,
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetItemGroupList();
							CategoryForm.close();
						}
						else {
							alert("오류");
						}
					}
				});
			}
		};
	</script>
</asp:Content>