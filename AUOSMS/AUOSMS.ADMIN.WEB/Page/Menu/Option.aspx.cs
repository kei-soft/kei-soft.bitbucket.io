﻿using AUOSMS.ADMIN.UTIL.DataBase;
using AUOSMS.ADMIN.UTIL.DataBase.DBAgent;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;

public partial class Page_Menu_Option : System.Web.UI.Page
{
	protected string OPTION_GROUP_CODE;

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			this.PageInit();
		}
	}

	void PageInit()
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_CODE_GROUP", MySQLDataType.VARCHAR, 40, "OPTION_GROUP"));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(ConfigurationManager.ConnectionStrings["AUOSMS_MYSQL"].ConnectionString, "USP_COMMON_CODE_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		OPTION_GROUP_CODE = Request.QueryString["group"] == null ? dt.Rows[0]["CODE_ID"].ToString() : Request.QueryString["group"];
		int wrongGroup = 0;
		StringBuilder sbHtml = new StringBuilder();
		foreach (DataRow dr in dt.Rows)
		{
			bool bPair = false;
			if (dr["CODE_ID"].ToString().Equals(OPTION_GROUP_CODE))
			{
				bPair = true;
				wrongGroup++;
			}
			sbHtml.AppendFormat("<li{0}><a{1}>{2}</a></li>{3}"
				, bPair ? " class=\"active\"" : ""
				, bPair ? "" : " href=\"Option.aspx?group=" + dr["CODE_ID"].ToString() + "\""
				, dr["CODE_NAME"]
				, Environment.NewLine
			);
		}
		this.litTabs.Text = sbHtml.ToString();

		if (wrongGroup.Equals(0))
			Response.Redirect("Option.aspx");

	}
}