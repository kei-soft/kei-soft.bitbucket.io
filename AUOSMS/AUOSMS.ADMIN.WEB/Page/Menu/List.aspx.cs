﻿using AUOSMS.ADMIN.UTIL.DataBase;
using AUOSMS.ADMIN.UTIL.DataBase.DBAgent;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;

public partial class Page_Menu_List : System.Web.UI.Page
{
	protected string OPTION_GROUP_CODE;

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			this.PageInit();
		}
	}

	void PageInit()
	{
		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(ConfigurationManager.ConnectionStrings["AUOSMS_MYSQL"].ConnectionString, "USP_ITEM_GROUP_LIST"))
		{
			dt = dbAgent.DataTable();
		}

		OPTION_GROUP_CODE = Request.QueryString["group"] == null ? dt.Rows[0]["ITEM_GROUP_CODE"].ToString() : Request.QueryString["group"];
		int wrongGroup = 0;
		StringBuilder sbHtml = new StringBuilder();
		foreach(DataRow dr in dt.Rows)
		{
			bool bPair = false;
			if (dr["ITEM_GROUP_CODE"].ToString().Equals(OPTION_GROUP_CODE))
			{
				bPair = true;
				wrongGroup++;
			}
			sbHtml.AppendFormat("<li{0}><a{1}>{2}</a></li>{3}"
				, bPair ? " class=\"active\"" : ""
				, bPair ? "" : " href=\"List.aspx?group=" + dr["ITEM_GROUP_CODE"].ToString() + "\""
				, dr["ITEM_GROUP_NAME"]
				, Environment.NewLine
			);
		}
		this.litTabs.Text = sbHtml.ToString();

		if (wrongGroup.Equals(0))
			Response.Redirect("List.aspx");

	}
}