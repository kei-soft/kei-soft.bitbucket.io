﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/StoreNewForm.ascx" TagPrefix="uc1" TagName="StoreNewForm" %>
<%@ Register Src="~/Include/POSNewForm.ascx" TagPrefix="uc1" TagName="POSNewForm" %>



<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	<h1 class="page-header">매장관리</h1>
	
    <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblStoreList">
		<colgroup>
			<col style="width: 100px;" />
			<col style="width: 150px;" />
			<col style="width: 180px;" />
			<col />
			<col style="width: 150px;" />
			<col style="width: 150px;" />
			<col style="width: 100px;" />
		</colgroup>
		<thead>
			<tr>
				<th class="text-center"></th>
				<th class="text-center">매장명</th>
				<th class="text-center">지역</th>
				<th class="text-center">주소</th>
				<th class="text-center">주문가능시간</th>
				<th class="text-center">전화번호</th>
				<th class="text-center">등록일시</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-primary" id="btnNewStore">매장추가</a>
	</div>
	
	<uc1:StoreNewForm runat="server" ID="StoreNewForm" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();

			$("#btnNewStore").click(function () {
				StoreNewForm.open(function () {
					InitSearchInfo();
					GetStoreList();
				});
			});

			GetStoreList();
		});

		function GetStoreList() {
			//ajaxTestPopup(contextPath + "Ajax/Json.aspx", "AjaxTest", "GetStoreList", SearchInfo); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\"><img src=\"" + data.FILEPATH + data.LIST[i].STORE_IMAGE + "\" style=\"width: 80px; height: 80px;\" onError=\"this.src='" + contextPath + "images/no_image.png'\" /></td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\"><a href=\"Info/View.aspx?shop_id=" + data.LIST[i].STORE_ID + "\">" + data.LIST[i].STORE_NAME + "</a></td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].STATE + " " + data.LIST[i].CITY + "</td>";
							strHtml += "<td class=\"\" style=\"vertical-align:middle\">(" + data.LIST[i].POST_CODE + ")<br />" + data.LIST[i].ADDRESS + " " + data.LIST[i].ADDRESS_DETAIL + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].ORDER_START_TIME + "~" + data.LIST[i].ORDER_END_TIME + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].PHONE + "</td>";
							strHtml += "<td class=\"text-center\" style=\"vertical-align:middle\">" + data.LIST[i].CREATE_DATE.substr(0, 10) + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"7\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblStoreList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 100;
		}

	</script>
</asp:Content>