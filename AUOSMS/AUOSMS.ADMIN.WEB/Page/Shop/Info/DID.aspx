﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/DIDRegForm.ascx" TagPrefix="uc1" TagName="DIDRegForm" %>
<%@ Register Src="~/Include/StoreTabs.ascx" TagPrefix="uc1" TagName="StoreTabs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<h2 id="viewTitle"><span></span><small> DID  관리</small></h2>

	<uc1:StoreTabs runat="server" ID="StoreTabs" Catergory="DID" />

	<div class="list-group" id="divDIDList"></div>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-primary" id="btnNewDID">이미지 추가</a>
	</div>

	<uc1:DIDRegForm runat="server" ID="DIDRegForm" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var STORE_ID = "<%= Request.QueryString["shop_id"]%>";
		$(document).ready(function () {
			$("#btnNewDID").click(function () {
				DIDRegForm.open(null, STORE_ID, function () { GetDIDImageList(); });
			});
			
			$("#divDIDList").on("click", "a.DID_up", function () {
				var $DID = $(this).parents("div.list-group-item");
				var currentIndex = $("#divDIDList div.list-group-item").index($DID);

				if (currentIndex == 0) {
					return;
				}

				var DID_id1 = $DID.data("id");
				var DID_id2 = $DID.prev("div.list-group-item").data("id");

				UpdateDIDImageSort(DID_id1, DID_id2);
			});
			$("#divDIDList").on("click", "a.DID_down", function () {
				var $DID = $(this).parents("div.list-group-item");
				var DID_count = $("#divDIDList div.list-group-item").length;
				var currentIndex = $("#divDIDList div.list-group-item").index($DID);

				if (DID_count <= currentIndex + 1) {
					return;
				}

				var DID_id1 = $DID.data("id");
				var DID_id2 = $DID.next("div.list-group-item").data("id");

				UpdateDIDImageSort(DID_id1, DID_id2);
			});
			$("#divDIDList").on("click", "a.DID_edit", function () {
				DIDRegForm.open($(this).parents("div.list-group-item").data("id"), STORE_ID, function () { GetDIDImageList() });
			});
			$("#divDIDList").on("click", "a.DID_delete", function () {
				if (!confirm("이미지를 삭제 하시겠습니까?")) {
					return;
				}
				
				var params = {};
				params.ID = $(this).parents("div.list-group-item").data("id");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "DeleteDIDImage",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetDIDImageList();
						}
						else {
							alert("오류");
						}
					}
				});
			});

			GetStoreInfo(function () {
				GetDIDImageList();
			});
		});

		function GetStoreInfo(callback) {
			var params = {};
			params.ID = STORE_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						$("#viewTitle > span").text(data.INFO[0].STORE_NAME);
						if (callback) callback();
					}
					else {
						alert("잘못된 매장 코드입니다.");
						location.href = "../LIst.aspx";
					}
				}
			});
		}

		function GetDIDImageList() {
			var params = {};
			params.PAGE_SIZE = 1000;
			params.PAGE = 0;
			params.STORE_ID = STORE_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Params: params,
				Command: "GetDIDImageList",
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<div class=\"list-group-item\" data-id=\"" + data.LIST[i].IMAGE_ID + "\"" + (data.LIST[i].USE_YN == "N" ? " style=\"background-color: silver;\"" : "") + ">";
							strHtml += "	<img src=\"" + data.FILEPATH + data.LIST[i].IMAGE_NAME + "\" style=\"width: 200px;\"  onError=\"this.src='" + contextPath + "images/no_image.png'\" />";
							strHtml += "	<div class=\"pull-right\">";
							strHtml += "		<div class=\"btn-group\">";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"DID_up\" title=\"올리기\"><span class=\"fa fa-arrow-up fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"DID_down\" title=\"내리기\"><span class=\"fa fa-arrow-down fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"DID_edit\" title=\"수정\"><span class=\"fa fa-pencil fa-fw\"></span></a>";
							strHtml += "			<a style=\"cursor:pointer;\" class=\"DID_delete\" title=\"삭제\"><span class=\"fa fa-trash-o fa-fw\"></span></a>";
							strHtml += "		</div>";
							strHtml += "	</div>";
							strHtml += "</div>";
						}
					}
					else {
						strHtml += "<div class=\"list-group-item text-center\">";
						strHtml += "		등록된 이미지가 없습니다.";
						strHtml += "</div>";
					}

					$("#divDIDList").html(strHtml);
				}
			});
		}

		function UpdateDIDImageSort(id1, id2) {
			var params = {};
			params.ID1 = id1;
			params.ID2 = id2;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "UpdateDIDImageSort",
				Params: params,
				SuccessCallBack: function (data) {
					if (data) {
						GetDIDImageList();
					}
					else {
						alert("오류!");
					}
				}
			});
		}
	</script>
</asp:Content>