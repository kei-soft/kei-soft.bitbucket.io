﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/StoreTabs.ascx" TagPrefix="uc1" TagName="StoreTabs" %>
<%@ Register Src="~/Include/SaleTabs.ascx" TagPrefix="uc1" TagName="SaleTabs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
	<style>
		table.report_table th, td{
			padding:8px 3px !important;
			vertical-align: middle !important;
		}
		table.report_table tr.total{
			font-weight: bold;
			background-color: antiquewhite;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h2 id="viewTitle"><span></span><small> 시간대별 상품 매출 상세 현황</small></h2>

	<uc1:StoreTabs runat="server" ID="StoreTabs" Catergory="Sale" />

	<uc1:SaleTabs runat="server" ID="SaleTabs" Catergory="SaleReportByItemTimeList" />

	<div class="panel panel-default" style="padding: 5px; text-align: right; margin-top: 5px;">
		매출일자 : 
		<input class="form-control datepicker readonly" name="txtFR_DATE" id="txtFR_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd") %>">
		~
		<input class="form-control datepicker readonly" name="txtTO_DATE" id="txtTO_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.ToString("yyyy-MM-dd") %>">
		<a class="btn btn-success" id="btnSearch">조회</a>
		<a class="btn btn-success" id="btnToExcel">Excel</a>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover report_table" id="tblSaleList">
		<thead>
			<tr>
				<th class="text-center" rowspan="2">상품코드</th>
				<th class="text-center" rowspan="2">상품명</th>

				<th class="text-center" colspan="2">08시</th>
				<th class="text-center" colspan="2">09시</th>
				<th class="text-center" colspan="2">10시</th>
				<th class="text-center" colspan="2">11시</th>
				<th class="text-center" colspan="2">12시</th>
				<th class="text-center" colspan="2">13시</th>
				<th class="text-center" colspan="2">14시</th>
				<th class="text-center" colspan="2">15시</th>
				<th class="text-center" colspan="2">16시</th>
				<th class="text-center" colspan="2">17시</th>
				<th class="text-center" colspan="2">18시</th>
				<th class="text-center" colspan="2">19시</th>
				<th class="text-center" colspan="2">20시</th>
				<th class="text-center" colspan="2">21시</th>
				<th class="text-center" colspan="2">22시</th>
			</tr>
			<tr>
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
				
				<th class="text-center">수량</th>
				<th class="text-center">매출액</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var STORE_ID = "<%= Request.QueryString["shop_id"]%>";
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.START_DATE = $("#txtFR_DATE").val();
				SearchInfo.END_DATE = $("#txtTO_DATE").val();
				GetSaleList();
			});

			$("#btnToExcel").click(function () {
				do_cmd.excelExport("tblSaleList", "시간대별 상품 매출 상세 현황");
			});

			GetStoreInfo(function () {
				GetSaleList();
			});
		});

		function GetStoreInfo(callback) {
			var params = {};
			params.ID = STORE_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						$("#viewTitle > span").text(data.INFO[0].STORE_NAME);
						if (callback) callback();
					}
					else {
						alert("잘못된 매장 코드입니다.");
						location.href = "../LIst.aspx";
					}
				}
			});
		}

		function GetSaleList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetSaleReportByItemTimeList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						var tot_cnt_08 = tot_cnt_09 = tot_cnt_10 = tot_cnt_11 = tot_cnt_12 = tot_cnt_13 = tot_cnt_14 = tot_cnt_15 = tot_cnt_16 = tot_cnt_17 = tot_cnt_18 = tot_cnt_19 = tot_cnt_20= tot_cnt_21= tot_cnt_22 = 0;
						var tot_price_08 = tot_price_09 = tot_price_10 = tot_price_11 = tot_price_12 = tot_price_13 = tot_price_14 = tot_price_15 = tot_price_16 = tot_price_17 = tot_price_18 = tot_price_19 = tot_price_20= tot_price_21= tot_price_22 = 0;
						for (var i = 0; i < data.length; i++) {
							tot_cnt_08 += data[i].COUNT_08;
							tot_cnt_09 += data[i].COUNT_09;
							tot_cnt_10 += data[i].COUNT_10;
							tot_cnt_11 += data[i].COUNT_11;
							tot_cnt_12 += data[i].COUNT_12;
							tot_cnt_13 += data[i].COUNT_13;
							tot_cnt_14 += data[i].COUNT_14;
							tot_cnt_15 += data[i].COUNT_15;
							tot_cnt_16 += data[i].COUNT_16;
							tot_cnt_17 += data[i].COUNT_17;
							tot_cnt_18 += data[i].COUNT_18;
							tot_cnt_19 += data[i].COUNT_19;
							tot_cnt_20 += data[i].COUNT_20;
							tot_cnt_21 += data[i].COUNT_21;
							tot_cnt_22 += data[i].COUNT_22;

							tot_price_08 += data[i].PRICE_08;
							tot_price_09 += data[i].PRICE_09;
							tot_price_10 += data[i].PRICE_10;
							tot_price_11 += data[i].PRICE_11;
							tot_price_12 += data[i].PRICE_12;
							tot_price_13 += data[i].PRICE_13;
							tot_price_14 += data[i].PRICE_14;
							tot_price_15 += data[i].PRICE_15;
							tot_price_16 += data[i].PRICE_16;
							tot_price_17 += data[i].PRICE_17;
							tot_price_18 += data[i].PRICE_18;
							tot_price_19 += data[i].PRICE_19;
							tot_price_20 += data[i].PRICE_20;
							tot_price_21 += data[i].PRICE_21;
							tot_price_22 += data[i].PRICE_22;

							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_ID + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_NAME + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_08 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_08).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_09 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_09).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_10 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_10).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_11 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_11).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_12 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_12).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_13 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_13).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_14 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_14).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_15 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_15).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_16 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_16).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_17 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_17).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_18 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_18).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_19 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_19).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_20 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_20).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_21 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_21).currencyFormat() + "</td>";
							strHtml += "<td class=\"text-center\">" + data[i].COUNT_22 + "</td>";
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE_22).currencyFormat() + "</td>";
							strHtml += "</tr>";
						}

						
						strHtml += "<tr class=\"total\">";
						strHtml += "<td class=\"text-center\" colspan=\"2\">합계</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_08 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_08).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_09 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_09).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_10 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_10).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_11 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_11).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_12 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_12).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_13 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_13).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_14 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_14).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_15 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_15).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_16 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_16).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_17 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_17).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_18 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_18).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_19 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_19).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_20 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_20).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_21 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_21).currencyFormat() + "</td>";
						strHtml += "<td class=\"text-center\">" + tot_cnt_22 + "</td>";
						strHtml += "<td class=\"text-center\">" + String(tot_price_22).currencyFormat() + "</td>";
						strHtml += "</tr>";
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"32\" class=\"text-center\">매출내역이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblSaleList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.STORE_ID = STORE_ID;
			SearchInfo.START_DATE = $("#txtFR_DATE").val();
			SearchInfo.END_DATE = $("#txtTO_DATE").val();
		}
	</script>
</asp:Content>

