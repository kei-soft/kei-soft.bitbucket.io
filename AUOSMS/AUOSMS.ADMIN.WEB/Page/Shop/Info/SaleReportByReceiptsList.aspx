﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/StoreTabs.ascx" TagPrefix="uc1" TagName="StoreTabs" %>
<%@ Register Src="~/Include/SaleTabs.ascx" TagPrefix="uc1" TagName="SaleTabs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
	<style>
		table.report_table th, td{
			padding:8px 3px !important;
			vertical-align: middle !important;
		}
		table.report_table tr.total{
			font-weight: bold;
			background-color: antiquewhite;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h2 id="viewTitle"><span></span><small> 영수증별 매출 상세 현황</small></h2>

	<uc1:StoreTabs runat="server" ID="StoreTabs" Catergory="Sale" />

	<uc1:SaleTabs runat="server" ID="SaleTabs" Catergory="SaleReportByReceiptsList" />

	<div class="panel panel-default" style="padding: 5px; text-align: right; margin-top: 5px;">
		매출일자 : 
		<input class="form-control datepicker readonly" name="txtDATE" id="txtDATE" type="text" style="width: 100px;" value="<%= DateTime.Today.ToString("yyyy-MM-dd") %>">
		<a class="btn btn-success" id="btnSearch">조회</a>
		<a class="btn btn-success" id="btnToExcel">Excel</a>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover report_table" id="tblSaleList">
		<thead>
			<tr>
				<th class="text-center">포스번호</th>
				<th class="text-center">영수증번호</th>
				<th class="text-center">주문시각</th>
				<th class="text-center">결제시각</th>
				<th class="text-center">상품코드</th>
				<th class="text-center">상품명</th>
				<th class="text-center">수량</th>
				<th class="text-center">총매출액</th>
				<th class="text-center">할인액</th>
				<th class="text-center">할인구분</th>
				<th class="text-center">실매출</th>
				<th class="text-center">가액</th>
				<th class="text-center">부가세</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var STORE_ID = "<%= Request.QueryString["shop_id"]%>";
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.ORDER_DATE = $("#txtDATE").val();
				GetSaleList();
			});

			$("#btnToExcel").click(function () {
				do_cmd.excelExport("tblSaleList", "영수증별 매출 상세 현황");
			});

			GetStoreInfo(function () {
				GetSaleList();
			});
		});

		function GetStoreInfo(callback) {
			var params = {};
			params.ID = STORE_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						$("#viewTitle > span").text(data.INFO[0].STORE_NAME);
						if (callback) callback();
					}
					else {
						alert("잘못된 매장 코드입니다.");
						location.href = "../LIst.aspx";
					}
				}
			});
		}

		function GetSaleList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetSaleReportByReceiptsList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						var tot_quantity = tot_price = tot_dc_price = tot_real_price = tot_ori_price = tot_vat_price = 0;
						var cur_order_no = "";
						for (var i = 0; i < data.length; i++) {
							tot_quantity += data[i].QUANTITY;
							tot_price += data[i].PRICE;
							tot_dc_price += data[i].DC_PRICE;
							tot_real_price += data[i].REAL_PRICE;
							tot_ori_price += data[i].ORI_PRICE;
							tot_vat_price += data[i].VAT_PRICE;

							strHtml += "<tr>";
							if (i == 0) {
								strHtml += "<td class=\"text-center\" rowspan=\"" + data.length + "\">N/A</td>"; // 포스번호
							}

							if (cur_order_no != data[i].ORDER_NO) {
								cur_order_no = data[i].ORDER_NO;
								var row = 1;
								while (true) {
									if (data[i + row] == null || data[i + row].ORDER_NO == undefined || data[i + row].ORDER_NO != cur_order_no) {
										strHtml += "<td class=\"text-center\" rowspan=\"" + row + "\">" + data[i].ORDER_NO + "</td>"; // 영수증번호
										break;
									}
									else {
										row++;
									}
								}
							}
							//strHtml += "<td class=\"text-center\">" + data[i].ORDER_NO + "</td>"; // 영수증번호
							strHtml += "<td class=\"text-center\">" + data[i].ORDER_TIME + "</td>"; // 주문시각
							strHtml += "<td class=\"text-center\">" + data[i].PAY_TIME + "</td>"; // 결제시각
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_ID + "</td>"; // 상품코드
							strHtml += "<td class=\"text-center\">" + data[i].ITEM_NAME + "</td>"; // 상품명
							strHtml += "<td class=\"text-center\">" + String(data[i].QUANTITY).currencyFormat() + "</td>"; // 수량
							strHtml += "<td class=\"text-center\">" + String(data[i].PRICE).currencyFormat() + "</td>"; // 총매출액
							strHtml += "<td class=\"text-center\">" + String(data[i].DC_PRICE).currencyFormat() + "</td>"; // 할인액
							strHtml += "<td class=\"text-center\">" + data[i].DC_TYPE + "</td>"; // 할인구분
							strHtml += "<td class=\"text-center\">" + String(data[i].REAL_PRICE).currencyFormat() + "</td>"; // 실매출
							strHtml += "<td class=\"text-center\">" + String(data[i].ORI_PRICE).currencyFormat() + "</td>"; // 가액
							strHtml += "<td class=\"text-center\">" + String(data[i].VAT_PRICE).currencyFormat() + "</td>"; // 부가세
							strHtml += "</tr>";
						}
						
						strHtml += "<tr class=\"total\">";
						strHtml += "<td class=\"text-center\" colspan=\"6\">합계</td>"; // 합계
						strHtml += "<td class=\"text-center\">" + String(tot_quantity).currencyFormat() + "</td>"; // 수량
						strHtml += "<td class=\"text-center\">" + String(tot_price).currencyFormat() + "</td>"; // 총매출액
						strHtml += "<td class=\"text-center\">" + String(tot_dc_price).currencyFormat() + "</td>"; // 할인액
						strHtml += "<td class=\"text-center\"></td>"; // 할인구분
						strHtml += "<td class=\"text-center\">" + String(tot_real_price).currencyFormat() + "</td>"; // 실매출
						strHtml += "<td class=\"text-center\">" + String(tot_ori_price).currencyFormat() + "</td>"; // 가액
						strHtml += "<td class=\"text-center\">" + String(tot_vat_price).currencyFormat() + "</td>"; // 부가세
						strHtml += "</tr>";
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"13\" class=\"text-center\">매출내역이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblSaleList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.STORE_ID = STORE_ID;
			SearchInfo.ORDER_DATE = $("#txtDATE").val();
		}
	</script>
</asp:Content>

