﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/StoreTabs.ascx" TagPrefix="uc1" TagName="StoreTabs" %>


<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h2 id="viewTitle"><span></span><small> 주문내역</small></h2>

	<uc1:StoreTabs runat="server" ID="StoreTabs" Catergory="Order" />

	<div class="panel panel-default form-inline" style="padding: 5px; text-align: right; margin-top: 5px;">
		주문상태 :
		<select class="form-inline form-control common_code" style="width: 150px;" data-group="ORDER_STATUS" data-select="kor" id="selOrderStatus"></select>
		&nbsp; &nbsp;
		매출일자 : 
		<input class="form-control datepicker readonly" name="txtFR_DATE" id="txtFR_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd") %>">
		~
		<input class="form-control datepicker readonly" name="txtTO_DATE" id="txtTO_DATE" type="text" style="width: 100px;" value="<%= DateTime.Today.ToString("yyyy-MM-dd") %>">
		<a class="btn btn-success" id="btnSearch">조회</a>
	</div>

	<div class="text-right" style="margin-bottom:5px;">
		<i class="fa fa-caret-right fa-fw"></i>총
		<span id="spTotalCnt">0건</span>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblOrderList">
		<thead>
			<tr>
				<th class="text-center">주문번호</th>
				<th class="text-center">주문일시</th>
				<th class="text-center">주문금액</th>
				<th class="text-center">주문상태</th>
				<th class="text-center">주문메뉴</th>
				<th class="text-center">주문방법</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<%--<div class="text-center">
		<ul class="pagination pagination-sm">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
		</ul>
	</div>--%>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var STORE_ID = "<%= Request.QueryString["shop_id"]%>";
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.PAGE = 0;
				SearchInfo.START_DATE = $("#txtFR_DATE").val();
				SearchInfo.END_DATE = $("#txtTO_DATE").val();
				SearchInfo.ORDER_STATUS = $("#selOrderStatus").val();
				GetOrderList();
			});

			GetStoreInfo(function () {
				GetOrderList();
			});
		});

		function GetStoreInfo(callback) {
			var params = {};
			params.ID = STORE_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						$("#viewTitle > span").text(data.INFO[0].STORE_NAME);
						if (callback) callback();
					}
					else {
						alert("잘못된 매장 코드입니다.");
						location.href = "../LIst.aspx";
					}
				}
			});
		}

		function GetOrderList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreOrderList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							var item_list = data.LIST[i].ITEM_NAME.split('|');

							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].ORDER_NO + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].ORDER_DATE + "</td>";
							strHtml += "<td class=\"text-right\">" + String(data.LIST[i].PRICE).currencyFormat() + "원</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].ORDER_STATUS + "</td>";
							strHtml += "<td class=\"text-center\">" + item_list[0] + (item_list.length > 1 ? " 외 " + (item_list.length - 1) + "건" : "") + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PAY_PATH + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"6\" class=\"text-center\">주문내역이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#spTotalCnt").text(String(data.TOTAL_CNT).currencyFormat() + "건");

					$("#tblOrderList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.STORE_ID = STORE_ID;
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 1000;
			SearchInfo.START_DATE = $("#txtFR_DATE").val();
			SearchInfo.END_DATE = $("#txtTO_DATE").val();
			SearchInfo.ORDER_STATUS = $("#selOrderStatus").val();
		}
	</script>
</asp:Content>

