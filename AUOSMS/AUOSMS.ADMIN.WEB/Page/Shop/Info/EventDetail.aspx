﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/EventRegForm.ascx" TagPrefix="uc1" TagName="EventRegForm" %>
<%@ Register Src="~/Include/StoreTabs.ascx" TagPrefix="uc1" TagName="StoreTabs" %>



<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<h2 id="viewTitle"><span></span><small> 이벤트</small></h2>

	<uc1:StoreTabs runat="server" ID="StoreTabs" Catergory="Event" />

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			이벤트 정보
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-info btn-xs" id="btnToEdit">수정</button>
					<button type="button" class="btn btn-danger btn-xs" id="btnDelete">삭제</button>
					<button type="button" class="btn btn-primary btn-xs" id="btnToList">목록</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<img src="#" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="max-width:200px;" id="vwEventImage" />
			<br /><br />
			<dl>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">이벤트명</p></dt>
				<dd>
					<blockquote id="vwEventName"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">기간</p></dt>
				<dd>
					<blockquote id="vwEventDate"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">사은품</p></dt>
				<dd>
					<img src="#" onerror="this.src='<%= Page.ResolveUrl("~") %>images/no_image.png';" style="max-width:200px;" id="vwGiftImage" />
					<blockquote id="vwGiftName"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">제공기준</p></dt>
				<dd>
					<blockquote id="vwCondition"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">이벤트상태</p></dt>
				<dd>
					<blockquote id="vwEventStatus"></blockquote>
				</dd>
				<dt><p class="bg-success" style="text-indent: 10px; font-size:14px;">사용유무</p></dt>
				<dd>
					<blockquote id="vwUseYN"></blockquote>
				</dd>
			</dl>
		</div>
		<!-- /.panel-body -->
	</div>

	<uc1:EventRegForm runat="server" ID="EventRegForm" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var STORE_ID = "<%= Request.QueryString["shop_id"]%>";
		var EVENT_ID = "<%= Request.QueryString["event_id"]%>";

		$(document).ready(function () {
			$("#btnToEdit").click(function () {
				EventRegForm.open(EVENT_ID, STORE_ID, function () { GetEventInfo(); });
			});
			
			$("#btnDelete").click(function () {
				if (confirm("이벤트를 삭제하시겠습니까>")) {
					var params = {};
					params.ID = EVENT_ID;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteStoreEvent",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								location.href = "Event.aspx?shop_id=" + STORE_ID;
							}
							else {
								alert("오류");
							}
						}
					});

				}
			});
			
			$("#btnToList").click(function () {
				location.href = "Event.aspx?shop_id=" + STORE_ID;
			});

			GetStoreInfo(function () {
				GetEventInfo();
			});
		});

		function GetStoreInfo(callback) {
			var params = {};
			params.ID = STORE_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						$("#viewTitle > span").text(data.INFO[0].STORE_NAME);
						if (callback) callback();
					}
					else {
						alert("잘못된 매장 코드입니다.");
						location.href = "../LIst.aspx";
					}
				}
			});
		}

		function GetEventInfo() {
			var params = {};
			params.ID = EVENT_ID;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreEventInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.INFO.length > 0) {
						$("#vwEventImage").attr("src", data.EVENTFILEPATH + data.INFO[0].EVENT_INFO_IMAGE);
						$("#vwEventName").text(data.INFO[0].EVENT_NAME);
						$("#vwEventDate").text(data.INFO[0].EVENT_START_DATE + " ~ " + data.INFO[0].EVENT_END_DATE);
						$("#vwGiftImage").attr("src", (data.INFO[0].EVENT_GIFT_TYPE == "MENU" ? data.ITEMFILEPATH : data.EVENTFILEPATH) + data.INFO[0].EVENT_IMAGE);
						$("#vwGiftName").text(data.INFO[0].EVENT_GIFT_NAME);
						$("#vwCondition").text(data.INFO[0].EVENT_CONDITION_TYPE == "QUANTITY" ? "음료 " + data.INFO[0].EVENT_CONDITION_VALUE + "잔당" : String(data.INFO[0].EVENT_CONDITION_VALUE).currencyFormat() + "원 이상");
						$("#vwEventStatus").text((data.INFO[0].EVENT_STATUS == 1 ? "진행중" : (data.INFO[0].EVENT_STATUS == 2 ? "시작전" : "종료")));
						$("#vwUseYN").text(data.INFO[0].USE_YN);
					}
					else {
						alert("잘못된 이벤트 코드입니다.");
						location.href = "Event.aspx?shop_id=" + STORE_ID;
					}
				}
			});
		}
	</script>
</asp:Content>

