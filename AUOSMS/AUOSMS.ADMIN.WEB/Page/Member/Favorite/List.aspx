﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">회원 취향 정보 관리</h1>

	<div class="panel panel-default form-inline" style="padding: 5px; text-align: right; margin-top: 5px;">
		<select class="form-inline form-control" id="selSearchCondition" style="width: 150px;">
			<option value="ITEM_GROUP">음료 종류</option>
			<option value="COFFEE_SHOT">커피 농도</option>
			<option value="SYRUP_TYPE">시럽 종류</option>
			<option value="SYRUP_DENSITY">시럽 농도</option>
			<option value="ICE">얼음 분량</option>
			<option value="CREAM">휘핑 크림</option>
			<option value="MILK_HOT">우유 온도</option>
		</select>
		<input class="form-control" id="txtSearchKeyword" style="width: 200px;" placeholder="검색" />
		<a class="btn btn-success" id="btnSearch">조회</a>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblMemberTasteList">
		<thead>
			<tr>
				<%--<th class="text-center">회원코드</th>--%>
				<th class="text-center">회원명</th>
				<th class="text-center">음료 종류</th>
				<th class="text-center">커피 농도</th>
				<th class="text-center">시럽 종류</th>
				<th class="text-center">시럽 농도</th>
				<th class="text-center">얼음 분량</th>
				<th class="text-center">휘핑 크림</th>
				<th class="text-center">우유 온도</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<%--<div class="text-center">
		<ul class="pagination pagination-sm">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
		</ul>
	</div>--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.PAGE = 0;
				SearchInfo.ITEM_GROUP = "";
				SearchInfo.COFFEE_SHOT = "";
				SearchInfo.SYRUP_TYPE = "";
				SearchInfo.SYRUP_DENSITY = "";
				SearchInfo.ICE = "";
				SearchInfo.CREAM = "";
				SearchInfo.MILK_HOT = "";
				if ($("#selSearchCondition").val() == "ITEM_GROUP") SearchInfo.ITEM_GROUP = $("#txtSearchKeyword").val();
				if ($("#selSearchCondition").val() == "COFFEE_SHOT") SearchInfo.COFFEE_SHOT = $("#txtSearchKeyword").val();
				if ($("#selSearchCondition").val() == "SYRUP_TYPE") SearchInfo.SYRUP_TYPE = $("#txtSearchKeyword").val();
				if ($("#selSearchCondition").val() == "SYRUP_DENSITY") SearchInfo.SYRUP_DENSITY = $("#txtSearchKeyword").val();
				if ($("#selSearchCondition").val() == "ICE") SearchInfo.ICE = $("#txtSearchKeyword").val();
				if ($("#selSearchCondition").val() == "CREAM") SearchInfo.CREAM = $("#txtSearchKeyword").val();
				if ($("#selSearchCondition").val() == "MILK_HOT") SearchInfo.MILK_HOT = $("#txtSearchKeyword").val();

				GetMemberTasteList();
			});

			GetMemberTasteList();
		});

		function GetMemberTasteList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetMemberTasteList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							//strHtml += "<td class=\"text-center\">" + data.LIST[i].MEMBER_ID + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].MEMBER_NAME + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].ITEM_GROUP + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].COFFEE_SHOT + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].SYRUP_TYPE + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].SYRUP_DENSITY + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].ICE + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].CREAM + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].MILK_HOT + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"8\" class=\"text-center\">취향정보가 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblMemberTasteList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 100;
			SearchInfo.ITEM_GROUP = "";
			SearchInfo.COFFEE_SHOT = "";
			SearchInfo.SYRUP_TYPE = "";
			SearchInfo.SYRUP_DENSITY = "";
			SearchInfo.ICE = "";
			SearchInfo.CREAM = "";
			SearchInfo.MILK_HOT = "";
		}
	</script>
</asp:Content>

