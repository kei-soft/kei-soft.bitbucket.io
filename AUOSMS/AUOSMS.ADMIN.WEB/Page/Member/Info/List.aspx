﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">회원 정보 관리</h1>

	<div class="panel panel-default form-inline" style="padding: 5px; text-align: right; margin-top: 5px;">
		<select class="form-inline form-control" id="selSearchCondition" style="width: 100px;">
			<option value="NAME">회원명</option>
			<option value="PHONE">전화번호</option>
		</select>
		<input class="form-control" id="txtSearchKeyword" style="width: 200px;" placeholder="검색" />
		<a class="btn btn-success" id="btnSearch">조회</a>
	</div>

	 <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblMemberList">
		<thead>
			<tr>
				<%--<th class="text-center">회원코드</th>--%>
				<th class="text-center">이름</th>
				<th class="text-center">전화번호</th>
				<th class="text-center">등록일시</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<%--<div class="text-center">
		<ul class="pagination pagination-sm">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
		</ul>
	</div>--%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();
			$("#btnSearch").click(function () {
				SearchInfo.PAGE = 0;
				if ($("#selSearchCondition").val() == "NAME") {
					SearchInfo.KEYWORD_NAME = $("#txtSearchKeyword").val();
					SearchInfo.KEYWORD_PHONE = "";
				}
				else if ($("#selSearchCondition").val() == "PHONE") {
					SearchInfo.KEYWORD_NAME = "";
					SearchInfo.KEYWORD_PHONE = $("#txtSearchKeyword").val();
				}
				GetMemberInfoList();
			});

			GetMemberInfoList();
		});

		function GetMemberInfoList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetMemberInfoList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							//strHtml += "<td class=\"text-center\">" + data.LIST[i].MEMBER_ID + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].MEMBER_NAME + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].PHONE + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].CREATE_DATE + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">회원목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblMemberList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 100;
			SearchInfo.KEYWORD_NAME = "";
			SearchInfo.KEYWORD_PHONE = "";
		}
	</script>
</asp:Content>

