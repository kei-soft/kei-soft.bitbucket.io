﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/KIOSKNewForm.ascx" TagPrefix="uc1" TagName="KIOSKNewForm" %>


<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">KIOSK 관리</h1>
	
    <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblKIOSKList">
		<colgroup>
			<col style="width: 300px;" />
			<col />
			<col style="width: 200px;" />
			<col style="width: 100px;" />
			<col style="width: 200px;" />
		</colgroup>
		<thead>
			<tr>
				<th class="text-center">ID</th>
				<th class="text-center">키오스크명</th>
				<th class="text-center">등록상점</th>
				<th class="text-center">등록일시</th>
				<th class="text-center"></th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<div class="text-right" style="margin-bottom: 20px;">
		<a class="btn btn-primary" id="btnNewKIOSK">키오스크 추가</a>
	</div>
	
	<!-- 매장 목록 -->
	<div class="modal fade" role="dialog" id="divStoreSearchList">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">매장 등록</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblStoreSearchList">
						<colgroup>
							<col style="width:200px;" />
							<col />
							<col style="width:100px;" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">상점명</th>
								<th class="text-center">주소</th>
								<th class="text-center">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>

	<uc1:KIOSKNewForm runat="server" ID="KIOSKNewForm" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};
		var AddStore_KioskID = null;

		$(document).ready(function () {
			$("#btnNewKIOSK").click(function () {
				KIOSKNewForm.open(null, null, function () {
					GetKIOSKList();
				});
			});

			$("#tblKIOSKList > tbody").on("click", "button.kiosk_edit", function () {
				KIOSKNewForm.open(null, $(this).data("kiosk"), function () {
					GetKIOSKList();
				});
			});
			$("#tblKIOSKList > tbody").on("click", "button.kiosk_delete", function () {
				if (!confirm("키오스크를 삭제하시겠습니까?")) {
					return;
				}

				var params = {};
				params.ID = $(this).data("kiosk");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "DeleteKIOSK",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetKIOSKList();
						}
						else {
							alert("오류!");
						}
					}
				});

			});
			$("#tblKIOSKList > tbody").on("click", "button.store_add", function () {
				AddStore_KioskID = $(this).data("kiosk");
				GetStoreSearchList();
			});
			$("#tblStoreSearchList > tbody").on("click", "button.select_store", function () {
				if (confirm("매장을 등록 하시겠습니까?")) {
					var params = {};
					params.KIOSK_ID = AddStore_KioskID;
					params.STORE_ID = $(this).data("store");

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "InsertStoreKIOSK",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								$("#divStoreSearchList").modal("hide");
								GetKIOSKList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			$("#tblKIOSKList > tbody").on("click", "button.store_delete", function () {
				if (!confirm("키오스크에서 매장정보를 삭제하시겠습니까?")) {
					return;
				}

				var params = {};
				params.ID = $(this).data("kiosk");

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "UpdateKIOSKStore",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							GetKIOSKList();
						}
						else {
							alert("오류!");
						}
					}
				});
			});

			InitSearchInfo();
			GetKIOSKList();
		});

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 100;
		}

		function GetKIOSKList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetKIOSKList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].KIOSK_ID + "</td>"
							strHtml += "<td class=\"text-center\">" + data.LIST[i].KIOSK_NAME + "</td>"
							strHtml += "<td class=\"text-center\">" + data.LIST[i].STORE_NAME + "</td>"
							strHtml += "<td class=\"text-center\">" + data.LIST[i].CREATE_DATE.substr(0, 10) + "</td>"
							strHtml += "<td class=\"text-center\">"
							strHtml += "	<button class=\"btn btn-primary btn-xs kiosk_edit\" data-kiosk=\"" + data.LIST[i].KIOSK_ID + "\">수정</button>";
							strHtml += "	<button class=\"btn btn-danger btn-xs kiosk_delete\" data-kiosk=\"" + data.LIST[i].KIOSK_ID + "\">삭제</button>";
							if (data.LIST[i].STORE_NAME == "") {
							strHtml += "	<button class=\"btn btn-success btn-xs store_add\" data-kiosk=\"" + data.LIST[i].KIOSK_ID + "\">매장등록</button>";
							}
							else {
							strHtml += "	<button class=\"btn btn-warning btn-xs store_delete\" data-kiosk=\"" + data.LIST[i].KIOSK_ID + "\">매장삭제</button>";
							}
							strHtml += "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"5\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblKIOSKList > tbody").html(strHtml);
				}
			});
		}

		function GetStoreSearchList() {
			var params = {};
			params.PAGE_SIZE = 100;
			params.PAGE = 0;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerStoreSearchList", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetStoreList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].STORE_NAME + "</td>"
							strHtml += "<td class=\"\">" + data.LIST[i].ADDRESS + " " + data.LIST[i].ADDRESS_DETAIL + "</td>"
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs select_store\" data-store=\"" + data.LIST[i].STORE_ID + "\">선택</button></td>"
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">선택할 매장이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblStoreSearchList > tbody").html(strHtml);
				}
			});
			$("#divStoreSearchList").modal();
		}
	</script>
</asp:Content>

