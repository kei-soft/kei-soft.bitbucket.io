﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/Board/Board.ascx" TagPrefix="uc1" TagName="Board" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	
	<h1 class="page-header">공지사항</h1>

	<uc1:Board runat="server" ID="Board" 
		Board_Type="NOTICE" 
		Page_Size="10" 
		Store_Search_YN="N" 
		LIST_TYPE="ALL"
		WRITABLE="Y"
		REPLY_WRITABLE="Y"
		NOTICE_YN="Y"
		REPLY_CNT="0"
		FILE_SIZE="1"
	/>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
</asp:Content>