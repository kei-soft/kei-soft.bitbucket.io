﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			관리자 정보
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-warning btn-xs" id="btnToChangPW">비밀번호변경</button>
					<button type="button" class="btn btn-info btn-xs" id="btnToEdit">수정</button>
					<button type="button" class="btn btn-danger btn-xs" id="btnDelete">삭제</button>
					<button type="button" class="btn btn-primary btn-xs" id="btnToList">목록</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<form role="form">
                <fieldset>
                    <div class="form-group">
						<label>관리자 이름</label>
						<input class="form-control readonly" id="vwManagerName" type="text" value="">
                    </div>
                    <div class="form-group">
						<label>관리자 권한</label>
						<input class="form-control readonly" id="vwManagerGroup" type="text" value="">
                    </div>
                    <div class="form-group">
						<label>관리자 아이디</label>
						<input class="form-control readonly" id="vwManagerID" type="text" value="">
                    </div>
                </fieldset>
            </form>
		</div>
		<!-- /.panel-body -->
	</div>

	<div class="panel panel-default" style="margin-top: 20px;">
		<div class="panel-heading">
			매장 관리
			<div class="pull-right">
				<div class="btn-group">
					<button type="button" class="btn btn-warning btn-xs" id="btnStoreSearch">매장검색</button>
				</div>
			</div>
		</div>
		<!-- /.panel-heading -->
		<div class="panel-body">
			<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblStoreList">
				<colgroup>
					<col style="width:200px;" />
					<col />
					<col style="width:100px;" />
				</colgroup>
				<thead>
					<tr>
						<th class="text-center">상점명</th>
						<th class="text-center">주소</th>
						<th class="text-center">삭제</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<!-- /.panel-body -->
	</div>

	<!-- 비밀번호 변경 -->
	<div class="modal fade" role="dialog" id="divChangeManagerPW">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">비밀번호 변경 </h4>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="txtPWD">비밀번호 : </label>
							<input type="password" class="form-control" placeholder="새 비밀번호" id="txtPWD" />
						</div>
						<div class="form-group">
							<label for="txtPWDConfirm">비밀번호 확인 : </label>
							<input type="password" class="form-control" placeholder="비밀번호 확인" id="txtPWDConfirm" />
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnManagerPWSave">저장</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>	

	<!-- 정보 변경 -->
	<div class="modal fade" role="dialog" id="divChangeManagerInfo">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">관리자 수정</h4>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label for="txtName">관리자이름 : </label>
							<input type="text" class="form-control" placeholder="NAME" id="txtName" />
						</div>
						<div class="form-group">
							<label for="selGroup">관리자권한 : </label>
							<select class="form-control common_code" data-group="MANAGER_GROUP" data-select="eng" id="selGroup"></select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnManagerInfoSave">저장</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>
	
	<!-- 매장 목록 -->
	<div class="modal fade" role="dialog" id="divStoreSearchList">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">상점 검색</h4>
				</div>
				<div class="modal-body" style="min-height:500px;">
					<table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblStoreSearchList">
						<colgroup>
							<col style="width:200px;" />
							<col />
							<col style="width:100px;" />
						</colgroup>
						<thead>
							<tr>
								<th class="text-center">상점명</th>
								<th class="text-center">주소</th>
								<th class="text-center">선택</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> 닫기</button>
				</div>
			</div>

		</div>
	</div>
	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var MANAGER_CODE = "<%= Request.QueryString["code"]%>";
		var ManagerInfo;
		$(document).ready(function () {
			$("#btnToChangPW").click(function () {
				$("#txtPWD").val("");
				$("#txtPWDConfirm").val("");

				$("#divChangeManagerPW").modal();
			});
			$("#btnManagerPWSave").click(function () {
				var $e, $e2;
				$e = $("#txtPWD");
				if ($e.val().length < 4) {
					alert("패스워드를 4자 이상 입력하세요.");
					$e.focus();
					return;
				}

				$e2 = $("#txtPWDConfirm");
				if ($e.val() != $e2.val()) {
					alert("패스워드가 맞지 않습니다.");
					$e2.focus();
					return;
				}

				var params = {};
				params.PATH = "ADMIN";
				params.CODE = MANAGER_CODE;
				params.PW = $e.val();

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "UpdateManagerPassword",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							alert("비밀번호를 변경했습니다.");
							$("#divChangeManagerPW").modal("hide");
						}
						else {
							alert("오류!");
						}
					}
				});
			});
			
			$("#btnToEdit").click(function () {
				$("#txtName").val(ManagerInfo.MANAGER_NAME);
				$("#selGroup").val(ManagerInfo.MANAGER_GROUP);

				$("#divChangeManagerInfo").modal();
			});
			$("#btnManagerInfoSave").click(function () {
				var $e, $e2;
				$e = $("#txtName");
				if ($e.val() == "") {
					alert("관리자 이름을 선택하세요.");
					$e.focus();
					return false;
				}

				$e = $("#selGroup");
				if ($e.val() == "") {
					alert("관리자 권한을 선택하세요.");
					$e.focus();
					return false;
				}

				var params = {};
				params.CODE = MANAGER_CODE;
				params.GROUP = $("#selGroup").val();
				params.NAME = $("#txtName").val();

				do_cmd.ajax({
					URL: contextPath + "Ajax/Json.aspx",
					async: true,
					Command: "UpdateManagerInfo",
					Params: params,
					SuccessCallBack: function (data) {
						if (data) {
							alert("관리자 정보가 변경되었습니다.");
							$("#divChangeManagerInfo").modal("hide");
							GetMangerInfo();
						}
						else {
							alert("오류!");
						}
					}
				});
			});
			
			$("#btnDelete").click(function () {
				if (confirm("관리자를 삭제하시겠습니까?")) {
					var params = {};
					params.CODE = MANAGER_CODE;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteManager",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("삭제 되었습니다.");
								location.href = "LIst.aspx";
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			
			$("#btnToList").click(function () {
				location.href = "LIst.aspx";
			});

			$("#btnStoreSearch").click(function () {
				GetStoreSearchList();
			});

			GetMangerInfo();
			GetManagerStoreList();

			$("#tblStoreList").on("click", "button.delete_store", function () {
				if (confirm("삭제 하시겠습니까?")) {
					var params = {};
					params.STORE_ID = $(this).data("store");
					params.MANAGER_CODE = MANAGER_CODE;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "DeleteManagerStore",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("매장이 삭제 되었습니다.");
								GetManagerStoreList();
							}
							else {
								alert("오류!");
							}
						}
					});
				}
			});
			
			$("#tblStoreSearchList").on("click", "button.select_store", function () {
				if (confirm("선택 하시겠습니까?")) {
					var params = {};
					params.STORE_ID = $(this).data("store");
					params.MANAGER_CODE = MANAGER_CODE;

					do_cmd.ajax({
						URL: contextPath + "Ajax/Json.aspx",
						async: true,
						Command: "InsertManagerStore",
						Params: params,
						SuccessCallBack: function (data) {
							if (data) {
								alert("매장이 추가 되었습니다.");
								$("#divStoreSearchList").modal("hide");
								GetManagerStoreList();
							}
							else {
								alert("매장은 하나만 추가할 수 없습니다.");
								//alert("오류!");
							}
						}
					});
				}
			});
		});

		function GetMangerInfo() {
			var params = {};
			params.CODE = MANAGER_CODE;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerInfo", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetManagerInfo",
				Params: params,
				SuccessCallBack: function (data) {
					if (data.length > 0) {
						ManagerInfo = data[0];
						$("#vwManagerName").val(ManagerInfo.MANAGER_NAME);
						$("#vwManagerGroup").val(ManagerInfo.GROUP_NAME);
						$("#vwManagerID").val(ManagerInfo.MANAGER_ID);
					}
					else {
						alert("잘못된 관리자 코드입니다.");
						location.href = "LIst.aspx";
					}
				}
			});

		}

		function GetManagerStoreList() {
			var params = {};
			params.CODE = MANAGER_CODE;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerInfo", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetManagerStoreList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].STORE_NAME + "</td>"
							strHtml += "<td class=\"\">" + data[i].ADDRESS + " " + data[i].ADDRESS_DETAIL + "</td>"
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs delete_store\" data-store=\"" + data[i].STORE_ID + "\">삭제</button></td>"
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblStoreList > tbody").html(strHtml);
				}
			});
		}

		function GetStoreSearchList() {
			var params = {};
			params.CODE = MANAGER_CODE;

			//ajaxTestPopup("/Ajax/Json.aspx", "AjaxTest", "GetManagerStoreSearchList", params); return;

			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetManagerStoreSearchList",
				Params: params,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\">" + data[i].STORE_NAME + "</td>"
							strHtml += "<td class=\"\">" + data[i].ADDRESS + " " + data[i].ADDRESS_DETAIL + "</td>"
							strHtml += "<td class=\"text-center\"><button class=\"btn btn-danger btn-xs select_store\" data-store=\"" + data[i].STORE_ID + "\">선택</button></td>"
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"3\" class=\"text-center\">선택할 매장이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblStoreSearchList > tbody").html(strHtml);
				}
			});
			$("#divStoreSearchList").modal();
		}

	</script>
</asp:Content>

