﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<%@ Register Src="~/Include/ManagerNewForm.ascx" TagPrefix="uc1" TagName="ManagerNewForm" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	<h1 class="page-header">관리자</h1>

	<%--<div class="panel panel-default" style="padding: 5px; text-align: right;">
		<input class="form-control datepicker readonly" name="txtFR_DATE" id="txtFR_DATE" type="text" style="width: 100px;">
		~
		<input class="form-control datepicker readonly" name="txtTO_DATE" id="txtTO_DATE" type="text" style="width: 100px;">
		<a class="btn btn-success" id="btnSearch">조회</a>
	</div>--%>
	
    <table style="width: 100%" class="table table-striped table-bordered table-hover" id="tblManagerList">
		<thead>
			<tr>
				<th class="text-center">관리자명</th>
				<th class="text-center">관리자ID</th>
				<th class="text-center">관리권한</th>
				<th class="text-center">등록일시</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>

	<%--<div class="text-center">
		<ul class="pagination pagination-sm">
			<li><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
		</ul>
	</div>--%>

	<div class="text-right">
		<a class="btn btn-primary" id="btnNewManager">관리자생성</a>
	</div>

	<uc1:ManagerNewForm runat="server" ID="ManagerNewForm" />

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
	<script>
		var SearchInfo = {};

		$(document).ready(function () {
			InitSearchInfo();

			$("#btnNewManager").click(function () {
				ManagerNewForm.open(function () {
					InitSearchInfo();
					GetManagerList();
				});
			});

			GetManagerList();
		});

		function GetManagerList() {
			do_cmd.ajax({
				URL: contextPath + "Ajax/Json.aspx",
				async: true,
				Command: "GetManagerList",
				Params: SearchInfo,
				SuccessCallBack: function (data) {
					var strHtml = "";
					if (data.LIST.length > 0) {
						for (var i = 0; i < data.LIST.length; i++) {
							strHtml += "<tr>";
							strHtml += "<td class=\"text-center\"><a href=\"View.aspx?code=" + data.LIST[i].MANAGER_CODE + "\">" + data.LIST[i].MANAGER_NAME + "</a></td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].MANAGER_ID + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].CODE_NAME + "</td>";
							strHtml += "<td class=\"text-center\">" + data.LIST[i].CREATE_DATE + "</td>";
							strHtml += "</tr>";
						}
					}
					else {
						strHtml += "<tr>";
						strHtml += "<td colspan=\"4\" class=\"text-center\">목록이 없습니다.</td>"
						strHtml += "</tr>";
					}

					$("#tblManagerList > tbody").html(strHtml);
				}
			});
		}

		function InitSearchInfo() {
			SearchInfo.PAGE = 0;
			SearchInfo.PAGE_SIZE = 100;
		}
	</script>
</asp:Content>