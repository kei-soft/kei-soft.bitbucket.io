﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">
	<br />
    <div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">패스워드 변경</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <fieldset>
                                <div class="form-group">
									<label>기존 패스워드</label>
                                    <input class="form-control" placeholder="기존 패스워드" name="txtPasswordOLD" id="txtPasswordOLD" type="password" autofocus>
                                </div>
                                <div class="form-group">
									<label>패스워드</label>
                                    <input class="form-control" placeholder="패스워드" name="txtPassword1" id="txtPassword1" type="password" value="">
                                </div>
                                <div class="form-group">
									<label>패스워드 확인</label>
                                    <input class="form-control" placeholder="패스워드 확인" name="txtPassword2" id="txtPassword2" type="password" value="">
                                </div>
                                <a class="btn btn-lg btn-success btn-block" id="btnChange">변경</a>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
</asp:Content>