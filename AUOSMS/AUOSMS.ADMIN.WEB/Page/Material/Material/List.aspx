﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Admin.master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="Page_Material_Material_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyContentPlaceHolder" Runat="Server">

	<h2 id="viewTitle"><span></span><small> 품목 관리</small></h2>

	<ul class="nav nav-tabs nav-justified" style="margin-bottom: 20px;">
		<asp:Literal runat="server" ID="litTabs"></asp:Literal>
	</ul>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footContentPlaceHolder" Runat="Server">
</asp:Content>