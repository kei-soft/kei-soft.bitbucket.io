﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using AUOSMS.ADMIN.UTIL.Log;

/// <summary>
/// LOG의 요약 설명입니다.
/// </summary>
public static class LOG
{
	public static void Write(HttpRequest req, string str, params object[] args)
	{
		Write(req, string.Format(str, args));
	}

	public static void Write(HttpRequest req ,string str)
	{
		bool useLog = Convert.ToBoolean(WebConfigurationManager.AppSettings["WebLogUse"]);
		if (useLog)
		{
			ProgramLogger.SetLogPath(WebConfigurationManager.AppSettings["WebLogPath"]);
			ProgramLogger.WriteLog(string.Format("({0}) {1}", req.UserHostAddress, str));
		}
	}
}