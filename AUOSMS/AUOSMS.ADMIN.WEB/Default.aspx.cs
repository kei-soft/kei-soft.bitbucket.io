﻿using AUOSMS.ADMIN.UTIL.Authentication;

using System;

public partial class _Default : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
			this.CheckLogin();
	}

	private void CheckLogin()
	{
		if (AUOSMSUser.IsLogin())
		{
			Response.Redirect(Page.ResolveUrl("~") + "Page/Admin/LIst.aspx");
		}
	}
}