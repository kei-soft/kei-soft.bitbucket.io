﻿using AUOSMS.ADMIN.UTIL.Authentication;

using System;
using System.Text;

public partial class Common_Admin : System.Web.UI.MasterPage
{
	protected string contextPath;

	protected void Page_Load(object sender, EventArgs e)
	{
		contextPath = Page.ResolveUrl("~");

		if (!IsPostBack)
			this.PageInit();
	}

	void PageInit()
	{
		string url = Request.Url.AbsolutePath.ToLower();
		//url = url.Substring(url.LastIndexOf("/") + 1).ToLower();

		StringBuilder sbMenu = new StringBuilder();
		if (!AUOSMSUser.IsLogin())
		{
			Response.Redirect(contextPath + "Default.aspx");
		}
		else
		{
			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/admin/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Admin/List.aspx\"><i class=\"fa fa-user-md fa-fw\"></i> 관리자 관리</a></li>");

			//sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/intro/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Intro/List.aspx\"><i class=\"fa fa-bell fa-fw\"></i> INTRO  관리</a></li>");

			sbMenu.AppendLine("<li><a style=\"cursor:pointer;\"><i class=\"fa fa-list-alt fa-fw\"></i> 게시판<span class=\"fa arrow\"></span></a>");
			sbMenu.AppendLine("<ul class=\"nav nav-second-level collapse" + (url.StartsWith("/page/board/") ? " in" : "") + "\">");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/board/notice/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Board/Notice/List.aspx\">공지사항</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/board/channe/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Board/Channel/List.aspx\">소통채널</a></li>");
			sbMenu.AppendLine("</ul>");
			sbMenu.AppendLine("</li>");

			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/shop/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Shop/List.aspx\"><i class=\"fa fa-home fa-fw\"></i> 매장 관리</a></li>");

			sbMenu.AppendLine("<li><a style=\"cursor:pointer;\"><i class=\"fa fa-user fa-fw\"></i> 회원 관리<span class=\"fa arrow\"></span></a>");
			sbMenu.AppendLine("<ul class=\"nav nav-second-level collapse" + (url.StartsWith("/page/member/") ? " in" : "") + "\">");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/member/info/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Member/Info/List.aspx\">회원 정보 관리</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/member/favorite/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Member/Favorite/List.aspx\">회원 취향 정보 관리</a></li>");
			sbMenu.AppendLine("</ul>");
			sbMenu.AppendLine("</li>");

			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/kiosk/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/KIOSK/List.aspx\"><i class=\"fa fa-building fa-fw\"></i> 키오스크 관리</a></li>");

			sbMenu.AppendLine("<li><a" + (url.StartsWith("/page/pos/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/POS/List.aspx\"><i class=\"fa fa-desktop fa-fw\"></i> 단말 관리</a></li>");

			sbMenu.AppendLine("<li><a style=\"cursor:pointer;\"><i class=\"fa fa-file-text fa-fw\"></i> 메뉴 관리<span class=\"fa arrow\"></span></a>");
			sbMenu.AppendLine("<ul class=\"nav nav-second-level collapse" + (url.StartsWith("/page/menu/") ? " in" : "") + "\">");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/menu/list.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Menu/List.aspx\">메뉴 관리</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/menu/category.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Menu/Category.aspx\">메뉴 카테고리 관리</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/menu/option.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Menu/Option.aspx\">메뉴 옵션 관리</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/menu/recommend.aspx") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Menu/Recommend.aspx\">추천 메뉴 관리</a></li>");
			sbMenu.AppendLine("</ul>");
			sbMenu.AppendLine("</li>");

			sbMenu.AppendLine("<li><a style=\"cursor:pointer;\"><i class=\"fa fa-file-text fa-fw\"></i> 재료 관리<span class=\"fa arrow\"></span></a>");
			sbMenu.AppendLine("<ul class=\"nav nav-second-level collapse" + (url.StartsWith("/page/material/") ? " in" : "") + "\">");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/material/material/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Material/Material/List.aspx\">품목 관리</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/material/in/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Material/In/List.aspx\">매입 관리</a></li>");
			sbMenu.AppendLine("	<li><a" + (url.StartsWith("/page/material/out/") ? " class=\"active\"" : "") + " href=\"" + contextPath + "Page/Material/Out/List.aspx\">출고 관리</a></li>");
			sbMenu.AppendLine("</ul>");
			sbMenu.AppendLine("</li>");
		}
		this.litSideMenu.Text = sbMenu.ToString();

		//Response.Write(Request.Url.AbsolutePath);
	}
}
