﻿using AUOSMS.ADMIN.UTIL;
using AUOSMS.ADMIN.UTIL.Authentication;
using AUOSMS.ADMIN.UTIL.DataBase;
using AUOSMS.ADMIN.UTIL.DataBase.DBAgent;

using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Configuration;

public partial class Ajax_Json : System.Web.UI.Page
{
	string connectionString = ConfigurationManager.ConnectionStrings["AUOSMS_MYSQL"].ConnectionString;

	protected void Page_Load(object sender, EventArgs e)
	{
		AJAX.Add3P3Header(Response);

		object jsonData;
		try
		{
			jsonData = AJAX.CheckReferrer(Request);
			if (jsonData == null)
			{
				LOG.Write(Request, "AJAX START - CMD : {0}, PARAMS : {1}", Request.Form["Command"], Request.Form["Params"]);

				switch (Request.Form["Command"])
				{
					case "ChkManagerLogin": // 로그인처리
						jsonData = ChkManagerLogin(JObject.Parse(Request.Form["Params"]));
						break;
					case "LogOut": // 로그아웃처리
						jsonData = AJAX.CheckPermission(1);
						if (jsonData == null)
							jsonData = LogOut();
						break;
					case "GetCommonCodeList": // 공통코드 목록
						jsonData = AJAX.CheckPermission(1);
						if (jsonData == null)
							jsonData = GetCommonCodeList(JObject.Parse(Request.Form["Params"]));
						break;
					#region 관리자
					case "ChkManagerID": // ID중복검사
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = ChkManagerID(JObject.Parse(Request.Form["Params"]));
						break;
					case "ChkManagerPWD": // 기존비밀번호 검사
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = ChkManagerPWD(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertManager": // 관리자 입력
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertManager(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateManagerInfo": // 관리자 정보수정
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateManagerInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateManagerPassword": // 관리자 비밀번호 변경
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateManagerPassword(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteManager": // 관리자 삭제
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteManager(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetManagerList": // 관리자 목록
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetManagerList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetManagerInfo": // 관리자 정보
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetManagerInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetManagerStoreList": // 관리자 매장 매칭정보 목록
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetManagerStoreList(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteManagerStore": // 관리자 매장 매칭정보 삭제
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteManagerStore(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertManagerStore": // 관리자 매장 매칭정보 저장
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertManagerStore(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetManagerStoreSearchList": // 관리자 매칭할 매장 목록 
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetManagerStoreSearchList(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region 매장
					case "GetStoreList": // 
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetStoreList(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertStore": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertStore(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "GetStoreInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteStore": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteStore(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateStoreInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateStoreInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "GetStoreManagerList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreManagerList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreManagerSearchList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreManagerSearchList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreKioskList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreKioskList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreKioskSearchList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreKioskSearchList();
						break;
					case "InsertStoreKIOSK": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertStoreKIOSK(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteStoreKIOSK": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteStoreKIOSK(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStorePosList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStorePosList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStorePosSearchList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStorePosSearchList();
						break;
					case "InsertStorePOS": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertStorePOS(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteStorePOS": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteStorePOS(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreItemList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreItemList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreItemInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreItemInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateStoreItemLimit": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateStoreItemLimit(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateStoreItemSort": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateStoreItemSort(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteStoreItem": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteStoreItem(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreItemSearchList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreItemSearchList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreSaleList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreSaleList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreOrderList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreOrderList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreSaleByItemList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreSaleByItemList(JObject.Parse(Request.Form["Params"]));
						break;
					#region Intro 관리
					case "GetIntroImageList": // 
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetIntroImageList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetIntroImageInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetIntroImageInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertIntroImage": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertIntroImage(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "UpdateIntroImageInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateIntroImageInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "DeleteIntroImage": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteIntroImage(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateIntroImageSort": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateIntroImageSort(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region DID 관리
					case "GetDIDImageList": // 
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetDIDImageList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetDIDImageInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetDIDImageInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertDIDImage": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertDIDImage(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "UpdateDIDImageInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateDIDImageInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "DeleteDIDImage": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteDIDImage(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateDIDImageSort": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateDIDImageSort(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region EVENT
					case "GetStoreEventList": // 
						jsonData = AJAX.CheckPermission(50);
						if (jsonData == null)
							jsonData = GetStoreEventList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetStoreEventInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetStoreEventInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertStoreEvent": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertStoreEvent(JObject.Parse(Request.Form["Params"]), Request.Files["Files1"], Request.Files["Files2"]);
						break;
					case "UpdateStoreEventInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateStoreEventInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files1"], Request.Files["Files2"]);
						break;
					case "DeleteStoreEvent": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteStoreEvent(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetOrderItemGroupList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetOrderItemGroupList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetOrderMenuList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetOrderMenuList(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#endregion
					#region 회원관리
					case "GetMemberInfoList": // 공통코드 목록
						jsonData = AJAX.CheckPermission(1);
						if (jsonData == null)
							jsonData = GetMemberInfoList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetMemberTasteList": // 공통코드 목록
						jsonData = AJAX.CheckPermission(1);
						if (jsonData == null)
							jsonData = GetMemberTasteList(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region 메뉴 관리
					case "GetItemList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemGroupList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemGroupList();
						break;
					case "InsertItem": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertItem(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "UpdateItemInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateItemInfo(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "DeleteItem": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteItem(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteItemCategory": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteItemCategory(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertItemCategory": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertItemCategory(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemSearchList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemSearchList(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdaeItemGroupSort": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdaeItemGroupSort(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteItemGroup": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteItemGroup(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertItemGroup": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertItemGroup(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateItemGroup": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateItemGroup(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetOptionList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetOptionList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetOptionInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetOptionInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteOption": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteOption(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertOption": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertOption(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "UpdateOption": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateOption(JObject.Parse(Request.Form["Params"]), Request.Files["Files"]);
						break;
					case "GetItemInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetOptionSearchList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetOptionSearchList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemOptionList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemOptionList(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertItemOption": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertItemOption(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdaeItemOptionSort": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdaeItemOptionSort(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteItemOptionSort": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteItemOptionSort(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemStoreList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemStoreList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemStoreSearchList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemStoreSearchList(JObject.Parse(Request.Form["Params"]));
						break;
					case "InsertItemStore": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertItemStore(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteItemStore": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteItemStore(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetItemRecommendList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetItemRecommendList(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateItemRecommend": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateItemRecommend(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region KIOSK
					case "InsertKIOSK": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertKIOSK(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetKIOSKList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetKIOSKList(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateKIOSKInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateKIOSKInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdateKIOSKStore": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdateKIOSKStore(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeleteKIOSK": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeleteKIOSK(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetKIOSKInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetKIOSKInfo(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					#region POS
					case "InsertPOS": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = InsertPOS(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetPOSList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetPOSList(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdatePOSInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdatePOSInfo(JObject.Parse(Request.Form["Params"]));
						break;
					case "UpdatePOSStore": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = UpdatePOSStore(JObject.Parse(Request.Form["Params"]));
						break;
					case "DeletePOS": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = DeletePOS(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetPOSInfo": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetPOSInfo(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion 매출관리
					#region 매출관리
					case "GetSaleReportByDaysList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetSaleReportByDaysList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByMonthList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetSaleReportByMonthList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByReceiptsList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetSaleReportByReceiptsList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByItemTimeList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetSaleReportByItemTimeList(JObject.Parse(Request.Form["Params"]));
						break;
					case "GetSaleReportByItemGroupList": // 
						jsonData = AJAX.CheckPermission(99);
						if (jsonData == null)
							jsonData = GetSaleReportByItemGroupList(JObject.Parse(Request.Form["Params"]));
						break;
					#endregion
					default:
						jsonData = new { STATUS = "WrongCommand", MSG = string.Format("잘못된 명령어입니다 ({0})", Request.Form["Command"]) };
						break;
				}
			}
		}
		catch (Exception ex)
		{
			jsonData = new { STATUS = "SystemError", MSG = ex.ToString() };
		}

		LOG.Write(Request, "AJAX RESULT - {0}", JsonConvertor.SerializeObject(jsonData));
		AJAX.PrintJson(Response, jsonData);
	}

	object ChkManagerLogin(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_PASS", MySQLDataType.VARCHAR, 40, CommFunc.EncryptSHA1(Params.Value<string>("PW"))));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_GROUP", MySQLDataType.VARCHAR, 40, "99"));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_CHK", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		bool Result;
		if (dt.Rows.Count > 0)
		{
			Result = true;

			AUOSMSUser.Set(dt);
		}
		else
		{
			Result = false;
		}

		return new { STATUS = "OK", RESULT = Result };
	}

	object LogOut()
	{
		if (AUOSMSUser.IsLogin())
		{
			AUOSMSUser.Expires();

			return new { STATUS = "OK", RESULT = true };
		}
		else
		{
			return new { STATUS = "OK", RESULT = false };
		}

	}

	object GetCommonCodeList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_CODE_GROUP", MySQLDataType.VARCHAR, 40, Params.Value<string>("CODE_GROUP")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_COMMON_CODE_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	#region 관리자

	object ChkManagerID(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));

		int idCnt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_ID_CHK", SQLParams))
		{
			idCnt = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = idCnt > 0 ? false : true };
	}

	object ChkManagerPWD(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_PASS", MySQLDataType.VARCHAR, 40, CommFunc.EncryptSHA1(Params.Value<string>("PW"))));

		int ipwCnt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_PWD_CHK", SQLParams))
		{
			ipwCnt = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = ipwCnt > 0 ? true : false };
	}

	object InsertManager(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_GROUP", MySQLDataType.VARCHAR, 40, Params.Value<string>("GROUP")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_PASS", MySQLDataType.VARCHAR, 40, CommFunc.EncryptSHA1(Params.Value<string>("PW"))));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateManagerInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, Params.Value<string>("CODE")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_GROUP", MySQLDataType.VARCHAR, 40, Params.Value<string>("GROUP")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_INFO_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateManagerPassword(JObject Params)
	{
		// 접근경로 - 본인 비밀번호변경(SELF) 또는 관리자가 비밀번호 초기화(ADMIN)
		string changePath = Params.Value<string>("PATH");

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, changePath.Equals("SELF") ? AUOSMSUser.GetCode() : Params.Value<string>("CODE")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_PASS", MySQLDataType.VARCHAR, 40, CommFunc.EncryptSHA1(Params.Value<string>("PW"))));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_PWD_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteManager(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, Params.Value<string>("CODE")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetManagerList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_GROUP", MySQLDataType.INT32, 10, AUOSMSUser.GetLevel()));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], DTCNT = ds.Tables.Count } };
	}

	object GetManagerInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, Params.Value<string>("CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetManagerStoreList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, Params.Value<string>("CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_STORE_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object DeleteManagerStore(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, Params.Value<string>("MANAGER_CODE")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_STORE_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object InsertManagerStore(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, Params.Value<string>("MANAGER_CODE")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_STORE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetManagerStoreSearchList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_MANAGER_CODE", MySQLDataType.INT32, 10, Params.Value<string>("CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_STORE_SEARCH_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}
	#endregion

	#region 매장관리
	object GetStoreList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/STORE/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object InsertStore(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/STORE/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_STORE_IMAGE", MySQLDataType.VARCHAR, 40, fileName));
		SQLParams.Add(new MySQLParamModel("P_POST_CODE", MySQLDataType.VARCHAR, 40, Params.Value<string>("PCODE")));
		SQLParams.Add(new MySQLParamModel("P_STATE", MySQLDataType.VARCHAR, 40, Params.Value<string>("STATE")));
		SQLParams.Add(new MySQLParamModel("P_CITY", MySQLDataType.VARCHAR, 40, Params.Value<string>("CITY")));
		SQLParams.Add(new MySQLParamModel("P_ADDRESS", MySQLDataType.VARCHAR, 40, Params.Value<string>("ADDR")));
		SQLParams.Add(new MySQLParamModel("P_ADDRESS_DETAIL", MySQLDataType.VARCHAR, 40, Params.Value<string>("ADDR_D")));
		SQLParams.Add(new MySQLParamModel("P_PHONE", MySQLDataType.VARCHAR, 40, Params.Value<string>("PHONE")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_ORDER_START_TIME", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDERSTART")));
		SQLParams.Add(new MySQLParamModel("P_ORDER_END_TIME", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDEREND")));
		SQLParams.Add(new MySQLParamModel("P_MEMO", MySQLDataType.VARCHAR, 100, Params.Value<string>("MEMO")));
		SQLParams.Add(new MySQLParamModel("P_LOC_LATITUDE", MySQLDataType.VARCHAR, 20, Params.Value<string>("LATITUDE")));
		SQLParams.Add(new MySQLParamModel("P_LOC_LONGITUDE", MySQLDataType.VARCHAR, 20, Params.Value<string>("LONGITUDE")));
		SQLParams.Add(new MySQLParamModel("P_BUSINESS_CODE", MySQLDataType.VARCHAR, 20, Params.Value<string>("BUSINESS_CODE")));
		SQLParams.Add(new MySQLParamModel("P_BUSINESS_OWNER", MySQLDataType.VARCHAR, 40, Params.Value<string>("BUSINESS_OWNER")));
		SQLParams.Add(new MySQLParamModel("P_PG_SIETCODE", MySQLDataType.VARCHAR, 100, Params.Value<string>("PG_SITECODE")));
		SQLParams.Add(new MySQLParamModel("P_PG_KEY", MySQLDataType.VARCHAR, 100, Params.Value<string>("PG_KEY")));
		SQLParams.Add(new MySQLParamModel("P_IMPORT_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("IMPORT_ID")));
		SQLParams.Add(new MySQLParamModel("P_IMPORT_PASS", MySQLDataType.VARCHAR, 40, Params.Value<string>("IMPORT_PASS")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetStoreInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/STORE/";

		return new { STATUS = "OK", RESULT = new { INFO= dt, FILEPATH = imgPath } };
	}

	object DeleteStore(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateStoreInfo(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/STORE/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_STORE_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_STORE_IMAGE", MySQLDataType.VARCHAR, 40, fileName));
		SQLParams.Add(new MySQLParamModel("P_POST_CODE", MySQLDataType.VARCHAR, 40, Params.Value<string>("PCODE")));
		SQLParams.Add(new MySQLParamModel("P_STATE", MySQLDataType.VARCHAR, 40, Params.Value<string>("STATE")));
		SQLParams.Add(new MySQLParamModel("P_CITY", MySQLDataType.VARCHAR, 40, Params.Value<string>("CITY")));
		SQLParams.Add(new MySQLParamModel("P_ADDRESS", MySQLDataType.VARCHAR, 40, Params.Value<string>("ADDR")));
		SQLParams.Add(new MySQLParamModel("P_ADDRESS_DETAIL", MySQLDataType.VARCHAR, 40, Params.Value<string>("ADDR_D")));
		SQLParams.Add(new MySQLParamModel("P_PHONE", MySQLDataType.VARCHAR, 40, Params.Value<string>("PHONE")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_ORDER_START_TIME", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDERSTART")));
		SQLParams.Add(new MySQLParamModel("P_ORDER_END_TIME", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDEREND")));
		SQLParams.Add(new MySQLParamModel("P_MEMO", MySQLDataType.VARCHAR, 100, Params.Value<string>("MEMO")));
		SQLParams.Add(new MySQLParamModel("P_LOC_LATITUDE", MySQLDataType.VARCHAR, 20, Params.Value<string>("LATITUDE")));
		SQLParams.Add(new MySQLParamModel("P_LOC_LONGITUDE", MySQLDataType.VARCHAR, 20, Params.Value<string>("LONGITUDE")));
		SQLParams.Add(new MySQLParamModel("P_BUSINESS_CODE", MySQLDataType.VARCHAR, 20, Params.Value<string>("BUSINESS_CODE")));
		SQLParams.Add(new MySQLParamModel("P_BUSINESS_OWNER", MySQLDataType.VARCHAR, 40, Params.Value<string>("BUSINESS_OWNER")));
		SQLParams.Add(new MySQLParamModel("P_PG_SIETCODE", MySQLDataType.VARCHAR, 100, Params.Value<string>("PG_SITECODE")));
		SQLParams.Add(new MySQLParamModel("P_PG_KEY", MySQLDataType.VARCHAR, 100, Params.Value<string>("PG_KEY")));
		SQLParams.Add(new MySQLParamModel("P_IMPORT_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("IMPORT_ID")));
		SQLParams.Add(new MySQLParamModel("P_IMPORT_PASS", MySQLDataType.VARCHAR, 40, Params.Value<string>("IMPORT_PASS")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetStoreManagerList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_MANAGER_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetStoreManagerSearchList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_MANAGER_SEARCH_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetStoreKioskList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_KIOSK_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetStoreKioskSearchList()
	{
		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_KIOSK_SEARCH_LST"))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertStoreKIOSK(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("KIOSK_ID")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_KIOSK_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStoreKIOSK(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("KIOSK_ID")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_KIOSK_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetStorePosList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_POS_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetStorePosSearchList()
	{
		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_POS_SEARCH_LST"))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertStorePOS(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("POS_ID")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_POS_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStorePOS(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("POS_ID")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_POS_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetStoreItemList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 100, Params.Value<string>("GROUP_CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { LIST = dt, FILEPATH = imgPath } };
	}

	object GetStoreItemInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object UpdateStoreItemLimit(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_LIMIT_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("LIMIT_YN")));
		SQLParams.Add(new MySQLParamModel("P_LIMIT_QUANTITY", MySQLDataType.INT32, 10, Params.Value<string>("LIMIT_QUANTITY")));
		SQLParams.Add(new MySQLParamModel("P_SOLDOUT_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("SOLDOUT_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_STORE_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateStoreItemSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID1", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID1")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID2", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID2")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStoreItem(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetStoreItemSearchList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 100, Params.Value<string>("GROUP_CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ITEM_SEARCH_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}
		
		return new { STATUS = "OK", RESULT = dt };
	}

	object GetStoreSaleList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_SALE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), TOTAL_PRICE = ds.Tables[0].Rows[0]["TOTAL_PRICE"].ToString(), LIST = ds.Tables[1] } };
	}

	object GetStoreOrderList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_ORDER_STATUS", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDER_STATUS")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_ORDER_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1] } };
	}

	object GetStoreSaleByItemList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 10, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_GROUP_CODE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_SALE_BY_ITEM_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL = ds.Tables[0], LIST = ds.Tables[1] } };
	}

	#region Intro 관리
	object GetIntroImageList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object GetIntroImageInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, FILEPATH = imgPath } };
	}

	object InsertIntroImage(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 40, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateIntroImageInfo(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/INTRO/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 100, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteIntroImage(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateIntroImageSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID1", MySQLDataType.INT32, 10, Params.Value<string>("ID1")));
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID2", MySQLDataType.INT32, 10, Params.Value<string>("ID2")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_INTRO_IMAGE_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region DID 관리
	object GetDIDImageList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object GetDIDImageInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, FILEPATH = imgPath } };
	}

	object InsertDIDImage(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 40, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateDIDImageInfo(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/DID/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_IMAGE_NAME", MySQLDataType.VARCHAR, 100, fileName));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteDIDImage(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_IMAGE_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateDIDImageSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID1", MySQLDataType.INT32, 10, Params.Value<string>("ID1")));
		SQLParams.Add(new MySQLParamModel("P_INTRO_IMAGE_ID2", MySQLDataType.INT32, 10, Params.Value<string>("ID2")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_DID_IMAGE_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region EVENT
	object GetStoreEventList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string itemImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";
		string eventImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], ITEMFILEPATH = itemImgPath, EVENTFILEPATH = eventImgPath } };
	}

	object GetStoreEventInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string itemImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";
		string eventImgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, ITEMFILEPATH = itemImgPath, EVENTFILEPATH = eventImgPath } };
	}

	object InsertStoreEvent(JObject Params, System.Web.HttpPostedFile file1, System.Web.HttpPostedFile file2)
	{
		string gift_type = Params.Value<string>("GIFT_TYPE");
		string gift_code = string.Empty;
		string event_image = string.Empty;

		if (file1 != null)
		{
			event_image = file1.FileName;
			event_image = event_image.Substring(event_image.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
			event_image = CommFunc.GetFileName(dirPath, event_image);
			file1.SaveAs(dirPath + event_image);
		}

		if (gift_type.Equals("MENU"))
		{
			gift_code = Params.Value<string>("GIFT_CODE");
		}
		else
		{
			if (file2 != null)
			{
				gift_code = file2.FileName;
				gift_code = gift_code.Substring(gift_code.LastIndexOf("\\") + 1);
				string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
				gift_code = CommFunc.GetFileName(dirPath, gift_code);
				file2.SaveAs(dirPath + gift_code);
			}
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("EVENT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_INFO_IMAGE", MySQLDataType.VARCHAR, 100, event_image));
		SQLParams.Add(new MySQLParamModel("P_EVENT_START_DATE", MySQLDataType.DATE, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_END_DATE", MySQLDataType.DATE, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_TYPE", MySQLDataType.VARCHAR, 100, gift_type));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_CODE", MySQLDataType.VARCHAR, 100, gift_code));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("GIFT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_TYPE", MySQLDataType.VARCHAR, 100, Params.Value<string>("CONDITION_TYPE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_VALUE", MySQLDataType.INT32, 10, Params.Value<string>("CONDITION_VALUE")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateStoreEventInfo(JObject Params, System.Web.HttpPostedFile file1, System.Web.HttpPostedFile file2)
	{
		string gift_type = Params.Value<string>("GIFT_TYPE");
		string gift_code = string.Empty;
		string event_image = string.Empty;

		if (file1 != null)
		{
			event_image = file1.FileName;
			event_image = event_image.Substring(event_image.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
			event_image = CommFunc.GetFileName(dirPath, event_image);
			file1.SaveAs(dirPath + event_image);
		}

		if (gift_type.Equals("MENU"))
		{
			gift_code = Params.Value<string>("GIFT_CODE");
		}
		else
		{
			if (file2 != null)
			{
				gift_code = file2.FileName;
				gift_code = gift_code.Substring(gift_code.LastIndexOf("\\") + 1);
				string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/EVENT/");
				gift_code = CommFunc.GetFileName(dirPath, gift_code);
				file2.SaveAs(dirPath + gift_code);
			}
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("EVENT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_INFO_IMAGE", MySQLDataType.VARCHAR, 100, event_image));
		SQLParams.Add(new MySQLParamModel("P_EVENT_START_DATE", MySQLDataType.DATE, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_END_DATE", MySQLDataType.DATE, 10, Params.Value<string>("END_DATE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_TYPE", MySQLDataType.VARCHAR, 100, gift_type));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_CODE", MySQLDataType.VARCHAR, 100, gift_code));
		SQLParams.Add(new MySQLParamModel("P_EVENT_GIFT_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("GIFT_NAME")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_TYPE", MySQLDataType.VARCHAR, 100, Params.Value<string>("CONDITION_TYPE")));
		SQLParams.Add(new MySQLParamModel("P_EVENT_CONDITION_VALUE", MySQLDataType.INT32, 10, Params.Value<string>("CONDITION_VALUE")));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteStoreEvent(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_EVENT_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_EVENT_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetOrderItemGroupList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ORDER_ITEM_GROUP_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetOrderMenuList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 10, Params.Value<string>("GROUP_CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ORDER_MENU_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { LIST = dt, FILEPATH = imgPath } };
	}
	#endregion

	#endregion

	#region 회원관리
	object GetMemberInfoList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("KEYWORD_NAME")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_PHONE", MySQLDataType.VARCHAR, 100, Params.Value<string>("KEYWORD_PHONE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MEMBER_INFO_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1] } };
	}

	object GetMemberTasteList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_ITEM_GROUP", MySQLDataType.VARCHAR, 100, Params.Value<string>("ITEM_GROUP")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_COFFEE_SHOT", MySQLDataType.VARCHAR, 100, Params.Value<string>("COFFEE_SHOT")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_SYRUP_TYPE", MySQLDataType.VARCHAR, 100, Params.Value<string>("SYRUP_TYPE")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_SYRUP_DENSITY", MySQLDataType.VARCHAR, 100, Params.Value<string>("SYRUP_DENSITY")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_ICE", MySQLDataType.VARCHAR, 100, Params.Value<string>("ICE")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_CREAM", MySQLDataType.VARCHAR, 100, Params.Value<string>("CREAM")));
		SQLParams.Add(new MySQLParamModel("P_KEYWORD_MILK_HOT", MySQLDataType.VARCHAR, 100, Params.Value<string>("MILK_HOT")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MEMBER_TASTE_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1] } };
	}
	#endregion

	#region 메뉴 관리
	object GetItemList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 100, Params.Value<string>("GROUP_CODE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object GetItemGroupList()
	{
		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GROUP_LIST"))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertItem(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ENG_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("ENG_NAME")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_DESC", MySQLDataType.VARCHAR, 2000, Params.Value<string>("DESC")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_IMAGE", MySQLDataType.VARCHAR, 200, fileName));
		SQLParams.Add(new MySQLParamModel("P_ITEM_PRICE", MySQLDataType.INT32, 10, Params.Value<string>("PRICE")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_NUTRITION", MySQLDataType.VARCHAR, 200, Params.Value<string>("NUTRITION")));
		SQLParams.Add(new MySQLParamModel("P_ORIGIN", MySQLDataType.VARCHAR, 200, Params.Value<string>("ORIGIN")));
		SQLParams.Add(new MySQLParamModel("P_ALLERGIE", MySQLDataType.VARCHAR, 200, Params.Value<string>("ALLERGIE")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result };
	}

	object UpdateItemInfo(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ENG_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("ENG_NAME")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_DESC", MySQLDataType.VARCHAR, 2000, Params.Value<string>("DESC")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_IMAGE", MySQLDataType.VARCHAR, 200, fileName));
		SQLParams.Add(new MySQLParamModel("P_ITEM_PRICE", MySQLDataType.INT32, 10, Params.Value<string>("PRICE")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_NUTRITION", MySQLDataType.VARCHAR, 200, Params.Value<string>("NUTRITION")));
		SQLParams.Add(new MySQLParamModel("P_ORIGIN", MySQLDataType.VARCHAR, 200, Params.Value<string>("ORIGIN")));
		SQLParams.Add(new MySQLParamModel("P_ALLERGIE", MySQLDataType.VARCHAR, 200, Params.Value<string>("ALLERGIE")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteItem(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteItemCategory(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 10, Params.Value<string>("GROUP_CODE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_CATEGORY_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object InsertItemCategory(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 10, Params.Value<string>("GROUP_CODE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_CATEGORY_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetItemSearchList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 10, Params.Value<string>("GROUP_CODE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_CATEGORY_SEARCH_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object UpdaeItemGroupSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE1", MySQLDataType.INT32, 10, Params.Value<string>("GROUP_CODE1")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE2", MySQLDataType.INT32, 10, Params.Value<string>("GROUP_CODE2")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GROUP_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteItemGroup(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.VARCHAR, 40, Params.Value<string>("GROUP_CODE")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GROUP_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object InsertItemGroup(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("GROUP_NAME")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_NOOPTION", MySQLDataType.VARCHAR, 1, Params.Value<string>("NOOPTION")));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GROUP_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateItemGroup(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_CODE", MySQLDataType.INT32, 10, Params.Value<string>("GROUP_CODE")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_GROUP_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("GROUP_NAME")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_NOOPTION", MySQLDataType.VARCHAR, 1, Params.Value<string>("NOOPTION")));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GROUP_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	
	object GetOptionList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_OPTION_CATEGORY", MySQLDataType.VARCHAR, 40, Params.Value<string>("GROUP_CODE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_OPTION_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/OPTION/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object GetOptionInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE", MySQLDataType.INT32, 10, Params.Value<string>("OPTION_CODE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_OPTION_GET", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/OPTION/";

		return new { STATUS = "OK", RESULT = new { INFO = ds.Tables[0], LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object DeleteOption(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE", MySQLDataType.INT32, 10, Params.Value<string>("OPTION_CODE")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_OPTION_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object InsertOption(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/OPTION/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_OPTION_CATEGORY", MySQLDataType.VARCHAR, 40, Params.Value<string>("CATEGORY")));
		SQLParams.Add(new MySQLParamModel("P_OPTION_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_ESSENTIAL", MySQLDataType.VARCHAR, 1, Params.Value<string>("ESSENTIAL")));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_OPTION_IMAGE", MySQLDataType.VARCHAR, 200, fileName));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_OPTION_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		if (Result > 0)
		{
			int Result2 = 0;
			int seq = 0;
			JArray details = (JArray)Params["DETAILS"];
			foreach (JObject param in details)
			{
				seq++;
				Result2 += this.InsertOptionDetail(
					Result.ToString()
					, seq.ToString()
					, param.Value<string>("NAME")
					, param.Value<string>("EXTRA_COST")
					, param.Value<string>("USE_YN")
				);
			}

			return new { STATUS = "OK", RESULT = Result2 > 0 ? true : false };
		}
		else
		{
			return new { STATUS = "Error", MSG = "등록에 실패했습니다." };
		}
	}

	object UpdateOption(JObject Params, System.Web.HttpPostedFile file)
	{
		string fileName = string.Empty;
		if (file != null)
		{
			fileName = file.FileName;
			fileName = fileName.Substring(fileName.LastIndexOf("\\") + 1);
			string dirPath = Server.MapPath(Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/OPTION/");
			fileName = CommFunc.GetFileName(dirPath, fileName);
			file.SaveAs(dirPath + fileName);
		}

		string option_code = Params.Value<string>("CODE");

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE", MySQLDataType.INT32, 10, option_code));
		SQLParams.Add(new MySQLParamModel("P_OPTION_NAME", MySQLDataType.VARCHAR, 100, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_ESSENTIAL", MySQLDataType.VARCHAR, 1, Params.Value<string>("ESSENTIAL")));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("USE_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));
		SQLParams.Add(new MySQLParamModel("P_OPTION_IMAGE", MySQLDataType.VARCHAR, 200, fileName));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_OPTION_INFO_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		if (Result > 0)
		{
			int Result2 = 0;
			int seq = 0;
			JArray details = (JArray)Params["DETAILS"];
			foreach (JObject param in details)
			{
				seq++;
				Result2 += this.InsertOptionDetail(
					option_code
					, seq.ToString()
					, param.Value<string>("NAME")
					, param.Value<string>("EXTRA_COST")
					, param.Value<string>("USE_YN")
				);
			}

			return new { STATUS = "OK", RESULT = Result2 > 0 ? true : false };
		}
		else
		{
			return new { STATUS = "Error", MSG = "등록에 실패했습니다." };
		}
	}

	int InsertOptionDetail(string option_code, string detail_no, string detail_name, string extra_cost, string use_yn)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE", MySQLDataType.INT32, 10, option_code));
		SQLParams.Add(new MySQLParamModel("P_OPTION_DETAIL_NO", MySQLDataType.INT32, 10, detail_no));
		SQLParams.Add(new MySQLParamModel("P_OPTION_DETAIL_NAME", MySQLDataType.VARCHAR, 100, detail_name));
		SQLParams.Add(new MySQLParamModel("P_EXTRA_COST", MySQLDataType.INT32, 10, extra_cost));
		SQLParams.Add(new MySQLParamModel("P_USE_YN", MySQLDataType.VARCHAR, 1, use_yn));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_OPTION_DETAIL_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return Result;
	}

	object GetItemInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { INFO = dt, FILEPATH = imgPath } };
	}

	object GetOptionSearchList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_OPTION_SEARCH_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetItemOptionList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_OPTION_LIST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertItemOption(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE", MySQLDataType.INT32, 10, Params.Value<string>("OPTION_CODE")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_OPTION_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdaeItemOptionSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE1", MySQLDataType.INT32, 10, Params.Value<string>("OPTION_CODE1")));
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE2", MySQLDataType.INT32, 10, Params.Value<string>("OPTION_CODE2")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_OPTION_SORT_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteItemOptionSort(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_OPTION_CODE", MySQLDataType.INT32, 10, Params.Value<string>("OPTION_CODE")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_OPTION_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetItemStoreList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_STORE_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetItemStoreSearchList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_MANAGER_ITEM_SEARCH_LST", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object InsertItemStore(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_STORE_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteItemStore(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_STORE_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetItemRecommendList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));
		SQLParams.Add(new MySQLParamModel("P_RECOMMEND_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("RECOMMEND_YN")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_RECOMMEND_LIST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object UpdateItemRecommend(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_ITEM_ID", MySQLDataType.INT32, 10, Params.Value<string>("ITEM_ID")));
		SQLParams.Add(new MySQLParamModel("P_RECOMMEND_YN", MySQLDataType.VARCHAR, 1, Params.Value<string>("RECOMMEND_YN")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_ITEM_RECOMMEND_UPD", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}
	#endregion

	#region KIOSK
	object InsertKIOSK(JObject Params)
	{
		string store_id = null;
		if (Params.ContainsKey("STORE_ID"))
			store_id = Params.Value<string>("STORE_ID");

		string kiosk_id = System.Guid.NewGuid().ToString();

		//return new { STATUS = "OK", RESULT = string.Format("{0}, {1}, {2}", store_id, kiosk_id, Params.Value<string>("NAME")) };

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, kiosk_id));
		SQLParams.Add(new MySQLParamModel("P_KIOSK_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, store_id));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetKIOSKList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_LST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object UpdateKIOSKInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_KIOSK_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_INFO_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdateKIOSKStore(JObject Params)
	{
		string store_id = null;
		if (Params.ContainsKey("STORE_ID"))
			store_id = Params.Value<string>("STORE_ID");

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, store_id));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_STORE_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeleteKIOSK(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetKIOSKInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_KIOSK_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_KIOSK_INFO_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}
	#endregion

	#region POS
	object InsertPOS(JObject Params)
	{
		string store_id = null;
		if (Params.ContainsKey("STORE_ID"))
			store_id = Params.Value<string>("STORE_ID");

		string pos_id = System.Guid.NewGuid().ToString();

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, pos_id));
		SQLParams.Add(new MySQLParamModel("P_POS_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, store_id));
		SQLParams.Add(new MySQLParamModel("P_CREATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_INS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetPOSList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_PAGE_SIZE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE_SIZE")));
		SQLParams.Add(new MySQLParamModel("P_PAGE", MySQLDataType.INT32, 100, Params.Value<string>("PAGE")));

		DataSet ds;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_LST", SQLParams))
		{
			ds = dbAgent.DataSet();
		}

		string imgPath = Page.ResolveUrl("~") + WebConfigurationManager.AppSettings["SaveFilePath"] + "/ITEM/";

		return new { STATUS = "OK", RESULT = new { TOTAL_CNT = ds.Tables[0].Rows[0]["TOTAL_CNT"].ToString(), LIST = ds.Tables[1], FILEPATH = imgPath } };
	}

	object UpdatePOSInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_POS_NAME", MySQLDataType.VARCHAR, 40, Params.Value<string>("NAME")));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_INFO_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object UpdatePOSStore(JObject Params)
	{
		string store_id = null;
		if (Params.ContainsKey("STORE_ID"))
			store_id = Params.Value<string>("STORE_ID");

		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, store_id));
		SQLParams.Add(new MySQLParamModel("P_UPDATE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_STORE_UPS", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object DeletePOS(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));
		SQLParams.Add(new MySQLParamModel("P_DELETE_USER", MySQLDataType.VARCHAR, 40, AUOSMSUser.GetCode()));

		int Result;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_DEL", SQLParams))
		{
			Result = Convert.ToInt32(dbAgent.ExecuteScalar());
		}

		return new { STATUS = "OK", RESULT = Result > 0 ? true : false };
	}

	object GetPOSInfo(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_POS_ID", MySQLDataType.VARCHAR, 40, Params.Value<string>("ID")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_POS_INFO_GET", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}
	#endregion

	#region 매출관리
	object GetSaleReportByDaysList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_DAYS", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByMonthList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_START_MON", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_MON")));
		SQLParams.Add(new MySQLParamModel("P_END_MON", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_MON")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_MONTH", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByReceiptsList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_ORDER_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("ORDER_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_RECEIPTS", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByItemTimeList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_ITEM_TIME", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}

	object GetSaleReportByItemGroupList(JObject Params)
	{
		List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
		SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 100, Params.Value<string>("STORE_ID")));
		SQLParams.Add(new MySQLParamModel("P_START_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("START_DATE")));
		SQLParams.Add(new MySQLParamModel("P_END_DATE", MySQLDataType.VARCHAR, 10, Params.Value<string>("END_DATE")));

		DataTable dt;
		using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_SALE_REPORT_BY_ITEM_GROUP", SQLParams))
		{
			dt = dbAgent.DataTable();
		}

		return new { STATUS = "OK", RESULT = dt };
	}
	#endregion

}