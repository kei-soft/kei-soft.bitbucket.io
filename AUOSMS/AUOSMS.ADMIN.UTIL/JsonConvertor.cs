﻿using Newtonsoft.Json;

namespace AUOSMS.ADMIN.UTIL
{
	public class JsonConvertor
	{
		static public string SerializeObject(object obj)
		{
			return JsonConvert.SerializeObject(obj);
		}

		static public T DeserializeObject<T>(string s)
		{
			return JsonConvert.DeserializeObject<T>(s);
		}
	}
}
