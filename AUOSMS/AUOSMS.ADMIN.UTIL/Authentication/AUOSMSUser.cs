﻿using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using AUOSMS.ADMIN.UTIL.DataBase;
using AUOSMS.ADMIN.UTIL.DataBase.DBAgent;

namespace AUOSMS.ADMIN.UTIL.Authentication
{
	public static class AUOSMSUser
	{
		//public const string connectionString = "server=211.57.201.122;user=auosms2_user;database=AUOSMS2;password=auosms20201!";
		public const string connectionString = "server=211.115.107.41;user=auosms2_user;database=AUOSMS2;password=auosms20201!";
		public const string LOGIN_COLUMN_NAME = "MANAGER_CODE";
		public const string LOGIN_COLUMN_LEVEL = "MANAGER_GROUP";
		public const string STORE_LIST_COLUMN = "STORES";
		public const string CURRENT_STORE_ID_COLUMN = "STORE_ID";
		public const string CURRENT_STORE_NAME_COLUMN = "STORE_NAME";

		public static string GetCode()
		{
			return Get(LOGIN_COLUMN_NAME);
		}

		public static int GetLevel()
		{
			return Convert.ToInt32(Get(LOGIN_COLUMN_LEVEL));
		}

		public static string GetStoreID()
		{
			return Get(CURRENT_STORE_ID_COLUMN);
		}

		public static string GetStoreName()
		{
			return Get(CURRENT_STORE_NAME_COLUMN);
		}

		public static string[] GetStoreList()
		{
			return Get(STORE_LIST_COLUMN).Split('|');
		}

		public static string Get(string key)
		{
			return (HttpContext.Current.Request.Cookies[key] == null) ? String.Empty : HttpContext.Current.Server.UrlDecode(HttpContext.Current.Request.Cookies[key].Value);
		}

		public static void Set(string key, string value)
		{
			HttpContext.Current.Response.Cookies[key].Value = HttpContext.Current.Server.UrlEncode(value);
		}

		public static void Set(DataTable dt)
		{
			if (dt != null)
			{
				for (int i = 0; i < dt.Columns.Count; i++)
				{
					HttpContext.Current.Response.Cookies[dt.Columns[i].ColumnName].Value = HttpContext.Current.Server.UrlEncode(dt.Rows[0][i].ToString());
				}

				if (!string.IsNullOrEmpty(Get(STORE_LIST_COLUMN)))
				{
					Set(CURRENT_STORE_ID_COLUMN, GetStoreList()[0]);
					SetStoreName(GetStoreList()[0]);
				}
			}
		}

		public static void SetStoreName(string storeID)
		{
			List<MySQLParamModel> SQLParams = new List<MySQLParamModel>();
			SQLParams.Add(new MySQLParamModel("P_STORE_ID", MySQLDataType.INT32, 10, storeID));

			DataTable dt;
			using (DBAgentCommon dbAgent = new DBAgentCommon(connectionString, "USP_STORE_GET", SQLParams))
			{
				dt = dbAgent.DataTable();
			}

			if (dt.Rows.Count > 0)
			{
				Set(CURRENT_STORE_NAME_COLUMN, dt.Rows[0]["STORE_NAME"].ToString());
			}
		}

		public static bool IsLogin()
		{
			return Has(LOGIN_COLUMN_NAME);
		}

		public static bool Has(string key)
		{
			return (HttpContext.Current.Request.Cookies[key] == null) ? false : true;
		}

		public static void Expires()
		{
			string[] keys = HttpContext.Current.Request.Cookies.AllKeys;
			for (int i = 0; i < keys.Length; i++)
			{
				HttpCookie cookie = new HttpCookie(keys[i]);
				cookie.Expires = DateTime.Now.AddYears(-1);
				HttpContext.Current.Response.Cookies.Add(cookie);
			}
		}

	}
}
