﻿using System;
using System.IO;
using System.Text;

namespace AUOSMS.ADMIN.UTIL.Log
{
	public class ProgramLogger
	{
		public static void SetLogPath(string path)
		{
			LogFilePath.LOGPATH = path;
		}

		public static void WriteLog(string str, params object[] args)
		{
			WriteLog(string.Format(str, args));
		}

		public static void WriteLog(string str)
		{
			string logPath = LogFilePath.LOGPATH;
			if (string.IsNullOrEmpty(logPath))
				return;

			DirectoryInfo loDI = new DirectoryInfo(logPath);

			string sFileName = DateTime.Now.ToString("yyyyMMdd") + ".log";
			if (!loDI.Exists)
				loDI.Create();

			StringBuilder sbLog = new StringBuilder();
			sbLog.AppendFormat("[0:yyyy-MM-dd hh:mm:ss.ffff] : ", DateTime.Now);
			sbLog.Append(str);

			LogFilePath.FILEPATH = logPath + "\\" + sFileName;
			LogStreamHelper.asynctw.WriteLine(sbLog.ToString());
			LogStreamHelper.asynctw.Flush();

			Console.WriteLine(sbLog.ToString());
		}
	}

	internal static class LogStreamHelper
	{
		public static TextWriter asynctw = null;
		static LogStreamHelper()
		{
			asynctw = TextWriter.Synchronized(new StreamWriter(LogFilePath.FILEPATH, true));
		}
	}

	internal static class LogFilePath
	{
		private static string _filePath = string.Empty;
		private static string _logPath = string.Empty;
		public static string FILEPATH
		{
			get { return _filePath; }
			set { _filePath = value; }
		}
		public static string LOGPATH
		{
			get { return _logPath; }
			set { _filePath = value; }
		}
	}
}