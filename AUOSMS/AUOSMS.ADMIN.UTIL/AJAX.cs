﻿using AUOSMS.ADMIN.UTIL.Authentication;

using Newtonsoft.Json;

using System.Web;

namespace AUOSMS.ADMIN.UTIL
{
	public static class AJAX
	{
		public static void Add3P3Header(HttpResponse res)
		{
			res.AddHeader("P3P", "CP='CAO PSA CONi OTR OUR DEM ONL'");
		}

		public static void PrintJson(HttpResponse res, object jsonData)
		{
			res.ContentType = "aoolication/json";
			res.Clear();
			res.Write(ToJson(jsonData));
			res.Flush();
			res.End();
		}

		public static object CheckReferrer(HttpRequest res)
		{
			object jsonData = null;

			if (res.UrlReferrer == null)
				jsonData = new { STATUS = "NoReferrer", MSG = "접속 불가" };
			else if (res.UrlReferrer.DnsSafeHost.ToString().IndexOf(res.Url.DnsSafeHost.ToString()) == -1)
				jsonData = new { STATUS = "WrongReferrer", MSG = "잘못된 경로입니다." };
			else if (res.Form["Command"] == null)
				jsonData = new { STATUS = "NoCommand", MSG = "명령이 잘못되었습니다." };

			return jsonData;
		}

		public static object CheckPermission(int permission_code)
		{
			int level = 0;
			if (AUOSMSUser.IsLogin())
			{
				level = AUOSMSUser.GetLevel();
			}

			if ((level >= permission_code))
			{
				return null;
			}
			else
			{
				return new { STATUS = "PermissionError", MSG = "접속 권한이 없습니다." };
			}
		}

		public static object ToJson(object obj)
		{
			return JsonConvert.SerializeObject(obj, new Newtonsoft.Json.Converters.IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd hh:mm:ss" });
		}
	}
}
