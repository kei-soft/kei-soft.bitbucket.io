﻿using MySql.Data.MySqlClient;

using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace AUOSMS.ADMIN.UTIL
{
	public class CommFunc
	{
		/// <summary>
		/// 암호화
		/// </summary>
		/// <param name="text"></param>
		/// <returns>40자</returns>
		public static string EncryptSHA1(string text)
		{
			using (SHA1 sha1 = new SHA1CryptoServiceProvider())
			{
				byte[] hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(text));

				StringBuilder sb = new StringBuilder();
				foreach (byte b in hash)
				{
					sb.Append(b.ToString("x2", CultureInfo.CurrentCulture));
				}

				return sb.ToString();
			}
		}

		public class HardCoding
		{
			public string[] LogoutURLList = {
				"/",
				"/Default.aspx"
			};
		}

		/// <summary>
		/// 중복되지 않는 파일명을 가져오는 매서드
		/// </summary>
		/// <param name="strPath">검사할 폴더</param>
		/// <param name="strFile">업로드할 파일명</param>
		/// <returns></returns>
		public static string GetFileName(string strPath, string strFile)
		{
			System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(strPath);
			if (!dir.Exists)
				dir.Create();

			int dotindex = strFile.LastIndexOf('.');
			string strExt = strFile.Substring(dotindex);
			strFile = strFile.Substring(0, dotindex);
			string strFileName = strFile + strExt;

			bool exists = true;
			int strcount = 0;

			while (exists)
			{
				if (System.IO.File.Exists(strPath + strFileName))
				{
					strcount++;
					strFileName = strFile + "(" + strcount + ")" + strExt;
				}
				else
				{
					exists = false;
				}
			}

			return strFileName;
		}
	}
}
