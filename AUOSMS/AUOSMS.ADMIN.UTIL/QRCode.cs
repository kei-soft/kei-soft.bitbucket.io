﻿namespace AUOSMS.ADMIN.UTIL
{
	class QRCode
	{
		public static void saveQRCode(string savePath, string qrString)
		{
			BusinessRefinery.Barcode.QRCode qrCode = new BusinessRefinery.Barcode.QRCode();
			qrCode.Code = qrString;
			qrCode.ModuleSize = 6.0f;
			qrCode.Resolution = 300;
			qrCode.drawBarcode2ImageFile(savePath);
		}
	}
}
