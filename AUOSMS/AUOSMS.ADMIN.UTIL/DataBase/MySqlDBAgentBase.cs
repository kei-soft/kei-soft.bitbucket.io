﻿using MySql.Data.MySqlClient;

using System;
using System.Collections.Generic;
using System.Text;

namespace AUOSMS.ADMIN.UTIL.DataBase
{
	class MySqlDBAgentBase
	{
		public DateTime safeGetDateTime(MySqlDataReader reader, int colIndex)
		{
			DateTime dt;
			if (!reader.IsDBNull(colIndex))
			{
				if (reader[colIndex].ToString() == "")
					dt = DateTime.Now;
				else
					dt = reader.GetDateTime(colIndex);
			}
			else
				dt = DateTime.Now;

			return dt;
		}

		public double safeGetDouble(MySqlDataReader reader, int colIndex)
		{
			double d;
			if (!reader.IsDBNull(colIndex))
			{
				if (reader[colIndex].ToString() == "")
					d = double.NaN;
				else if (reader[colIndex].GetType() == typeof(Decimal))
					d = Convert.ToDouble(reader.GetDecimal(colIndex));
				else if (reader[colIndex].GetType() == typeof(double))
					d = Convert.ToDouble(reader[colIndex].ToString());
				else
					d = Convert.ToDouble(reader.GetDecimal(colIndex));
			}
			else
				d = double.NaN;

			return d;
		}

		public string ConvertBindText(string Value, string Query, string BindText)
		{
			string[] ValueList = Value.Split(',');
			string LineBindText = string.Empty;
			for (int i = 0; i < ValueList.Length; i++)
			{
				LineBindText += BindText + i.ToString() + ",";
			}
			return Query.Replace(BindText + ":", LineBindText.TrimEnd(';'));
		}

		public string ConvertBindXMLText(List<string> colList, string Query, string BindText, string RootName, string XmlColumnName, string identifier = "")
		{
			StringBuilder sbXmlQuery = new StringBuilder();
			string newLine = System.Environment.NewLine;

			sbXmlQuery.AppendFormat("'{0}'{1}", RootName, newLine);
			sbXmlQuery.AppendFormat("PASSING {0}{1}", XmlColumnName, newLine);
			sbXmlQuery.Append("COLUMN");

			StringBuilder sbXmlColumn = new StringBuilder();
			foreach (string col in colList)
			{
				string colType = "VARCHAR2(200)";

				sbXmlColumn.AppendFormat(",{0}{1} {2} PATH '{1}' {3}", identifier, col, colType, newLine);
			}

			sbXmlQuery.Append(sbXmlColumn.ToString().Trim(','));

			return Query.Replace(BindText, sbXmlQuery.ToString());
		}
	}
}
