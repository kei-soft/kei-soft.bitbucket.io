﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace AUOSMS.ADMIN.UTIL.DataBase.Query
{
	class QueryHelper
	{
		public string GetQuery(string psQueryName, string psValidMarks)
		{
			StringBuilder lsSQL = new StringBuilder();
			Stream loSM = null;
			StreamReader loSR = null;
			string lsResourceFullName, lsLine;
			int liStart, ii;
			psQueryName = (psQueryName.StartsWith(".") ? "" : ".") + psQueryName;
			psQueryName = psQueryName + (psQueryName.EndsWith(".sql") ? "" : ".sql");
			lsResourceFullName = ExcuteAssemblyType + psQueryName;

			try
			{
				if ((loSM = ExcuteAssembly.GetManifestResourceStream(lsResourceFullName)) != null)
				{
					loSR = new StreamReader(loSM, Encoding.Default);
					while ((lsLine = loSR.ReadLine()) != null)
					{
						for (ii = 0; ii < psValidMarks.Length; ii++)
							lsLine = lsLine.Replace("//" + psValidMarks.Substring(ii, 1), "");

						lsLine = lsLine.Replace("//", "--");

						if ((liStart = lsLine.IndexOf("--")) >= 0)
							lsLine = lsLine.Remove(liStart, lsLine.Length - liStart);

						if (lsLine.Trim().Length > 0)
							lsSQL.Append(lsLine + Environment.NewLine);
					}
				}
			}
			catch
			{
			}
			finally
			{
				if (loSM != null)
					loSM.Close();

				if (loSR != null)
					loSR.Close();

				loSM = null;
				loSR = null;
			}

			return lsSQL.ToString();
		}

		public string GetQuery(string psQueryName)
		{
			return GetQuery(psQueryName, "");
		}

		static QueryHelper _resource;
		static QueryHelper()
		{
			_resource = new QueryHelper();
		}

		public static QueryHelper InstanseObject
		{
			get { return _resource; }
		}

		protected Assembly ExcuteAssembly
		{
			get { return Assembly.GetExecutingAssembly(); }
		}

		protected string ExcuteAssemblyType
		{
			get { return this.GetType().Namespace; }
		}
	}
}