﻿using MySql.Data.MySqlClient;

using System;
using System.Data;

namespace AUOSMS.ADMIN.UTIL.DataBase
{
	class MySqlDBConnection : IDisposable
	{
		#region Dispose
		void IDisposable.Dispose() { }
		public void Dispose()
		{
			DBConnDisconnect();
		}
		#endregion

		#region Properties
		private MySqlConnection _DBConnConn =null;
		public MySqlConnection DBConnConn
		{
			get { return _DBConnConn; }
			set { _DBConnConn = value; }
		}

		private MySqlCommand _DBConnComm = null;
		public MySqlCommand DBConnComm
		{
			get { return _DBConnComm; }
			set { _DBConnComm = value; }
		}

		private MySqlTransaction _DBConnTrans = null;
		public MySqlTransaction DBConnTrans
		{
			get { return _DBConnTrans; }
			set { _DBConnTrans = value; }
		}

		private MySqlDataReader _DBConnReader = null;
		public MySqlDataReader DBConnReader
		{
			get { return _DBConnReader; }
			set { _DBConnReader = value; }
		}

		private Exception _DBConnExt = null;
		public Exception DBConnExt
		{
			set
			{
				_DBConnExt = value;
				//WriteErrorLog();
			}
			get { return _DBConnExt; }
		}

		public string ConnectionString { get; set; }
		#endregion

		public MySqlDBConnection(string connString)
		{
			this.ConnectionString = connString;
			DBConnConnection();
		}

		public bool DBConnConnection()
		{
			try
			{
				DBConnConn = new MySqlConnection(ConnectionString);
				DBConnConn.Open();

				DBConnComm = DBConnConn.CreateCommand();
				return true;
			}
			catch (Exception ex)
			{
				_DBConnExt = ex;
				return false;
			}
		}

		public void DBConnDisconnect()
		{
			try
			{
				if (DBConnConn != null && DBConnConn.State != ConnectionState.Closed && DBConnComm != null)
					DBConnComm.Dispose();

				if (DBConnConn != null && DBConnConn.State != ConnectionState.Closed)
					DBConnConn.Close();
			}
			catch (Exception ex)
			{
				_DBConnExt = ex;
			}
		}

		public MySqlTransaction DBConnBeginTrans()
		{
			if (DBConnConn == null)
				return null;

			try
			{
				DBConnTrans = DBConnConn.BeginTransaction();
			}
			catch (Exception ex)
			{
				_DBConnExt = ex;
				DBConnTrans = null;
			}

			return DBConnTrans;
		}

		public MySqlDataReader DBConnExcuteReader()
		{
			if (DBConnConn == null)
				return null;

			try
			{
				return DBConnComm.ExecuteReader();
			}
			catch (Exception ex)
			{
				_DBConnExt = ex;
				DBConnReader = null;
			}

			return DBConnReader;
		}

		public int DBConnExcuteNonQuery()
		{
			if (DBConnConn == null)
				return -1;

			try
			{
				return DBConnComm.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				_DBConnExt = ex;
				return -1;
			}
		}

		public object DBConnExcuteScalar()
		{
			if (DBConnConn == null)
				return null;

			try
			{
				return DBConnComm.ExecuteScalar();
			}
			catch (Exception ex)
			{
				_DBConnExt = ex;
				return null;
			}

		}

	}
}
