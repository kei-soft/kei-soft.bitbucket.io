﻿using MySql.Data.MySqlClient;

using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace AUOSMS.ADMIN.UTIL.DataBase
{
	public static class MySQLConn
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:보안상 취약한 부분이 있는지 SQL 쿼리를 검토하십시오.")]
		public static object ExecuteScalar(string connectionString, bool isStoredProcedure, string query, List<MySqlParameter> parameters)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection())
				{
					connection.ConnectionString = connectionString;

					using (MySqlCommand command = new MySqlCommand(query, connection))
					{
						command.CommandType = (isStoredProcedure == true) ? CommandType.StoredProcedure : CommandType.Text;

						if (parameters != null)
						{
							foreach (MySqlParameter parameter in parameters)
							{
								command.Parameters.Add(parameter);
							}
						}

						connection.Open();
						object result = command.ExecuteScalar();
						connection.Close();

						return result;
					}
				}
			}
			catch
			{
				throw;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:보안상 취약한 부분이 있는지 SQL 쿼리를 검토하십시오.")]
		public static int ExecuteNonQuery(string connectionString, bool isStoredProcedure, string query, List<MySqlParameter> parameters)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection())
				{
					connection.ConnectionString = connectionString;

					using (MySqlCommand command = new MySqlCommand(query, connection))
					{
						command.CommandType = (isStoredProcedure == true) ? CommandType.StoredProcedure : CommandType.Text;

						if (parameters != null)
						{
							foreach (MySqlParameter parameter in parameters)
							{
								command.Parameters.Add(parameter);
							}
						}

						connection.Open();
						int rows = command.ExecuteNonQuery();
						connection.Close();

						return rows;
					}
				}
			}
			catch
			{
				throw;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:보안상 취약한 부분이 있는지 SQL 쿼리를 검토하십시오.")]
		public static DataSet DataSet(string connectionString, bool isStoredProcedure, string query, List<MySqlParameter> parameters)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection())
				{
					{
						connection.ConnectionString = connectionString;

						using (MySqlCommand command = new MySqlCommand(query, connection))
						{
							command.CommandType = (isStoredProcedure == true) ? CommandType.StoredProcedure : CommandType.Text;

							if (parameters != null)
							{
								foreach (MySqlParameter parameter in parameters)
								{
									command.Parameters.Add(parameter);
								}
							}

							using (DataSet dataSet = new DataSet())
							{
								using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter())
								{
									dataSet.Locale = CultureInfo.CurrentCulture;

									connection.Open();
									dataAdapter.SelectCommand = command;
									dataAdapter.Fill(dataSet);
									connection.Close();

									return dataSet;
								}
							}
						}
					}
				}
			}
			catch
			{
				throw;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:보안상 취약한 부분이 있는지 SQL 쿼리를 검토하십시오.")]
		public static DataTable DataTable(string connectionString, bool isStoredProcedure, string query, List<MySqlParameter> parameters)
		{
			try
			{
				using (MySqlConnection connection = new MySqlConnection())
				{
					connection.ConnectionString = connectionString;

					using (MySqlCommand command = new MySqlCommand(query, connection))
					{
						command.CommandType = (isStoredProcedure == true) ? CommandType.StoredProcedure : CommandType.Text;

						if (parameters != null)
						{
							foreach (MySqlParameter parameter in parameters)
							{
								command.Parameters.Add(parameter);
							}
						}

						using (DataTable dataTable = new DataTable())
						{
							using (MySqlDataAdapter dataAdapter = new MySqlDataAdapter())
							{
								dataTable.Locale = CultureInfo.CurrentCulture;

								connection.Open();
								dataAdapter.SelectCommand = command;
								dataAdapter.Fill(dataTable);
								connection.Close();

								return dataTable;
							}
						}
					}
				}
			}
			catch
			{
				throw;
			}
		}

	}
}