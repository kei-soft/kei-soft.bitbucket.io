﻿using MySql.Data.MySqlClient;
using System.Data;

namespace AUOSMS.ADMIN.UTIL.DataBase
{
	public class MySQLParamModel
	{
		public string PARAM_NAME { get; set; }
		public MySqlDbType PARAM_TYPE { get; set; }
		public int PARAM_SIZE { get; set; }
		public object PARAM_VALUE { get; set; }
		public ParameterDirection PARAM_DIRECTION { get; set; }

		public MySQLParamModel(string param_name, MySQLDataType param_type, int param_size, object param_value)
		{
			this.PARAM_NAME = param_name;
			this.PARAM_TYPE = (MySqlDbType)param_type;
			this.PARAM_SIZE = param_size;
			this.PARAM_VALUE = param_value;
			this.PARAM_DIRECTION = ParameterDirection.Input;
		}

		public MySQLParamModel(string param_name, MySQLDataType param_type, int param_size, object param_value, ParameterDirection param_direction)
		{
			this.PARAM_NAME = param_name;
			this.PARAM_TYPE = (MySqlDbType)param_type;
			this.PARAM_SIZE = param_size;
			this.PARAM_VALUE = param_value;
			this.PARAM_DIRECTION = param_direction;
		}
	}

	public enum MySQLDataType
	{
		BINARY = MySqlDbType.VarChar,
		BIT = MySqlDbType.Bit,
		BLOB = MySqlDbType.Blob,
		BYTE = MySqlDbType.Byte,
		DATE = MySqlDbType.Date,
		DATETIME = MySqlDbType.DateTime,
		DECIMAL = MySqlDbType.Decimal,
		DOUBLE = MySqlDbType.Double,
		FLOAT = MySqlDbType.Float,
		GUID = MySqlDbType.Guid,
		INT16 = MySqlDbType.Int16,
		INT24 = MySqlDbType.Int24,
		INT32 = MySqlDbType.Int32,
		INT64 = MySqlDbType.Int64,
		JSON = MySqlDbType.JSON,
		LONGTEXT = MySqlDbType.LongText,
		STRING = MySqlDbType.String,
		TEXT = MySqlDbType.Text,
		TIME = MySqlDbType.Time,
		TIMESTAMP = MySqlDbType.Timestamp,
		VARCHAR = MySqlDbType.VarChar,
		VARSTRING = MySqlDbType.VarString,
		YEAR = MySqlDbType.Year
	}
}
