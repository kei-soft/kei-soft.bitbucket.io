﻿using AUOSMS.ADMIN.UTIL.DataBase.Query;

using MySql.Data.MySqlClient;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace AUOSMS.ADMIN.UTIL.DataBase.DBAgent
{
	class DBAgentSample : MySqlDBAgentBase
	{
		public ObservableCollection<string> GetData(MySqlDBConnection dbCon)
		{
			ObservableCollection<string> r = new ObservableCollection<string>();
			string lsValidMark = "";

			string query = QueryHelper.InstanseObject.GetQuery("aaa.sql", lsValidMark);

			query = ConvertBindText("aaa,bbb,ccc", query, ":PAR3");
			query = ConvertBindXMLText(new List<string>(), ":XMLTABLE:", "/RESULT_XML/List", "T1.XMT_COLUMN", "");


			dbCon.DBConnComm.CommandType = CommandType.Text;
			dbCon.DBConnComm.CommandTimeout = 60;
			dbCon.DBConnComm.CommandText = query;

			dbCon.DBConnComm.Parameters.Clear();
			//dbCon.DBConnComm.Parameters.Add("PAsR1", "text1");
			//dbCon.DBConnComm.Parameters.Add("PAR2", "text2");

			MySqlDataReader reader = null;

			try
			{
				reader = dbCon.DBConnExcuteReader();

				while (reader.Read())
				{
					string temp = reader[0].ToString();
					r.Add(temp);
				}
			}
			catch (Exception ex)
			{
				dbCon.DBConnExt = ex;
			}
			finally
			{
				reader.Close();
				reader.Dispose();
			}

			return r;

		}

		public int InsertData(MySqlDBConnection dbCon)
		{
			int r = 0;

			string lsValidMark = string.Empty;
			try
			{
				string query = QueryHelper.InstanseObject.GetQuery("aaa.sql", lsValidMark);

				dbCon.DBConnComm.CommandType = CommandType.Text;
				dbCon.DBConnComm.CommandTimeout = 60;
				dbCon.DBConnComm.CommandText = query;
				dbCon.DBConnBeginTrans();

				dbCon.DBConnComm.Parameters.Clear();
				//dbCon.DBConnComm.Parameters.Add("PAR1", "text1");
				//dbCon.DBConnComm.Parameters.Add("PAR2", "text2");

				r = dbCon.DBConnExcuteNonQuery();
				dbCon.DBConnTrans.Commit();
			}
			catch (Exception ex)
			{
				r = -1;
				dbCon.DBConnTrans.Rollback();
				dbCon.DBConnExt = ex;
			}

			return r;

		}
	}
}