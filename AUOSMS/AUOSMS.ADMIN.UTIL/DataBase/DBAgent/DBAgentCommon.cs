﻿using MySql.Data.MySqlClient;

using System;
using System.Collections.Generic;
using System.Data;

namespace AUOSMS.ADMIN.UTIL.DataBase.DBAgent
{
	public class DBAgentCommon : IDisposable
	{
		void IDisposable.Dispose() { }
		public void Dispose()
		{
		}

		private string ConnectionString;
		private string PROC_NAME;
		private List<MySQLParamModel> SQLParams;

		public DBAgentCommon(string _connectionString, string _procName, List<MySQLParamModel> _sqlParams = null)
		{
			this.ConnectionString = _connectionString;
			this.PROC_NAME = _procName;
			this.SQLParams = _sqlParams;
		}

		public DataTable DataTable()
		{
			return MySQLConn.DataTable(this.ConnectionString, true, this.PROC_NAME, GetMySqlParameter());
		}

		public DataSet DataSet()
		{
			return MySQLConn.DataSet(this.ConnectionString, true, this.PROC_NAME, GetMySqlParameter());
		}

		public int ExecuteNonQuery()
		{
			return MySQLConn.ExecuteNonQuery(this.ConnectionString, true, this.PROC_NAME, GetMySqlParameter());
		}

		public object ExecuteScalar()
		{
			return MySQLConn.ExecuteScalar(this.ConnectionString, true, this.PROC_NAME, GetMySqlParameter());
		}

		public List<MySqlParameter> GetMySqlParameter()
		{
			if (SQLParams == null)
				return null;

			List<MySqlParameter> parameters = new List<MySqlParameter>();
			foreach(MySQLParamModel param in SQLParams)
			{
				MySqlParameter parameter = new MySqlParameter(param.PARAM_NAME, param.PARAM_TYPE, param.PARAM_SIZE);
				parameter.Value = param.PARAM_VALUE;
				parameter.Direction = param.PARAM_DIRECTION;

				parameters.Add(parameter);
			}

			return parameters;
		}

	}
}