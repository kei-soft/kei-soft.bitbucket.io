﻿using MySql.Data.MySqlClient;

using System;
using System.Collections.Generic;
using System.Data;

namespace AUOSMS.ADMIN.UTIL.DataBase.DBAgent.ADMIN_WEB
{
	public class DBAgentAdmin : IDisposable
	{
		void IDisposable.Dispose() { }
		public void Dispose()
		{
		}

		private string ConnectionString;
		private string PROC_NAME;
		private Dictionary<string, object> SQLParams;

		public DBAgentAdmin(string _connectionString, string _procName, Dictionary<string, object> _sqlParams = null)
		{
			this.ConnectionString = _connectionString;
			this.PROC_NAME = _procName;
			this.SQLParams = _sqlParams;
		}

		public DataTable DataTable()
		{
			return MySQLConn.DataTable(this.ConnectionString, true, this.PROC_NAME, GetParamInfoList());
		}

		public DataSet DataSet()
		{
			return MySQLConn.DataSet(this.ConnectionString, true, this.PROC_NAME, GetParamInfoList());
		}

		public int ExecuteNonQuery()
		{
			return MySQLConn.ExecuteNonQuery(this.ConnectionString, true, this.PROC_NAME, GetParamInfoList());
		}

		public object ExecuteScalar()
		{
			return MySQLConn.ExecuteScalar(this.ConnectionString, true, this.PROC_NAME, GetParamInfoList());
		}

		public List<MySqlParameter> GetParamInfoList()
		{
			List<MySqlParameter> parameters = new List<MySqlParameter>();
			if (this.SQLParams != null && this.SQLParams.Count > 0)
			{
				foreach (string key in this.SQLParams.Keys)
				{
					parameters.Add(new MySqlParameter(key, this.SQLParams[key]));
				}

				return parameters;
			}
			else
			{
				return null;
			}
		}

		public DataTable GetTest()
		{
			return MySQLConn.DataTable(ConnectionString, true, "USP_TEST_LST", null);
		}
	}



}
